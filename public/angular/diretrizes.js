app.directive('formatter', function () {
    return {
        require: 'ngModel',
        terminal: true,
        link: function ($scope, element, attrs, ngModel) {

            var format = function (value) {

                var formatters = attrs.formatter.split(" ");
                for (var i = 0; i < formatters.length; i++) {
                    try {
                        var formatter = orbFormatters[formatters[i]];
                        if (formatter) {
                            value = formatter(value);
                        }
                    } catch (err) {
                    }
                }
                return (value);
            };
            
            // This runs when the model gets updated on the scope directly and keeps our view in sync
            ngModel.$render = function () {
                element.val(ngModel.$viewValue);
            };

            //format text going to user (model to view)
            ngModel.$formatters.push(function (value) {
//                var currentValue = ngModel.$modelValue;
//                ngModel.$setViewValue(currentValue);
//                ngModel.$render();
                return value;
            });

            //format text from the user (view to model)
            ngModel.$parsers.push(function (value) {
                value = format(value);
                ngModel.$setViewValue(value);
                ngModel.$render();
                return (value);
            });

        }
    }; 
});

var orbFormatters = {};

orbFormatters.cpf = function (value) {
    value = value.replace(/\D/g, ""); //Remove tudo o que n�o � d�gito
    value = value.substring(0, 11);

    value = value.replace(/(\d{3})(\d)/, "$1.$2"); //Coloca um ponto entre o terceiro e o quarto d�gitos
    value = value.replace(/(\d{3})(\d)/, "$1.$2"); //Coloca um ponto entre o terceiro e o quarto d�gitos
    //de novo (para o segundo bloco de n�meros)
    value = value.replace(/(\d{3})(\d{1,2})$/, "$1-$2"); //Coloca um h�fen entre o terceiro e o quarto d�gitos

    return value;
};

orbFormatters.numberOnly = function (value) {
    value = value.replace(/\D/g, ""); //Remove tudo o que n�o � d�gito
    return value;
};


orbFormatters.date = function (value) {
    value = value.replace(/\D/g, ""); //Remove tudo o que n�o � d�gito
    value = value.substring(0, 8);
    value = value.replace(/(\d{2})(\d)/, "$1/$2");//Coloca uma barra entre o segundo e o terceiro d�gitos
    value = value.replace(/(\d{2})(\d)/, "$1/$2");//Coloca uma barra entre o quarto e o quinto d�gitos
    return value;
};

orbFormatters.timestamp = function (value) {
    value = value.replace(/\D/g, ""); //Remove tudo o que n�o � d�gito
    var data = value.substring(0, 8);
    data = data.replace(/\D/g, ""); //Remove tudo o que n�o � d�gito
    data = data.replace(/(\d{2})(\d)/, "$1/$2");//Coloca uma barra entre o segundo e o terceiro d�gitos
    data = data.replace(/(\d{2})(\d)/, "$1/$2");//Coloca uma barra entre o quarto e o quinto d�gitos

    var toda_hora = value.substring(8, 14);
    var hora = toda_hora.substr(0, 2);
    var min = toda_hora.substr(2, 2);
    var seg = toda_hora.substr(4, 2);
    if (hora.length > 0 && hora > 23) {
        hora = '23';
    }
    if (min.length > 0 && min > 59) {
        min = '59';
    }
    if (seg.length > 0 && seg > 59) {
        seg = '59';
    }
    toda_hora = hora + min + seg;
    toda_hora = toda_hora.replace(/(\d{2})(\d)/, "$1:$2");
    toda_hora = toda_hora.replace(/(\d{2})(\d)/, "$1:$2");

    if (toda_hora.length > 0) {
        return data + " - " + toda_hora;
    } else {
        return data;
    }
};

orbFormatters.money = function (value) {
    value = value.toString();
    var negativo = '';
    if (value[0] === '-') {
        negativo = '-';
    }
    var parteInteira = value.split(",")[0];
    var parteDecimal = value.split(",")[1];
    if (parteInteira) {
        parteInteira = parteInteira.replace(/\D/g, "");
        value = negativo + parteInteira;
    }
    if (parteDecimal !== undefined) {
        parteDecimal = parteDecimal.replace(/\D/g, "");
        parteDecimal = parteDecimal.substring(0, 2);
        value = negativo + parteInteira + ',' + parteDecimal;
    }
    return value;
};



//-------------------------------------------------------//



app.directive('validation', function () {
    return {
        require: '^ngModel',
        terminal: true,
        link: function ($scope, element, attrs, ngModel) {

            if (attrs.validation) {
                var validators = attrs.validation.split(' ');
                for (var i = 0; i < validators.length; i++) {
                    var validator = validators[i];
                    if (orbValidators[validator]) {
                        ngModel.$validators[validator] = orbValidators[validator];
                    } else {
                        console.error('orbValidators[' + validator + '] nao encontrado!');
                    }
                }
            }
        }
    };
});


var orbValidators = [];

orbValidators.required = function (modelValue, viewValue) {

    if (typeof modelValue === 'string') {
        return modelValue !== '';
    } else if (modelValue instanceof Array) {
        return modelValue.length > 0;
    } else if (typeof modelValue === 'object') {
        return modelValue.value !== undefined && modelValue.value !== '';
    }

};

orbValidators.cpf = function (value) {

    var patt = /^\d{3}\.\d{3}\.\d{3}\-\d{2}$/;

    if (!(patt.test(value))) {
        return false;
    }

    //Verifica se CPF � v�lido
    var Soma;
    var Resto;
    Soma = 0;
    var strCPF = value.replace(/\D/g, "").substring(0, 11);

    if (strCPF === "00000000000") {
        return false;
    }

    for (i = 1; i <= 9; i++) {
        Soma = Soma + parseInt(strCPF.substring(i - 1, i)) * (11 - i);
    }

    Resto = (Soma * 10) % 11;
    if ((Resto === 10) || (Resto === 11)) {
        Resto = 0;
    }
    if (Resto !== parseInt(strCPF.substring(9, 10))) {
        return false;
    }
    Soma = 0;
    for (i = 1; i <= 10; i++) {
        Soma = Soma + parseInt(strCPF.substring(i - 1, i)) * (12 - i);
    }
    Resto = (Soma * 10) % 11;
    if ((Resto === 10) || (Resto === 11)) {
        Resto = 0;
    }
    if (Resto !== parseInt(strCPF.substring(10, 11))) {
        return false;
    }

    return true;
};

orbValidators.date = function (value) {
	var format = 'DD/MM/YYYY';
	if(format.length !== value.length)
		return false;
	
    var m = moment(value, format);
    return m.isValid();
};

orbValidators.timestamp = function (value) {
	var format = 'DD/MM/YYYY - HH:mm:ss';
	if(format.length !== value.length)
		return false;
	
    var m = moment(value, format);
    return m.isValid();
};

orbValidators.money = function (value) {
    var patt = /^\d*$/;
    var patt2 = /^\d+,(\d\d?)?$/;
    if (!(patt.test(value) || patt2.test(value))) {
        return false;
    }
    return true;
};

orbValidators.maiorQue20 = function (value) {
    if(value.length<20)
		return false;
    return true;
};