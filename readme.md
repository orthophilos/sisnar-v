## SISNAR - Sistema Nacional de Remoção de Servidores da Polícia Rodoviária Federal 


Sisnar é o nome do concurso nacional de remoção de servidores baseado em instrução normativa que prevê três formas de alteração de lotação do servidor : recrutamento, remanejamento ou permuta. 


###Remanejamento 
A presente aplicação , versão 5, pretende abarcar o remanejamento de servidores e a permuta .
Por remanejamento entende-se a abertura de vagas em Unidades Organizacionais - UORGs para servidores PRFs dada a iminência de nomeação de novos policiais, e sabe-se que legalmente não se pode lotar o servidor mais novo no local que há interesse de um servidor mais antigo. 

No sistema de Remanejamento poderá haver UORGs em que, após a classificação dos candidatos (pelos comandos de linha :  php artisan executarConcurso e  php artsan balancearConcurso), poderão ter uma diferença entre prfs que entraram e sairam . 
Chamamos a isso de método não conservativo .

### Permuta ###

Permuta é o cruzamento de interesses de lotação entre servidores, a idéia é que o concurso de permuta se dê anualmente  , independente de lotacao de novos policiais, e que cruze interesses de remoção entre os atuais agentes públicos sendo que no final as UORGs não serão prejudicadas pois a diferença entre entrada e saída de servidres de cada unidade deverá ser obrigatoriamente zero, a isso damos o nome de método conservativo. 

  
### Principais Funcionalidades ###

1. Cadastramento das inscricoes dos servidores;
1. Visualização de dados estatistico quanto à suas intenções de remoção;
1. Emissão de comprovante de inscrição em .pdf;
1. Gestor pode editar dados de inscricao dos candidatos mediante justificativa registrada em banco;
1. Gestor pode executar algoritmo de classificação
1. Sistema define qual dos dois algoritmos serão executados com base nas vagas previamente configuradas pelo gestor do sistema, caso zero vagas para todas as uorgs, roda algoritmo conservativo, caso tenha alguma uorg com vaga ajustada, roda algoritmo nao conservativo
1. Planilha de relatorio de aprovados no concurso;
1. Arquiva dados do concurso;
1. Controle de acessos e prerequisitos para inscricao;
1. Gestor pode ajustar o numero de opcoes de intencao de remocao entre 1 e 10;



### Ficha Técnica ###

VERSAO : 5.0 gama      DATA: Jan/2016


EQUIPE DESENVOLVEDORES: 

* Thyago Ribeiro - Arquetipo;
* Jeferson Tadeu - Banco, Algoritmo Conservativo** e Nao Conservativo, BackEnds ;
* Claudir Galesky - Front End ;


** Algoritmo conservativo foi evolução do primeiro algoritmo desenvolvido por Abel Vanderlei. 

DPRF
*** DIRETORA GERAL** : Maria Alice do Nascimento

* **CH . GAB**: Eduardo Aggio

* **CGRH** : Antonio Paim


* GERENTE PROJETO SETORIAL SISNAR: Guilherme Andrade
* REVISOR DE CONTEUDO DE BANCO: Bruno e Hudson



## Laravel PHP Framework

[![Build Status](https://travis-ci.org/laravel/framework.svg)](https://travis-ci.org/laravel/framework)
[![Total Downloads](https://poser.pugx.org/laravel/framework/downloads.svg)](https://packagist.org/packages/laravel/framework)
[![Latest Stable Version](https://poser.pugx.org/laravel/framework/v/stable.svg)](https://packagist.org/packages/laravel/framework)
[![Latest Unstable Version](https://poser.pugx.org/laravel/framework/v/unstable.svg)](https://packagist.org/packages/laravel/framework)
[![License](https://poser.pugx.org/laravel/framework/license.svg)](https://packagist.org/packages/laravel/framework)


### License

The Laravel framework is open-sourced software licensed under the [MIT license](http://opensource.org/licenses/MIT)