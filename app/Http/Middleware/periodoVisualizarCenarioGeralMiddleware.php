<?php namespace App\Http\Middleware;

use App\ExecutaConcurso\ConfiguracaoConcurso as ConfiguracaoConcurso;
use Closure;

class periodoVisualizarCenarioGeralMiddleware {

	/**
	 * Handle an incoming request.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \Closure  $next
	 * @return mixed
	 */
/*
 * Foi acordado com o CGRH que os servidores poderão ver o  cenário somente até tantos dias de antecedência do encerra
 * mento das inscricoes.
 *
 *
 * */
	public function handle($request, Closure $next)

	{
		if (
			!ConfiguracaoConcurso::checkVisualizaEstatisticaHoje() &&
			!ConfiguracaoConcurso::checkPodeAlterarDadosAlheios()
			)
		{
			return redirect()->back()->withErrors('Atualmente não é possível visualizar o seu cenário de probabilidades');
		}
		return $next($request);

	}

}
