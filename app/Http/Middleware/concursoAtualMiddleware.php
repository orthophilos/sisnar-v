<?php namespace App\Http\Middleware;

use Closure;
use App\Funcoes\Funcoes as Funcoes;
class concursoAtualMiddleware {

	/**
	 * Handle an incoming request.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \Closure  $next
	 * @return mixed
	 */
	public function handle($request, Closure $next)
	{
		if(!Funcoes::temConcurso()){
			return redirect()->back()->withErrors('Não há concurso ativo atualmente');
		}

		return $next($request);
	}

}
