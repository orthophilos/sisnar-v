<?php namespace App\Http\Middleware;

use Closure;
use App\Models\Inscricao\InscricaoRepository as Inscricao,
    App\Models\Servidor\ServidorRepository as Servidor;
use Illuminate\Support\Facades\Auth;


class inscricaoConfirmadaMiddleware {

/*
 * Filtra se a inscricao já foi confirmada pelo candidato  , é usada para impossibilitar alteracoes de inscricao após a
 * confirmaçao definitiva.
 *
 * */

	/**
	 * Handle an incoming request.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \Closure  $next
	 * @return mixed
	 */
    protected  $Servidor;
    protected  $Inscricao;
    function __construct (Servidor $Servidor, Inscricao $Inscricao ){
        $this->servidor = $Servidor;
        $this->inscricao = $Inscricao;

    }

    /**
     * @param $request
     * @param Closure $next
     * @return $this
     */
    public function handle($request, Closure $next)
	{
        $servidorCadastrado = $this->servidor->getByCPF(Auth::user()->cpf);
        if (!$servidorCadastrado)
        {
            return redirect()->back()->withErrors('Sua inscrição ainda não foi confirmada');
        }
        $dadosInscricao =  $this->inscricao->getInscricaoFromServidorID($servidorCadastrado->sd_id_servidor);
        if (is_null($dadosInscricao) || $dadosInscricao->ic_st_inscricao_confirmada == 0){
            return redirect()->back()->withErrors('Sua inscrição ainda não foi confirmada');
        }
    	return $next($request);
	}

}
