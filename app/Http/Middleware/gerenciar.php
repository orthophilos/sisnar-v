<?php namespace App\Http\Middleware;
use App\Funcoes\Funcoes as Funcoes;
use Closure;

class gerenciar {

	/**
	 * Handle an incoming request.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \Closure  $next
	 * @return mixed
	 */
	public function handle($request, Closure $next)
	{
		if (!(Funcoes::temPermissao('gerenciar'))) {
			return redirect()->back()->withErrors('Você não tem permissão para esta funcionalidade!');
		}
		return $next($request);
	}

}
