<?php namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

use App\ExecutaConcurso\ConfiguracaoConcurso as ConfiguracaoConcurso;
use Illuminate\Support\Facades\Redirect;

use App\Models\Inscricao\InscricaoRepository as Inscricao,
	App\Models\Servidor\ServidorRepository as Servidor;

class periodoInscricaoMiddleware {


	/*
	 * Periodo no qual estarao liberadas e acessíveis as funcionalidades de MinhaInscricaoController (  o candidato fazer
	 * a inscricao )
	 *
	 * */
	/**
	 * Handle an incoming request.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \Closure  $next
	 * @return mixed
	 */
	protected  $Servidor;
	protected  $Inscricao;

	function __construct (Servidor $Servidor, Inscricao $Inscricao ){
		$this->servidor = $Servidor;
		$this->inscricao = $Inscricao;
	}

	/**
	 * @param $request
	 * @param Closure $next
	 * @return mixed
     */
	public function handle($request, Closure $next)
	{
        $inscricaoConfirmada = 0 ;
        $servidorCadastrado = $this->servidor->getByCPF(Auth::user()->cpf);
        if (!is_null($servidorCadastrado)){
            $inscricaoCadastrada = $this->inscricao->getInscricaoFromServidorID($servidorCadastrado->sd_id_servidor);
            if (!is_null($inscricaoCadastrada)){
                $inscricaoConfirmada = $this->inscricao
                                            ->getInscricaoFromServidorID($servidorCadastrado->sd_id_servidor)
                                            ->ic_st_inscricao_confirmada;

            }
        }
        $inscricaoConfirmada = (is_null($servidorCadastrado) ? 0 : $inscricaoConfirmada );
		if (
			!ConfiguracaoConcurso::checkPeriodoDeInscricao() ||
			!$inscricaoConfirmada == 0
			)
		{
			return redirect()->back()->withErrors(	'Atualmente você não pode se inscrever no concurso,'.
													' verifique períodos conforme edital ou se já realizou sua inscrição');
		}
		return $next($request);
	}

}
