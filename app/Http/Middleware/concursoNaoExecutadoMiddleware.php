<?php namespace App\Http\Middleware;
use App\Models\Concurso\ConcursoRepository as Concurso;
use Closure;

class concursoNaoExecutadoMiddleware {
/*
 *  A ser usado para verificar acesso ao botão executar Concurso
 * */
	/**
	 * Handle an incoming request.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \Closure  $next
	 * @return mixed
	 */

	//protected  $Concurso;

	function __construct ( Concurso $Concurso){
		$this->concurso = $Concurso;

	}

	public function handle($request, Closure $next)
	{
		if($this->concurso->checkConcursoExecutado()){
			return redirect()->back()->withErrors('No momento esta função está desabilitada');
		}

		return $next($request);
	}

}
