<?php namespace App\Http\Middleware;

use Closure;
use App\ExecutaConcurso\ConfiguracaoConcurso as ConfiguracaoConcurso;


class periodoExecutaConcursoManualmenteMiddleware {

	/*
	 * Forcar a execucao do concurso so pode ser permitida depois da data cadastrada no banco do concurso atual.
	 *
	 * */
	/**
	 * Handle an incoming request.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \Closure  $next
	 * @return mixed
	 */
	public function handle($request, Closure $next)
	{
		if (!ConfiguracaoConcurso::checkPodeExecutarConcursoManualmente())
		{
			return redirect()->back()->withErrors('Atualmente não é possível executar o concurso manualmente');
		}
		return $next($request);
	}

}
