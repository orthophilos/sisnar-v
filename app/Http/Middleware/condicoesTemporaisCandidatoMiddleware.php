<?php namespace App\Http\Middleware;

use App\Funcoes\Funcoes;
use Closure;
use App\ExecutaConcurso\ConfiguracaoConcurso as ConfiguracaoConcurso;
use App\Models\Servidor\ServidorRepository as Servidor;
use Illuminate\Support\Facades\Auth;


class condicoesTemporaisCandidatoMiddleware {

    /*
     * O candidato para se inscrever deve obedecer algumas condições relativas a temporalidae como tempo de lotação,
     * tempo como servidor, etc... essa classe funcionará como o filtro dessa condição para viabilizar a inscrição do
     * servidor.
     *
     *
     * */
	/**
	 * Handle an incoming request.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \Closure  $next
	 * @return mixed
	 */
 //   protected $Servidor;

    function __construct ( Servidor $Servidor){
           $this->servidor = $Servidor;

    }

    /**
     * @param $request
     * @param Closure $next
     * @return $this
     */
    public function handle($request, Closure $next)
	{


        if (is_null($request->input('dataExercicio'))){
           $inputs =  $this->servidor->getServidorDadosFromWebService(Auth::user()->cpf);

       }else{
           $inputs['dataExercicio'] = Funcoes::MyDateFormatToMyUnixTime($request->input('dataExercicio'));
           $inputs['dataLotacao'] = Funcoes::MyDateFormatToMyUnixTime($request->input('dataLotacao'));
       }
       // checa se obedece as condicoes temporais
       if (!ConfiguracaoConcurso::checkCondicoesTemporaisDoCandidato($inputs))
       {
            return redirect()->back()->withErrors(
                'As datas do registro de inscrição são incompatíveis com as regras do concurso'.
                ' Data Entrada em Exercicio '. Funcoes::MyUnixTimeToDate($inputs['dataExercicio'])   .
                ' Data Ultima Lotacao '. Funcoes::MyUnixTimeToDate($inputs['dataLotacao'])

            );
       }
       if (!ConfiguracaoConcurso::checaCargoPublicoAdequado($inputs['cargo']))
       {
           return redirect()->back()->withErrors(
               'O cargo ocupado pelo servidor não condiz com o das regras do concurso atual.'.
               ' Seu cargo atual é '. $inputs['cargo']

           );

       }
  	   return $next($request);
	}

}
