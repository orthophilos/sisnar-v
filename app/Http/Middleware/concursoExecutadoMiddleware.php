<?php namespace App\Http\Middleware;
use App\Models\Concurso\ConcursoRepository as Concurso;
use Closure;

class concursoExecutadoMiddleware {

	//protected  $Concurso;

	function __construct ( Concurso $Concurso){
		$this->concurso = $Concurso;

	}
	/**
	 * Handle an incoming request.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \Closure  $next
	 * @return mixed
	 */
	public function handle($request, Closure $next)
	{
		if(!$this->concurso->checkConcursoExecutado()){
			return redirect()->back()->withErrors('O presente concurso ainda não sofreu processo de classificação');
		}

		return $next($request);
	}

}
