<?php namespace App\Http\Middleware;

use Closure;
use App\Funcoes\Funcoes;

class root {

	/**
	 * Handle an incoming request.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \Closure  $next
	 * @return mixed
	 */
	public function handle($request, Closure $next)
	{
		if (!(Funcoes::temPermissao('root'))){
			return redirect()->back()->withErrors('Você não tem permissão para esta funcionalidade!');
		}
		return $next($request);
	}

}
