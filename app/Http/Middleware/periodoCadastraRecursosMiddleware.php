<?php namespace App\Http\Middleware;

use Closure;
use App\ExecutaConcurso\ConfiguracaoConcurso as ConfiguracaoConcurso;


class periodoCadastraRecursosMiddleware {
/*
 * está no período de alterar manualmente os dados das inscricoes devido a recursos ?
 * Periodo que o gerente pode alterar as inscricoes manualmente
 *
 *
 */
	/**
	 * Handle an incoming request.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \Closure  $next
	 * @return mixed
	 */
	public function handle($request, Closure $next)
	{
		if (!ConfiguracaoConcurso::checkPodeAlterarDadosAlheios())
		{
			return redirect()->back()->withErrors('Atualmente não é possível alterar manualmente os dados do candidato');
		}
		return $next($request);
	}

}
