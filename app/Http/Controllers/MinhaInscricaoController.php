<?php namespace App\Http\Controllers;

//outros
use App\Funcoes\Funcoes;
use App\Http\Requests;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;

//models
use App\Models\Inscricao\InscricaoRepository as Inscricao,
    App\Models\Servidor\ServidorRepository as Servidor,
    App\Models\Inscricao_Opcao\inscricaoOpcaoCRUDRepository as InscricaoOpcao,
    App\Models\Vaga\VagaRepository as Vaga,
    App\Models\Inscricao_Opcao\inscricaoOpcaoEstatistica as Estatistica;
use App\Models\Inscricao_Opcao\Tb_io_inscricao_opcao;

//configuracao
use App\ExecutaConcurso\ConfiguracaoConcurso as ConfiguracaoConcurso;

//requests
use Illuminate\Http\Request;
//use App\Http\Requests\InscricaoOpcaoRequest;
use App\Http\Requests\SyncMinhaOpcaoInscricaoRequest as SyncMinhaOpcaoInscricaoRequest;
use SimpleSoftwareIO\QrCode\Facades\QrCode;

// qr code

class MinhaInscricaoController extends Controller {

    /*
   |--------------------------------------------------------------------------
   | MinhaInscricaoController
   |--------------------------------------------------------------------------
   |
   | Todos os métodos deste controller são relativos às funcionalidades de realização da inscrição pelo candidato
   | não estão incluídas aqui as inscricoes que acaso possam ser incluídas mediante algum recurso.
   |
   */

//protected  $Inscricao;
//protected  $Servidor;
//protected  $InscricaoOpcao;
//protected  $Vaga;
//protected  $Estatistica
    function __construct(   Inscricao $Inscricao,
                            Servidor $Servidor,
                            InscricaoOpcao $InscricaoOpcao,
                            Vaga $Vaga,
                            Estatistica $Estatistica
                        )
    {
        //$this->middleware('auth');
        $this->inscricao = $Inscricao;
        $this->servidor = $Servidor;
        $this->inscricaoOpcao = $InscricaoOpcao;
        $this->vaga = $Vaga;
        $this->estatistica = $Estatistica;
    }


    /**
     * @return mixed
     */
    private function _pegaMinhasInscricoesOpcoes(){
        $dadosInscricao = $this->dadosInscricao();
        return $this->inscricaoOpcao->getInscricaoOpcaoByInscricaoID($dadosInscricao->ic_id_inscricao);
    }

    /**
     * @param $dadosInscricao
     * @param $dadosInscricaoOpcoes
     * @return string
     */
    private function _makeMyHash($dadosInscricao, $dadosInscricaoOpcoes){

        $hashDados = 'INSC';
        $hashDados .= 'sd_matr:'              . $dadosInscricao->sd_cd_matricula_servidor;
        $hashDados .= 'ic_pont:'              . $dadosInscricao->ic_vl_pontuacao;


        foreach ( $dadosInscricaoOpcoes as $dadosInscricaoOpcao  ) {
            $hashDados .= 'OPC';
            $hashDados .=  'io_dest:'        .   $dadosInscricaoOpcao->io_cd_uorg_destino;
        }

        //   return chunk_split(Funcoes::strToHex($hashDados), 40);
        return chunk_split($hashDados, 40);
//       return Funcoes::strToHex($hashDados);
        // return Qr
    }


    /**
     * @return \BladeView|bool|\Illuminate\View\View
     * @throws \Exception
     */
    public function cadastroInscricao()
    {
        //midlewares usados através se
        // route = inscricaoPasso1: condicoesTemporaisCandidatoMiddleware, aliases :
        // podeInscrever
        // condicoesTemporalidadeCandidato

        // faleconosco
        $faleconosco = $this->prfFaleConosco(); // funcao no Controller.php

        $dadosUsuario = $this->servidor->getServidorDadosFromWebService(Auth::user()->cpf);
        //dd('minhainscricaocontroller',$dadosUsuario);
        // tratando cadastramento de servidor
        //
        // pega dados de inscricao cadastrada
        $servidorCadastrado = $this->servidor->getByCPF(Auth::user()->cpf);

        // se não estiver cadastrado o Servidor , cadastre
        if (is_null($servidorCadastrado)  && !empty($dadosUsuario))
        {
            $servidorCadastrado = $this->servidor->add($dadosUsuario);
        }

        // tratando cadastramento da inscricao
        $dadosInscricao = $this->inscricao->getInscricaoFromServidorID($servidorCadastrado->sd_id_servidor);
        //dd($dadosInscricao);
        // se não houver inscricao , faça
        if (is_null($dadosInscricao) &&
            !empty($dadosUsuario)
            )
        {
            $this->inscricao->add();
            $dadosInscricao = $this->inscricao->getInscricaoFromServidorID($servidorCadastrado->sd_id_servidor);
        }
       // dd($servidorCadastrado,$dadosInscricao, $dadosUsuario, $faleconosco);
        return view('inscricao.passo1',compact('servidorCadastrado','dadosInscricao','dadosUsuario','faleconosco'));
    }

    /**
     * @return $this
     */
    public function montaTelaInscricaoOpcao()
    {
        $dadosInscricao = $this->dadosInscricao();

        // se não tem inscricao retorna
        if (is_null($dadosInscricao)) {Redirect::action('MinhaInscricaoController@cadastroInscricao'); }

        // se inscricao ja foi confirmada entao vai pro comprovante
        if (!$dadosInscricao->ic_st_inscricao_confirmada == 0){
            return Redirect::action('MinhaInscricaoController@comprovanteInscricao');
        }

        $predadosOpcoesCadastradas = $this->_pegaMinhasInscricoesOpcoes();
        $i = 0 ;
        if (count($predadosOpcoesCadastradas) > 0 ) {
            foreach ($predadosOpcoesCadastradas as $predadosOpcaoCadastrada) {
                $uorg = $this->vaga->getById($predadosOpcaoCadastrada['io_cd_uorg_destino']);
                $dadosOpcoesCadastradas[$i]['vg_id_uorg'] = $predadosOpcaoCadastrada['io_cd_uorg_destino'];
                $dadosOpcoesCadastradas[$i]['vg_no_nome'] = $uorg->vg_no_nome;
                $dadosOpcoesCadastradas[$i]['vg_ed_uf'] = $uorg->vg_ed_uf;
                $i++;
            }
        }else{$dadosOpcoesCadastradas  = [];}

       // dd('montaTelaInscricaoOpcao',$dadosOpcoesCadastradas);
        // setando variaveis
        $checkVisualizaEstatisticaHoje =   (ConfiguracaoConcurso::checkVisualizaEstatisticaHoje()? 1: 0);
        $dataMaxVisualizarEstatistica = ConfiguracaoConcurso::dataMaxVisualizarEstatistica();
        $dadosOpcoesCadastradas = json_encode($dadosOpcoesCadastradas);
        $quantidadeMaxOpcoes = ConfiguracaoConcurso::QTE_OPCOES;
        $mostraEstatisticaCandidato = (ConfiguracaoConcurso::MOSTRAR_PROBABILIDADE_AO_CANDIDATO);
        $listaUorgs = $this->vaga->getListUorgs($dadosInscricao->vg_id_uorg);


     //   dd($checkVisualizaEstatisticaHoje, $dataMaxVisualizarEstatistica, $mostraEstatisticaCandidato);
        return view('inscricao.passo2') ->with('dadosOpcoesCadastradas', $dadosOpcoesCadastradas)
                                        ->with('quantidadeMaxOpcoes', $quantidadeMaxOpcoes)
                                        ->with('mostraEstatisticaCandidato', $mostraEstatisticaCandidato)
                                        ->with('listaUorgs',$listaUorgs)
                                        ->with('dataMaxVisualizarEstatistica',$dataMaxVisualizarEstatistica)
                                        ->with('checkVisualizaEstatisticaHoje', $checkVisualizaEstatisticaHoje);

    }


    /**
     * @param SyncMinhaOpcaoInscricaoRequest $request
     * @return mixed
     */
    public function syncMinhasOpcoes( SyncMinhaOpcaoInscricaoRequest $request)  //Request $request) // InscricaoOpcaoRequest $request{
    {
        // pega dados de inscricao cadastrada
        $dadosInscricao = $this->dadosInscricao();

        $i = 0 ;

        // apaga todas as inscricoesOpcoes existentes
        $this->inscricaoOpcao->forceDeleteByInscricaoID($dadosInscricao->ic_id_inscricao);

        // reescreve todas novamente
        foreach ( $request->all()  as $key => $io_cd_uorg_destino )
        {
            if ($key == "_token"){continue;}
            $inputs =
                [
                    'io_fk_ic_inscricao' => $dadosInscricao->ic_id_inscricao,
                    'io_cd_uorg_destino' => $io_cd_uorg_destino,
                    'io_vl_ordem_preferencia' => ++$i
                ];

            $this->inscricaoOpcao->add($inputs);
        }
        if(ConfiguracaoConcurso::checkVisualizaEstatisticaHoje()){
            return Redirect::action('MinhaInscricaoController@montaTelaEstatistica');
        }else{
            return Redirect::action('MinhaInscricaoController@confirmaInscricao');
        }
    }


    /**
     * @return $this
     */
    public function montaTelaEstatistica()
    {
        // monta o cenario para o candidato

        $estatisticadaOpcao = [];
        $i = 0;
        $dadosInscricoesOpcoes = $this->_pegaMinhasInscricoesOpcoes();

        if ($dadosInscricoesOpcoes->isEmpty()){ Redirect::action('MinhaInscricaoController@montaTelaInscricaoOpcao');  }

        foreach ($dadosInscricoesOpcoes as $dadoInscricaoOpcao ) {

            $estatisticadaOpcao[$i]['probabilidadeSucesso'] = $this   ->estatistica
                                                ->getProbabilidadeSucesso($dadoInscricaoOpcao->io_id_inscricao_opcao);

            $estatisticadaOpcao[$i]['estatisticaConcorrencia'] = $this->estatistica
                                                    ->getStatisticasConcorrencia($dadoInscricaoOpcao->io_id_inscricao_opcao);
            $i++;

        }

        $inscricaoConfirmada = $this->dadosInscricao()->ic_st_inscricao_confirmada;
        $dataMaxVisualizarEstatistica = ConfiguracaoConcurso::dataMaxVisualizarEstatistica();

       // dd($inscricaoConfirmada);
        // se quiser em json
        //$estatisticadaOpcao = json_encode($estatisticadaOpcao);
        return view('inscricao.passo3') ->with('estatisticadaOpcao',$estatisticadaOpcao)
                                        ->with('inscricaoConfirmada', $inscricaoConfirmada )
                                        ->with('dataMaxVisualizarEstatistica', $dataMaxVisualizarEstatistica);


    }

    /**
     * @return mixed
     */
    public function confirmaInscricao()
    {
        $dadosInscricao = $this->dadosInscricao();
        if($this->inscricao->confirmaInscricao($dadosInscricao->ic_id_inscricao)){
            return Redirect::action('MinhaInscricaoController@comprovanteInscricao');
        }else{
            return redirect()->back()->withErrors('Não foi possível confirmar definitivamente sua inscrição');
        }
    }

    /**
     * @return mixed
     */
    public function comprovanteInscricao(){
        $dadosInscricaoOpcoes = $this->_pegaMinhasInscricoesOpcoes();
        if ($dadosInscricaoOpcoes == null){
            redirect()->back()->withErrors('Não foi encontrada inscricao confirmada');
        }
        $dadosInscricao = $this->dadosInscricao();

        $myHash = $this->_makeMyHash($dadosInscricao, $dadosInscricaoOpcoes);
       $myQrCode = QrCode::format('svg')->size(300)->generate($myHash);
           //     $view =  \View::make('inscricao.comprovanteInscricao',
           //                 compact( 'dadosInscricaoOpcoes','dadosInscricao','myHash'))
           //                 ->render();

      //  $pdf = \App::make('dompdf.wrapper');
      //  $pdf->loadHTML($view);
      //  return $pdf->stream('invoice');
         return view('inscricao.comprovanteInscricao')
            ->with('myQrCode', $myQrCode)
            ->with('dadosInscricaoOpcoes',$dadosInscricaoOpcoes )
             ->with('myHash', $myHash)
            ->with('dadosInscricao',$dadosInscricao ); /**/
    }

    /**
     * @return $this
     */
    public function montaTelaCenarioGeral()
    {
       $sumarioInscricoes = $this->inscricao->getSumario();
       $cenarioGeral = $this->estatistica->getCenarioGeral();
//        dd($cenarioGeral);
       return view('inscricao.cenarioGeral')->with('cenarioGeral',$cenarioGeral)
                                            ->with('sumarioInscricoes',$sumarioInscricoes);

    }
}
