<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

use App\Events\regrasConcursoAlteradas as regrasConcursoAlteradas;

use App\Models\Concurso\ConcursoRepository as Concurso,
    App\Models\Inscricao_Opcao\inscricaoOpcaoCRUDRepository as InscricaoOpcao,
    App\Models\Inscricao\InscricaoRepository as Inscricao,
    App\Models\Vaga\VagaRepository as Vagas,
    App\Models\ResultadoFinal\ResultadoFinalRepository as ResultadoFinal,


    App\ExecutaConcurso\ExecutaConcursoRepository as ExecutaConcurso;


use Illuminate\Routing\Route;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Event;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Redirect;
use App\Http\Requests\ConcursoRequest as ConcursoRequest;
use Illuminate\Support\Facades\Session;

/**
 * Class AdminController
 * @package App\Http\Controllers
 */
class AdminController extends Controller {

    /*
     * Os métodos e funcionalidades deste controller devem ser usados apenas pelo administrador (root) do sistema
     * tem a ver com a configuração das vagas e do tipo de concurso. Lembrando que um concurso que tenha 0 vagas preestabelecidas
     * para todas as UORGs , na hora de classificar e balancear as vagas, o sistema usará automaticamente o método conservativo
     * entendendo que será um concurso de mera permuta.
     * */
	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
    // protected $Concurso;
    // protected $InscricaoOpcao
    // protected $Vagas
    // protected $ResultadoFinal
    // protected $Inscricao
    // protected $ExecutaConcurso
    function __construct(
                          Concurso $Concurso ,
                          InscricaoOpcao $InscricaoOpcao,
                          Vagas $Vagas,
                          ResultadoFinal $ResultadoFinal,
                          Inscricao $Inscricao,
                          ExecutaConcurso $ExecutaClassificacao
                        )
    {

        $this->concurso = $Concurso;
        $this->inscricaoOpcao = $InscricaoOpcao;
        $this->vaga = $Vagas;
        $this->resultadoFinal   = $ResultadoFinal;
        $this->inscricao = $Inscricao;
        $this->executaClassificacao = $ExecutaClassificacao;


    }

    private function _updateSessionConcursoAtual(){
        $proximoConcurso = $this->concurso->getProximoConcurso(true);
        Session::put('proximoConcurso',$proximoConcurso);
    }
    /**
     * @param ConcursoRequest $request
     * @return array
     */
    private function _requestToArray (ConcursoRequest $request)
    {
        $inputs = [];

        if ($request->has('co_id_concurso')){
            $inputs['co_id_concurso'] = $request->co_id_concurso;
        }
        $inputs['co_dt_publicacao_edital'] = $request->co_dt_publicacao_edital;
        $inputs['co_dt_inicio_inscricoes'] = $request->co_dt_inicio_inscricoes;
        $inputs['co_dt_fim_inscricoes'] = $request->co_dt_fim_inscricoes;
        $inputs['co_dt_data_execucao'] = $request->co_dt_data_execucao;
        $inputs['co_dt_remocao'] = $request->co_dt_remocao;
    //    $index = 0;

        return $inputs;
    }


    /**
     * @param array $quadroVagas
     */
    private function _updateVagas( $json_vagas )
    {
        $vagas = json_decode( $json_vagas);
        foreach ($vagas as $vaga) {
            if(!$this->vaga->updateVagasPreestabelecidas($vaga->vg_vl_vagas_preestabelecidas,$vaga->vg_id_uorg)){
                return redirect()->back()->withErrors('Não foi possível salvar as vagas da uorg ' . $vaga->vg_id_uorg . ' '. $vaga->vg_no_nome );
            }
        }

        /*
        // faz update na tabela de vagas
        // modelo : $quadroVagas[0]['vg_id_uorg']
        //          $quadroVagas[0]['vg_vl_vagas_preestabelecidas']
        foreach ($quadroVagas as $quadroVaga) {
            $this->vaga->updateVagasPreestabelecidas(   $quadroVaga['vg_vl_vagas_preestabelecidas'],
                                                        $quadroVaga['vg_id_uorg']
                                                    );
        }
        */
    }


    /**
     * @return bool|void
     */
    private function _apaga(){
        // apaga o concurso atual
        $proximoConcurso = $this->concurso->getProximoConcurso(true);
        if (!is_null($proximoConcurso )){
            $this->concurso->destroy($proximoConcurso->co_id_concurso);
            $this->_updateSessionConcursoAtual();
            return true;
        }
        return false;
    }

    /**
     * @return $this
     */
    public function montaTelaNovoConcurso(){
        // em novo concurs as vagas são zeradas, o admin pode setar novas vagas, o banco de inscricoes é zerado ,
        // o concurso que estiver rodando é arquivado automaticamente, tudo é zerado e preparado para receber novo
        // concurso
        // pega dados do concurso atual e manda pra tela
        //Artisan::queue('Sisnar:rebootExecutarConcurso');

        $vagas = $this->vaga->getAll_UORGS_VagasPreestabelecidas();
        return view('admin.formularioNovoConcurso')->with('vagas',$vagas);
    }


    /**
     * @return $this
     */
    public function montaTelaConcursoAtual(){
        // o gestor pode apensa mudar uma data do concurso por exemplo e as inscricoes são mantidas , tambem , antes da
        // execução do concurso pode alterar o quantitativo de vagas .
        // pega dados do concurso atual e manda pra tela
        $concursoAtual = $this->concurso->getProximoConcurso(true);
        if (is_null($concursoAtual)){
            return redirect()->route('')->withErrors('Não existe concurso atualmente');
        }
        $vagas = $this->vaga->getAll_UORGS_VagasPreestabelecidas();
        return view('admin.formularioConcursoAtual')->with('concursoAtual',$concursoAtual)
                                                    ->with('vagas',$vagas);
    }

    /**
     *
     */
    public function updateConcursoAtual( ConcursoRequest $request){

        // tratando do concurso
       //dd('entrou no updateConcursoAtual',$request->all());
        $inputs = $this->_requestToArray($request);
        //dd($request->all(), $inputs);
        if(!$this->concurso->update($inputs,$inputs['co_id_concurso'])){
          // return 'nao ok';
            redirect()->back()->withErrors('Não foi possível atualizar os dados deste concurso');
            //return Redirect::action('AdminController@montaTelaConcursoAtual');
        }else{
            // recalculando os dados das inscrições já existentes e
            // apaga as inscricoes de candidatos intempestivos
            $this->_updateSessionConcursoAtual();
            Session::flash('warning_message', 'Aguarde até 15 minutos para que as inscrições atuais sejam atualizadas conforme as novas regras do concurso');
            // $this->inscricao->recalculaDadosTemporais($this->concurso->getProximoConcurso()->toArray());
            Event::fire(new regrasConcursoAlteradas());
        }

        // tratando das vagas
        $this->_updateVagas($request->get('vagas'));
        $this->_updateSessionConcursoAtual();
        Session::flash('success_message', 'O registro foi ALTERADO com sucesso');
        //return json_encode(['sucesso'=> true, 'mensagem'=> 'O registro foi ALTERADO com sucesso']) ;
        return Redirect::action('AdminController@montaTelaConcursoAtual');
    }

    /**
     * @param ConcursoRequest $request
     * @return mixed
     */
    public function gravaNovoConcurso( ConcursoRequest $request){
        // tratando da tabela concurso
        if($this->concurso->checkConcursoExecutado()){

            $co_id_concurso = $this->concurso->getProximoConcurso(true)->co_id_concurso;
             Artisan::queue('Sisnar:arquivarConcurso', ['co_id_concurso' => $co_id_concurso]);


            // pega todas as vagas
            $dadosConcurso['vagas'] = $this->vaga->getAll()->toArray();

            // salva  $dadosConcurso['vagas'] em co_vl_dados_vagas
            $this->concurso->saveDadosVagas($co_id_concurso, $dadosConcurso);
          //  $this->vaga->zeraVagas();

        }else{
            $this->_apaga();
        }
        $inputs = $this->_requestToArray($request);
        $this->concurso->add($inputs);

        //tratando das vagas
        $this->_updateVagas($request->get('vagas'));
        Session::flash('success_message', 'O registro foi INCLUÍDO com sucesso');
        Session::flash('warning_message', 'Aguarde até 10 minutos para que os dados do último concurso sejam arquivados'.
            ' e o sistema esteja preparado para receber novas inscrições');
        $this->_updateSessionConcursoAtual();
        return redirect()->route('index');
        //Redirect::action('AdminController@montaTelaConcursoAtual');

    }

    /**
     * @return mixed
     */
    public function apagaConcursoAtual(){
        $isSuccess = $this->_apaga();
        $this->_updateSessionConcursoAtual();
        if ($isSuccess){return redirect()->back()->withErrors('Concurso Atual foi apagado');}
        else{ return redirect()->back()->withErrors('Concurso Atual NÃO PODE ser apagado');}
    }
    public function executaConcurso(){
                    // $output = new BufferedOutput();
         //   $proximoConcurso = $this->concurso->getProximoConcurso();

            ob_start();
            Artisan::queue('Sisnar:rebootExecutarConcurso');
            Artisan::queue('Sisnar:executarConcurso');
            Artisan::queue('Sisnar:balancearConcurso');
            $output = ob_get_contents();
            ob_end_clean();
            Log::info($output);
        return 'Procedida a classificação dos candidatos!';//redirect()->route('resultadoFinalConcurso_xls');
        //\Response::Make($output)->header('Content-Type', 'plain/txt');



    }

}
