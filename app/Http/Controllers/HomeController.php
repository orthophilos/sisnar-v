<?php namespace App\Http\Controllers;

use Illuminate\Support\Facades\Session;
use PRF\FaleConosco;
use Illuminate\Support\Facades\Auth;
use App\Models\Concurso\ConcursoRepository as Concurso,
    App\Models\Vaga\VagaRepository as Vaga;

use App\Models\Inscricao\InscricaoRepository as Inscricao,
    App\Models\Servidor\ServidorRepository as Servidor;


class HomeController extends Controller
{

    /*
    |--------------------------------------------------------------------------
    | Home Controller
    |--------------------------------------------------------------------------
    |
    | This controller renders your application's "dashboard" for users that
    | are authenticated. Of course, you are free to change or remove the
    | controller as you wish. It is just here to get your app started!
    |
    */

    /**
     * Create a new controller instance.
     *
     * @return void
     */
//  protected  $Concurso;
//protected  $Inscricao;
//protected  $Servidor;

    public function __construct( Concurso $Concurso, Vaga $Vaga,  Inscricao $Inscricao,   Servidor $Servidor)
    {
      //  $this->middleware('auth');
        $this->concurso = $Concurso ;
        $this->vaga = $Vaga;
        $this->inscricao = $Inscricao;
        $this->servidor = $Servidor;

    }

    /**
     * Show the application dashboard to the user.
     *
     * @return Response
     */
    public function index()
    {
     //   dd(Session::get('proximoConcurso'));

        $dadosInscricao  = $this->dadosInscricao();
        $incricaoConfirmada = (is_null($dadosInscricao) ? 0 : $this->dadosInscricao()->ic_st_inscricao_confirmada );
      //  $faleconoscoObj = new FaleConosco(config("PRF.producao"));
      //  $faleconosco = $faleconoscoObj->getAvisos(Auth::user()->cpf, config("PRF.siglaSistema"));
        $proximoConcurso =  Session::get('proximoConcurso');
        $teste = $this->vaga->getTotalVagasRemanescentes();
        $tipoConcurso = ($teste[0]->TotalVagasRemanescentes == 0   ? 'Remanejamento' : 'SISNAR' );
        return view('index', compact('faleconosco','proximoConcurso','tipoConcurso','incricaoConfirmada'));
    }

}
