<?php namespace App\Http\Controllers;

use App\Http\Controllers\Auth\AuthController;
use Illuminate\Foundation\Bus\DispatchesCommands;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;


use Illuminate\Support\Facades\Auth;
use PRF\FaleConosco as FaleConosco;
use PRF\RH as RH;

//use App\Models\Inscricao\InscricaoRepository as Inscricao,
//	App\Models\Servidor\ServidorRepository as Servidor;

abstract class Controller extends BaseController {

	use DispatchesCommands, ValidatesRequests;


	/**
	 * @return mixed
	 * @throws \Exception
     */
	public function prfFaleConosco()
	{
		try {
			$prefaleconosco = new FaleConosco(config("PRF.producao"));
			return $prefaleconosco->getAvisos(Auth::user()->cpf, config("PRF.siglaSistema"));

		} catch (\Exception $e) {
			throw new \Exception("Erro ao buscar dados do Servidor no WebService" . $e->getMessage());

		}

	}

	/**
	 * @return null
     */
	public function dadosInscricao(){
		$servidorCadastrado = $this->servidor->getByCPF(Auth::user()->cpf);
        if (is_null($servidorCadastrado)){return null;}
		return $this->inscricao->getInscricaoFromServidorID($servidorCadastrado->sd_id_servidor);

	}
}
