<?php namespace App\Http\Controllers;

use App\Models\Inscricao\Tb_ic_inscricao;
use App\Models\Inscricao_Opcao\Tb_io_inscricao_opcao;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests;
use App\Http\Controllers\Controller;

// models
use App\Models\Inscricao\InscricaoRepository as Inscricao,
    App\Models\Servidor\ServidorRepository as Servidor,
    App\Models\Inscricao_Opcao\inscricaoOpcaoCRUDRepository as InscricaoOpcao,
    App\Models\Vaga\VagaRepository as Uorgs,
    App\Models\Concurso\ConcursoRepository as Concurso;

use App\ExecutaConcurso\ConfiguracaoConcurso as ConfiguracaoConcurso;

use Illuminate\Http\Request;
use App\Http\Requests\InscricaoRequest as InscricaoRequest;

use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Symfony\Component\HttpFoundation\Response;

use PRF\RH;
//excel

use Maatwebsite\Excel\Excel as Excel;

class RecursoController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
 //   protected   $Inscricao;
  //  protected   $InscricaoOpcao;
  //  protected   $Uorgs;
//    protected   $Concurso;
//    protected  $Servidor
//    protected $RH
    function __construct (  Inscricao $Inscricao,
                            InscricaoOpcao $InscricaoOpcao,
                            Uorgs $Uorgs,
                            Concurso $Concurso,
                            Servidor $Servidor
							,Excel $Excel
                            ,RH $RH
                        )

    {
        $this->inscricao = $Inscricao;
        $this->inscricaoOpcao = $InscricaoOpcao;
        $this->uorgs = $Uorgs;
        $this->concurso = $Concurso;
        $this->servidor = $Servidor;
        $this->myexcel = $Excel;
        $this-> RH = $RH;

     //   parent::__construct();

    }

    private function _geraPlanilha($todosDados, $nomePlanilha, $setTitle, $description,
                                   $colunas = ['NOME','MATRICULA','CPF','IDADE','ID_OPCAO','ID_INSCRICAO','ORDEM DE PRIORIDADE',
                                       'ID_UORG_ORIGEM','UORG ORIGEM','UF ORIGEM','ID_UORG_DESTINO','UORG DESTINO','UF DESTINO','VAGAS_PREESTABELECIDAS',
                                       'PONTUACAO_DERIVADA','CONTEMPLADO','COMPUTADO','PONTUACAO','INSCRICAO CONFIRMADA', 'APAGADOS']

     ){
        $dataConcurso = $this->concurso->getProximoConcurso();
        $nome_planilha = $nomePlanilha .$dataConcurso->co_dt_publicacao_edital;

        $this->myexcel->create($nome_planilha,

            function($excel) use($todosDados, $setTitle, $description, $colunas) {
                // File details
                $excel  ->setTitle($setTitle)
                    ->setCreator('Sisnar-V')
                    ->setCompany('PRF');
                // ->setOrientation('landscape');
                // Call them separately
                $excel->setDescription($description);

                //content
                //   dd(todasClassificacoes);
                //$sheet->fromArray($data, null, 'A1', false, false);

                $excel->sheet('Inscrições',
                    function ($sheet) use ($todosDados, $colunas) {
                        $sheet->fromArray($todosDados);
                        $sheet->prependRow($colunas);
                        $sheet->freezeFirstRow();
                        $sheet->setAutoSize(true);
                        $sheet->cells('A1:O1', function($cells) {
                            // manipulate the range of cells
                            $cells->setBackground('#DFFFF9');
                            $cells->setFont(array(
                                'size'       => '14',
                                'bold'       =>  true
                            ));
                        });

                    });

            })->download('xls');


    }

    /**
     * @param string $orderBy
     * @param string $direction
     * @return \BladeView|bool|\Illuminate\View\View
     */
    public function indexRecurso( $orderBy = 'sd_no_nome' , $direction = 'asc')
	{
        $inscricoes = $this->inscricao  ->indexRecurso($orderBy , $direction);
        $selected = null;
        $value = null;
        return view('recursos.indexRecurso', compact('inscricoes','selected','value'));


//            return Response::json(view('recursos.index', compact('inscricoes'))->render());
//        }
//$inscricoes->render(); // para as paginacoes.
      //  return view('recursos.indexRecurso', compact('inscricoes'));
	}


    /**
     * @param Request $request
     * @return \BladeView|bool|\Illuminate\View\View
     */
    public function search(Request $request)
    {

        $inscricoes = $this->inscricao->search($request->fieldname,$request->value, $request->fieldname, 'ASC' );
        $selected = $request->fieldname;
        $value = $request->value;

        return view('recursos.indexRecurso', compact('inscricoes','selected','value'));

    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     *
     *

     */
    public function show($id)
    {
        $inscricao =  $this ->inscricao->getById($id);
        $inscricaoOpcoes = $this->inscricaoOpcao->getInscricaoOpcaoByInscricaoID($inscricao->ic_id_inscricao);
        $quantidadeMaxOpcoes = ConfiguracaoConcurso::QTE_OPCOES;
        $proximoConcurso = Session::get('proximoConcurso');
        $uorgs  = $this->uorgs->getListUorgs($inscricao->ic_id_inscricao);
        //$uorgs = json_encode($uorgs);
      //  dd($inscricao);
        return view('recursos.mostraInscricao', compact( 'inscricao',
                                                        'inscricaoOpcoes',
                                                        'quantidadeMaxOpcoes',
                                                        'proximoConcurso',
                                                        'uorgs'
        ));
    }



    /**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function gravaInscricao(InscricaoRequest $InscricaoRequest) //, $edit = false)
	{
        // serve tanto para add quanto para edit

      //  dd($InscricaoRequest->all());
        // adicionando/alterando em servidor

        $servidorInputs['sd_no_nome'] = $InscricaoRequest->sd_no_nome;
        $servidorInputs['sd_cd_matricula_servidor'] = $InscricaoRequest->sd_cd_matricula_servidor;
        $dadosWebService = $this->RH->getByMatricula($InscricaoRequest->sd_cd_matricula_servidor);
        if($dadosWebService == false) {
            return redirect()->back()->withErrors('Matricula não encontrada no sistema RH   ');
        }
        $dadosWebService = end($dadosWebService);

      //  $key = key($dadosWebService);
        $servidorInputs['sd_cd_cpf'] = $dadosWebService['cpf'];
        $servidorInputs['sd_cd_servidor'] =$dadosWebService['id'];

        if ($InscricaoRequest->exists('sd_id_servidor')){
            $InscricaoSalva = $this->servidor->update($servidorInputs, $InscricaoRequest->sd_id_servidor);
        }else{
            $sd_cd_cpf_servidor =  preg_replace("/[^0-9]/", "", $InscricaoRequest->get('sd_cd_cpf_servidor'));
           if (!$InscricaoRequest->has('forcaServidor'))
           {
                $this->servidor->destroy($this->servidor->getByCPF($sd_cd_cpf_servidor)->sd_id_servidor);
           }
            $InscricaoSalva = $this->servidor->add($servidorInputs);
        }



        // adicionando/alterando em Inscricao


        $inscricaoInputs['ic_fk_sd_servidor'] = $this->servidor
            ->getServidorByMatricula($InscricaoRequest->sd_cd_matricula_servidor)
            ->sd_id_servidor;
        $inscricaoInputs['ic_fk_vg_uorg_origem'] = $InscricaoRequest->vg_id_uorg;
        $inscricaoInputs['dataExercicio'] = $InscricaoRequest->ic_vl_data_ini_serv;
        $inscricaoInputs['dataLotacao'] = $InscricaoRequest->ic_vl_data_ult_lotacao;
        $inscricaoInputs['dataNascimento'] = $InscricaoRequest->ic_vl_data_nasc;
        $inscricaoInputs['ic_vl_alteracoes'] = $InscricaoRequest->ic_st_justificativa_alteracao;
        if ($InscricaoRequest->exists('ic_id_inscricao')){
            $InscricaoSalva = $this->inscricao->update($inscricaoInputs, $InscricaoRequest->ic_id_inscricao);
        }else{
            $InscricaoSalva = $this->inscricao->add($inscricaoInputs);
            $this->inscricao->confirmaInscricao($InscricaoSalva->ic_id_inscricao);
        }



        //adicionando/alterando as opcoes


        $i = 0 ;
        $teste = $this->inscricaoOpcao->forceDeleteByInscricaoID($InscricaoSalva->ic_id_inscricao);
        //tratando e salvando as opcoes de UORGs
        $inscricaoInputs = $InscricaoRequest->all();
        foreach ( $inscricaoInputs  as $key => $io_cd_uorg_destino )
        {
            if (!preg_match('/io_cd_uorg_destino_+[0-9]/', $key)){continue;}
            $inputs =
                [
                    'io_fk_ic_inscricao' => $InscricaoSalva->ic_id_inscricao,
                    'io_cd_uorg_destino' => $io_cd_uorg_destino,
                    'io_vl_ordem_preferencia' => ++$i

                ];
            $inscricaoOpcao = $this->inscricaoOpcao->add($inputs);
        }

        return Redirect::action('RecursoController@show',['ic_id_inscricao' => $InscricaoSalva->ic_id_inscricao ]);

    }

    /**
     * @return \BladeView|bool|\Illuminate\View\View
     */
    public function montaTelaAddInscricao(){
        $uorgs  = $this->uorgs->getListUorgs('99999999');
       // $uorgs = json_encode($uorgs);


        $quantidadeMaxOpcoes = ConfiguracaoConcurso::QTE_OPCOES;
        $proximoConcurso = Session::get('proximoConcurso');
        return view('recursos.addInscricao', compact(   'proximoConcurso',
                                                        'quantidadeMaxOpcoes',
                                                        'uorgs'
                                                   ));

    }

    public function getDadosWebService($cpf)
    {
        try {
            $dadosWebService = $this->RH->getByCPF($cpf);
        }
        catch(MyException $e){
            $dadosWebService = [];
        }
       // $dadosWebService = $this->servidor->getServidorDadosFromWebService($cpf);
        if (!is_null($dadosWebService)){
            $dadosWebService_Json = json_encode($dadosWebService);
            return $dadosWebService_Json;
        }else{
            return null;
        }

    }


    public function apagar(Request $Request)
    {
       // $Inscricao = Tb_ic_inscricao::find($Request->get('ic_id_inscricao'));
      //  $ic_fk_sd_servidor = $Inscricao->ic_fk_sd_servidor;

        //insere justificativa
        $this->inscricao->insertJustificativa($Request->get('ic_id_inscricao'),$Request->get('ic_st_justificativa_alteracao'));

        // deleta as opcoes do candidato
        $this->inscricaoOpcao->forceDeleteByInscricaoID($Request->get('ic_id_inscricao'));

        //deleta inscricao
        $this->inscricao->destroy($Request->get('ic_id_inscricao'));

        //deleta servidor
      //  $this->servidor->destroy($ic_fk_sd_servidor);

        return Redirect::route('indexRecurso');


    }


    /**
     * @return $this
     */
    private function getResultadoFinalConcurso(){
        $aprovadosConcurso = $this->inscricaoOpcao->getResultadoConcurso()->toArray();
        if (count($aprovadosConcurso) == 0 ){
            return redirect()->back()->withErrors('Ainda não há resultado do concurso atual');
        }
        return $aprovadosConcurso;
    }

    /**
     * @return $this
     */
    public function resultadoFinalConcurso (){
        $aprovadosConcurso = json_encode($this->getResultadoFinalConcurso());
        return view('recursos.resultadoConcurso')->with('aprovadosConcurso',$aprovadosConcurso);
    }




    public function planilhaTodasInscricoes()
    {
        $this->_geraPlanilha(
            $this->inscricaoOpcao->getAllWithTrash(),
            'Inscricoes_Sisnar',
            'Lista de Inscricoes do Concurso Anual de Remanejamento/SISNAR',
            'Arquivo criado a partir do Sistema SISNAR-V listando  as inscrições
                do concursos SISNAR/Permuta/Remanejamento'
        );
    }


    public function planilhaResultadoFinal()
    {

        $this->_geraPlanilha(
            $this->getResultadoFinalConcurso(),
            'Resultado_Sisnar',
            'Lista de Classificação do Concurso Anual de Remanejamento/SISNAR',
            'Arquivo criado a partir do Sistema SISNAR-V listando  as classificações
                do concursos SISNAR/Permuta/Remanejamento'
        );
    }

    /**
     * Gera planilha com o deficit de efetivo gerado em cada delegacia devido ao SISNAR
     */
    public function planilhaDeficitPosSISNAR(){
        $this->_geraPlanilha(
            $this->uorgs->queryDeficit()->toArray(),
            'Deficit Pos Sisnar',
            'Lista dos deficits gerados nas delegacias após a classificação SISNAR',
            'Arquivo criado a partir do Sistema SISNAR-V listando  os deficits
                nas delegacias devido ao concurso de Remanejamento',['nome','uf','vagas preestabelecidas','vagas ganhas',
                'vagas perdidas','deficit']
        );
    }


    /**
     *
     */
/*    public function planilhaResultadoFinal(){

        $todasClassificacoes = $this->getResultadoFinalConcurso();
        $dataConcurso = $this->concurso->getProximoConcurso();
        $nome_planilha = 'Resultado_Sisnar'.$dataConcurso->co_dt_publicacao_edital;
        $this->myexcel->create($nome_planilha,

            function($excel) use($todasClassificacoes) {
                // File details
                $excel  ->setTitle('Resultado Final de Classificao do Concurso Anual de Remanejamento/SISNAR')
                    ->setCreator('Sisnar-V')
                    ->setCompany('PRF');
                // ->setOrientation('landscape');
                // Call them separately
                $excel->setDescription('Arquivo criado a partir do Sistema SISNAR-V listando o resultado
                da classificação do concursos SISNAR/Permuta/Remanejamento');

                //content
             //   dd(todasClassificacoes);
                //$sheet->fromArray($data, null, 'A1', false, false);

                $excel->sheet('Classificacao',
                        function ($sheet) use ($todasClassificacoes) {
                            $sheet->fromArray($todasClassificacoes);
                            $sheet->prependRow([
                                'NOME',
                                'MATRICULA',
                                'CPF',
                                'io_id_inscricao_opcao',
                                'io_fk_ic_inscricao',
                                'ORDEM DE PRIORIDADE',
                                'io_cd_vg_uorg_origem',
                                'io_cd_uorg_destino',
                                'UORG DESTINO',
                                'UF DESTINO',
                                'vg_vl_vagas_preestabelecidas',
                                'io_vl_pontuacao_derivada',
                                'io_st_contemplado',
                                'io_st_computado',
                                'PONTUACAO'
                            ]);
                            $sheet->freezeFirstRow();
                            $sheet->setAutoSize(true);
                            $sheet->cells('A1:O1', function($cells) {
                                // manipulate the range of cells
                                $cells->setBackground('#DFFFF9');
                                $cells->setFont(array(
                                    'size'       => '14',
                                    'bold'       =>  true
                                ));
                            });

                        });

            })->download('xls');
    }*/
}
