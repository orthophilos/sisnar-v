<?php namespace App\Http;

use Illuminate\Foundation\Http\Kernel as HttpKernel;

class Kernel extends HttpKernel {

	/**
	 * The application's global HTTP middleware stack.
	 *
	 * @var array
	 */
	protected $middleware = [
		'Illuminate\Foundation\Http\Middleware\CheckForMaintenanceMode',
		'Illuminate\Cookie\Middleware\EncryptCookies',
		'Illuminate\Cookie\Middleware\AddQueuedCookiesToResponse',
		'Illuminate\Session\Middleware\StartSession',
		'Illuminate\View\Middleware\ShareErrorsFromSession',
		'App\Http\Middleware\VerifyCsrfToken',
	];

	/**
	 * The application's route middleware.
	 *
	 * @var array
	 */
	protected $routeMiddleware = [
		'auth' => 'App\Http\Middleware\Authenticate',
		'auth.basic' => 'Illuminate\Auth\Middleware\AuthenticateWithBasicAuth',
		'guest' => 'App\Http\Middleware\RedirectIfAuthenticated',
		'podeVerEstatistica' => 'App\Http\Middleware\periodoVisualizarEstatisticaMiddleware',
        'podeInscrever' => 'App\Http\Middleware\periodoInscricaoMiddleware',
        'podeExecutarConcursoManualmente' => 'App\Http\Middleware\periodoExecutaConcursoManualmenteMiddleware',
        'periodoCadastroRecurso' => 'App\Http\Middleware\periodoCadastraRecursosMiddleware',
        'condicoesTemporalidadeCandidato' =>'App\Http\Middleware\condicoesTemporaisCandidatoMiddleware' ,
		'inscricaoConfirmada' => 'App\Http\Middleware\inscricaoConfirmadaMiddleware',
		'periodoVisualizarCenarioGeral' => 'App\Http\Middleware\periodoVisualizarCenarioGeralMiddleware',
		'ConcursoNaoExecutado' => 'App\Http\Middleware\concursoNaoExecutadoMiddleware',
		'ConcursoExecutado' => 'App\Http\Middleware\concursoExecutadoMiddleware',
		'temConcurso' => 'App\Http\Middleware\concursoAtualMiddleware',
		'gerenciar' => 'App\Http\Middleware\gerenciar',
		'root' => 'App\Http\Middleware\root'

	];

}
