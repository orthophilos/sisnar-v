<?php

// la pietra Rosetta!!!
/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

//Rotas de Login
get('login', 'LoginController@index');
post('login', ['as' => 'doLogin', 'uses' => 'LoginController@doLogin']);
get('logout', 'LoginController@logout');
get( 'instrucoesDeUso', [
        'as' => 'instrucoesDeUso',
        function () {
            /*
            $dados = [];
            $dados['QTE_OPCOES'] = \app\ExecutaConcurso\ConfiguracaoConcurso::QTE_OPCOES; //quantidade de opçoes que o candidato pode escolher para trabalhar
            $dados['PERMUTA'] = \app\ExecutaConcurso\ConfiguracaoConcurso::PERMUTA;// Ativado sistema de permuta, do contrario o sistema não calculará o cruzamento de dados
            $dados['MOSTRAR_PROBABILIDADE_AO_CANDIDATO'] =\app\ExecutaConcurso\ConfiguracaoConcurso::MOSTRAR_PROBABILIDADE_AO_CANDIDATO; // ate quantos dias antes de encerrar as inscrições se deve mostrar as estatisticas
            $dados['INTERTICIO_DIAS_ULTIMA_REMOCAO'] = \app\ExecutaConcurso\ConfiguracaoConcurso::INTERTICIO_DIAS_ULTIMA_REMOCAO; // periodo de interticio em dias desde a ultima remocao.

     //       $dados = json_encode($dados);
*/
            return view('instrucoesDeUso');
        }
    ]
);

Route::group (['middleware' => ['auth']], function() {
//Rota página inicial
    get('/', ['as' => 'index', 'uses' => 'HomeController@index']);

//    get( 'semConcurso', [
//            'as' => 'semConcurso',
//            function () {
//                return view('semConcurso');
//            }
//        ]
//    );


    Route::group(['middleware' => ['temConcurso']], function () {


    //Rotas de Inscricao
        get('/inscricao/comprovanteInscricao', ['as' => 'comprovanteInscricao',
            'uses' => 'MinhaInscricaoController@comprovanteInscricao',
            'middleware' => ['inscricaoConfirmada','ConcursoNaoExecutado']]);
        //'middleware' => 'ConcursoNaoExecutado'
    //temConcurso

        Route::group(['middleware' => [ 'podeInscrever','condicoesTemporalidadeCandidato']], function () {

            get('/inscricao/passo1', ['as' => 'inscricaoPasso1', 'uses' => 'MinhaInscricaoController@cadastroInscricao']);
            get('/inscricao/passo2', ['as' => 'inscricaoPasso2', 'uses' => 'MinhaInscricaoController@montaTelaInscricaoOpcao']);
            get('/inscricao/passo3', ['as' => 'inscricaoPasso3', 'uses' => 'MinhaInscricaoController@montaTelaEstatistica','middleware' => 'periodoVisualizarCenarioGeral' ]);
            post('/inscricao/sincronizaMinhasOpcoes', ['as' => 'sincMinhasOpcoes', 'uses' => 'MinhaInscricaoController@syncMinhasOpcoes']);
            get('/inscricao/confirmaInscricao', ['as' => 'confirmaInscricao', 'uses' => 'MinhaInscricaoController@confirmaInscricao']);
        });

        get('/estatistica/cenarioGeral', [  'as' => 'cenarioGeral',
                                            'uses' => 'MinhaInscricaoController@montaTelaCenarioGeral',
                                            'middleware' => 'periodoVisualizarCenarioGeral']);


    //Rotas de Recurso

        Route::group( ['middleware' => ['periodoCadastroRecurso', 'gerenciar']], function () {

            get('/recursos/indexRecursos', ['as'=>'indexRecurso', 'uses'=>'RecursoController@indexRecurso']);
            get('recursos/pesquisa/', ['as' =>'recursoSearch', 'uses' =>'RecursoController@search' ]); //
            get('/recursos/mostraInscricao/{id}', ['as' => 'mostraInscricao', 'uses' => 'RecursoController@show']);
            post('/recursos/gravaInscricao', ['as' => 'gravaInscricao', 'uses' => 'RecursoController@gravaInscricao', 'middleware' => 'ConcursoNaoExecutado']);
            get('recursos/montaTelaAddInscricao/', ['as' =>'montaTelaAddInscricao', 'uses' =>'RecursoController@montaTelaAddInscricao' , 'middleware' => 'ConcursoNaoExecutado' ]);

            Route::group( ['middleware' => ['ConcursoExecutado']], function () {

                get('recursos/resultadoFinalConcurso/', ['as' => 'resultadoFinalConcurso', 'uses' => 'RecursoController@resultadoFinalConcurso']); //
                get('recursos/resultadoFinalConcurso_xls/', ['as' => 'resultadoFinalConcurso_xls', 'uses' => 'RecursoController@planilhaResultadoFinal']); //
                get('recursos/listaDeficit_xls/', ['as' => 'listaDeficit_xls', 'uses' => 'RecursoController@planilhaDeficitPosSISNAR']); //

            });

            get('recursos/listaInscricoes_xls/', ['as' =>'listaInscricoes_xls', 'uses' =>'RecursoController@planilhaTodasInscricoes' ]); //
            post('/recursos/apagar/{id}', ['as' => 'apagaInscricao', 'uses' => 'RecursoController@apagar', 'middleware' => 'ConcursoNaoExecutado']);

            get ('/recursos/getDadosWebService/{cpf}', ['as'=>'getDadosWebService_Json', 'uses'=>'RecursoController@getDadosWebService' ]);
       });
    });
   Route::group(['middleware' => ['root']], function () {

        get('/admin/concursoAtual',         ['as' => 'montaTelaConcursoAtual',  'uses' => 'AdminController@montaTelaConcursoAtual']);

        get('/admin/novoConcurso',          ['as' => 'montaTelaNovoConcurso',   'uses' => 'AdminController@montaTelaNovoConcurso']);

        post('/admin/gravaConcursoAtual',   ['as' => 'updateConcursoAtual',     'uses' => 'AdminController@updateConcursoAtual']);


        post('/admin/gravaNovoConcurso',    ['as' => 'gravaNovoConcurso',       'uses' => 'AdminController@gravaNovoConcurso']);

        post('/admin/updateVagas',    ['as' => 'updateVagas',       'uses' => 'AdminController@updateVagas']);

        get('/admin/apagarConcursoAtual', ['as' => 'apagaConcursoAtual', 'uses' => 'AdminController@apagaConcursoAtual']);


        // PAINEL DE COMANDOS DO SISNAR
        get( '/recursos/painelDeComandos', [
                                                'as' => 'painelDeComandos',
                                                function () {
                                                        return view('recursos.painelComandos');
                                                }]);
        get( 'executarConcurso.txt',   [
                                            'as' => 'executarConcurso',
                                           // 'uses'=>'AdminController@executaConcurso', //]);//,
                                            'middleware' => ['ConcursoNaoExecutado','podeExecutarConcursoManualmente'],
                                            function () {
                                                 ob_start();
                                                 Artisan::queue('Sisnar:executarConcurso');
                                                 Artisan::queue('Sisnar:balancearConcurso');

//                                                $output = ob_get_contents();
//                                                 ob_end_clean();
//                                                 \Illuminate\Support\Facades\Log::info($output);
                                                 \Illuminate\Support\Facades\Session::flash
                                                 ('warning_message', 'Aguarde até 15 minutos para que seja executada a classificação do concurso');
                                                 return redirect()->route('painelDeComandos');
                                            }]);
        get( 'rebootConcurso.txt',   [
                                            'as' => 'rebootExecutarConcurso',
                                            function () {
//                                                ob_start();
                                                Artisan::queue('Sisnar:rebootExecutarConcurso');
//                                                $output = ob_get_contents();
//                                                ob_end_clean();
//                                                \Illuminate\Support\Facades\Log::info($output);
                                                \Illuminate\Support\Facades\Session::flash
                                                ('warning_message', 'Aguarde até 15 minutos para que seja executado o reboot do concurso');
                                                return redirect()->route('painelDeComandos'); //'Reboot Executado!';
                                            }]);


        get( 'arquivarConcurso.txt',   [
                                            'as' => 'arquivarConcurso',
                                            function () {

//                                                ob_start();
                                                Artisan::queue('Sisnar:arquivarConcurso',
                                                                        ['co_id_concurso' =>
                                                                                \Illuminate\Support\Facades\Session::get('proximoConcurso')->co_id_concurso]);
//                                                $output = ob_get_contents();
//                                                ob_end_clean();
//                                                \Illuminate\Support\Facades\Log::info($output);
                                                \Illuminate\Support\Facades\Session::flash
                                                ('warning_message', 'Concurso será arquivado! Inscrições transportadas para arquivo morto,
                                                vagas zeradas, os dados deste concurso , a partir de agora,
                                                podem ser obtidos somente acessando diretamente o banco de
                                                dados');

                                                return redirect()->route('painelDeComandos');


                                                //\Response::Make($output)->header('Content-Type', 'plain/txt');
                                            }]);
        get( 'revisaoNotas.txt',   [
                                           'as' => 'revisaNotas',
                                           function () {

//                                               ob_start();
                                               Artisan::queue('Sisnar:revisaoNotas');
//                                               $output = ob_get_contents();
//                                               ob_end_clean();
//                                               \Illuminate\Support\Facades\Log::info($output);
                                               \Illuminate\Support\Facades\Session::flash
                                               ('warning_message', 'Todas as notas serão revistas , aguarde até 15 minutos');
                                               return redirect()->route('painelDeComandos');


                                               //\Response::Make($output)->header('Content-Type', 'plain/txt');
                                           }]);


   });
});