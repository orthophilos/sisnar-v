<?php namespace App\Http\Requests;
use App\ExecutaConcurso\ConfiguracaoConcurso as ConfiguracaoConcurso;
use App\Http\Requests\Request;

class VagaRequest extends Request {

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		if (Funcoes::temPermissao('gerenciar'))
		{
			return true;
		}else{
			return false;
		}


	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
			'vg_no_nome' => 'required| min: 10',
			'vg_ed_uf' => 'required| min:2',
		];
	}

}
