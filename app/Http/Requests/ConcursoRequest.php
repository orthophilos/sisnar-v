<?php namespace App\Http\Requests;

use App\Http\Requests\Request;
use App\ExecutaConcurso\ConfiguracaoConcurso as ConfiguracaoConcurso;
use App\Funcoes\Funcoes as Funcoes;
use App\Models\Vaga\Tb_vg_vaga;

class ConcursoRequest extends Request {

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
    {
   //     if (Funcoes::temPermissao('gerenciar'))
    //    {
            return true;
    //    }else{
    //        return false;
    //    }

    }

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
        $rules =  [
			'co_dt_publicacao_edital' => 'required|date_format:"'.Funcoes::$dateFormat.'"',
			'co_dt_inicio_inscricoes' => 'required|date_format:"'.Funcoes::$dateFormat.'"|after:yesterday',
			'co_dt_fim_inscricoes' => 'required|date_format:"'.Funcoes::$dateFormat.'"|after:co_dt_inicio_inscricoes',
			'co_dt_data_execucao' => 'required|date_format:"'.Funcoes::$dateFormat.'"|after:co_dt_fim_inscricoes',
			'co_dt_remocao' => 'required|date_format:"'.Funcoes::$dateFormat.'"|after:co_dt_data_execucao'

		];
        $vagas = json_decode( $this->request->get('vagas'));
        //$i = 0;
        foreach ( $vagas  as $vaga )
        {
           $a = !is_int((int) $vaga->vg_vl_vagas_preestabelecidas);
           $b = (Tb_vg_vaga::where('vg_id_uorg','=',$vaga->vg_id_uorg)->count() == 0);
           $c =  !($vaga->vg_vl_vagas_preestabelecidas >= 0);
            if   ($a  || $b || $c )
           {

              $rules['vaga_inconsistente'] = 'required';

           }

        }

        return $rules;

	}

	public function messages()
	{
		return
            [
			'co_dt_publicacao_edital.required'          => 'Data de publicação de edital é requirida',
            'co_dt_publicacao_edital.date_format'       => 'Formato de data de publicacao de edital incorreto',

            'co_dt_inicio_inscricoes.required'    => 'Data de início das inscrições é requerida',
            'co_dt_inicio_inscricoes.after'       => 'Data de início das inscrições deve ser posterior a data de amanhã',
            'co_dt_inicio_inscricoes.date_format' => 'Formato de data de Inicio de Inscricoes incorreto',

            'co_dt_fim_inscricoes.required'     => 'Data de Encerramento de inscrições é requerida',
            'co_dt_fim_inscricoes.after'        => 'Data de encerramento das inscrições deve ser superior a data de início das inscrições',
            'co_dt_fim_inscricoes.date_format'  => 'Formato de data de Encerramento de Inscricoés incorreto',

            'co_dt_data_execucao.required'      => 'Data de execução da classificação é requerida',
            'co_dt_data_execucao.after'         => 'Data de execução da classificação deve ser posterior a data de encerramento das inscrições',
             'co_dt_data_execucao.date_format'  => 'Formato de data de Execução da Classificação incorreto',

            'co_dt_remocao.required'     => 'Data de remoção é requerida',
            'co_dt_remocao.after'        => 'Data de remoção deve ser posterior a data de execução do concurso',
            'co_dt_remocao.date_format'  => 'formato de data de remoção incorreto',

            'vaga_insconsistente.required'  => 'Erro de inconsistencia de vaga, ou número incorreto ou uorg inexistente'
            ];


	}


}
