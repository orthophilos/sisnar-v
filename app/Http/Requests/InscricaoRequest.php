<?php namespace App\Http\Requests;
use App\ExecutaConcurso\ConfiguracaoConcurso as ConfiguracaoConcurso;
use App\Http\Requests\Request;
use App\Funcoes\Funcoes;
use Illuminate\Support\Facades\Auth;

class InscricaoRequest extends Request {

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		if 	(
			Funcoes::temPermissao('gerenciar')  &&
			ConfiguracaoConcurso::checkPodeAlterarDadosAlheios()
			)
		{
			return true;
		}else{
			return false;
		}

	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		//dd($this->request->all());
		$rules =
		[
			'sd_cd_cpf_servidor' => ' sometimes | required | unique:tb_sd_servidor,sd_cd_cpf,'.$this->request->get('sd_cd_cpf_servidor'). ',sd_id_servidor',
			'sd_no_nome' => 'required',
			'vg_id_uorg' => 'required|exists:tb_vg_vaga,vg_id_uorg',
			'ic_vl_dias_desd_ini_serv' => 'required | numeric', // | greater_than_field:ic_vl_dias_desd_ult_lotacao',
			'ic_vl_dias_desd_ult_lotacao' => 'required | numeric',
			'ic_vl_idade' => 'required | numeric',
			'ic_st_justificativa_alteracao' => 'required | min:20',
		];

		if ($this->request->has('forcaServidor'))
		{
			$rules =
				[
					'sd_cd_matricula_servidor' =>   'required | unique:tb_sd_servidor,sd_cd_matricula_servidor, '.$this->request->get('sd_id_servidor'). ',sd_id_servidor',
					'sd_cd_cpf_servidor' => ' sometimes | required | unique:tb_sd_servidor,sd_cd_cpf,'.$this->request->get('sd_cd_cpf_servidor'). ',sd_id_servidor'
				];
		}

		if ($this->request->get('vg_id_uorg_0') == null){
			$rules['io_cd_uorg_destino_0'] = 'required|exists:tb_vg_vaga,vg_id_uorg';
			return $rules;
		}

		$i = 0 ;

		foreach ( $this->request->all()  as $key => $val )
		{
			if ($key == "_token"){continue;}
			$rules['io_cd_uorg_destino_'.$i++ ] = 'required|exists:tb_vg_vaga,vg_id_uorg';

		}
		return $rules;

	}
    public function messages()
    {
        return
            [
                'sd_cd_matricula_servidor.required' => 'É necessária a inclusão de matrícula do servidor',
                'sd_no_nome.required' => 'É necessário incluir nome do servidor',
                'ic_vl_dias_ini_serv.required' =>'A data da entrada em exercício do servidor, no Órgão, é obrigatória',
                'ic_vl_dias_ini_serv.numeric' =>'O Form da View não está calculando os dias adequadamente da entrada em exercício',
				'ic_vl_dias_desd_ini_serv.greater_than_field' => 'A data de entrada em exercício deve ser anterior à data da última lotação',
				'ic_vl_dias_desd_ult_lotacao.required' => 'A data da ultima lotação do servidor',
				'ic_vl_dias_desd_ult_lotacao.numeric' => 'O Form da View não está calculando os dias adequadamente da última lotação',
                'ic_vl_idade.required' => 'Data de nascimento  Requerida',
                'ic_vl_idade.numeric' => 'Não há conversão entre a data de nascimento e numero',
                'ic_st_justificativa_alteracao.required' => 'Necessário justificar a alteração os dados',
                'ic_st_justificativa_alteracao.min' => 'A justificativa deve ter pelo menos 20 caracteres',
                'vg_id_uorg.required' => 'Uorg de origem requerida',
                'vg_id_uorg.exists' => 'A Uorg deve estar previamente cadastrada no banco de dados',
                'sd_cd_matricula_servidor.unique' => 'Já existe inscrição com esta matrícula em nosso banco de dados',
                'sd_cd_cpf_servidor.unique' => 'Já existe inscrição com este cpf em nosso banco de dados ',
                'io_cd_uorg_destino_0.required' => 'É necessário ter ao menos um local de destino'
            ];


    }
}
