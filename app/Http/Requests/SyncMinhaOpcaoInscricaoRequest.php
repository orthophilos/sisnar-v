<?php namespace App\Http\Requests;
use App\ExecutaConcurso\ConfiguracaoConcurso as ConfiguracaoConcurso;
use App\Http\Requests\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Foundation\Http\FormRequest;
use Response;

class SyncMinhaOpcaoInscricaoRequest extends Request {

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{

		if (
			Auth::check() &&
			ConfiguracaoConcurso::checkPeriodoDeInscricao()
		)
		{
			return true;
		}else{
			return false;
		}
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
    public function rules()
    {
        if ($this->request->get('vg_id_uorg_0') == null){
            $rules['vg_id_uorg_0'] = 'required|exists:tb_vg_vaga,vg_id_uorg';
            return $rules;
        }

        $i = 0 ;

        foreach ( $this->request->all()  as $key => $val )
        {
            if ($key == "_token"){continue;}
            $rules['vg_id_uorg_'.$i++ ] = 'required|exists:tb_vg_vaga,vg_id_uorg';

        }


        return $rules;
    }
    public function messages()
    {
        return
            [
                'vg_id_uorg_0.required' => 'É necessário escolher ao menos uma opção'
            ];


    }

}
