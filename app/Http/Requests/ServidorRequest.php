<?php namespace App\Http\Requests;

use App\Http\Requests\Request;

class ServidorRequest extends Request {

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return false;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
            'sd_cd_servidor' => 'required|numeric',
            'sd_cd_cpf' => 'required|digits:11',
            'sd_cd_matricula_servidor' => 'required | digits:7',
            'sd_no_nome' => 'required | min:10'
        ];
	}

}
