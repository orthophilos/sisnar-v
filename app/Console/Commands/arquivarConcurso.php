<?php namespace App\Console\Commands;

use App\Funcoes\Funcoes;
use App\Models\Concurso\Tb_co_concurso;
use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

use App\ExecutaConcurso\ExecutaConcursoRepository as ExecutaConcurso;

class arquivarConcurso extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'Sisnar:arquivarConcurso';

	/**t
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description =
        'Zera o banco e guarda os dados do concurso de registro mais recent definitivamente em uma tabela.
     Execute apenas após publicação do resultado do concurso quando não preicsar mais manipular os dados';

	//protected $ExecutaConcurso;
    //protected $Concurso;
	public function __construct(ExecutaConcurso $ExecutaConcurso)
	{
		parent::__construct();
        $this->executaConcurso = $ExecutaConcurso;
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{
		$this->info('Arquivando definitivamente concurso e apagando os dados de inscricao');

	   //if ($this->confirm('Após essa ação será impossível manipular os dados do ultimo concurso, certeza que quer continuar? [y|N]'))
//		{
			//$co_id_concurso = Tb_co_concurso::select('co_id_concurso')->orderBy('co_id_concurso','DESC')->first();
			//dd('entrou no arquivarConcurso console');
			//echo "\r\n". ' Arquivando definitivamente concurso e apagando os dados de inscricao'. "\r\n";
			$this->executaConcurso->arquivarConcurso($this->argument('co_id_concurso')); //$co_id_concurso);
           // echo "\r\n". 'Dados do ultimo concurso arquivados definitivamente' . "\r\n";

  //      }
	}

	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	protected function getArguments()
	{
		return [
			['co_id_concurso', InputArgument::REQUIRED, 'Id do Concurso a ser arquivado	'],
		];
	}

	/**
	 * Get the console command options.
	 *
	 * @return array

	protected function getOptions()
	{
		return [
			['example', null, InputOption::VALUE_OPTIONAL, 'An example option.', null],
		];
	}
*/

}
