<?php namespace App\Console\Commands;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

use App\ExecutaConcurso\ExecutaConcursoRepository as ExecutaConcurso;

class rebootExecutarConcurso extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'Sisnar:rebootExecutarConcurso';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Faz com que o banco de inscricoes e opcoes volte a configuração original';
 //   protected $ExecutaConcurso;
	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct(ExecutaConcurso $ExecutaConcurso)
	{
		parent::__construct();
        $this->executaConcurso = $ExecutaConcurso;
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{
		// reboot na classificacao de concurso, traz ao estado anterior da classificacao
        //$this->info('Trazendo as inscricoes e opcoes a estágio anterior ao período de classificação');
        echo 'Voltando as condiçoes anteriores do processo de classificacao do SISNAR'."\r\n";
		$this->executaConcurso->rebootClassificacao();
		echo 'Inscricoes voltadas ao proceso anterior ao SISNAR'."\r\n";

		//$this->info('Reboot nas classificações foi executado');

    }


}
