<?php namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\ExecutaConcurso\ExecutaConcursoRepository as executaConcursoRepository;
//use Symfony\Component\Console\Input\InputOption;
//use Symfony\Component\Console\Input\InputArgument;

class executarConcurso extends Command {


	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'Sisnar:executarConcurso';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Executa  Permutas e Calculo de Vagas Remanescentes. Deve ser executado antes de gerar Resultado Final do SiSNAR';


    /**
     * @param InscricaoOpcao $InscricaoOpcao
     * @param Vagas $Vagas
     */

    protected $executaConcursoRepository;
    public function __construct(ExecutaConcursoRepository $executaConcursoRepository  )
	{

		parent::__construct();
        $this->executaConcurso = $executaConcursoRepository;



	}

    /**
     * Execute the console command.
     *
     * @return mixed
     */



    public function fire()
	{
        $this->executaConcurso->rebootClassificacao();
		$this->executaConcurso->classificarCandidatos();
	}


	/**
	 * Get the console command arguments.
	 *
	 * @return array

	protected function getArguments()
	{
		return [
			['example', InputArgument::REQUIRED, 'An example argument.'],
		];
	}

	/**
	 * Get the console command options.
	 *
	 * @return array

	protected function getOptions()
	{
		return [
			['example', null, InputOption::VALUE_OPTIONAL, 'An example option.', null],
		];
	} */

}
