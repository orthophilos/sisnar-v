<?php namespace App\Console\Commands;

use Illuminate\Console\Command;
//use Illuminate\Foundation\Inspiring;
//use Symfony\Component\Console\Input\InputOption;
//use Symfony\Component\Console\Input\InputArgument;
use App\ExecutaConcurso\ExecutaConcursoRepository as ExecutaConcurso;
use App\Models\Vaga\VagaRepository as Vagas;

class balancearConcurso extends Command {

	//protected $ExecutaConcurso;
    //protected $Vaga;
	public function __construct(  ExecutaConcurso $ExecutaConcurso, Vagas $Vaga )
	{

		parent::__construct();
		$this->executaConcurso = $ExecutaConcurso;
        $this->vaga = $Vaga;


	}

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'Sisnar:balancearConcurso';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Apaga o ultimo colocado de cada uorg para contrabalancear que havia uma vaga ganha para cada UORG antes de rodar executarconcurso';

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function handle()
	{
        $teste = $this->vaga->getTotalVagasRemanescentes();
        $isRemanejamento = ($teste[0]->TotalVagasRemanescentes == 0 ? true : false);
        $this->executaConcurso->balancearVagas($isRemanejamento); //verdadeiro apenas para concursos exclusivamente de remanejamento, sem novos servidores

	}

}
