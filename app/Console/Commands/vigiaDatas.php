<?php namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Artisan;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

// from model
use App\Models\Concurso\ConcursoRepository as Concurso;
// use App\ExecutaConcurso\ExecutaConcursoRepository as ExecutaConcurso;


class vigiaDatas extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'Sisnar:vigiaDatas';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Verifica no banco se hoje deverá ser executada alguma rotina.';



//	protected  $Concurso ;
//  protected $ExecutaConcurso;
	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct(Concurso $Concurso) //, ExecutaConcurso $ExecutaConcurso)
	{
		parent::__construct();
		$this->concurso = $Concurso;
	//	$this->executaConcurso = $ExecutaConcurso;
	}

	private function _matchDate ($date) {

			$curr_date=strtotime(date("Y-m-d H:i:s"));
			$the_date=strtotime($date);
			$diff=floor(($curr_date-$the_date)/(60*60*24));
			if ($diff == 0) {
				return true;
			}else{
				return false;
			}
	}
	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */

	public function fire()
	{
        $concursoAtual = $this->concurso->getProximoConcurso(true);

        $this->info('Verificando se hoje há alguma rotina a executar');
        $this->line('Horario no sistema : ' . date("Y-m-d H:i:s"));
        $this->line('Datas do concurso : ' );
       // dd($concursoAtual);
       // $c
        print_r($concursoAtual->toArray());
        $this->line('##################################################');
        sleep(2);
		// verifique se hoje é o dia da execução do concurso e executa
		if ($this->_matchDate($concursoAtual->co_dt_data_execucao))
		{
            $this->info('Foi verificado que hoje '.$concursoAtual->co_dt_data_execucao .' é dia de executar o concurso' );
            sleep(3);
            $this->line('executando a rotina');
            $this->info('Executando Reboot do Concurso');
            Artisan::call('Sisnar:rebootExecutarConcurso');
            $this->info('Executando Algoritmo de Classificação do Concurso');
            Artisan::call('Sisnar:executarConcurso');
            $this->info('Executando balanceamento do Concurso');
            Artisan::call('Sisnar:balancearConcurso');
		}else{
            $this->info('Foi verificado que hoje não é dia de executar nenhuma rotina do concurso');

        }
        $this->line('##################################################');
        $this->info('Fim do comando');
	}

	/**
	 * Get the console command arguments.
	 *
	 * @return array

	protected function getArguments()
	{
		return [
			['example', InputArgument::REQUIRED, 'An example argument.'],
		];
	}

	/**
	 * Get the console command options.
	 *
	 * @return array

	protected function getOptions()
	{
		return [
			['example', null, InputOption::VALUE_OPTIONAL, 'An example option.', null],
		];
	}
*/
}
