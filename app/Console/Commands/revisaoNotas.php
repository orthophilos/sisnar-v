<?php namespace App\Console\Commands;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use App\Models\Concurso\ConcursoRepository as Concurso;
use App\Models\Inscricao\InscricaoRepository as Inscricao;

class revisaoNotas extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'Sisnar:revisaoNotas';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Reve a nota de todos os candidatos fazendo novas consultas ao WebService e Historico de Remocoes';

//	protected $Concurso ;
//	protected $Inscricao ;
	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct( Concurso $Concurso, Inscricao $Inscricao)
	{
		parent::__construct();
		$this->concurso = $Concurso;
		$this->inscricao = $Inscricao;
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{
		// $concurso = $this->concurso->getProximoConcurso(true);
		//dd($concurso);
		echo 'Preparando para rever notas'."\r\n";
		$this->inscricao->recalculaDadosTemporais($this->concurso->getProximoConcurso(true)->toArray());
		echo 'Notas serão revistas em alguns minutos'."\r\n";

	}

	/**
	 * Get the console command arguments.
	 *
	 * @return array

	protected function getArguments()
	{
		return [
			['example', InputArgument::REQUIRED, 'An example argument.'],
		];
	}

	/**
	 * Get the console command options.
	 *
	 * @return array

	protected function getOptions()
	{
		return [
			['example', null, InputOption::VALUE_OPTIONAL, 'An example option.', null],
		];
	}
	 */

}
