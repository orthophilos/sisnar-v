<?php namespace App\Providers;

use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider {

	/**
	 * Bootstrap any application services.
	 *
	 * @return void
	 */
	public function boot()
	{
		URL::forceSchema('https');

		Validator::extend('greater_than_field', function($attribute, $value, $parameters, $validator) {
			$min_field = $parameters[0];
			$data = $validator->getData();
			//dd('AppServiceProvider:Boot',$data, $min_field);
			$min_value = $data[$min_field];
			return $value >= $min_value;
		});

		Validator::replacer('greater_than_field', function($message, $attribute, $rule, $parameters) {
			return str_replace(':field', $parameters[0], $message);
		});
	}

	/**
	 * Register any application services.
	 *
	 * This service provider is a great spot to register your various container
	 * bindings with the application. As you can see, we are registering our
	 * "Registrar" implementation here. You can add your own bindings too!
	 *
	 * @return void
	 */
	public function register()
	{
		$this->app->bind(
			'Illuminate\Contracts\Auth\Registrar',
			'App\Services\Registrar'
		);
	}

}
