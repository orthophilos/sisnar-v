<?php namespace app\Models\Inscricao_Opcao;

use App\Funcoes\Funcoes;
use App\Models\Inscricao\Tb_ic_inscricao;
use App\Models\Inscricao_Opcao\Tb_io_inscricao_opcao as InscricaoOpcao,
    App\Models\Vaga\VagaRepository as Vaga,
    App\Models\Servidor\ServidorRepository as Servidor,
    App\Models\Concurso\ConcursoRepository as Concurso;
use App\Models\BaseRepository as BaseRepository;
use App\Models\Inscricao_Opcao\Traits\inscricaoOpcaoQueryPattern as inscricaoOpcaoQueryPattern;
use App\ExecutaConcurso\ConfiguracaoConcurso;
//use App\Models\Vaga\Tb_vg_vaga;
//use Illuminate\Support\Facades\Auth;
//use Illuminate\Support\Facades\DB;
//use PRF\RH;

class inscricaoOpcaoCRUDRepository extends BaseRepository
{

    /*
     * Model utilizado nas operacoes de CRUD ( seja do MinhaInscricao seja do Recurso )
     * */
    use inscricaoOpcaoQueryPattern;  // query padrao das consultas em inscricaoOpcao (menos as de executar concurso)

    /**
     * The Inscricao instance.
     *
     *
     *
     */
    protected  $InscricaoOpcao;
    protected  $Inscricao;
    protected  $Vaga;
    protected  $Servidor;
    protected  $Concurso;

    public function __construct( InscricaoOpcao $InscricaoOpcao   ,
                                 Vaga $Vaga ,
                                 Servidor $Servidor,
                                 Concurso $Concurso )

    {

        $this->model = $InscricaoOpcao;
        $this->vaga = $Vaga;
        $this->servidor = $Servidor;
        $this->concurso = $Concurso;

    }

    private function _checaValidadeInscricao($inputs)
    {
        // checa se o candidato já fez outra opcao para o mesmo local ou repetiu a mema ordem de preferencia
        if (isset($inputs['io_id_inscricao_opcao']))
        {
            $InscricoesOpcoesSimilares =
                $this->model
                    ->where('io_id_inscricao_opcao','!=',$inputs['io_id_inscricao_opcao'])
                    ->where('io_fk_ic_inscricao', $inputs['io_fk_ic_inscricao'])
                    ->where(function ($query) use ($inputs) {
                        $query->where('io_cd_uorg_destino', $inputs['io_cd_uorg_destino'])
                            ->orWhere('io_vl_ordem_preferencia', $inputs['io_vl_ordem_preferencia']);

                    })
                    ->get();

        }else {
            $InscricoesOpcoesSimilares =
                $this->model
                    ->where('io_fk_ic_inscricao', $inputs['io_fk_ic_inscricao'])
                    ->where(function ($query) use ($inputs) {
                        $query->where('io_cd_uorg_destino', $inputs['io_cd_uorg_destino'])
                            ->orWhere('io_vl_ordem_preferencia', $inputs['io_vl_ordem_preferencia']);
                    })
                    ->get();
        }
        if ( $InscricoesOpcoesSimilares->isEmpty() ) {
            return true;
        }else{
            return false;
        }

    }

    /**
     * save($Inscricao, $inputs)
     * Usado pelos metodos update e add
     *
     * @param $Inscricao
     * @param $inputs
     * @return mixed
     */
    public function save ($InscricaoOpcao, array $inputs)
    {

        $InscricaoOpcao->io_fk_ic_inscricao = $inputs['io_fk_ic_inscricao'];
        $InscricaoOpcao->io_cd_vg_uorg_origem = $inputs['ic_fk_vg_uorg_origem'];
        $InscricaoOpcao->io_cd_uorg_destino = $inputs['io_cd_uorg_destino'];
        $InscricaoOpcao->io_vl_ordem_preferencia = $inputs['io_vl_ordem_preferencia'];
        $InscricaoOpcao->io_vl_idade = $inputs['io_vl_idade'];
        $InscricaoOpcao->io_st_contemplado = (isset($inputs['io_st_contemplado']) ? $inputs['io_st_contemplado']: null) ;
        $InscricaoOpcao->io_st_computado = (isset($inputs['io_st_computado']) ? $inputs['io_st_computado']: 0 ) ;

        if (!empty($InscricaoOpcao->io_id_inscricao_opcao))
        {$inputs['io_id_inscricao_opcao'] = $InscricaoOpcao->io_id_inscricao_opcao;}
        if ($this->_checaValidadeInscricao($inputs)) {
            $InscricaoOpcao->save();
            $this->calculaNotaDerivada($InscricaoOpcao->io_id_inscricao_opcao);
            return $InscricaoOpcao;
        } else {
            return false;
        }

    }

    public function calculaNotaDerivada ($io_id_inscricao_opcao) // acrescimo na nota devido a opcao
    {
        // aqui que vem o segredo da organizacao do troco, como a formula do SISNAR não contempla numeros inteiros,
        // optou-se por usar os decimais para organizar a ordem de prioridade da escolha do servidor e jogar tudo em uma lista só
        // evitando interminaveis "if's"  sendo assim as de maior prioridade terão maior pontuacao e em caso de empate entre dois servidores
        // aquele que escolheu como maior prioridade para determinada uorg irá ganhar.
        // Pega o acrescimo devido a nota, devido a ordem da opcao de escolha do servidor
        // Calcula a nota derivada  PontuacaoOriginal + $redutor -  log(number:opcao,base:totaldeOpcoes + 1 ) ;

        $dadosOpcao = $this->getById($io_id_inscricao_opcao);
        Tb_ic_inscricao::find($dadosOpcao->io_fk_ic_inscricao);
        $dadosInscricao = Tb_ic_inscricao::find($dadosOpcao->io_fk_ic_inscricao);;

        // PONTUACAO ORIGINAL + REDUTOR - Log(ordemOpcao , qte de opcoes disponíveis + 1)
        $ordemPreferenciaRecalculada1 = (ConfiguracaoConcurso::REDUTOR ) - (number_format(log($dadosOpcao->io_vl_ordem_preferencia, ConfiguracaoConcurso::QTE_OPCOES + 1 ),2)); //* pow(10, -2) ;
        $ordemPreferenciaRecalculada = number_format($ordemPreferenciaRecalculada1  * pow (10,-5), 8 );
        $idadeRecalculada = ($dadosOpcao->io_vl_idade * pow(10, -2));
        $pontuacaoDerivada =    ($dadosInscricao->ic_vl_pontuacao  +
            $idadeRecalculada +
            $ordemPreferenciaRecalculada) ;
        $this->model->where('io_id_inscricao_opcao','=',$io_id_inscricao_opcao)
            ->update(['io_vl_pontuacao_derivada' => $pontuacaoDerivada]);

    }


/*
    public function update($inputs, $id)


    {

        $Inscricao = $this->getById($id);
        $this->save($Inscricao, $inputs);

        return true;
    }
*/
    /**
     * add($inputs)
     *
     * @param $inputs
     * @return bool
     */
    public function add(array $inputs)
    {
        // MODELO:
        // $inputs[
        //          'io_fk_ic_inscricao'=>
        //          'io_cd_uorg_destino' =>
        //          'io_vl_ordem_preferencia' =>;
        //

        // preenchendo com demais dados da inscricao;
        $dadosInscricao =  Tb_ic_inscricao::find($inputs['io_fk_ic_inscricao']);
        $inputs['ic_fk_vg_uorg_origem'] = $dadosInscricao->ic_fk_vg_uorg_origem;
        $inputs['io_vl_idade'] = $dadosInscricao->ic_vl_idade;

        $inscricaoOpcao = new InscricaoOpcao();

        if($this->save($inscricaoOpcao, $inputs))
        {
            return $inscricaoOpcao;
        }else{
            return false;
        }
    }



    /**
     * index($orderBy = 'ic_vl_pontuacao', $direction = 'desc', $pagination = 30)
     *
     * @param string $orderBy
     * @param string $direction
     * @param int $pagination
     * @return mixed

    public function index($orderBy = 'ic_vl_pontuacao', $direction = 'desc', $pagination = 30)
    {
                return $this->_queryPattern()
            ->orderBy($orderBy,$direction )
            ->paginate($pagination);

    }
*/
    public function transcreveResultadoFinal($concursoId)
    {
        // Grava na planilha resultado_final o resultado do sisnar.  Filtra pelos contemplados

        $inputs = [];
    //    $concursoValido = $this->concurso->getProximoConcurso();

        $contemplados = $this->_queryPattern()->get();
        $i = 0;
        foreach ( $contemplados  as $contemplado  )
        {
            $inputs[$i]['rf_fk_co'] = $concursoId;   // qual o proximo concurso a ser rodado?
            $inputs[$i]['rf_st_contemplado'] = $contemplado->io_st_contemplado;
            $inputs[$i]['rf_no_uorg_origem'] = $this->vaga->getNomeUorgById($contemplado->io_cd_vg_uorg_origem);
            $inputs[$i]['rf_no_uorg_destino'] = $this->vaga->getNomeUorgById($contemplado->io_cd_uorg_destino);
            $inputs[$i]['rf_no_nome_servidor'] = $contemplado->sd_no_nome;
            $inputs[$i]['rf_cd_matricula_servidor'] = $contemplado->sd_cd_matricula_servidor;
            $inputs[$i]['rf_vl_pontuacao'] = $contemplado->ic_vl_pontuacao;
            $inputs[$i]['rf_st_transferido_tb_concurso'] = "1";
            $i++;

        }
        return $inputs;

    }

    /**
     * @param $io_fk_ic_inscricao
     * @return mixed
     */
    public function getInscricaoOpcaoByInscricaoID($io_fk_ic_inscricao)
    {
        // pega as opcoes relacionadas a determinada inscricao
        return $this->_queryPattern()
            ->where('io_fk_ic_inscricao',$io_fk_ic_inscricao)
            ->orderBy('io_vl_ordem_preferencia')
            ->get();


    }

    /**
     * @param $io_fk_ic_inscricao
     * @return mixed
     */
    public function forceDeleteByInscricaoID($io_fk_ic_inscricao){
        //apaga definitivamente ( e nao como o softDelete) os dados da tabela;
        return  $this->model->where('io_fk_ic_inscricao',$io_fk_ic_inscricao)->forceDelete();

    }

    /**
     * @return mixed
     */
    public function getResultadoConcurso(){
        // pega o resultado definitivo do concurso, (claro, após a classificacao ).
     //  dd( $this->_queryPattern()->where('io_st_contemplado',1)->toSql());

        return $this->_queryPattern()->where('io_st_contemplado',1)->get();

    }

    /**
     * @return string
     */
    public function getAllWithTrash(){
        // pega tudo, inclusive com as inscricoes que sofreram softDelete
        return $this->_queryPattern()->withTrashed()->get()->toArray();

    }
}