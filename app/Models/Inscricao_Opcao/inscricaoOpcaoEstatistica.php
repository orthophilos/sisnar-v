<?php namespace app\Models\Inscricao_Opcao;


use App\Funcoes\Funcoes;
//use App\Models\BaseRepository;
use App\Models\Inscricao_Opcao\Tb_io_inscricao_opcao as InscricaoOpcao;
use App\Models\Inscricao\InscricaoRepository as Inscricao,
    App\Models\Vaga\VagaRepository as Vaga,
    App\Models\Servidor\ServidorRepository as Servidor,
    App\Models\Concurso\ConcursoRepository as Concurso;
use App\Models\Inscricao_Opcao\Traits\inscricaoOpcaoQueryPattern as inscricaoOpcaoQueryPattern;
//use App\Models\Vaga\Tb_vg_vaga;
//use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
//use PRF\RH;

class inscricaoOpcaoEstatistica
{

    /*
     * Model utilizado na visualização do cenário (estatisticas)
     * */
    use inscricaoOpcaoQueryPattern; // query padrao usado no inscricaoOpcaoCRUD e no inscricaoOpcaoEstatistica

    /**
     * The Inscricao instance.
     *
     *
     *
     */

    protected  $InscricaoOpcao;
    protected  $Inscricao;
 //   protected  $Vaga;
    protected  $Servidor;
    protected  $Concurso;

    public function __construct( InscricaoOpcao $InscricaoOpcao   ,
                                 Inscricao $Inscricao,
                                 Vaga $Vaga ,
                                 Servidor $Servidor,
                                 Concurso $Concurso )

    {

        $this->model = $InscricaoOpcao;
        $this->inscricao = $Inscricao;
        $this->vaga = $Vaga;
        $this->servidor = $Servidor;
        $this->concurso = $Concurso;

    }


    /**
     * @param $io_cd_uorg_destino
     * @return mixed
     */
    public function getContemplados() //$io_cd_uorg_destino
    {
        return $this->_queryPattern()->where('io_st_contemplado',true)->toArray();

    }

    /**
     * @param $io_id_inscricao_opcao
     * @return array
     */


    public function getStatisticasConcorrencia($io_id_inscricao_opcao)
    // função usada na composição do cenário e estatistica
    {
        // pega os dados dessa opcoes
        $dadosInscricaoOpcao = $this ->_queryPattern()
                                ->where('io_id_inscricao_opcao',$io_id_inscricao_opcao)
                                ->first()
                                ->toArray();

        $conditions =   [
                        'io_cd_uorg_destino' =>$dadosInscricaoOpcao['io_cd_uorg_destino'] ,
                        'io_vl_ordem_preferencia' => 1
                        ];

        // total de candidatos concorrento para esta mesma opcao considerando apenas a ordem de preferencia 1
        $totalCandidatosDaUorg = $this  ->model
                                        ->select(DB::raw('count(io_id_inscricao_opcao) as totalCandidatos'))
                                        ->where($conditions)
                                        ->first()
                                        ->toArray();

        // nota do prieiro excedente considerando apenas os que colocaram a uorg como opcao 1
        $notaUltimoColocado =
            $this->model
            ->select('io_vl_pontuacao_derivada')
            ->where($conditions)
            ->take(1)
            ->skip($dadosInscricaoOpcao['vg_vl_vagas_preestabelecidas'] + 1)
            ->orderBy('io_vl_pontuacao_derivada','desc' )
            ->get()
            ->toArray();
           // ->toSql();
        // monta as estatistica da escolha
        $estatisticaEscolha =   [
                                    'idUorg'                    => $dadosInscricaoOpcao['io_cd_uorg_destino'],
                                    'nomeUorg'                  => $dadosInscricaoOpcao['vg_no_nome'],
                                    'totalCandidatos'           => $totalCandidatosDaUorg['totalCandidatos'],
                                    'totalVagas'                => $dadosInscricaoOpcao['vg_vl_vagas_preestabelecidas'],
                                    'pontacaoUltimoCandidato'   => (
                                                                    isset($notaUltimoColocado[0]['io_vl_pontuacao_derivada']) ?
                                                                    intval($notaUltimoColocado[0]['io_vl_pontuacao_derivada']):
                                                                    0
                                                                    ),
                                    'minhaPontuacao'            => intval($dadosInscricaoOpcao['ic_vl_pontuacao'])
                                ];



        //$estatisticaEscolha = json_encode($estatisticaEscolha);
        return $estatisticaEscolha; //compact('estatisticaEscolha');
    }


    /**
     * @param $io_id_inscricao_opcao
     * @return string
     */
    public function getProbabilidadeSucesso($io_id_inscricao_opcao)
    // função usada na composição do cenário e estatistica
    // pega a probabilidade de sucesso na escolha, esse metodo demanda aprimoramento estatistico que pode ser feito
    // a posteriori quando tivermos dados para fazer o estudo
    // no momento estamos considerando que uma pessoa que escolheu a uorg como 2a opcao e que esta mais pontuada que o candidato
    // tem a metade da probabilidade de  ocupar a vaga, a pesoa que escolheu como 3a opcao tem 1/3 de probabilidade , 4, 1/4 de probabilidade
    // and so on.
    {
        $dadosOpcao = $this->_queryPattern()->where('io_id_inscricao_opcao','=', $io_id_inscricao_opcao)->first();
        $qtdVagasRemanescentes = $this->vaga->getQtdVagasRemanPorUorg($dadosOpcao->io_cd_uorg_destino);
            $concorrentesMelhorPontuados = $this->model
            ->select(DB::raw('sum(1 / io_vl_ordem_preferencia ) as provavel_colocacao') )
            ->join('tb_ic_inscricao','io_fk_ic_inscricao', '=','ic_id_inscricao')
            ->join('tb_sd_servidor','ic_fk_sd_servidor','=','sd_id_servidor')
            ->where('io_st_computado','=','0')
            ->where('io_vl_pontuacao_derivada','>',$dadosOpcao->io_vl_pontuacao_derivada)
            ->where('io_cd_uorg_destino','=', $dadosOpcao->io_cd_uorg_destino)
            ->orderBy('io_vl_pontuacao_derivada','desc' )
            ->first()
            ->toArray();

        $possibilidadeDeSucesso = 0.009 + ($concorrentesMelhorPontuados['provavel_colocacao'] /  ($qtdVagasRemanescentes['Total_Vagas_Remanescentes'] + 0.009)) ;
        // 0.009 foi usado paenas para nao dar o problema de divisao 0 / 0;

      //  dd($concorrentesMelhorPontuados);
     //   if (intval($qtdVagasRemanescentes['Total_Vagas_Remanescentes']) < 2 )
      //  {
      //      return 'baixa probabilidade';
      //  }
        switch ($possibilidadeDeSucesso)
        {
            case ($possibilidadeDeSucesso <= 1 ): // alta probabilidade
                return 'alta probabilidade';
                break;
            case ($possibilidadeDeSucesso <= 2 ): // media probabilidade
                return 'media probabilidade';
                break;
            default: // baixa probabilidade
                return 'baixa probabilidade';
        }


    }

    /**
     * @return array
     */
    public function getCenarioGeral(){

         // pega os dados de concorrencia de todas as uorgs para montar o cenario geral

        $todasUorgs = $this->vaga->getAll();
        $i =0;
        $cenarioGeral = [];
        foreach ($todasUorgs as $uorg) {
            $conditions =   [
                'io_cd_uorg_destino' =>$uorg->vg_id_uorg ,
                'io_vl_ordem_preferencia' => 1 ];

            //dados Uorg

            // total  de candidato por uorg
            $cenarioGeral[$i] =  $this  ->model
                ->select(DB::raw('count(io_id_inscricao_opcao) as totalCandidatos'))
                ->where($conditions)
                ->first()
                ->toArray();

            // ultimo colocado de cada uorg considerando as primeiras posicoes;
            $ultimoColocado =
                $this->model
                    ->select('io_vl_pontuacao_derivada')
                    ->where($conditions)
                    ->take(1)
                    ->skip($uorg->vg_vl_vagas_preestabelecidas + 1)
                    ->orderBy('io_vl_pontuacao_derivada','desc' )
                    ->get()
                    ->toArray();
            $cenarioGeral[$i]['pontuacaoUltimoColocado'] =
                    (
                        isset($ultimoColocado[0]['io_vl_pontuacao_derivada'])  ?
                        floor($ultimoColocado[0]['io_vl_pontuacao_derivada'])  :
                        0
                    );
            $cenarioGeral[$i]['nomeUorg']              = $uorg->vg_no_nome;
            $cenarioGeral[$i]['UF'] = $uorg->vg_ed_uf;
            $cenarioGeral[$i]['vagasPreEstabelecidas'] = $uorg->vg_vl_vagas_preestabelecidas;
            $i++;
        }

        return $cenarioGeral;
    }

}