<?php namespace app\Models\Inscricao_Opcao\Traits;


trait inscricaoOpcaoQueryPattern

{
    // trait com o query padrao para as consutlas utilizadas
    // no InscricaoOpcaoCrudRepository e no InscricaoOpcaoEstatistca

    public function _queryPattern()
    {
        return $this->model
            ->select(
                'sd_no_nome',
                'sd_cd_matricula_servidor',
                'sd_cd_cpf',
                'io_vl_idade',
                'io_id_inscricao_opcao',
                'io_fk_ic_inscricao',
                'io_vl_ordem_preferencia',
                'io_cd_vg_uorg_origem as Uorg_Origem_id',
                'origem.vg_no_nome as Uorg_Origem',
                'origem.vg_ed_uf as UF_Origem',
                'io_cd_uorg_destino',
                'destino.vg_no_nome',
                'destino.vg_ed_uf',
                'destino.vg_vl_vagas_preestabelecidas',
                'io_vl_pontuacao_derivada',
                'io_st_contemplado',
                'io_st_computado',
                'ic_vl_pontuacao',
                'ic_st_inscricao_confirmada',
                'ic_dt_deleted_at'

            )
            ->join('tb_ic_inscricao','io_fk_ic_inscricao', '=','ic_id_inscricao')
            ->join('tb_sd_servidor','ic_fk_sd_servidor','=','sd_id_servidor')
            ->join('tb_vg_vaga as destino','io_cd_uorg_destino','=','destino.vg_id_uorg')
            ->join('tb_vg_vaga as origem','io_cd_vg_uorg_origem','=','origem.vg_id_uorg')
            ->orderBy('io_vl_pontuacao_derivada','desc' )  ;
    }
}