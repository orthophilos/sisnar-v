<?php namespace App\Models\Inscricao_Opcao;

use App\Funcoes\Funcoes;
use App\Models\Vaga\Tb_vg_vaga;
use Doctrine\DBAL\Query\QueryBuilder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;

class Tb_io_inscricao_opcao extends Model {

	/**
	 * The database table used by the model.
	 * TYPE : myISAM
	 * @var string
	 */


	use SoftDeletes;

	protected $table = 'tb_io_inscricao_opcao'; // define o nome da tabela
	protected $primaryKey   = 'io_id_inscricao_opcao'; //
	protected $fillable= array('*'); //  The fillable property specifies which attributes should be mass-assignable
	protected $guarded = ['io_cd_uorg_destino']; // not should be mass-assignable When using guarded, you should still never pass Input::get()
	public $timestamps = true; // created_at, update_at


	/**
	 * The name of the "created at" column.
	 *
	 * @var string
	 */
	const CREATED_AT = 'io_dt_created_at';
	/**
	 * The name of the "updated at" column.
	 *
	 * @var string
	 */
	const UPDATED_AT = 'io_dt_updated_at';
	/**
	 * The name of the "deleted at" column.
	 *
	 * @var string
	 */

	const DELETED_AT = 'io_dt_deleted_at';
	/*
     *  public function Tb_mm_modelo_de_medicao()
            {
            ** a tabela é myISAM
                return $this->belongsTo('App\Models\Modelo_de_Medicao\Tb_mm_modelo_de_medicao','qm_fk_mm_modelo_de_medicao');
            }

     * */
	/**
	 * @return array
     */
	public static function getBalancodeVagasContempladas()
	{
		// pega o balanco de entradas e saídas e irregularidades - entradas maior que saidas , de servidores em cada UORG
		//retorna array cp, esses dados
		//
		//
		$balancoUorg = [];
		// pega o total de CANDIDATOS que entraram e sairam nas uorgs
		// pega todas as uorgs
		$todasUorgs = Tb_vg_vaga::all(['vg_id_uorg','vg_id_uorg','vg_no_nome','vg_ed_uf'])->toArray();
		// pega todos os candidatos que entrarao para esta uorg
		$i = 0;
        $totalIrregularidades = 0;
		foreach ($todasUorgs as $uorg) {
			$entradaUorg = Tb_io_inscricao_opcao::query()
				->select(DB::raw('sum(io_st_contemplado) as entrarao'))
				->where('io_cd_uorg_destino',$uorg['vg_id_uorg'])
				->get()
				->toArray();
			$saidaUorg  = Tb_io_inscricao_opcao::query()
				->select(DB::raw('sum(io_st_contemplado) as sairao'))
				->where('io_cd_vg_uorg_origem',$uorg['vg_id_uorg'])
				->get()
				->toArray();
            $balanco = $entradaUorg[0]['entrarao'] - $saidaUorg[0]['sairao'];
            if ($balanco > 0) {
                $totalIrregularidades += $balanco;
            }
			$balancoUorg['uorg'][$i]['vg_id_uorg'] = $uorg['vg_id_uorg'];
			$balancoUorg['uorg'][$i]['vg_no_nome'] = $uorg['vg_no_nome'];
			$balancoUorg['uorg'][$i]['vg_ed_uf'] = $uorg['vg_ed_uf'];
			$balancoUorg['uorg'][$i]['entrarao'] = $entradaUorg[0]['entrarao'];
			$balancoUorg['uorg'][$i]['sairao'] = $saidaUorg[0]['sairao'];
			$balancoUorg['uorg'][$i]['balanco'] = $balanco ;
			$i++;
		}
        $balancoUorg['totalIrregularidades'] = $totalIrregularidades;
        return $balancoUorg;

	}
}
