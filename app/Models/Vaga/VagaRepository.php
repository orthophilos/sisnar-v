<?php namespace App\Models\Vaga;
//use App\Funcoes\Funcoes;
use App\Exceptions\MyException;
use App\Models\BaseRepository;
use App\Models\Inscricao_Opcao\Tb_io_inscricao_opcao;
use App\Models\Vaga\Tb_vg_vaga as Vaga;
//use Illuminate\Database\Eloquent\Model;
//use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;
use App\ExecutaConcurso\ConfiguracaoConcurso as ConfiguracaoConcurso;

//use PRF\RH;

/*
 * Vaga comporta o código das uorgs bem como a quantidade de vagas inicial e o calculo de vagas remanescentes devido ao
 * sistema de permuta
 *
 */
/**
 * Class VagaRepository
 * @package App\Models\Vaga\VagaRepository
 */
class VagaRepository extends BaseRepository
{



    protected $Vaga;
   // public $balancoVagasGeradas = 1; // a ser populado pelo metodo contabilidade e usado pelo comando executarConcurso


    public function __construct(Vaga $Vaga )

    {

        $this->model = $Vaga;

    }

    /**
     * _queryPattern()
     * Query padrão das pesquisas
     * **********************************
     *
     * @return mixed
     */
    private function _queryPattern()
    {


        return $this->model
            ->select(          'vg_no_nome',
                'vg_ed_uf',
                'vg_vl_vagas_preestabelecidas',
                DB::raw('vg_vl_vagas_preestabelecidas+vg_vl_vagas_ganhas-vg_vl_vagas_perdidas AS Total_Vagas_Remanescentes' )

            )
            ;

    }

    /**
     * @return mixed
     */
    public function queryDeficit()
    {
        $query = $this->model
                    ->select(
                        'vg_no_nome',
                        'vg_ed_uf',
                        'vg_vl_vagas_preestabelecidas',
                        'vg_vl_vagas_ganhas',
                        'vg_vl_vagas_perdidas',
                        DB::raw('( `vg_vl_vagas_preestabelecidas` + `vg_vl_vagas_ganhas`-`vg_vl_vagas_perdidas`) AS deficit'))
                    //->where(DB::raw('(`vg_vl_vagas_preestabelecidas` + `vg_vl_vagas_ganhas`-`vg_vl_vagas_perdidas`)'),'>',0)
                    ->get();
                    //->toSql();  //`vg_vl_vagas_preestabelecidas` +
       // dd($query);
        return $query;
    }


    /**
     * save($Vaga, $inputs)
     * Usado pelos metodos update e add
     *
     * @param $Vaga
     * @param $inputs
     * @return mixed
     */
    public function save($Vaga, array $inputs)
    {
        //  dd($Vaga, $inputs);
        $Vaga->vg_no_nome = $inputs['vg_no_nome']; // nome da uorg
        $Vaga->vg_ed_uf = $inputs['qm_fk_mm_modelo_de_medicao'];
        $Vaga->vg_vl_vagas_preestabelecidas = $inputs['vg_vl_vagas_preestabelecidas'];

        $Vaga->save();

        //  $res = \Event::fire(new TeiaDadosManequimTocada($Vaga));

        return $Vaga;
    }
/*
    public function incrementVagasGeradas($quantidade = 1 ){
        $this->balancoVagasGeradas = $this->balancoVagasGeradas + $quantidade;

    }

    public function decrementVagasGeradas ($quantidade = 1)
    {
        $this->balancoVagasGeradas = $this->balancoVagasGeradas - $quantidade;

    }
*/
    /**
     * update($inputs, $id)
     *
     * @param $inputs
     * @param $id
     * @return bool

    public function update($inputs, $id)
    {
        $Vaga = $this->getById($id);
        $this->save($Vaga, $inputs);
        return true;
    }
     */

    /**
     * add($inputs)
     *
     * @param $inputs
     * @return bool

    public function add($inputs)
    {
        $Vaga = new Vaga();
        $this->save($Vaga, $inputs);
        return true;
    }
     */

    /**
     * getById($vg_id_uorg)
     * Diferente um pouco do methodo da classe BaseRepository
     *
     * @param int $vg_id_uorg
     * @return mixed

    public function getById($vg_id_uorg){

        return $this->_queryPattern()
            ->where('vg_id_uorg','=',vg_id_uorg)
            ->orderBy('vg_no_nome','asc')
            ->get();


    }
*/
    /**
     * getByNomeUorg($vg_no_nome)
     *
     * @param $vg_no_nome
     * @return mixed
     */

    public function  getByNomeUorg($vg_no_nome)
    {
        return $this->_queryPattern()
            ->where('vg_no_nome','=',$vg_no_nome)
            ->get();
    }

    /**
     * @param $vg_id_uorg
     * @return mixed
     */
    public function getNomeUorgById($vg_id_uorg)
    {
       return $this->model->select('vg_no_nome')->where('vg_id_uorg','=',$vg_id_uorg)->first()->vg_no_nome;
        }
    /**
     * index($orderBy = 'vg_no_nome', $direction = 'asc', $pagination = 30)
     *
     * @param string $orderBy
     * @param string $direction
     * @param int $pagination
     * @return mixed

    public function index($orderBy = 'vg_no_nome', $direction = 'asc', $pagination = 30)
    {
        return $this->_queryPattern()
                    ->orderBy($orderBy,$direction )
                    ->paginate($pagination);

    }
*/


    // funcoes utilizadas para o calculo das vagas remanescentes

    /**
     * @param $inscricaoOpcao
     * @param bool|false $reverse
     */
    public function contabilidade ( $inscricaoOpcao, $reverse = false)
    {
       // $uorgsEnvolvidas->io_cd_vg_uorg_origem e ->io_cd_uorg_destino
       // echo 'uorgs envolvidas IO_ID_INSCRICAO_OPCAO'.$io_id_inscricao_opcao ."\r\n" ;
        if (!($reverse) ){

            if (ConfiguracaoConcurso::PERMUTA)
            {
                // ADICIONANDO uma vaga no uorg ORIGEM
                // DB::table('users')->whereId(Auth::user()->id)->increment('position');
                $this->model->where('vg_id_uorg', '=', $inscricaoOpcao->io_cd_vg_uorg_origem)->increment('vg_vl_vagas_ganhas');
            }

            // RETIRANDO uma vaga do uorg DESTINO
            //Customer::find($customer_id)->decrement('loyalty_points', 50);
            $this->model->where('vg_id_uorg','=',$inscricaoOpcao->io_cd_uorg_destino)->increment('vg_vl_vagas_perdidas');
            //$this->balancoVagasGeradas--;


        } else {
            if (ConfiguracaoConcurso::PERMUTA)
            {

                // RETIRANDO  uma vaga no uorg ORIGEM
                $this->model->where('vg_id_uorg', '=', $inscricaoOpcao->io_cd_vg_uorg_origem)->decrement('vg_vl_vagas_ganhas');
               // $this->balancoVagasGeradas++;

            }
            // ADICIONANDO uma vaga do uorg DESTINO
            $this->model->where('vg_id_uorg','=',$inscricaoOpcao->io_cd_uorg_destino)->decrement('vg_vl_vagas_perdidas');
           // $this->balancoVagasGeradas--;
        }
    }

    /**
     * @param $vg_id_uorg
     * @return mixed
     */
    public function getQtdVagasRemanPorUorg($vg_id_uorg)
    {
     //DB::raw('vg_vl_vagas_preestabelecidas+vg_vl_vagas_ganhas-vg_vl_vagas_perdidas AS Total_Vagas_Remanescentes' )
        return    $this->model->select(
            DB::raw('vg_vl_vagas_preestabelecidas + vg_vl_vagas_ganhas - vg_vl_vagas_perdidas AS Total_Vagas_Remanescentes' ))
            ->Where('vg_id_uorg','=',$vg_id_uorg)
            ->first()
            ->toArray();
    }

    public function getAll_UORGS_VagasPreestabelecidas(){
        return  $this->model->get(['vg_id_uorg','vg_no_nome','vg_ed_uf','vg_vl_vagas_preestabelecidas']);
    }
    /**
     * @return mixed
     */
    public function getTotalVagasRemanescentes()
    {
        return $this->model->select(DB::raw('sum(vg_vl_vagas_preestabelecidas) as TotalVagasRemanescentes'))->get();
    }

    /**
     * @param $uorgId
     * @return mixed
     * @throws MyException
     */
    public function get_vg_id_uorg_from_Webservice_Id ($sp_cd_id_webservice, $dataEmployee = null)
    {
        $sp_fk_vg_vaga = Tb_sp_siape::where('sp_cd_id_webservice',$sp_cd_id_webservice )->get(['sp_fk_vg_vaga'])->toArray();
      //  dd($sp_cd_id_webservice[0]['sp_fk_vg_vaga']);
        if (!isset($sp_fk_vg_vaga[0]['sp_fk_vg_vaga']))
        {
        dd($sp_fk_vg_vaga, $sp_cd_id_webservice, $dataEmployee);
        //    throw new  MyException($uorgId. '<uorg | webservice> '. implode("|",   $sp_cd_id_webservice). ' Não consta a atual UORG do servidor no banco  de agregação de grupos de UORGs do sistema');
        }
        return $sp_fk_vg_vaga;
    }


    /**
     * @return mixed
     */
    public function getNotComputed ()
    {
        return  Tb_io_inscricao_opcao::query()
            ->where('io_st_computado','!=','1')
            ->where('io_st_contemplado','!=','1')
            ->orderBy('io_vl_pontuacao_derivada','DESC')
            ->orderBy('io_cd_uorg_destino','DESC')
            ->get(['io_id_inscricao_opcao']);
    }

    /**
     * @param $ic_fk_vg_uorg_origem
     * @return mixed
     */
    public function getListUorgs($ic_fk_vg_uorg_origem = null){
        // pega a lista de todas as uorgs $ic_fk_vg_uorg_origem será excluída desta lista
        // gerlamente essa ic_fk_vg_uorg_origem é a lotacao atual do servidor.

        if (is_null($ic_fk_vg_uorg_origem)){
            // sem filtro
            $output = $this->model
                ->orderBy('vg_ed_uf', 'ASC')
                ->orderBy('vg_no_nome', 'ASC')
                ->get(['vg_id_uorg', 'vg_no_nome', 'vg_ed_uf'])
                //->toArray();
                ->toJson();
        }else {
            // com o filtro de excluir a uorg de Origem do servidor
            $output = $this->model
                ->where('tb_vg_vaga.vg_id_uorg', '!=', $ic_fk_vg_uorg_origem)
                ->orderBy('vg_ed_uf', 'ASC')
                ->orderBy('vg_no_nome', 'ASC')
                ->get(['vg_id_uorg', 'vg_no_nome', 'vg_ed_uf'])
                //->toArray();
                ->toJson();

        }
        return $output;
    }


    /**
     * @param bool|true $include_vagas_preestabelecidas
     */

    public function zeraVagas($include_vagas_preestabelecidas = true){
        // zera o quadro de vagas, usado quando se vai fazer um novo concuros, apaga as vagas preestabelecidas
        // usado em novo concuros e arquivar concurso, só pode ser usado após a execucao do ultimo concurso

        $camposAZerar = [
            'vg_vl_vagas_ganhas' => 1,
            'vg_vl_vagas_perdidas' => 0
        ];

       if ($include_vagas_preestabelecidas){
          $camposAZerar['vg_vl_vagas_preestabelecidas'] = 0;
       }
       // dd('zeraVagas', $camposAZerar);
        $this->model->update($camposAZerar);

    }

    /**
     * @param $vg_vl_vagas_preestabelecidas
     * @param $vg_id_uorg
     * @return mixed
     */
    public function updateVagasPreestabelecidas($vg_vl_vagas_preestabelecidas, $vg_id_uorg)
    {
       return  $this->model->where('vg_id_uorg',$vg_id_uorg)->update(['vg_vl_vagas_preestabelecidas' => $vg_vl_vagas_preestabelecidas]);

    }

}
