<?php namespace App\Models\Vaga;

use Illuminate\Database\Eloquent\Model;

class Tb_sp_siape extends Model {
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'tb_sp_siape';
    protected $primaryKey   = 'sp_id_siape';
    protected $fillable = [];
    protected $guarded = ['*'];

    public $timestamps = false;

    /**
     * The name of the "created at" column.
     *
     * @var string
     */
//    const CREATED_AT = 'vg_dt_created_at';
    /**
     * The name of the "updated at" column.
     *
     * @var string
     */
 //   const UPDATED_AT = 'vg_dt_updated_at';
    /**
     * The name of the "deleted at" column.
     *
     * @var string
     */

  //  const DELETED_AT = 'vg_dt_deleted_at';

}
