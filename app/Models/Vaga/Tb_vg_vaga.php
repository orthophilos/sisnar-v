<?php namespace App\Models\Vaga;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Tb_vg_vaga extends Model {
    /**
     * The database table used by the model.
     *
     * @var string
     */
    use SoftDeletes;
    protected $table = 'tb_vg_vaga';
    protected $primaryKey   = 'vg_id_uorg';
    protected $fillable = ['vg_vl_vagas_ganhas','vg_vl_vagas_perdidas'];
    protected $guarded = ['vg_dt_created_at','vg_dt_updated_at','vg_dt_deleted_at' ];

    public $timestamps = true;

    /**
     * The name of the "created at" column.
     *
     * @var string
     */
    const CREATED_AT = 'vg_dt_created_at';
    /**
     * The name of the "updated at" column.
     *
     * @var string
     */
    const UPDATED_AT = 'vg_dt_updated_at';
    /**
     * The name of the "deleted at" column.
     *
     * @var string
     */

    const DELETED_AT = 'vg_dt_deleted_at';

}
