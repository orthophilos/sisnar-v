<?php namespace App\Models;
use Illuminate\Support\Facades\Session;

/**
 * Created by PhpStorm.
 * User: epe
 * Date: 09/04/15
 * Time: 16:54
 */

abstract class BaseRepository
{
    /**
     * The Model instance.
     *
     * @var Illuminate\Database\Eloquent\Model
     */
    protected $model;


    /**
     * @param $Object
     * @param array $inputs
     * @return mixed
     */
    abstract function save($Object, array $inputs);

    /**
     * @param array $inputs
     * @param $id
     * @return mixed
     */
    public function update(array $inputs, $id)
    {
       $oldModel = $this->getById($id);
        $success =  $this->save($oldModel, $inputs);
        if ($success){
            Session::flash('success_message','O registro foi ALTERADO com sucesso');
        }
        return $success;
    }

    /**
     * @param array $inputs
     * @return mixed
     */
    public function add(array $inputs){

        $newModel = new $this->model;
        try {
            $this->save($newModel, $inputs);
        } catch (\Exception $e) {

            return redirect()->back()->withErrors('Não foi possível salvar o registro com estes dados');
        }
       // dd($inputs,$newModel);

        $success = $this->save($newModel, $inputs);
        if ($success) {
            Session::flash('success_message', 'O registro foi INCLUÍDO com sucesso');
        }
        return $success;

    }

    /**
     * @param $orderBy
     * @param string $direction
     * @param int $pagination
     * @return mixed
     */
    public function index($orderBy, $direction = 'asc',$pagination = 30)
    {
         return $this->_queryPattern()
        ->orderBy($orderBy,$direction )
        ->paginate($pagination);
    }

    /**
     * Destroy a model.
     *
     * @param int $id
     * @return void
     */
    public function destroy($id){
            try {
                $this->model->findOrFail($id)->delete();
                Session::flash('success_message','O registro foi APAGADO com sucesso');
            } catch (\Exception $e) {
                return redirect()->back()->withErrors('A deleçao do registro não foi possível');
            }

    }

    /**
     * Get Model by id.
     *
     * @param int $id
     * @return App\Models\Model
     */
    public function getById($id)
    {

        return $this->model->find($id);

    }

    /**
     * Get Model by cpf.
     *
     * @param int $cpf
     * @return App\Models\Model
     */

        public function getByCPF($cpf)
    {
        $attributes = $this->model->getConnection()->getSchemaBuilder()->getColumnListing($this->model->getTable());
        $CPFField = array_values(preg_grep('/CPF/i', $attributes));
        return $this->model->where($CPFField[0], '=', $cpf)->first(); //first()
    }


    /**
     * displayField()
     * Return que main field on a table
     *
     * @return mixed

    public function displayField()
    {
        $pattern['table'] = $this->model->getTable();
        $pattern['attributes'] = $this->model->getConnection()->getSchemaBuilder()->getColumnListing($this->model->getTable());
        return $pattern['attributes'][1];
    }

*/


   /*
    * getall()
    *
    */
    /**
     * @return mixed
     */
    public function getAll(){
       return  $this->model->all();
    }

    public function truncate(){
        return $this->model->truncate();
    }


}

