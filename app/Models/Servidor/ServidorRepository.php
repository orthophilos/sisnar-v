<?php namespace App\Models\Servidor;
use App\Exceptions\MyException;
use App\ExecutaConcurso\ConfiguracaoConcurso;
use App\Models\BaseRepository;
use App\Models\Servidor\Tb_sd_servidor as Servidor,
    App\Models\Concurso\ConcursoRepository as Concurso,
    App\Models\HistoricoRemocao\HistoricoRemocaoRepository as HistoricoRemocao,
    App\Models\Vaga\VagaRepository as Vaga;
use Illuminate\Support\Facades\View;
use PRF\RH;
use App\Funcoes\Funcoes;
use TijsVerkoyen\CssToInlineStyles\Exception;

/*
 * Servdor  é uma tabela relativa ao cadastramento do Policial no processo do sisnar. Apenas dados pessoais imutaveis.
 *
 */
/**
 * Class InscricaoServidorRepository
 * @package App\Models\Inscricao
 */
class ServidorRepository extends BaseRepository
{


    /**
     * The Servidor instance.
     *
     *
     *
     */
    protected $Servidor;
    protected $RH;
    protected $funcao;
    protected $concurso;
 //   protected $historicoRemocao;

    public function __construct(
                                Vaga $vaga,
                                Servidor $Servidor,
                                RH $RH,
                                Funcoes $funcao,
                                Concurso $concurso,
                                HistoricoRemocao $historicoRemocao

                            )

    {

        $this->model = $Servidor;
        $this->RH = $RH;
        $this->funcao = $funcao;
        $this->concurso = $concurso;
        $this->vaga = $vaga;
        $this->historicoRemocao = $historicoRemocao;
    }



    /**
     * saveServidor($Inscricao, $inputs)
     * Usado pelos metodos update e add
     *
     * @param $Inscricao
     * @param $inputs
     * @return mixed
     */
    public function save ($Servidor, array $inputs)
    {

        //  dd($Servidor, $inputs);
        $Servidor->sd_cd_cpf = $inputs['sd_cd_cpf'];
        $Servidor->sd_cd_matricula_servidor = $inputs['sd_cd_matricula_servidor'];
        $Servidor->sd_no_nome = $inputs['sd_no_nome'];
        $Servidor->sd_cd_servidor = $inputs['sd_cd_servidor'];

        $Servidor->save();


        return $Servidor;
    }

    /**
     * _queryPattern()
     * Query padrão das pesquisas
     * **********************************
     *
     * @return mixed
     */
    private function _queryPattern()
    {
        return $this->model
            ->select(
                'sd_no_nome',
                'sd_cd_matricula_servidor',
                'sd_cd_cpf',
                'sd_cd_servidor'

            );

    }



    /**
     * update($inputs, $id)
     *
     * @param $inputs
     * @param $id
     * @return bool


    public function update($inputs, $id)
    {
        $Inscricao = $this->getById($id);
        $this->save($Inscricao, $inputs);
        return true;
    }


    /**
     * add($inputs)
     *
     * @param $inputs
     * @return bool

    public function add($inputs)
    {

        $Inscricao = new Inscricao();
        $this->save($Inscricao, $inputs);
        return true;
    }
*/
    /**
     * index($orderBy = 'ic_vl_pontuacao', $direction = 'desc', $pagination = 30)
     *
     * @param string $orderBy
     * @param string $direction
     * @param int $pagination
     * @return mixed

    public function index($orderBy = 'ic_vl_pontuacao', $direction = 'desc', $pagination = 30)
    {
        return $this->_queryPattern()
            ->orderBy($orderBy,$direction )
            ->paginate($pagination);

    }

     */
    public function getServidorByMatricula($matricula)
    {
        try{
         //   dd('dados do servidor: ', $this->model->where('sd_cd_matricula_servidor',$matricula)->first());
            return $this->model->where('sd_cd_matricula_servidor',$matricula)->first();
        }
        catch (MyException $e ){
            $errorMessage = 'deu pau!';
            View::share('errorMessage',
                'Servidor não identificado em nosso cadastro');
            abort(500);

        }
    }
    public function getServidorDadosFromWebService($cpf)
    {
    // pega dados do webService RH e faz filtro conforme se pode ver abaixo
        try {
            $dataEmployee = $this->RH->getByCPF($cpf); // $employeeRH->getByCPF($ServidorCPF)[0]
        }
        catch(MyException $e){

            View::share('errorMessage',
                'Web Service fora do ar ');
            abort(404);
        }
        try {
       // echo "dataemployee:";

         end($dataEmployee); // isso aqui é pq tem servidor com dois , três registros no RH, pega o ultimo
         $key= key($dataEmployee);

            // echo "dataemployee:";
           // dd($dataEmployee);

        if (
            ConfiguracaoConcurso::checkCondicoesGeraisDoCandidato($dataEmployee,$key)
            )

            {
               try {

                   $ic_fk_vg_uorg_origem = $this->vaga->get_vg_id_uorg_from_Webservice_Id($dataEmployee[$key]['unidadeOrganizacional']['id'], $dataEmployee);
               }catch (MyException $e)
               {
                   View::share('errorMessage',
                       'Problemas em relação à vinculação entre a lotação do servidor e o agrupamento de UORGs '.
                       'constante no banco de dados da aplicação ');
                   abort(500);
               }

                $foo = $dataEmployee[$key]['dataLotacao'];
                $dataValidaByHistoricoRemocao = $this   ->historicoRemocao
                    ->getUltimaDataLotacaoValida(
                        $dataEmployee[$key]['unidadeOrganizacional']['id'],
                        intval($dataEmployee[$key]['matricula'])
                    );
                /*
                dd(
                    'ServidorRepository->getServidorDadosFromWebservice',
                    $dataValidaByHistoricoRemocao,
                    $dataEmployee[$key]['unidadeOrganizacional']['id'],
                    intval($dataEmployee[$key]['matricula'])
                );
                   */
                if (!is_null($dataValidaByHistoricoRemocao)    && !($dataValidaByHistoricoRemocao) == 0  )
                {
                    $dataEmployee[$key]['dataLotacao'] = $dataValidaByHistoricoRemocao;
                   // dd($dataValidaByHistoricoRemocao, $dataEmployee[$key]['dataLotacao']);
                }
                //dd($dataEmployee);
                // pega somente o dia do unixtime miliseconds = (24 * 3600 * 1000)
               $resultado =  [
                    'sd_no_nome' => $dataEmployee[$key]['nomeCompleto'],
                    'ativo' => $dataEmployee[$key]['situacao'],
                    'sd_cd_servidor' => $dataEmployee[$key]['id'],
                    'sd_cd_cpf' => $dataEmployee[$key]['cpf'],
                    'sd_cd_matricula_servidor' => $dataEmployee[$key]['matricula'],
                    'cargo' => $dataEmployee[$key]['classePadrao']['cargo']['descricao'],
                    'dataExercicio' => strtotime(date('d-m-Y',$dataEmployee[$key]['dataExercicio']/1000)),
                    'dataLotacao' => strtotime(date('d-m-Y',$dataEmployee[$key]['dataLotacao']/1000)),
                    'dataNascimento' => strtotime(date('d-m-Y',$dataEmployee[$key]['dataNascimento']/1000)) ,
                    'uf' => $dataEmployee[$key]['unidadeOrganizacional']['cidade']['uf']['sigla'],
                    'cidade' => $dataEmployee[$key]['unidadeOrganizacional']['cidade']['nome'],
                    'delegacia' => $dataEmployee[$key]['unidadeOrganizacional']['nome'],
                    'ic_fk_vg_uorg_origem' => $ic_fk_vg_uorg_origem[0]['sp_fk_vg_vaga'],
                    'dataLotacaodoWebService' =>strtotime(date('d-m-Y',$foo/1000))
                ];
                return $resultado;
            } else  {
           // dd($dataEmployee);
                throw new \Exception( json_encode($dataEmployee, JSON_PRETTY_PRINT));
            }

       }
        catch (\Exception $e) {
            View::share('errorMessage',$e->getMessage());
            abort(500);
        }
    }

    /**
     * @param array $inputs
     * @return mixed
     */
    public function calculaPontuacao(array $inputs){
        // Calcula pontuacao servidor ( não é a pontuacao derivada , é a pontuacao da inscricao , não das opcoes)

       //
       // fórmula do SISNAR
       //
       //dd($inputs['ic_vl_dias_desd_ini_serv'] );

       // dd($inputs['ic_vl_dias_desd_ini_serv'], $inputs['ic_vl_dias_desd_ult_lotacao']  );

       return $inputs['ic_vl_dias_desd_ini_serv'] + ( 2 * $inputs['ic_vl_dias_desd_ult_lotacao'] );


    }


}
