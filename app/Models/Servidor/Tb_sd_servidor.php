<?php namespace App\Models\Servidor;

use Illuminate\Database\Eloquent\Model;

class Tb_sd_servidor extends Model {

	// use SoftDeletes;

	protected $table = 'tb_sd_servidor'; // define o nome da tabela
	protected $primaryKey   = 'sd_id_servidor'; //
//	protected $fillable= array('*'); //  The fillable property specifies which attributes should be mass-assignable
    protected $guarded = ['sd_cd_cpf','sd_cd_matricula_servidor','sd_no_nome']; // not should be mass-assignable When using guarded, you should still never pass Input::get()
	public $timestamps = false; // created_at, update_at

	/**
	 * The name of the "created at" column.
	 *
	 * @var string

	const CREATED_AT = 'rf_dt_created_at';

	 * The name of the "updated at" column.
	 *
	 * @var string

	const UPDATED_AT = 'rf_dt_updated_at';
	/**
	 * The name of the "deleted at" column.
	 *
	 * @var string


	const DELETED_AT = 'rf_dt_deleted_at';
	/*
     *  public function Tb_mm_modelo_de_medicao()
            {
            ** a tabela foi desnormalizada
                return $this->belongsTo('App\Models\Modelo_de_Medicao\Tb_mm_modelo_de_medicao','qm_fk_mm_modelo_de_medicao');
            }

     * */


}
