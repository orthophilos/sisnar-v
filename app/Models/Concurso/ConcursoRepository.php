<?php namespace App\Models\Concurso;

use App\Funcoes\Funcoes;
use App\Models\BaseRepository;
use App\Models\Concurso\Tb_co_concurso as Concurso;
use App\Models\Inscricao_Opcao\Tb_io_inscricao_opcao;
use Faker\Provider\DateTime;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\View;


/**
 * Concurso guarda apenas o número de inscricao do concurso (id) com os dados do período do certame
 * no campo co_vl_dados_vagas pretende-se colocar array com o número das vagas existentes
 *
 */
/**
 * Class MedicaoServidorRepository
 * @package App\Models\Medicao_Servidor
 */
class ConcursoRepository extends BaseRepository
{

    /**
     * The Concurso instance.
     *
     * @var App\Models\Concurso\ConcursoRepository
     *
     */

    protected $Concurso;


    public function __construct(Concurso $Concurso)

    {
        $this->model = $Concurso;

    }
    /**
     * _queryPattern()
     * Query padrão das pesquisas
     * **********************************
     *
     * @return mixed
     */
    private function _queryPattern()
    {
        //dd( date('Y-m-d H:i:s'));
        $hoje = new \DateTime();
        return $this->model
            ->select(   'co_id_concurso',
                'co_dt_publicacao_edital',
                'co_dt_inicio_inscricoes',
                'co_dt_fim_inscricoes',
                'co_dt_data_execucao',
                'co_dt_remocao'
            )
            ->orderBy('co_dt_remocao','asc')
            ->where('co_dt_remocao','>', $hoje->format('Y-m-d H:i:s'));
   //co_dt_data_execucao
    }
    /**
     * Create or update a Concurso.
     *
     * @param App\Models\Concurso\Tb_co_concurso
     * @param array $inputs
     * @return App\Models\Concurso\Tb_co_concurso
     */

    public function save($Concurso, array $inputs)
    {


        //@@todo verificar se ha concurso vigente antes se houver voltar erro com mensagem para apagar concurso vigentes
        /*
        if(!is_null($this->getProximoConcurso(true))){
            throw exception ($e->message('há concurso vigente, apague-o primeiro'));
            catch (Exception $e) {
            return false;

        }

         *
         * */
        $Concurso->co_dt_publicacao_edital = \DateTime::createFromFormat(Funcoes::$dateFormat, $inputs['co_dt_publicacao_edital'])
            ->format('Y-m-d H:i:s');
        $Concurso->co_dt_inicio_inscricoes = \DateTime::createFromFormat(Funcoes::$dateFormat, $inputs['co_dt_inicio_inscricoes'])
            ->format('Y-m-d H:i:s');
        $Concurso->co_dt_fim_inscricoes =  \DateTime::createFromFormat(Funcoes::$dateFormat, $inputs['co_dt_fim_inscricoes'])
            ->format('Y-m-d H:i:s');
        $Concurso->co_dt_data_execucao =  \DateTime::createFromFormat(Funcoes::$dateFormat, $inputs['co_dt_data_execucao'])
            ->format('Y-m-d H:i:s');
        $Concurso->co_dt_remocao  =  \DateTime::createFromFormat(Funcoes::$dateFormat, $inputs['co_dt_remocao'])
            ->format('Y-m-d H:i:s');
        // se origem vem do arquivamento do concurso entao
        if (isset($inputs['dadosVagas'])) {
            $Concurso->co_vl_dados_vagas = $inputs['dadosVagas'];
        }
        try {
            $Concurso->save();
            return true;
        }catch (Exception $e) {
            return false;
        }
        //return $Concurso;
    }

    public function checkConcursoExecutado(){
        $teste =  Tb_io_inscricao_opcao::select(DB::raw('count(*) as computados'))
                            ->Where('io_st_computado',1)
                            ->first();

        if ($teste->computados > 0){    return true;}else{ return false;}
    }
    /**
     * update($inputs, $id)
     *
     * @param $inputs
     * @param $id
     * @return bool
     */
    public function update(array $inputs, $id)
    {
        $Concurso = $this->getById($id);
        if (

            strtotime($Concurso->co_dt_publicacao_edital) !=
                       strtotime(date('d-m-Y',Funcoes::MyDateFormatToMyUnixTime($inputs['co_dt_publicacao_edital']))) ||

            strtotime($Concurso->co_dt_inicio_inscricoes) !=
                       strtotime(date('d-m-Y',Funcoes::MyDateFormatToMyUnixTime($inputs['co_dt_inicio_inscricoes']))) ||

            strtotime($Concurso->co_dt_fim_inscricoes) !=
                       strtotime(date('d-m-Y',Funcoes::MyDateFormatToMyUnixTime($inputs['co_dt_fim_inscricoes']))) ||

            strtotime($Concurso->co_dt_data_execucao) !=
                       strtotime(date('d-m-Y',Funcoes::MyDateFormatToMyUnixTime($inputs['co_dt_data_execucao']))) ||

            strtotime($Concurso->co_dt_remocao) !=
                       strtotime(date('d-m-Y',Funcoes::MyDateFormatToMyUnixTime($inputs['co_dt_remocao'])))

            )
        {
            /*
            dd('entrou aqui update ConcursoRepository, enrou no if',
                strtotime($Concurso->co_dt_publicacao_edital) !=
                strtotime(date('d-m-Y',Funcoes::MyDateFormatToMyUnixTime($inputs['co_dt_publicacao_edital']))) ,

                strtotime($Concurso->co_dt_inicio_inscricoes) !=
                strtotime(date('d-m-Y',Funcoes::MyDateFormatToMyUnixTime($inputs['co_dt_inicio_inscricoes']))) ,

                strtotime($Concurso->co_dt_fim_inscricoes) !=
                strtotime(date('d-m-Y',Funcoes::MyDateFormatToMyUnixTime($inputs['co_dt_fim_inscricoes']))) ,

                strtotime($Concurso->co_dt_data_execucao) !=
                strtotime(date('d-m-Y',Funcoes::MyDateFormatToMyUnixTime($inputs['co_dt_data_execucao']))) ,

                strtotime($Concurso->co_dt_remocao) !=
                strtotime(date('d-m-Y',Funcoes::MyDateFormatToMyUnixTime($inputs['co_dt_remocao'])))

                );
            */
            if ($this->save($Concurso, $inputs)){
                Session::flash('success_message', 'O registro foi INCLUÍDO com sucesso');
                return true;
            }else{
                Session::flash('errors', ['Não foi possível executar a gravação']);

                return false;
            }
        }
        /*
        dd('nao entrou no ifupdate ConcursoRepository', $inputs, strtotime($Concurso->co_dt_publicacao_edital) !=
            strtotime(date('d-m-Y',Funcoes::MyDateFormatToMyUnixTime($inputs['co_dt_publicacao_edital']))) ,

            strtotime($Concurso->co_dt_inicio_inscricoes) !=
            strtotime(date('d-m-Y',Funcoes::MyDateFormatToMyUnixTime($inputs['co_dt_inicio_inscricoes']))) ,

            strtotime($Concurso->co_dt_fim_inscricoes) !=
            strtotime(date('d-m-Y',Funcoes::MyDateFormatToMyUnixTime($inputs['co_dt_fim_inscricoes']))) ,

            strtotime($Concurso->co_dt_data_execucao) !=
            strtotime(date('d-m-Y',Funcoes::MyDateFormatToMyUnixTime($inputs['co_dt_data_execucao']))) ,

            strtotime($Concurso->co_dt_remocao) !=
            strtotime(date('d-m-Y',Funcoes::MyDateFormatToMyUnixTime($inputs['co_dt_remocao']))));
        */
        return false;
    }

    /**
     * add($inputs)
     *
     * @param $inputs
     * @return bool

    public function add($inputs)
    {
        $Concurso = new $this->model;
        $this->save($Concurso, $inputs);
        return true;
    }
     * */



    /**
     *
     * pega próximo concurso a ser executado
     * @return mixed
     */
    public function getProximoConcurso($returnFeedBack = false)
    {
        // pega proxima data de concurso aberto.
        $proximoConcurso = $this->_queryPattern()->first(); //->toSql(); //->first();
        //dd($proximoConcurso);
        if (is_null($proximoConcurso) && $returnFeedBack == false ){
            //Redirect::to('semConcurso');
            View::share('errorMessage','Não há concurso vigente');
            abort(500);
        };

        return $proximoConcurso;
    }


    /**
     * @param $dataServidor
     * @return float
     */
    public function diasEntreDataServidorEConcurso( $dataServidor)
    {
        // informa os dias entre  $dataServidor ( que pode ser entrada em exercicio, nascimento, lotacao ) e a data
        // de publicacao do edital
        //

        // não pode ser via session porque pode ser usado pelo queue command

        $output = Funcoes::daysbetween(
            intval($dataServidor),
            strtotime( $this->getProximoConcurso(true)->co_dt_publicacao_edital)
        );
        //dd('concurso->diasEntreDataServidorEConcurso',$output,$dataServidor, strtotime($this->getProximoConcurso(true)->co_dt_publicacao_edital));
        return $output;

    }


    /**
     * @param $dataServidor
     * @return float
     */
    public function diasEntreDataServidorERemocao( $dataServidor)
        // no caso da contagem do interticio da ultima lotacao para remocao a CGRH pediu para contar
        // ate a data da provavel remocao e nao a data de publicacao do edital de remocao
    {
        $output = Funcoes::daysbetween(
            intval($dataServidor),
            strtotime( $this->getProximoConcurso(true)->co_dt_remocao)
        );
        return $output;

    }
    /**
     * @param $co_id_concurso
     * @param $array
     * @return mixed
     */
    public function saveDadosVagas ($co_id_concurso, $array){
       // salva dos dados das vagas
        if (!is_null($array) && !is_null($array)){
            $co_vl_dados_vagas = print_r($array['vagas'],true);
            $teste = $this->model->where('co_id_concurso',$co_id_concurso)->update(['co_vl_dados_vagas' => $co_vl_dados_vagas]);
            //dd('entrou em saveDadosVagas', $teste, $co_id_concurso, $co_vl_dados_vagas);
            return $teste;
        }

    }


}
