<?php namespace App\Models\Concurso;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

// use Illuminate\Database\Eloquent\SoftDeletes;

class Tb_co_concurso extends Model {
    /**
     * The database table used by the model.
     *
     * @var string
     */
    use SoftDeletes ;
    protected $table = 'tb_co_concurso';
    protected $primaryKey   = 'co_id_concurso';
 /*   protected $fillable = [
/*
        'qm_fk_mt_manequim_tamanho',
        'qm_fk_mm_modelo_de_medicao',
        'qm_no_nome_da_medida',
        'qm_st_utilizar_no_calculo',
        'qm_vl_medida'

    ]; */
  protected $guarded = ['co_dt_inicio_inscricoes','co_dt_fim_inscricoes','co_dt_data_execucao','co_vl_dados_vagas' ];

    public $timestamps = false;

    /**
     * The name of the "created at" column.
     *
     * @var string
     */
    const CREATED_AT = 'co_dt_created_at';
    /**
     * The name of the "updated at" column.
     *
     * @var string
     */
     const UPDATED_AT = 'co_dt_updated_at';
    /**
     * The name of the "deleted at" column.
     *
     * @var string
     */

    const DELETED_AT = 'co_dt_deleted_at';


}