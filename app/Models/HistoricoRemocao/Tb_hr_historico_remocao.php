<?php namespace App\Models\HistoricoRemocao;

use Illuminate\Database\Eloquent\Model;


class Tb_hr_historico_remocao extends Model {
/**
	 * The database table used by the model.
	 *
	 * @var string
	 */

	protected $table = 'tb_hr_historico_remocao'; // define o nome da tabela

	protected $primaryKey   = 'hr_id_historico_remocao'; //
	protected $fillable = ['hr_cd_matricula_servidor','hr_cd_uorg','hr_dt_data_lotacao'];
	//protected $guarded = []; // not should be mass-assignable When using guarded, you should still never pass Input::get()
	public $timestamps = true; // created_at, update_at

	/**
	 * The name of the "created at" column.
	 *
	 * @var string
	 */
	const CREATED_AT = 'hr_dt_created_at';
	/**
	 * The name of the "updated at" column.
	 *
	 * @var string
	 */
	const UPDATED_AT = 'hr_dt_updated_at';
	/**
	 * The name of the "deleted at" column.
	 *
	 * @var string
	 */

}
