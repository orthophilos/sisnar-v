<?php namespace App\Models\HistoricoRemocao;
use App\Models\HistoricoRemocao\Tb_hr_historico_remocao as TbHrHistoricoRemocao;
use App\Models\BaseRepository;
use App\Models\Vaga\VagaRepository as Vaga;
use App\Funcoes\Funcoes;

/*
 * HistoricoRemocao é uma tabela relativa ao historico de remoções do servidor útil para calcular qual a ultima data de lotação válida.
 * de acordo com o agrupamento de uorgs (Tb_vg_vaga).
Isso ocorre porque remoções dentro do mesmo grupo de uorgs (Tb_vg_vaga) não devem prejudicar a pontuação do SISNAR, a exemplo , um servidor lotado na 
sede do DPRF , ou na superintendência, que foi movimentado entre uma seção/divisão dentro da mesma superintendência não poderia  ter sua data de lotação atualizada. Ou, em outro exemplo, um servidor que saiu no npf de uma delegacia para outro local de trabalho , dentro da mesma delegacia, deve ter sua data de lotacao contada  como o primeiro local de trabalho dentro daquela delegacia, desta forma, agrupa-se as diversas uorgs comuns : npf/1a del 2a super | UOP 1 /1a del 2a super, etc... 
no mesmo grupamento : 1a del 2a super.


 */
/**
 * Class HistoricoRemocaRepository
 * @package App\Models\HistoricoRemocao
 */
class HistoricoRemocaoRepository extends BaseRepository
{

    /**
     * The HistoricoRemocao instance.
     *
     *
     *
     */
  //  protected $HistoricoRemocao;
  //  protected $Vaga;

    function __construct( 
                         TbHrHistoricoRemocao $HistoricoRemocao,
                         Vaga $Vaga
                        )

    {

        $this->model = $HistoricoRemocao;
        $this->vaga = $Vaga;

    }

    public function save($Object, array $inputs){
        // construct save function
    }
    /**
     *
     * _queryPattern()
     * Query padrão das pesquisas
     * **********************************
     *
     * @return mixed
     */
    private function _queryPattern()
    {
        return $this->model
                    ->select(
                                'hr_cd_matricula_servidor',
                                'hr_cd_uorg',
                                'hr_dt_data_lotacao',
                                'sp_no_nome as hr_no_uorg_nome',
                                'sp_cd_id_webservice',
                                'sp_fk_vg_vaga'
                            )
                    ->join('tb_sp_siape','hr_cd_uorg','=','sp_cd_uorg')
                    ->orderBy('hr_dt_data_lotacao','DESC');
    }


    /**
     * @param $id
     */

    public function getUltimaDataLotacaoValida($sp_cd_id_webservice,  $hr_cd_matricula_servidor)
    {
        // pega o agrupamento ( Tb_vg_vaga) referente a lotacao atual
        $ultimaDataLotacaoValida = '';
        $historicoLotacoesServidor = $this->_queryPattern()
            ->where('hr_cd_matricula_servidor', '=', $hr_cd_matricula_servidor)
            ->get()
            ->toArray();
        // busca as ultimas lotacoes que obedecerem ao criterio
        $vg_id_uorg = $this->vaga->get_vg_id_uorg_from_Webservice_Id($sp_cd_id_webservice);
        //dd('historicoremocao->getUltimaDataLotacaoValida',$historicoLotacoesServidor, $vg_id_uorg, $sp_cd_id_webservice);


        $isExOfficio=false; // variavel para ser colocaca se o servidor foi removido exofficio daquela localidade.
        foreach ($historicoLotacoesServidor as $historicoLotacaoServidor) {

            if ($historicoLotacaoServidor['sp_fk_vg_vaga'] == $vg_id_uorg[0]['sp_fk_vg_vaga']  || $isExOfficio ) {
                $ultimaDataLotacaoValida = $historicoLotacaoServidor['hr_dt_data_lotacao'];

            }else{

                return strtotime($ultimaDataLotacaoValida)*1000;
            }
        }


        return strtotime($ultimaDataLotacaoValida) * 1000;


    }
}
