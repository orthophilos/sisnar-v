<?php namespace App\Models\Inscricao;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Tb_ic_inscricao extends Model {
	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */


	use SoftDeletes;

	protected $table = 'tb_ic_inscricao'; // define o nome da tabela
	protected $primaryKey   = 'ic_id_inscricao'; //
	protected $fillable = [ //  The fillable property specifies which attributes should be mass-assignable

	];
	protected $guarded = []; // not should be mass-assignable When using guarded, you should still never pass Input::get()
	public $timestamps = true; // created_at, update_at

	/**
	 * The name of the "created at" column.
	 *
	 * @var string
	 */
	const CREATED_AT = 'ic_dt_created_at';
	/**
	 * The name of the "updated at" column.
	 *
	 * @var string
	 */
	const UPDATED_AT = 'ic_dt_updated_at';
	/**
	 * The name of the "deleted at" column.
	 *
	 * @var string
	 */

	const DELETED_AT = 'ic_dt_deleted_at';

  public function servidor()
        {
            return $this->belongsTo('App\Models\Servidor\Tb_sd_servidor','ic_fk_sd_servidor');
        }
  public function vaga()
	{
		return $this->belongsTo('App\Models\Vaga\Tb_vg_vaga','ic_fk_vg_uorg_origem');
	}

}
