<?php namespace App\Models\Inscricao;
use App\Exceptions\MyException;
use App\ExecutaConcurso\ConfiguracaoConcurso;
use App\Funcoes\Funcoes;
use App\Models\BaseRepository;
use App\Models\Inscricao\Tb_ic_inscricao as Inscricao;
use App\Models\Vaga\Tb_vg_vaga;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use App\Models\Vaga\VagaRepository as Vaga,
    App\Models\Inscricao_Opcao\inscricaoOpcaoCRUDRepository as InscricaoOpcao,
    App\Models\Concurso\ConcursoRepository as Concurso,
    App\Models\Servidor\ServidorRepository as Servidor;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\View;
use PRF\RH;
//use TijsVerkoyen\CssToInlineStyles\Exception;

/*
 * Inscricao é uma tabela relativa ao cadastramento do Policial no processo do sisnar. Faz o cálculo de pontuacao.
 *
 */
/**
 * Class InscricaoServidorRepository
 * @package App\Models\Inscricao
 */
class InscricaoRepository extends BaseRepository
{

    /**
     * The Inscricao instance.
     *
     *
     *
     */
    protected $Inscricao;
    protected $Vaga;
 //   protected $Servidor;
//    protected $RH;

    protected $Concurso;
//    protected $InscricaoOpcao;

    function __construct(
                         Inscricao $Inscricao,
                         Vaga $Vaga ,
                         Servidor $Servidor ,
                         InscricaoOpcao $InscricaoOpcao ,
                         Concurso $Concurso,
                         RH $RH
                        )

    {

        $this->model = $Inscricao;
        $this->vaga = $Vaga;
        $this->servidor = $Servidor;
        $this->concurso = $Concurso;
        $this->inscricaoOpcao = $InscricaoOpcao;
        $this->RH = $RH;

    }

    /**
     *
     * _queryPattern()
     * Query padrão das pesquisas
     * **********************************
     *
     * @return mixed
     */

    private function _queryPattern()
    {
        return $this->model
                ->select(
                            'sd_id_servidor',
                            'sd_no_nome',
                            'sd_cd_matricula_servidor',
                            'vg_id_uorg',
                            'vg_no_nome',
                            'ic_id_inscricao',
                            'ic_vl_pontuacao',
                            'ic_vl_dias_desd_ini_serv',
                            'ic_st_inscricao_confirmada',
                            'ic_vl_dias_desd_ult_lotacao',
                            'ic_vl_idade',
                            'ic_cd_cpf_recurso',
                            'ic_vl_alteracoes',
                            DB::raw('(ic_vl_alteracoes IS NOT NULL) as alteradoManualmente')


                )
                ->join('tb_vg_vaga','ic_fk_vg_uorg_origem','=','vg_id_uorg')
                ->join('tb_sd_servidor','ic_fk_sd_servidor','=','sd_id_servidor')
                    ;
    }

    /**
     * @param $id
     */
    private function _recalculaNotasDerivadas($id)

    {
        //recalcula todas notas derivadas do candidato em InscricaoOpcao
        $opcoesdaInscricao = $this->inscricaoOpcao->getInscricaoOpcaoByInscricaoID($id);
        foreach ($opcoesdaInscricao as $opcaoInscricao )
        {
            $this->inscricaoOpcao->calculaNotaDerivada($opcaoInscricao->io_id_inscricao_opcao);
        }
    }

    /**
     * @param array $inputs
     * @return array
     * @throws Exception
     */

    private function _trataInput(array $inputs, $noStop = false) // as datas tem que estar sempre em formato Unix em miliseconds
    {
        //dd($inputs);
        if (!Funcoes::isValidUnixTime($inputs['dataExercicio'])){
            $inputs['dataExercicio'] = Funcoes::MyDateFormatToMyUnixTime($inputs['dataExercicio']);
        }
        if (!Funcoes::isValidUnixTime($inputs['dataLotacao'])){
            $inputs['dataLotacao'] = Funcoes::MyDateFormatToMyUnixTime($inputs['dataLotacao']);
        }
        if (!Funcoes::isValidUnixTime($inputs['dataNascimento'])){
            $inputs['dataNascimento'] = Funcoes::MyDateFormatToMyUnixTime($inputs['dataNascimento']);
        }
     //   dd(Funcoes::isValidUnixTime($inputs ), $inputs);
        $dataEditalConcurso = strtotime( $this->concurso->getProximoConcurso()->co_dt_publicacao_edital);
        try {
            if (
                $dataEditalConcurso < $inputs['dataExercicio'] ||
                $dataEditalConcurso < $inputs['dataLotacao'] ||
                $dataEditalConcurso < $inputs['dataNascimento']
            ) {
                throw new \Exception(
                    'Alguma das datas relacionadas ao servidor é posterior a data de publicação edital' .
                    'Data Edital Concurso   : ' . $dataEditalConcurso .
                    'Data Entrada Exercicio : ' . $inputs['dataExercicio'] .
                    'Data Ultima Lotação    : ' . $inputs['dataLotacao']

                );
            }
        }
        catch (\Exception $e) {
            if (!$noStop) {
                View::share('errorMessage',
                    implode('|', $inputs) . ' Problemas em relação à algum dado do WebService do Siape');
                abort(500);
            }
        }
        $outputs =
            [

            'ic_fk_sd_servidor' => $inputs['ic_fk_sd_servidor'],
            'ic_fk_vg_uorg_origem' => $inputs['ic_fk_vg_uorg_origem'],
            'ic_vl_dias_desd_ini_serv' => $this->concurso->diasEntreDataServidorEConcurso($inputs['dataExercicio']),
            'ic_vl_dias_desd_ult_lotacao' => $this->concurso->diasEntreDataServidorEConcurso($inputs['dataLotacao']),
            'ic_vl_idade' => round(($this->concurso->diasEntreDataServidorEConcurso($inputs['dataNascimento']) / 365), 3)
//            'ic_vl_idade' => ($this->concurso->diasEntreDataServidorEConcurso($inputs['dataNascimento']))
            ];
    //    dd($outputs, $inputs['dataNascimento'] );
        return $outputs;
    }


    private function _preparaDadosFromWebService( $cpf = null, $noStop = false )
    {
        // prepara dados oriundos do webservice para gravar na tabela
        if (is_null($cpf)){
            $cpf = Auth::user()->cpf;
        }
        $servidorCadastrado = $this->servidor->getByCPF($cpf);
        $dadosWebServiceServidor = $this->servidor->getServidorDadosFromWebService($cpf);
        if (empty($dadosWebServiceServidor)) {
            return false;
        }
        $dadosWebServiceServidor['ic_fk_sd_servidor'] = $servidorCadastrado->sd_id_servidor;
        try {
            $registro = $this->_trataInput($dadosWebServiceServidor, $noStop);

            } catch (Exception $e){
            View::shaxre('errorMessage',
                'Problemas em relação à alguma das  datas do PRF (nascimento, posse,lotação) que não '.
                'constam anteriores  à data de publicação do edital');
            abort(500);
        }
             return $registro;
    }

    /**
     * save($Inscricao, $inputs)
     * Usado pelos metodos update e add
     *
     * @param $Inscricao
     * @param $inputs
     * @return mixed
     */
    public function save ($Inscricao, array $inputs) //datas dos inputs tem que estar sempre em unix date format com miliseconds
    {
        // tem o filtro aqui porque em MinhaInscricao nao vem a inscricao mediante form de usuario, nao passa pelo request
        if ($inputs['ic_vl_dias_desd_ult_lotacao'] >$inputs['ic_vl_dias_desd_ini_serv'] ||
            $inputs['ic_vl_dias_desd_ult_lotacao'] > ($inputs['ic_vl_idade'] * 365 ) ){

            View::share('errorMessage',
                'Data de Lotação maior que tempo de serviço ou idade'.
                ' lotacao : '.$inputs['ic_vl_dias_desd_ult_lotacao'].
                ' servico : '.$inputs['ic_vl_dias_desd_ini_serv'].
                ' idade : '.$inputs['ic_vl_idade']  );
            abort(500);

        }

        $Inscricao->ic_fk_sd_servidor = $inputs['ic_fk_sd_servidor'];
        $Inscricao->ic_fk_vg_uorg_origem = $inputs['ic_fk_vg_uorg_origem'];
        $Inscricao->ic_vl_dias_desd_ini_serv = $inputs['ic_vl_dias_desd_ini_serv'];
        $Inscricao->ic_vl_dias_desd_ult_lotacao = $inputs['ic_vl_dias_desd_ult_lotacao'];
        $Inscricao->ic_vl_idade = $inputs['ic_vl_idade'];

        // campo calculado
        $Inscricao->ic_vl_pontuacao = $this->servidor->calculaPontuacao($inputs);

        $Inscricao->save();

        return $Inscricao;

    }


    public function insertJustificativa($ic_id_inscricao, $justificativa, $type = 'delete')
    {
        $Inscricao = $this->model->find($ic_id_inscricao);
        $Inscricao->ic_vl_alteracoes = $Inscricao->ic_vl_alteracoes . ' #### '.$type  .' por : '.Auth::user()->cpf.' >>>> '. $justificativa;
        $Inscricao->save();
    }
    /**
     * update($inputs, $id)
     *
     * @param $inputs
     * @param $id
     * @return bool
     */

    public function update(array $inputs, $id)
    {
        $registro = $this->_trataInput($inputs);
        $Inscricao = $this->model->find($id);
        $Inscricao->ic_vl_alteracoes = $Inscricao->ic_vl_alteracoes . ' #### Alterado por : '.Auth::user()->cpf.' >>>> '. $inputs['ic_vl_alteracoes'];
        $Inscricao->ic_cd_cpf_recurso =  Auth::user()->cpf;
        $this->save($Inscricao, $registro);
        $this->_recalculaNotasDerivadas($id); // ////// recalcula as notas derivadas nas opcoesinscricoe
        return $this->getById($id);
    }

    /**
     * @param int $id
     * @return mixed
     */
    public function getById($id)
    {
        return $this->_queryPattern()->where('ic_id_inscricao', $id)->first();
    }

    /**
     * @param $sd_id_servidor
     * @return mixed
     */
    public function getInscricaoFromServidorID($sd_id_servidor)
    {
        return $this->_queryPattern()->where('ic_fk_sd_servidor',$sd_id_servidor)->first();
    }

    /**
     * add($inputs)
     *
     * @param $inputs
     * @return bool
     */
    public function add(array $inputs = null)
    {
        // $inputs deve ser tipo array e é usado apenas quando a inserção da inscrição no sistema é feita de forma manual
        // por conta de recursos caso tenha havido algum tipo de impecilho da inserção tradicional que utiliza dados
        // da webservice.
        $Inscricao = new Inscricao(); // novo objeto;

        // verifica origem da chamada do metodo, se o usuario gestor está cadastrando manualmente as inscricoes


        if (is_null($inputs)) {
            // cadastro pelo candidato
            // não é o gestor que esta cadastrando entao pega dados do webservice
            $registro  = $this->_preparaDadosFromWebService();
        }else{
            // cadastro pelo gestor da CGRH
            // e o gestor cadastrando pegue o que ele mandar

            try {

                $registro = $this->_trataInput($inputs);

                $Inscricao->ic_cd_cpf_recurso =  Auth::user()->cpf;
                $Inscricao->ic_vl_alteracoes = ' #### Incluído por : '.Auth::user()->cpf.' >>>> '. $inputs['ic_vl_alteracoes'];

            } catch (Exception $e){
                View::share('errorMessage',
                    'Problemas em relação à alguma das  datas do PRF (nascimento, posse,lotação) que não '.
                    'Ou sem justificativa de alteracao'.
                    'constam anteriores  à data de publicação do edital');
                abort(500);
            }

        }



        //grava novo registro
        $InscricaoSalva = $this->save($Inscricao, $registro);

        return $InscricaoSalva;
    }

    /**
     * @return array
     */
    public function inscricoesNaoConfirmadas()
    {
        // pega inscricoes não confirmadas
        return  Funcoes::array_multi_getValuesByKey
            (
            $this   ->model
                    ->select('ic_id_inscricao')
                    ->where('ic_st_inscricao_confirmada','=',0)
                    ->get()
                    ->toArray()
            , 'ic_id_inscricao'
            );

    }

    /**
     * @param $ic_id_inscricao
     * @param bool|false $reverse
     * @return bool
     */
    public function confirmaInscricao($ic_id_inscricao, $reverse = false)
    {
        // confirma inscricao do servidor , $reverse = true, volta a estagio antes da confirmacao
        if ($reverse){
            if($this->model->where('ic_id_inscricao',$ic_id_inscricao)->update(['ic_st_inscricao_confirmada' => '0']))
            {
                return true;
            };
        }else{
            if($this->model->where('ic_id_inscricao',$ic_id_inscricao)->update(['ic_st_inscricao_confirmada' => '1']))
            {
                return true;
            };
        }
        return false;
    }

    /**
     * @return mixed
     */
    public function getSumario()
    {
        //total de inscritos no sisnar
        //$sumario['totalInscricoes'] = $this->model->where('ic_st_inscricao_confirmada','1')->count();
        return $this->model->where('ic_st_inscricao_confirmada','1')->count();

    }

    /**
     * @param $orderBy
     * @param $direction
     * @return mixed
     */
    public function indexRecurso($orderBy , $direction){
        // lista de inscricoes confirmadas para serem usadas quando for , devido a recurso, alterar algum dado do servidor
        return
        $this ->_queryPattern()
            ->Where('ic_st_inscricao_confirmada',1)
            ->orderBy('alteradoManualmente','desc')
            ->orderBy($orderBy, $direction)
            ->paginate(20);

    }

    /**
     * @param $fieldname
     * @param $value
     * @param $orderBy
     * @param $direction
     * @return mixed
     */

    public function search($fieldname, $value, $orderBy , $direction){
        $value = str_replace(" ", "%", $value);
        return $this->_queryPattern()   ->where('ic_st_inscricao_confirmada',1)
                                        ->where($fieldname,'LIKE', '%'.$value.'%')
                                        ->orderBy($orderBy, $direction)
                                        ->paginate(20)
                                        ->appends(['fieldname' => $fieldname, 'value' => $value]);
    }

    /**
     * @param array $proximoConcurso
     * @throws \Exception
     */
    public function recalculaDadosTemporais(array $proximoConcurso)
    {
        // recalcula os dados de ic_vl_dias_desd_ini_serv ,  ic_vl_dias_desd_ult_lotacao, notas e nota derivada
        // isso deve ocorrer quando o administrador do sistema altera as datas do concurso sendo que já existe inscrições
        // realizadas.

        // loop de todas as inscricoes
        $inputss = $this->getAll()->toArray();

        foreach ($inputss as $inputs )
        {
            $cpf = $this->servidor->getById($inputs['ic_fk_sd_servidor'])->sd_cd_cpf;

            print_r('Atualizando dados de inscricao de servidor com CPF'. $cpf) ;
            echo "\r\n";
            // verifica se o candidato é intempestivo e caso sim, exclui a inscricao
            try {
                $preDadosWebService  = $this->RH->getByCPF($cpf); // $employeeRH->getByCPF($ServidorCPF)[0]

                end($preDadosWebService); // isso aqui é pq tem servidor com dois , três registros no RH, pega o ultimo
                $key = key($preDadosWebService);
                // Checa condições mínimas gerais do candidato perante o novo cenário ,
                // se não tiver condições apaga a inscrição com a devida justificativa
                if (
                    !ConfiguracaoConcurso::checkCondicoesGeraisDoCandidato($preDadosWebService, $key)
                )
                {
                    $this->apagaInscricao($inputs['ic_id_inscricao'],'Inscrição deletada pelo sistema devido a alterações nas datas do concurso');
                    continue;
                }


                $dadosWebService = [
                        'dataExercicio' => strtotime(date('d-m-Y',$preDadosWebService[$key]['dataExercicio']/1000)),
                        'dataLotacao' => strtotime(date('d-m-Y',$preDadosWebService[$key]['dataLotacao']/1000)),
                        'dataNascimento' => strtotime(date('d-m-Y',$preDadosWebService[$key]['dataNascimento']/1000))
                ];
            }
            catch(MyException $e){
                  echo 'não foi possivel prosseguir';
                    View::share('errorMessage',
                      'Web Service fora do ar ');
                  abort(404);
            }
            // checa condições de temporalidade do candidato, se não tiver condições de temporalidade,
            // exclui com a devida justificativa
//            dd(ConfiguracaoConcurso::checkCondicoesTemporaisDoCandidato($dadosWebService));
            if (!ConfiguracaoConcurso::checkCondicoesTemporaisDoCandidato($dadosWebService))
            {
               // dd('entrou em if ',ConfiguracaoConcurso::checkCondicoesTemporaisDoCandidato($dadosWebService));
                $this->apagaInscricao($inputs['ic_id_inscricao'],
                    'Inscrição deletada pelo sistema devido a alterações nas datas do concurso');
                continue;
            }

            $registro = $this->_preparaDadosFromWebService($cpf, true);
            $ic_vl_pontuacao  = $this->servidor->calculaPontuacao($inputs);

           // $this->model->unguard();
            $update =
            $this   ->model
                    ->where('ic_id_inscricao',$inputs['ic_id_inscricao'])
                    ->update([
                            'ic_vl_dias_desd_ini_serv' =>       $registro['ic_vl_dias_desd_ini_serv'],
                            'ic_vl_dias_desd_ult_lotacao' =>    $registro['ic_vl_dias_desd_ult_lotacao'],
                            'ic_vl_idade'                  =>   $registro['ic_vl_idade'],
                            'ic_vl_pontuacao'               =>  $ic_vl_pontuacao,
                            'ic_vl_alteracoes'               => $inputs['ic_vl_alteracoes'].
                                            ' ### nota alterada pelo sistema devido a mudança das datas do concurso ###'

                        ]);
            $this->_recalculaNotasDerivadas($inputs['ic_id_inscricao']);
          //  $this->model->guard();

            Session::flash('success_message', 'Os dados de Inscrição dos Candidatos foram atualizados com sucesso');

        }
    }


    /**
     * @param $ic_id_inscricao
     * @param $ic_st_justificativa_alteracao
     * @throws \Exception
     */
    public function apagaInscricao($ic_id_inscricao, $ic_st_justificativa_alteracao){


        //insere justificativa
        $this->insertJustificativa($ic_id_inscricao,$ic_st_justificativa_alteracao);

        // deleta as opcoes do candidato
        $this->inscricaoOpcao->forceDeleteByInscricaoID($ic_id_inscricao);

        // apaga inscricao
        $this->model->find($ic_id_inscricao)->delete();




    }
}
