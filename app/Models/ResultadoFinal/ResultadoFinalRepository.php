<?php namespace App\Models\ResultadoFinal;
use App\Funcoes\Funcoes;
use App\Models\ResultadoFinal\Tb_rf_resultado_final as ResultadoFinal;
use App\Models\Inscricao_Opcao\inscricaoOpcaoCRUDRepository as InscricaoOpcao;

//use PRF\RH;

/*
 * ResultadoFinal é uma tabela relativa ao cadastramento do Policial no processo do sisnar. Faz o cálculo de pontuacao.
 *
 */
/**
 * Class ResultadoFinalResultadoFinalRepository
 * @package App\Models\ResultadoFinal
 */
class ResultadoFinalRepository
{


    /**
     * The ResultadoFinal instance.
     *
     *
     *
     */
    protected $ResultadoFinal;
  //  protected $InscricaoOpcao;

    public function __construct(ResultadoFinal $ResultadoFinal , InscricaoOpcao $InscricaoOpcao)

    {
        $this->model = $ResultadoFinal;
        $this->opcao = $InscricaoOpcao;
    }



    /**
     * _queryPattern()
     * Query padrão das pesquisas
     * **********************************
     *
     * @return mixed
     */
    private function _queryPattern()
    {
        return $this->model
            ->select(   'rf_no_uorg_origem',
                'rf_no_uorg_destino',
                'rf_cd_cpf_servidor',
                'rf_vl_pontuacao',
                'rf_dt_edicao'
            );

    }

    /**
     * saveResultadoFinal($ResultadoFinal, $inputs)
     * Usado pelos metodos update e add
     *
     * @param $ResultadoFinal
     * @param $inputs
     * @return mixed
     */
    private function _saveResultadoFinal ($ResultadoFinal, $inputs)
    {

        //  dd($ResultadoFinal, $inputs);
        $ResultadoFinal->rf_no_uorg_origem = $inputs    ['rf_no_uorg_origem'];
        $ResultadoFinal->rf_no_uorg_destino = $inputs['rf_no_uorg_destino'];
        $ResultadoFinal->rf_no_nome_servidor = $inputs['rf_no_nome_servidor'];
        $ResultadoFinal->rf_cd_cpf_servidor =  $inputs['rf_cd_cpf_servidor'];
        $ResultadoFinal->rf_vl_pontuacao =  $inputs['rf_vl_pontuacao'];
        $ResultadoFinal->rf_dt_edicao =  $inputs['rf_dt_edicao'];

        $ResultadoFinal->save();


        return $ResultadoFinal;
    }

    /**
     * index($orderBy = 'ic_vl_pontuacao', $direction = 'desc', $pagination = 30)
     *
     * @param string $orderBy
     * @param string $direction
     * @param int $pagination
     * @return mixed

    public function index($orderBy = 'ic_vl_pontuacao', $direction = 'desc', $pagination = 30)
    {
        return $this->_queryPattern()
            ->orderBy($orderBy,$direction )
            ->paginate($pagination);

    }
     */


    /**
     * add($inputs)
     *
     * @param $inputs
     * @return bool
     */
    public function add($concursoId)
    {
        $inputs = $this->opcao->transcreveResultadoFinal($concursoId);

        foreach ($inputs as $input )
        {
            Funcoes::myInfoConsole('Arquivando a inscricao de '.$input['rf_no_nome_servidor']. 'Opcao :'.$input['rf_no_uorg_destino'] );
            $this->model->unguard();
            $this->model->create([
                'rf_fk_co'=>   $input['rf_fk_co'],
                'rf_st_contemplado' => $input['rf_st_contemplado'],
                'rf_no_uorg_origem'=> $input['rf_no_uorg_origem'],
                'rf_no_uorg_destino' => $input['rf_no_uorg_destino'],
                'rf_no_nome_servidor' => $input['rf_no_nome_servidor'],
                'rf_cd_matricula_servidor'=> $input['rf_cd_matricula_servidor'],
                'rf_vl_pontuacao' => $input['rf_vl_pontuacao'],
                'rf_st_transferido_tb_concurso' => $input['rf_st_transferido_tb_concurso']
            ]);
        }
        return true;
    }



    /**
     * update($inputs, $id)
     *
     * @param $inputs
     * @param $id
     * @return bool

    public function update($inputs, $id)
    {
    $ResultadoFinal = $this->getById($id);
    $this->saveResultadoFinal($ResultadoFinal, $inputs);
    return true;
    }

     */



}
