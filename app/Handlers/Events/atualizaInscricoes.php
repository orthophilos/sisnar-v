<?php namespace App\Handlers\Events;

use App\Events\regrasConcursoAlteradas as regrasConcursoAlteradas;

use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldBeQueued;
use App\Models\Concurso\ConcursoRepository as Concurso;
use App\Models\Inscricao\InscricaoRepository as Inscricao;
use Illuminate\Support\Facades\Session;


class atualizaInscricoes implements  ShouldBeQueued {

	use InteractsWithQueue;

   // protected  $Concurso;
   // protecte $Inscricao;
	/**
	 * Create the event handler.
	 *
	 * @return void
	 */
	public function __construct(Concurso $Concurso, Inscricao $Inscricao)
	{
		$this->concurso  = $Concurso;
        $this->inscricao = $Inscricao;
	}

	/**
	 * Handle the event.
	 *
	 * @param  regrasConcursoAlteradas  $event
	 * @return void
	 */
	public function handle()// regrasConcursoAlteradas $event)
	{
		// aqui coloco o codigo que deve ser executado.

        $this->inscricao->recalculaDadosTemporais($this->concurso->getProximoConcurso()->toArray());


    }

}
