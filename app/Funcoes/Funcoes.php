<?php namespace App\Funcoes;
/**
 * Created by PhpStorm.
 * User: thyago
 * Date: 05/09/14
 * Time: 10:59
 */

use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Session;
use PRF\FaleConosco;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Auth;
use Illuminate\Console\Command;


class Funcoes {

    /**
     * Função retorna true caso o usuário tenha a permissão passada como parâmetro, false caso não tenha
     *
     * @param $permissao
     * @return bool
     */
    public static $dateFormat = 'd/m/Y';
    public static $dateTimeFormat = 'd-m-Y H:i:s';

    /**
     * @param $permissao
     * @return bool
     */
    public static function temPermissao($permissao) {
        if (!is_array(Session::get('permissoes'))) {
            return false;
        }
        if (in_array($permissao, Session::get('permissoes'))) {
            return true;
        }
        else {
            return false;
        }
    }


    public static function temConcurso() {

        if (is_null(Session::get('proximoConcurso'))) {
            return false;
        }
        else
        {
            return true;
        }
    }
    /**
     * @return mixed
     */
    public static function urlFaleConosco() {
        $faleconosco = new FaleConosco(Config::get("PRF.producao"));

        $url = '';

        if (Auth::check()) {
            $url = "?usuarioCpf=" . Auth::user()->cpf . "&siglaSistema=" . config('PRF.siglaSistema');
        }

        return str_replace("/ws","/generico/mensagemInterna/Create.xhtml" . $url,$faleconosco->getUrl());

    }

    /**
     * @param $boolean
     * @return string
     */
    public static function simNao($boolean){
        switch ($boolean) {
            case 0:
                return 'Não';
                break;
            case 1:
                return 'Sim';
                break;
            default:
                return 'Tipo de dados não é boolean';

        }

    }

    /**
     * @param $array
     * @param $key
     * @return array
     */
    public function unique_multidim_array($array, $key){
        // da um smooth na array
        $temp_array = array();
        $i = 0;
        $key_array = array();

        foreach($array as $val){
            if(!in_array($val[$key],$key_array)){
                $key_array[$i] = $val[$key];
                $temp_array[$i] = $val;
            }
            $i++;
        }
        return $temp_array;
    }

    /**
     * @param $array
     * @param $key
     * @param $value
     * @return mixed
     */
    public static function removeElementWithValue($array, $key, $value){
        // remove elementos em uma array multidimensional desde que forneceida a key e a value
        foreach($array as $subKey => $subArray){
            if($subArray[$key] == $value){
                echo 'Removendo elementos indesejaveis #'.$key."\r";
                unset($array[$subKey]);
            }
        }
        return $array;
    }
    /*
     * Filtra uma array bidimensional
     * */
    /**
     * @param array $array
     * @param $key
     * @param $value
     * @return array
     */
    public static function filtraArray ($array = array(), $key, $value)
    {
        // encontra uma chave e um valor na array multidimensional e retorna uma array filtrada apenas com esses valores
        return
        array_filter($array, function ($array) use ($key, $value)  {
                                            return $array[$key] == $value;
                                            }

			);
    }

    /**
     * @param $date1
     * @param $date2
     * @return float
     */
    public static function  daysbetween ($date1, $date2) // formato ja sem milisegundos
    {
        // quantos dias existem entre duas datas
       // $datediff = abs($date1 - $date2);
        return floor(abs($date1 - $date2)/(60*60*24));
       // return floor($date1 - $date2/(60*60*24)); //*1000 se nao tiver  tratado webservice

    }
    /**
     * array_multi_getValuesByKey(array $array, $search)
     * retorna array com os valores encontrados em uma array que tenha $search por key.
     * @param array $array
     * @param $search
     * @return array
     */
    public static function array_multi_getValuesByKey(array $array, $search) {
        $saida = [];

        foreach (new \RecursiveIteratorIterator(new \RecursiveArrayIterator($array)) as $key => $value) {
            if ($search === $key)
                array_push($saida, $value);
        }
        return $saida;
    }

    /**
     * @param string $s
     */
    public static function wait($s='Press any key to continue...')
        {
            echo "\n".$s."\n";
            fgetc(STDIN);
        }


    /**
     * @param $epochDateTime
     * @return bool|string
     */
    public static function MyUnixTimeToDate($epochDateTime){
        return date(self::$dateFormat, ($epochDateTime)); // *  (24 * 3600) ) );  // output = 15.08.2012
    }

    /**
     * @param $myDateStringFormat
     * @return int
     */
    public static function MyDateFormatToMyUnixTime($myDateStringFormat){
       // \DateTime::createFromFormat(self::$dateFormat, $myDateStringFormat);

        $myDateFormat =  \DateTime::createFromFormat(self::$dateFormat, $myDateStringFormat);
        //dd($myDateStringFormat);
        $myDateFormat->setTime(0, 0);
       /*
        dd('entrou em MydateFormat', $myDateStringFormat,
            $myDateFormat,
            $myDateFormat->format(self::$dateFormat),
            $myDateFormat->getTimestamp()
        );
       */
        if (!$myDateFormat ){ abort(500,'Tipo de data inadequado');}
        return $myDateFormat->getTimestamp(); //self::$dateTimeFormat

    }

    /**
     * @param $timestamp
     * @return bool
     */
    public static function isValidUnixTime($timestamp){
       return ((string) (int) $timestamp == $timestamp) // alterado === para ==
        && ($timestamp <= PHP_INT_MAX)
        && ($timestamp >= ~PHP_INT_MAX);
    }

    /**
     * @param $string
     * @return string
     */
    public static function strToHex($string)
    {
        $hex='';
        for ($i=0; $i < strlen($string); $i++)
        {
            $hex .= dechex(ord($string[$i]));
        }
        return $hex;
    }
    public static function myInfoConsole($message)
    {
        if (App::runningInConsole())
        {
            echo "\r\n" ." ". $message . "\r\n" ;

        }

    }
 //   public static function getActualMethod(){
//        $trace=debug_backtrace(); return $trace[0]["function"];
//
//    }
}