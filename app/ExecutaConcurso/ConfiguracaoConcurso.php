<?php namespace app\ExecutaConcurso;

use App\Models\Concurso\ConcursoRepository as Concurso;
use App\Models\Concurso\Tb_co_concurso;
use Illuminate\Support\Facades\Session;

class ConfiguracaoConcurso
{
    /** CONSTANTES RELATIVAS AOS CONCURSOS */



    const QTE_OPCOES = 5;  //quantidade de opçoes que o candidato pode escolher para trabalhar
    /**
     *
     */
    const REDUTOR = 0.99; // utilizado para a pontuacao derivada PONTUACAO ORIGINAL + REDUTOR - Log(ordemOpcao , qte de opcoes disponíveis + 1)
    /**
     *
     */
    const PERMUTA = true; // Ativado sistema de permuta, do contrario o sistema não calculará o cruzamento de dados
    /**
     *
     */
    const AUTO_ZERAR_BANCO = true; // Apaga todos os dados anteriores a X anos.
    /**
     *
     */
    const MOSTRAR_PROBABILIDADE_AO_CANDIDATO = 3; // ate quantos dias antes de encerrar as inscrições se deve mostrar o
    // quadro resumo e o cenário d eocncorrência ao candidato. Na hora de fazer a inscrição .
    /**
     *
     */
    const DECURSO_ZERAR_BANCO_EM_ANOS = 2; // dados de X anos para trás serão automaticamente apagados.
    /**
     *
     */
    const INTERTICIO_DIAS_ULTIMA_REMOCAO = 0; // periodo de interticio em dias desde a ultima remocao.
    /**
     *
     */
    const INTERTICIO_DIAS_EXERCICIO = null; // quantos dias no cargo o policial precisa ter
    /**
     *
     */
    const DIAS_MINIMOS_PARA_INSCRICAO_APOS_EDITAL = 30; // apos lancado o edital qual o intervalo de tempo minimo para abrir inscricao
    /**
     *
     */
    const DIAS_MINIMOS_PERIODO_INSCRICAO = 10; // Prazo mínimo em dias que a inscricao deve ficar aberta
    /**
     *
     */
    const DIAS_MINIMOS_APOS_INSCRICAO_A_EXECUCAO = 20; // apos quantos dias de fechamento da inscricao se pode executar concurso
    /**
     *
     */
    const CARGO = 'PRF'; // cargo que pode concorrer no SISNAR.


    /** METHODOS QUE UTILIZAM AS CONST ACIMA E DEMAIS TEMPORALIDADES PARA VALIDACOES   E ACESSO  */

    public static function checaCargoPublicoAdequado($cargo)
    {
        return $cargo == self::CARGO;
    }


    /**
     * @return null|string
     */
    public static function dataMaxVisualizarEstatistica(){
        // o quadro resumo e a visuzalição de cenário, conforme determinação do CGRH só poderão ser vistas até
        // MOSTRAR_PROBABILIDADE_AO_CANDIDATO antes de terminar o prazo de inscricao;
        // o presente método retorna até que dia se pode ver tais estatísticas

            if(!is_null(self::MOSTRAR_PROBABILIDADE_AO_CANDIDATO))
        {
           // $tb_co_concurso = new Tb_co_concurso();
           // $Concurso = new Concurso($tb_co_concurso);
            $dataEncerraVisualizacoes = new \DateTime(Session::get('proximoConcurso')->co_dt_fim_inscricoes);
            $dataEncerraVisualizacoes->sub(new \DateInterval('P' . self::MOSTRAR_PROBABILIDADE_AO_CANDIDATO . 'D'));
            return $dataEncerraVisualizacoes->format('d.m.Y');
        }else{
            return null;
        }
    }

    /**
     * @return bool
     */
    public static function checkVisualizaEstatisticaHoje(){
        //  o candidato pode visualizar usa estatística hoje ??? true or false

        if(!is_null(self::MOSTRAR_PROBABILIDADE_AO_CANDIDATO))
        {
           // $tb_co_concurso = new Tb_co_concurso();
           // $Concurso = new Concurso($tb_co_concurso);

            $dataEncerraVisualizacoes = new \DateTime(Session::get('proximoConcurso')->co_dt_fim_inscricoes);
            $dataEncerraVisualizacoes->sub(new \DateInterval('P' . self::MOSTRAR_PROBABILIDADE_AO_CANDIDATO . 'D'));
            $dataEncerraVisualizacoes->add(new \DateInterval('PT23H59M59S'));
            $dataInicioInscricoes = new \DateTime(Session::get('proximoConcurso')->co_dt_inicio_inscricoes);
            $hoje = new \DateTime();
         //   $dataEncerraInscricoes->add(new \DateInterval('PT23H59M59S'))
            return ($hoje < $dataEncerraVisualizacoes && $hoje > $dataInicioInscricoes ? true : false);
        }else{
            return false;
        }
    }

    /**
     * @return bool
     */


    public static function checkPeriodoDeInscricao()
    {
        // estamos em período de inscricao ?
      //  $tb_co_concurso = new Tb_co_concurso();
      //  $Concurso = new Concurso($tb_co_concurso);
      //  $dataEncerraInscricoes = new \DateTime(Session::get('proximoConcurso')->co_dt_fim_inscricoes);
        $dataEncerraInscricoes = new \DateTime(Session::get('proximoConcurso')->co_dt_fim_inscricoes);
     //   $dataInicioInscricoes = new \DateTime(Session::get('proximoConcurso')->co_dt_inicio_inscricoes);
        $dataInicioInscricoes = new \DateTime(Session::get('proximoConcurso')->co_dt_inicio_inscricoes);
        $hoje = new \DateTime();
        //$hoje->sub(new \DateInterval('PT03H00S')); // horario vinha com 3 horas a mais que horario local.
        //  @@todo checar isso no servidor
        $dataEncerraInscricoes->add(new \DateInterval('PT23H59M59S')); // concurso vale até o último segundo do dia ficando ate a data mais 23h59m59s
      //  dd($hoje,$dataEncerraInscricoes);

        return ((($hoje <= $dataEncerraInscricoes) && ($hoje >= $dataInicioInscricoes)) ? true : false);
    }


    /**
     * @return bool
     */
    public static function checkPodeExecutarConcursoManualmente()
    {
        // só pode executar concurso manualmente depois da data programada da primeira execucao do concurso
        // a execucao manual do concurso se deve a necessidade de reclassificacao deposi do periodo de recursos
       // $tb_co_concurso = new Tb_co_concurso();
       // $Concurso = new Concurso($tb_co_concurso);
        $dataExecutaConcursoProgramada = new \DateTime(Session::get('proximoConcurso')->co_dt_data_execucao);
        $hoje = new \DateTime();

        return ($hoje > $dataExecutaConcursoProgramada ? true : false);
    }


    /**
     * @return bool
     */
    public static function checkPodeAlterarDadosAlheios(){
        // somente após o periodo de inscricao o gestor, ao analisar os recursos podera alterar manualmente os dados
        // das inscricoes dos candidatos , justificadamente , mediante recurso
        //$tb_co_concurso = new Tb_co_concurso();
        //$Concurso = new Concurso($tb_co_concurso);
        $dataInicioRecursos = new \DateTime(Session::get('proximoConcurso')->co_dt_fim_inscricoes);
        $dataInicioRecursos->add(new \DateInterval('PT23H59M59S'));
        $hoje = new \DateTime();
        return ($hoje > $dataInicioRecursos ? true : false);
    }

    /**
     * @param array $inputs
     * @return bool
     */
    public static function checkCondicoesTemporaisDoCandidato(array $inputs){
        // o candidato tem que obedecer algumas condições de temporalidade para se inscrver , como por exemplo
        // o intertício de dias que entrou em exercício,  e o intertício desde a data da ultima remocao,  períodos esses
        // definidos na constante acima.
        $tb_co_concurso = new Tb_co_concurso();
        $Concurso = new Concurso($tb_co_concurso);
        $ic_vl_dias_desd_ini_serv = $Concurso->diasEntreDataServidorEConcurso($inputs['dataExercicio']);
        $ic_vl_dias_desd_ult_lotacao = $Concurso->diasEntreDataServidorERemocao($inputs['dataLotacao']);
        return
            (
                $ic_vl_dias_desd_ini_serv > self::INTERTICIO_DIAS_EXERCICIO &&
                $ic_vl_dias_desd_ult_lotacao > self::INTERTICIO_DIAS_ULTIMA_REMOCAO ?
                true : false
            );

    }

    /**
     * @param array $dataEmployee
     * @param $key
     * @return bool
     */
    public static function checkCondicoesGeraisDoCandidato(array $dataEmployee, $key)
    {
        // checa condicoes gerais do candidato a partir  para ver se pode concorrer ao concurso
        // ddd
        if(
            !empty($dataEmployee[$key]['situacao']) &&
            !empty($dataEmployee[$key]['classePadrao']['cargo']['descricao']) &&
            !empty($dataEmployee[$key]['dataExercicio']) &&
            !empty($dataEmployee[$key]['dataLotacao'])  &&
            !empty($dataEmployee[$key]['dataNascimento']) &&
            !empty($dataEmployee[$key]['unidadeOrganizacional']['id']) &&
            !empty($dataEmployee[$key]['matricula']) &&
            $dataEmployee[$key]['matricula'] != "0000000" &&
            $dataEmployee[$key]['classePadrao']['cargo']['descricao'] ==  self::CARGO &&
            $dataEmployee[$key]['situacao'] != -1
        )
        {
            return true;
        }else{
            return false;
        }
    }
    /**
     * @param /DateTime $publicacaoEdital
     * @return array
     */
    public static function prazosDefaultConcursos( $publicacaoEdital ) // DateTime
    {
        // a partir do início das inscricoes os prazos normais para seguir em um edital;

        $inicioInscricoes = new \DateTime($publicacaoEdital->format('Y-m-d H:i:s'));
        $inicioInscricoes->modify('+'.self::DIAS_MINIMOS_PARA_INSCRICAO_APOS_EDITAL.' day');;
        $fimInscricoes = new \DateTime($inicioInscricoes->format('Y-m-d H:i:s'));
        $fimInscricoes->modify('+'. self::DIAS_MINIMOS_PERIODO_INSCRICAO .' day');
        $dataExecucao = new \DateTime($fimInscricoes->format('Y-m-d H:i:s'));
        $dataExecucao->modify('+'. self::DIAS_MINIMOS_APOS_INSCRICAO_A_EXECUCAO .' day');
        return
            [
                'publicacaoEdital' =>  $publicacaoEdital->format('Y-m-d H:i:s'),
                'inicioInscricoes'  => $inicioInscricoes->format('Y-m-d H:i:s'),
                'fimInscricoes'     => $fimInscricoes->format('Y-m-d H:i:s'),
                'dataExecucao'      => $dataExecucao->format('Y-m-d H:i:s')
            ];

    }
}
