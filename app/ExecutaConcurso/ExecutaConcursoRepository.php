<?php namespace App\ExecutaConcurso;

use App\Funcoes\Funcoes;
use App\Models\Inscricao_Opcao\Tb_io_inscricao_opcao as InscricaoOpcao;
use App\Models\Inscricao\InscricaoRepository as Inscricao,
    App\Models\Vaga\VagaRepository as Vaga,
    App\Models\Servidor\ServidorRepository as Servidor,
    App\Models\Concurso\ConcursoRepository as Concurso,
    App\Models\ResultadoFinal\ResultadoFinalRepository as ResultadoFinal;
//use App\Models\Inscricao_Opcao\Tb_io_inscricao_opcao;
use App\Models\Inscricao_Opcao\Tb_io_inscricao_opcao;
use App\Models\Vaga\Tb_vg_vaga;
// use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\ExecutaConcurso\Contratos\executaConcursoContract as executaConcursoContract;
//use PRF\RH;

/*
 *  É nesta clase que é executada a classificação do concurso. Ele parte por dois métodos de classificação o primeiro
 * que se denomina não-conservativo onde pode haver delegacias onde no final a diferença entre servidores que entraram e sairam é
 * diferente de zero e o método conservativo onde o balanco entre entrada e saída de servidores no final da classificação sempre
 * terá que ser igual a zero
 * O método não-conservativo será executado quando o gestor setar vagas preestabelecidas no concurso enquanto o
 * conservativo quando o gestor não setar vagas . Enquanto o primeiro serve para o SISNAR  o segundo é para mero remanejamento (permuta)
 * entre servidores fora da época de concurso  .
 *
 * A classificação de concurso poderá ocorrer via crontab em data preestabelecida pelo servidor, bem como executada pelo gestor do sistema
 * na tela .
 *
 * Importante dizer que se a constante ConfiguracaoConcurso::PERMUTA=true refere ao fato de quando o servidor sair de uma localidade
 * e abrir vaga para outra pessoa ocupar
 *
 */

class ExecutaConcursoRepository implements executaConcursoContract
{



    protected  $InscricaoOpcao;
    protected  $Inscricao;
    protected  $Vaga;
    protected  $Servidor;
 //   protected  $Concurso;

    /**
     * @param Tb_io_inscricao_opcao $InscricaoOpcao
     * @param Inscricao $Inscricao
     * @param Vaga $Vaga
     * @param Servidor $Servidor
     * @param Concurso $Concurso
     * @param $ResultadoFinal $ResultadoFinal
     */
    public function __construct( InscricaoOpcao $InscricaoOpcao   ,
                                  Inscricao $Inscricao,
                                 Vaga $Vaga ,
                                 Servidor $Servidor,
                                 Concurso $Concurso,
                                 ResultadoFinal $ResultadoFinal   )

    {

        $this->model = $InscricaoOpcao;
        $this->inscricao = $Inscricao;
        $this->vaga = $Vaga;
        $this->servidor = $Servidor;
        $this->concurso = $Concurso;
        $this->resultadoFinal = $ResultadoFinal;

    }

    /**
     *
     */
    private function _apagaInscricoesNaoConfirmadas()
    {

        // apaga em tb_io_inscricao_opcao wherein IDs;
        $this->model->whereIn('io_fk_ic_inscricao',$this->inscricao->inscricoesNaoConfirmadas())
                    ->forceDelete(); // apaga de verdade no banco
    }

    /**
     * @param $inscricaoOpcao
     * @return bool
     */
    private function _desclasCandidMenoresNotas($inscricaoOpcao)
    {
        // desclassifica outros candidatos que estão contemplados mas com menores notas
        //
        $candidatosMenoresNotas = $this->model
            ->where('io_vl_pontuacao_derivada', '<', $inscricaoOpcao->io_vl_pontuacao_derivada)
            ->where('io_st_contemplado', '=',1)
            ->where('io_cd_uorg_destino','=',$inscricaoOpcao->io_cd_uorg_destino)
            ->get();
        if ( $candidatosMenoresNotas->isEmpty() ) {
            return false;
        }else {

            foreach ($candidatosMenoresNotas as $candidatoMenorNota) {
                $this->model
                    ->where('io_id_inscricao_opcao', '=', $candidatoMenorNota->io_id_inscricao_opcao)
                    ->update(['io_st_contemplado' => '0']);
                $this->vaga->contabilidade($candidatoMenorNota, true);
            }

            return true;
        }
    }

    /**
     * @param $inscricaoOpcao
     * @param bool|true $check
     * @return bool
     */
    private function _doCheckContemplado($inscricaoOpcao, $check = true)
    {
        // contempla a opcao do candidato e envia o fato para a contabilidade;
        // a opção $check = false faz o reverso, descontempla o candidato (também envia para contabilidade reversa)

        if ($this   ->model
                    ->where('io_id_inscricao_opcao','=',$inscricaoOpcao->io_id_inscricao_opcao)
                    ->update(['io_st_contemplado' => $check])){

            if ($check){ // transferir para executarConcurso
              //  echo " do check contemplado check true". "\r\n";

                $this->vaga->contabilidade($inscricaoOpcao);
            } else {
              //  echo " do check contemplado check false". "\r\n";
                $this->vaga->contabilidade($inscricaoOpcao,true);
            }

            return true;

        }else{

            return false;

        }

    }

    /**
     * @param   $candidatoADescontemplar query
     */
    private function _repescagem($candidatoADescontemplar)
    {
     //   Funcoes::myInfoConsole('entrou na repescagem');
        $pointer = false;
        // Caso tenha desclassificado algum candidato verifica se ele não poderia se reenquadrar nas outras opções
        // de menor prioridade considerando se sua nota for melhor que nota dos outros candidatos classificados naquela UORG


        // pegar as demais opcoes com menor pontuacao trashed

       $this   ->model
                ->where('io_fk_ic_inscricao',$candidatoADescontemplar->io_fk_ic_inscricao)
                ->where('io_vl_pontuacao_derivada','<=', $candidatoADescontemplar->io_vl_pontuacao_derivada)
                ->onlyTrashed()->restore();
       $demaisOpcoesCandidato =     $this   ->model
           ->where('io_fk_ic_inscricao',$candidatoADescontemplar->io_fk_ic_inscricao)
           ->where('io_vl_pontuacao_derivada','<', $candidatoADescontemplar->io_vl_pontuacao_derivada)
           ->orderBy('io_vl_pontuacao_derivada','DESC')->get();

        //$demaisOpcoesCandidato->restore();
        // pega demais candidatos contemplados das demais opcoes
       foreach ($demaisOpcoesCandidato as $outraOpcaoCandidato)
       {
            $this->model
                ->where('io_id_inscricao_opcao', $outraOpcaoCandidato->io_id_inscricao_opcao)
                ->update(['io_st_computado' => '1']);

            // pega o ultimo candidato desta uorg
            $ultimoClassificadoDestaUorg = $this->model
                ->where('io_cd_uorg_destino' , $outraOpcaoCandidato->io_cd_uorg_destino)
                ->where('io_st_contemplado',1)
                ->where('io_vl_pontuacao_derivada','<', $outraOpcaoCandidato->io_vl_pontuacao_derivada )
                ->orderBy('io_vl_pontuacao_derivada','ASC')
                ->first();

           if($ultimoClassificadoDestaUorg)
           {
           //  Funcoes::wait( $outraOpcaoCandidato."\r\n". $ultimoClassificadoDestaUorg);
               // descontempla este ultimo candidato
               $this->_doCheckContemplado($ultimoClassificadoDestaUorg,false);

               $this->model
                                    ->where('io_fk_ic_inscricao',$ultimoClassificadoDestaUorg->io_fk_ic_inscricao)
                                    ->where('io_vl_pontuacao_derivada','<',$ultimoClassificadoDestaUorg->io_vl_pontuacao_derivada)
                                    ->orderBy('io_vl_pontuacao_derivada','DESC')
                                    ->onlyTrashed()
                                    ->restore();
               $outrasOpcoesUltimoClassificado =   $this->model
                   ->where('io_fk_ic_inscricao',$ultimoClassificadoDestaUorg->io_fk_ic_inscricao)
                   ->where('io_vl_pontuacao_derivada','<',$ultimoClassificadoDestaUorg->io_vl_pontuacao_derivada)
                   ->orderBy('io_vl_pontuacao_derivada','DESC')
                   ->first();
               if ($outrasOpcoesUltimoClassificado)
               {

                //   Funcoes::wait("\r\n".$outrasOpcoesUltimoClassificado."\r\n".'da uma olhada agora'."\r\n");
                   $this->_doCheckContemplado($outrasOpcoesUltimoClassificado);
                //   Funcoes::wait('esse é o erro');
               }
           }
           //$this->_doCheckContemplado($ultimoClassificadoDestaUorg,false);
           $candidatoMelhorPontuadosNestaUorgNaoContemplados =
               $this->model
                    ->where('io_cd_uorg_destino',$outraOpcaoCandidato->io_cd_uorg_destino)
                    ->where('io_vl_pontuacao_derivada','>',$outraOpcaoCandidato->io_vl_pontuacao_derivada)
                    ->where('io_st_contemplado', '!=', 1)
                    ->orderBy('io_vl_pontuacao_derivada','desc')
                    ->withTrashed()
                    ->first();
           if (!is_null($candidatoMelhorPontuadosNestaUorgNaoContemplados))
           {
              //verifica se este candidato não está contemplado em outras opcoes de maior pontuacao
               $demaisOpcoesDaqueleCandidato =     $this   ->model
                   ->where('io_fk_ic_inscricao',$candidatoMelhorPontuadosNestaUorgNaoContemplados->io_fk_ic_inscricao)
                   ->where('io_vl_pontuacao_derivada','>', $candidatoMelhorPontuadosNestaUorgNaoContemplados->io_vl_pontuacao_derivada)
                   ->where('io_st_contemplado',1)
                   ->orderBy('io_vl_pontuacao_derivada','DESC')
                   ->first();
               if(is_null($demaisOpcoesDaqueleCandidato)){
                   $this->_doCheckContemplado($candidatoMelhorPontuadosNestaUorgNaoContemplados);
               }else{$this->_doCheckContemplado($outraOpcaoCandidato);}
           }else {$this->_doCheckContemplado($outraOpcaoCandidato);}
          // $this->_doCheckContemplado($outraOpcaoCandidato);

           $pointer = true;
           break;

           //else{ contempla ? }
       }
        return $pointer;
    }
    /**
     * @param $inscricaoOpcao
     * @return int
     */
    private function _apagaOutrasOpcoes( $inscricaoOpcao)
       {
          // pega demais opçoes com notas derifadas inferiores a do rec  $io_fk_ic_inscricao
           $demaisOpcoesDoCandidato = $this->model
               ->where('io_fk_ic_inscricao',$inscricaoOpcao->io_fk_ic_inscricao)
               ->where('io_vl_pontuacao_derivada','<',$inscricaoOpcao->io_vl_pontuacao_derivada)
               ->orderBy('io_vl_pontuacao_derivada','desc')
               ->get();
           $opcoesAApagar = [];

          // print_r($demaisOpcoesDoCandidato);
          // Funcoes::wait();

           foreach ( $demaisOpcoesDoCandidato as $outraOpcaoDoCandidato )
           {
               // faz contabilidade reversa dessas opcoes ja contempladas pois serão apagadas
               if ($outraOpcaoDoCandidato->io_st_contemplado)
               {

                //   echo "$outraOpcaoDoCandidato->io_st_contemplado == 1". "\r\n";

                   $this->model->where('io_id_inscricao_opcao', '=', $outraOpcaoDoCandidato->io_id_inscricao_opcao)
                       ->update(['io_st_contemplado' => '0']);

                   $this->vaga->contabilidade($outraOpcaoDoCandidato,true);
               }
               array_push($opcoesAApagar,$outraOpcaoDoCandidato->io_id_inscricao_opcao);
           }
           $this->model->whereIn('io_id_inscricao_opcao', $opcoesAApagar)->delete();
           return count($opcoesAApagar);
       }

    /**
     * @param int $check
     * @return mixed
     */
    private function _doCheckComputadoAll($check = 1)
    {
        // Update record with $io_st_computado = true
        return $this->model->where('io_st_computado','!=',$check)->update(['io_st_computado' => $check]);
    }


    /**
     *
     */
    private function _balancemanentoConservativo(){
        //Executa o algoritmo de balanceamento conservativo. Isso significa o seguinte, após a classificação dos candidatos,
        // o sistema irá fazer os balanceamentos, no caso de concurso tipo Remanejamento (SISNAR) o balanceamento será o não
        // conservativo, no caso de permuta, deve ser o balanceamento conservativo.
        // No conservativo o total de entradas e saidas de PRFs em uma UORG deve ser igual, isso para fazer com que o
        // total do efetivo se conserve e nenhuma UORG seja prejudicada.
        //
       // $contabilizaVagas = Tb_io_inscricao_opcao::getBalancodeVagasContempladas();

        echo "\r\n".'Vagas serão balanceadas com algoritmo conservativo  de efetivo nas UORGS'."\r\n";

        // capita quais estão desbalanceadas
        $i = 0;
        $pointer = true;
        $volta = 1;
        $totalIdsDescontemplar = [];
        while ($pointer) {
            $idsDescontemplar = [];
            $contabilizaVagas = Tb_io_inscricao_opcao::getBalancodeVagasContempladas();
            if($contabilizaVagas['totalIrregularidades'] == 0){ $pointer = false; continue;}
            foreach ($contabilizaVagas['uorg'] as $contabilizaVaga) {
                if ($contabilizaVaga['balanco'] < 1){  continue; }
                $candidatosADescontemplar =
                    $this->model
                        ->where('io_st_contemplado', 1)
                        ->where('io_cd_uorg_destino', $contabilizaVaga['vg_id_uorg'])
                        ->orderBy('io_vl_pontuacao_derivada', 'ASC')
                        ->take($contabilizaVaga['balanco'])
                        ->get();


                foreach ($candidatosADescontemplar as $candidatoADescontemplar) {
                    array_push($idsDescontemplar,$candidatoADescontemplar->io_id_inscricao_opcao );
                    $pointer = true;

                    $this->vaga->contabilidade($candidatoADescontemplar,false);
                    Funcoes::myInfoConsole(
                                "Volta de no." . $volta++.
                                " Analise de Opcoes : " . $i++ );
                }
                array_push($totalIdsDescontemplar,$idsDescontemplar);
                $this->model->whereIn('io_id_inscricao_opcao',$idsDescontemplar)->update(['io_st_contemplado' => '0']);
            }
        }
    }

    /**
     *
     */
    private function _balanceamentoNaoConservativo(){
// já o balanceamento não conservativo , o sistema não se preocupará se houve mais saidas que entradas na UORG, cortará
// apenas os casos em que a entrada se deu acima do limite vagas preestabelecidas + remanescentes . Isso as vezes acontece
// porque um candidato que outrora estava classificado e tena aberto uma vaga remanescente em seu local de origem, foi,
// durante o ciclo, desclassificado sem que o sistema tenha feito o regresso daquele que ocuparia sua vaga na uorg origem.
//
        Tb_vg_vaga::where('vg_vl_vagas_ganhas','>',0)->decrement('vg_vl_vagas_ganhas');
        $pointer = true;
        $i = 0;
        while ($pointer) {
            $pointer = false;
            // verifica todo candidato que não está contemplado em nenhum lugar mas uma de suas opções tem vaga e contempla-o





            $candidatosADescontemplar =
                $this->model
                    ->join(DB::raw('(SELECT
                                            min(io_vl_pontuacao_derivada) notaDerivada,
                                            io_cd_uorg_destino
                                     FROM tb_io_inscricao_opcao
                                     where io_st_contemplado = 1
                                     group by io_cd_uorg_destino
                                     ) t2'),
                        function ($join) {
                            $join->on('t2.io_cd_uorg_destino', '=', 'tb_io_inscricao_opcao.io_cd_uorg_destino');
                            $join->on('t2.notaDerivada', '=', 'tb_io_inscricao_opcao.io_vl_pontuacao_derivada');
                        })
                    ->where('io_st_contemplado', '=', 1)
                    ->orderBy('io_vl_pontuacao_derivada','DESC')
                    ->get();
                    //->toSql();
           //     dd($candidatosADescontemplar);
          //  Funcoes::wait($candidatosADescontemplar);

            foreach ($candidatosADescontemplar as $candidatoADescontemplar) {

                // retira um de todos o vg_vl_vagas_ganhas
                $dadosVaga =
                    Tb_vg_vaga::where('vg_id_uorg', $candidatoADescontemplar->io_cd_uorg_destino)
                        ->first(['vg_id_uorg', 'vg_vl_vagas_preestabelecidas', 'vg_vl_vagas_ganhas', 'vg_vl_vagas_perdidas']);

                //Funcoes::wait($candidatoADescontemplar."\r\n".$dadosVaga);
                // dd($candidatoADescontemplar->toArray(),$dadosVaga->toArray());
//                if ($candidatoADescontemplar->io_fk_ic_inscricao == 635){
//                    Funcoes::wait($candidatoADescontemplar);
//                    Funcoes::wait($dadosVaga);
//
//                }

                if ($dadosVaga->vg_vl_vagas_preestabelecidas +
                    $dadosVaga->vg_vl_vagas_ganhas  < $dadosVaga->vg_vl_vagas_perdidas )
                {
                    $this->_doCheckContemplado($candidatoADescontemplar,false);
                    /*
                     $this->model
                         ->where('io_id_inscricao_opcao', $candidatoADescontemplar->io_id_inscricao_opcao)
                         ->update(['io_st_contemplado' => '0']);
                     $this->vaga->contabilidade($candidatoADescontemplar, true);
                     */
                    if($this->_repescagem($candidatoADescontemplar)){
                        $pointer = true;
                        break;
                    }
                    $pointer = true;
                    Funcoes::myInfoConsole("Candidatos descontemplados no balanceamento de vagas : " . ++$i );
                }

//                if ($dadosVaga->vg_vl_vagas_preestabelecidas +
//                    $dadosVaga->vg_vl_vagas_ganhas == $dadosVaga->vg_vl_vagas_perdidas)
//                {
//                    $this->_doCheckContemplado($candidatoADescontemplar);
//                    $pointer = true;
//                    break;
//                }
            }
        }
        echo "\r\n";

    }

    /**
     *
     */
    public function classificarCandidatos()
    {
    //Classifica as opcoes dos candidatos quanto a contemplado e não contemplado de acordo com as regras do SISNAR.

        //checa se o concurso já foi executado

        if ($this->concurso->checkConcursoExecutado()){
            echo "######## O presente concurso já passou pelo processo de classificação ######". "\r\n";
            return 0;
        }


        // retira inscricoes nao confirmadas pelos usuarios
        $this->_apagaInscricoesNaoConfirmadas();
        $vagasGeradas = true; // para ser usado no loop
        $s = 1; // so para aparecer na tela
        $totalOpcoesAnalisadas = 0; // so para aparecer na tela
        $totalContemplados = 0 ; // so para aparecer na tela
        $totalVoltas = 0 ; // so para aparecer na tela
        $totalDesclassificados = 0 ; // so para aparecer na tela
        // enquanto  dentro do loop gerar uma vaga por conta de remocao de servidor, continue dando loop
        while ( $vagasGeradas  ) {
            $desclassificados = 0 ;
            $i = 0;
            $contemplados = 0 ;
            $vagasGeradas = false; // se gerar vaga la na frente  entao true
            Funcoes::myInfoConsole('Buscando opcoes não computadas volta de nº' . $s );
               $todosInscricaoOpcao =  $this->vaga->getNotComputed();  //  pega registros com io_st_computado = false
            foreach ($todosInscricaoOpcao as $umaInscricaoOpcao )
            {
                $inscricaoOpcao = $this->model->find($umaInscricaoOpcao->io_id_inscricao_opcao);

                if(is_null($inscricaoOpcao)){
                    // se não a  opção por que já sofreu um softDelete por conta de já ter sido excluída em
                    // voltas anteriores (find retornar null)
                    $i++;
                    continue;
                }
                // desclassifica outras opcoes do candidato com menor pontuacao em
                // Nota Derivada ( Nota + fracao de desempate e prioridade de opcao)
                if ($this->_desclasCandidMenoresNotas($inscricaoOpcao)){
                    $desclassificados ++;
                }


                // busca o registro em tb_vg_vaga com id = rf_no_uorg_destino
                $vagasRemanescentes = $this->vaga->getQtdVagasRemanPorUorg($inscricaoOpcao->io_cd_uorg_destino);

                // se houver vagas remanescentes entao...
                if ($vagasRemanescentes['Total_Vagas_Remanescentes'] >  0 )
                {
                    // update como contemplado - e ja faz o servico de contabilidade
                    if($this->_doCheckContemplado($inscricaoOpcao)){
                        $vagasGeradas = true;
                        $contemplados ++;
                        $this->_apagaOutrasOpcoes($inscricaoOpcao);
                    }
                }
               // Funcoes::wait($inscricaoOpcao);

                $simNao = Funcoes::simNao($vagasGeradas);
                Funcoes::myInfoConsole(
                    "Opcao de nº #" .$i++ . " da volta ". $s ." Contemplados ".  $contemplados . " desclassificados ".
                    $desclassificados . " previsto mais volta ? ". $simNao
                );
                unset($vagasRemanescentes);
            }
            $totalDesclassificados  += $desclassificados ;
            $totalOpcoesAnalisadas += $i;
            $totalContemplados += $contemplados;
            $s++;
            Funcoes::myInfoConsole("#######################################################################################");
            sleep(1);
        }
        $this->_doCheckComputadoAll();
        Funcoes::myInfoConsole("Necessário agora balanceando vagas .....");

        echo "\r\n" . "Relatório : " . "\r\n";
        echo "Total de ".$totalOpcoesAnalisadas ." análises . Realizadas verificando o banco de opcoes ".$s ." vezes " . "\r\n";
        echo "Foram contemplados no máximo ";
        echo $totalContemplados - $totalDesclassificados;
        echo " candidatos em ". $totalContemplados ." classificacoes com pelo  menos ". $totalDesclassificados ." declassificaçoes". "\r\n";

    }

    /**
     * @param bool|false $conservaEfetivo
     */
    public function balancearVagas($conservaEfetivo = false)
    {

        /* considerando que para permitir que haja a permuta e rode o algoritimo
         é preciso que vg_vl_vagas_ganhas esteja inicialmente
        setado para 1 e não para 0 depois de executar o concurso é necessario que retire
        uma vaga de cada uorg, isso é balancear as vagas. */

        echo "\r\n".'############## BALANCEANDO VAGAS ###################################################'."\r\n";

        //dd($conservaEfetivo);
        if ($conservaEfetivo){
            $this->_balancemanentoConservativo();
//            echo "\r\n".' Opcoes Descontempladas : '. count($totalIdsDescontemplar) ."\r\n";
            echo "\r\n".'########Registrando entradas e saidas ############### ' ."\r\n";
        }else{
            echo "\r\n".'Vagas serão balanceadas sem conservar quantidade de efetivo em todas as UORGS'."\r\n";
            $this->_balanceamentoNaoConservativo();
        }
        echo "\r\n";
        echo " ############ Balanceamento Concluído ####################";
        echo "\r\n";
    }

    public function arquivarConcurso($co_id_concurso = null)
    {

        if (is_null($co_id_concurso)) {
            $co_id_concurso = $this->concurso->getProximoConcurso(true)->co_id_concurso;
            if (is_null($co_id_concurso)){
                return false;
            }
        }


        // pega todos o insccoes opcoes e salva em resultado final
        Funcoes::myInfoConsole('######## Salvando todas as inscricoes em resultado final ############### ' );
        $this->resultadoFinal->add($co_id_concurso);

        //dd('daqui pra frente apaga tudo - ExecutaConcurso ArquivarConcurso');
        // zera vagas remanecentes atuais atuais e 1  entrada e  0 saida no vagas
        $concurso = $this->concurso->getById($co_id_concurso);
        $dadosConcurso['configuracao'] = $concurso;
        $dadosConcurso['vagas'] = $this->vaga->getAll()->toArray();
        $this->concurso->saveDadosVagas($co_id_concurso, $dadosConcurso);
        $this->vaga->zeraVagas();



        // dd('passou do zeraVagas');
        // zera inscricoes e inscircoes opcoes e servidores
        Funcoes::myInfoConsole('######## Zerando inscrições para próximo concurso############### ' );
        $this->model->truncate();

        // zera inscricoes
        $this->inscricao->truncate();

        // zera servidores
        Funcoes::myInfoConsole('######## Zerando banco de servidores############### ');

        $this->servidor->truncate();

        // softDelete no concurso
        Funcoes::myInfoConsole('######## apagando o concurso ############### ' );
        $this->concurso->destroy($co_id_concurso);
        echo "\r\n". '######## Processo de execução do concurso  executado com sucesso! ########'. "\r\n";
        //dd('teste');
        return true;
    }

    public function rebootClassificacao (){
        // retorna o banco de dados ao estágio anterior ao de classificar e balancear os candidatos
        // esse método foi construído para ser acionado manualmente pelo gestor quando, por um acaso, após classificar
        // os candidatos, tenha encontrado algum erro dos dados, tenha mudar algo nos dados da inscrição.

        $this->vaga->zeraVagas(false);
        $this->model->onlyTrashed()->restore();
        $this->model->update(
            [
                'io_st_contemplado' => 0,
                'io_st_computado' => 0
            ]
        );


    }
}
