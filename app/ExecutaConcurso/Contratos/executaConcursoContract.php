<?php namespace App\ExecutaConcurso\Contratos;

interface executaConcursoContract
{

/*
 * Classifica os candidatos do concurso considerando o remanejamento de vagas
 *
 */

    public function classificarCandidatos(); // executar concurso

 /*
  *
  * Retira acrescenta uma vaga perdida virtual, considerando que o banco tem uma
  * vaga ganha virtual, para possibilitar as permutas e remanejamentos
  * tambem deve desclassificar o ultimo candidato de cada uorg se o candidato por vaga for > 1
  *
  * * conservaEfetivo
  *
  * No caso de conservação de efetivo , em todas as uorgs a quantidade de entrada e saida
  * de servidores tem que ser a mesma
  *
  */
    public function balancearvagas($conservaEfetivo = false);

  /*
   *  Pega os dado sdo concurso salva na tabela resultado final e zera vagas , inscricoes, inscricoes opcoes, servidores
   *
   * */
    public function arquivarConcurso($co_id_concurso);
  /*
   *  Zera os dados e alteracoes produzidospor executaConcurso e Balancear Vagas para poder permitir roda rde onvo a classificacao
   * caso o gestor detecte algum problema e precise, apos alterar algum dado ou incluir, rodar de novo a classificacao
   *
   */

    public function rebootClassificacao();
}
