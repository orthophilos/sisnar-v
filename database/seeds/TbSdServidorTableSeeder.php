<?php

// Composer: "fzaninotto/faker": "v1.4.0"
use Faker\Factory as Faker;
use Illuminate\Database\Seeder;
use App\Models\Servidor\Tb_sd_servidor as Servidor;
use PRF\RH;
use App\Funcoes\Funcoes as Funcao;
class TbSdServidorTableSeeder extends Seeder {

	public function __construct()
	{
		// parent::__construct();
		$this->rh = new RH();
	}
	public function run()

	{
      //  dd($this->rh->getByMatricula('1969360'));  //getByNome(' Jeferson Tadeu de Souza'));
        Servidor::unguard();
        $this->pegaDadosReais(); // utilizado se o webservice http://10.0.11.32:8080/DPRFWS/RHPD?find=TodosOsRegistros estiver funcionando
       // $this->pegaDadosFicticios();


    }

    private function pegaDadosReais()
    {
        $this->command->info('aguarde, fazendo contato com WebService recuperando dados do servidores');
        $this->command->info('isso pode demorar alguns minutos!');
        $todosRH = $this->rh->getTodosUsuarios();
        $todosRH = Funcao::filtraArray($todosRH,'descricaoTipo','PRF');
        $todosRH = Funcao::filtraArray($todosRH,'ativo',1);
        $todosRH  = Funcao::removeElementWithValue($todosRH, "matricula", "");
        $todosRH  = Funcao::removeElementWithValue($todosRH, "matricula", "0000000");
       // $todosRH  = $this->funcao->removeElementWithValue($todosRH, "excluido", false);


//    "ativo" => true

        $this->command->info('passou..');
        $contador = 0 ;
        foreach($todosRH as $umDoRH)
        {
            echo 'Recebendo dados de UORG , rec #'.$contador."\r";
            //dd$umDoRH);
            Servidor::create([
                'sd_cd_servidor' => $umDoRH['id'],
                'sd_cd_cpf'=>   $umDoRH['cpf'],
                'sd_cd_matricula_servidor' => $umDoRH['matricula'],
                'sd_no_nome' => $umDoRH['nomeCompleto']
            ]);
            $contador ++;
        }
        echo 'Total de Servidores encontrados..............:' .$contador;
        echo "\r\n";
        $this->command->info('Completada a tarefa');

    }

    private function pegaDadosFicticios()
    {
        $faker = Faker::create('pt_BR');
        $this->command->info('Criando dados simulados de Servidores 10000 registros, rec #');
        foreach(range(1, 10000) as $index)
        {
            echo "registro " .$index  ."# de 10000" . "\r";
            Servidor::create([

                'sd_cd_servidor' =>$faker->unique()->randomNumber(5),
                'sd_cd_cpf'=>   $faker->unique()->randomNumber(11),
                'sd_cd_matricula_servidor' => $faker->unique()->randomNumber(7),  // faker-> 7 digitos
                'sd_no_nome' => $faker->name
            ]);
        }

    }
   }
