<?php

// Composer: "fzaninotto/faker": "v1.4.0"
use Faker\Factory as Faker;
use Illuminate\Database\Seeder;
use App\Models\Inscricao_Opcao\Tb_io_inscricao_opcao as TbIoInscricaoOpcao;
use App\Models\Inscricao\Tb_ic_inscricao as Inscricao,
	App\Models\Vaga\Tb_vg_vaga as Vaga,
    App\Models\Inscricao_Opcao\inscricaoOpcaoCRUDRepository  as InscricaoOpcao;
use App\ExecutaConcurso\ConfiguracaoConcurso as ConfiguracaoConcurso;


class TbIoInscricaoOpcaoTableSeeder extends Seeder
{


    public function __construct( InscricaoOpcao $InscricaoOpcao)
    {
        $this->inscricaoOpcao = $InscricaoOpcao;

    }

    private function _getDestinoProvavel($io_cd_uorg_destinoOpcaoAnterior = null)
    {
        $faker = Faker::create();
        $vaga = Vaga::all()->lists('vg_id_uorg');

        // se ja foi escolhido algum estado, aumentar a probabilidade dele.
        if (!empty($io_cd_uorg_destinoOpcaoAnterior))
        {
            $UF = Vaga::select('vg_ed_uf')
                    ->where('vg_id_uorg',$io_cd_uorg_destinoOpcaoAnterior)
                    ->first();
            $vagaMesmaUF = Vaga::where('vg_ed_uf', $UF->vg_ed_uf)->lists('vg_id_uorg');

            if ($faker->boolean(85))
            {

               return $faker->randomElement($vagaMesmaUF);

            }else{
               return  $faker->randomElement($vaga);
            }

        }else {
            // se ainda não foi escolhido estado algum

            $localMaisPopuloso = Vaga::select('vg_vl_populacao')->orderBy('vg_vl_populacao', 'DESC')->take(1)->get();
            $localMenosPopuloso = Vaga::select('vg_vl_populacao')->orderBy('vg_vl_populacao', 'ASC')->take(1)->get();

            // sim, foi uma porcaria de uma gambiarra
            $fatorPorcentagem = 100 / (($localMaisPopuloso[0]->vg_vl_populacao - $localMenosPopuloso[0]->vg_vl_populacao) / 100); // multiplicar esse fator pela populacoa;




            $pointer = true;

            while ($pointer)
            {
                $localDestinoCandidato  = $faker->randomElement($vaga);
                $probabilidadeDaVaga = Vaga::select(DB::raw('vg_vl_populacao -'.$localMenosPopuloso[0]->vg_vl_populacao.' as probabilidade '))
                                                ->where('vg_id_uorg',$localDestinoCandidato)
                                                ->get();
                $probabilidade =(float) $probabilidadeDaVaga[0]->probabilidade * $fatorPorcentagem / 40;
                if ($probabilidade > 100) {$probabilidade= 99; }
                if ($faker->boolean($probabilidade)){
                    $pointer = false;
                }

            }

            return $localDestinoCandidato;
        }
    }

    public  function run()
    {
        TbIoInscricaoOpcao::unguard();
       // $faker = Faker::create();
        $inscricoes = Inscricao::select('ic_id_inscricao', 'ic_fk_vg_uorg_origem')->get();
       // $vaga = Vaga::all()->lists('vg_id_uorg');
        $totalDeOpcoes = ConfiguracaoConcurso::QTE_OPCOES;



        foreach ($inscricoes as $inscricao) {

            // $io_cd_uorg_destino = $this->_getDestinoProvavel($io_cd_uorg_destinoOpcaoAnterior);
            $io_cd_uorg_destino = $this->_getDestinoProvavel();
            $io_cd_uorg_destinoOpcaoAnterior = $io_cd_uorg_destino  ;

            foreach (range(1, $totalDeOpcoes) as $ordemOpcao) {




               while (  $io_cd_uorg_destino == $io_cd_uorg_destinoOpcaoAnterior ||
                        $io_cd_uorg_destino == $inscricao->ic_fk_vg_uorg_origem)
               {
                   //$io_cd_uorg_destino = $faker->randomElement($vaga);
                   $io_cd_uorg_destino = $this->_getDestinoProvavel($io_cd_uorg_destinoOpcaoAnterior);


               }
                $io_cd_uorg_destinoOpcaoAnterior = $io_cd_uorg_destino;

                $inscricaoOpcaoSalva =
                $this->inscricaoOpcao->add(
                    [

                        'io_fk_ic_inscricao' => $inscricao->ic_id_inscricao,
          //              'io_cd_vg_uorg_origem' => $inscricao->ic_fk_vg_uorg_origem, // preenchido pelo metodo add
                        'io_cd_uorg_destino' => $io_cd_uorg_destino,
                        'io_vl_ordem_preferencia' => $ordemOpcao,
                        'io_st_contemplado' => null,
                        'io_st_computado' => 0
                    ]);

                if ($inscricaoOpcaoSalva)
                {
                    echo "Registrando Opcao número nº# " . $ordemOpcao . " do candidato codigo " . $inscricao->ic_id_inscricao . "\r";
                }else
                {
                    echo "Impossivel Salvar Esta Opcao da Inscricao id.# ". $inscricao->ic_id_inscricao. " dada a incompatibilidade ". "\r\n";
                }

            }
        }
        echo "#####################################################################################" . "\r\n";
        $this->command->info('Todos os registros de Inscricao Opcao foram lançados. ');
    }
}


