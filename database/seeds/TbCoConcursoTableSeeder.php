<?php

// Composer: "fzaninotto/faker": "v1.4.0"
use Faker\Factory as Faker;
use Illuminate\Database\Seeder;
use App\Models\Concurso\Tb_co_concurso as Tb_co_concurso;
use App\ExecutaConcurso\ConfiguracaoConcurso as ConfiguracaoConcurso;
use App\Funcoes\Funcoes as Funcoes;

class TbCoConcursoTableSeeder extends Seeder {

	public function run()
	{
		$faker = Faker::create();

		for($index = 1 ; $index <= 10 ; $index += 1)

		{
			$inicioinscricoes =  $faker->dateTimeThisDecade($max = 'now');
			$cronogramaConcurso =  ConfiguracaoConcurso::prazosDefaultConcursos($inicioinscricoes);
			Tb_co_concurso::unguard();
			Tb_Co_Concurso::create([
				'co_dt_publicacao_edital' => 			date($cronogramaConcurso['publicacaoEdital']),
                'co_dt_inicio_inscricoes'=>       	  	date($cronogramaConcurso['inicioInscricoes']),
				'co_dt_fim_inscricoes'=>       	  		date($cronogramaConcurso['fimInscricoes']),
				'co_dt_data_execucao'=> 			    date($cronogramaConcurso['dataExecucao']),
				'co_vl_dados_vagas' => $faker->paragraph($nbSentences = 100)
							]);
			$this->command->info('inserindo registro no. #'. $index);
		}
	}

}
