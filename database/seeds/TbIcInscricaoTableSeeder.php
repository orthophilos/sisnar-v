<?php

// Composer: "fzaninotto/faker": "v1.4.0"
use Faker\Factory as Faker;
use Illuminate\Database\Seeder;
use App\Models\Servidor\ServidorRepository as Servidor;
use App\Models\Inscricao\InscricaoRepository as Inscricao;
use App\Models\Servidor\Tb_sd_servidor as Tb_sd_servidor;
// use App\Models\Vaga\Tb_vg_vaga as Tb_vg_vaga;
use App\Models\Inscricao\Tb_ic_inscricao as Tb_ic_inscricao;


class TbIcInscricaoTableSeeder extends Seeder {

protected  $servidor;
protected  $inscricao ;
	public function __construct(  Inscricao $inscricao , Servidor $servidor )
	{
		$this->servidor = $servidor;
        $this->inscricao = $inscricao;
	}

    private function _getInscricoesProvaveis()
    {
        $faker = Faker::create();

// considerando que se inscrevera os prfs com ate 7 anos de prf.
        $tempoServidores =
            Tb_ic_inscricao::select(DB::raw(' 1  - ic_vl_dias_desd_ini_serv / 3650 as probabilidade'), 'ic_id_inscricao' )
                       ->get();

        $paraApagar = [];
        foreach ($tempoServidores  as $tempoServidor)
        {


            if ($faker->boolean( 100 - (round($tempoServidor->probabilidade * 100))))
            {
                echo "\r\n". "Selecionando  a inscricao de id ".$tempoServidor->ic_id_inscricao . " para ser apagada ";
                array_push($paraApagar,$tempoServidor->ic_id_inscricao);
            }
        }
        echo "\r\n". "Apagando as inscricoes improváveis" ;
        Tb_ic_inscricao::whereIn('ic_id_inscricao', $paraApagar)->forceDelete();

    }

	public function run()
	{

		$faker = Faker::create();

        $servidores =  Tb_sd_servidor::all()->lists('sd_id_servidor');
       // $vaga = Tb_vg_vaga::all()->lists('vg_id_uorg');
        $index = 1;

        //	Tb_ic_inscricao::unguard();
        echo "iniciando preenchimento fake de inscricoes com dados reais dos servidores". "\r\n";
        foreach($servidores as $servidor)
        {
            $InscricaoFake = $this->inscricao->add($servidor);
           // dd($InscricaoFake, $InscricaoFake->ic_id_inscricao  , method_exists($InscricaoFake,'ic_id_inscricao'));
            if
            ($faker->boolean(80) && !empty ($InscricaoFake->ic_id_inscricao)){
               // echo "parou em  confirma inscricao";
                $this->inscricao->confirmaInscricao($InscricaoFake->ic_id_inscricao);
                //echo "<<<<<<<<<<<<<<<<<<<< mentira";
            }

            $index ++;
            echo "Inserindo inscricao fake nº".$index. "\n";
		}
        echo "\r\n";
        echo "################################################################################"."\r\n";
        $this->command->info('todas as inscricoes fakes foram criadas');
        sleep(1);

        echo "Analisando quais são as inscricoes mais prováveis de existir realmente"."\r\n";
        echo "################################################################################"."\r\n";
        $this->_getInscricoesProvaveis();
    }

}
