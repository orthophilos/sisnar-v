<?php

// Composer: "fzaninotto/faker": "v1.4.0"
use Faker\Factory as Faker;
use Illuminate\Database\Seeder;
use PRF\RH;
use PRF\Servo;
use App\Funcoes\Funcoes as Funcoes;
use App\Models\Vaga\Tb_vg_vaga as Vaga;

class TbTesteTableSeeder extends Seeder {


	public function __construct()
	{
		// parent::__construct();
		$this->rh = new RH();
		$this->servo = new Servo();
        $this->funcao = new Funcoes();
	}

	public function run()
	{
		Vaga::unguard();
        $faker = Faker::create();
     //  dd($this->rh->getByNome(' Jeferson Tadeu de Souza'));
        $todasUorgs = $this->getUorgs();
		// dd($todasUorgs);
		// loop pega todos os $todoRH[n]['unidadeOrganizacional']'[codigoSiape'] e $todoRH[n]['unidadeOrganizacional']['nome']
        echo "\r\n";
        foreach ($todasUorgs as $contador => $uorg)
		{
			$vaga = new Vaga();

			$vaga->vg_id_uorg = $uorg['id'];
			$vaga->vg_no_nome = $uorg['nome'];
            $vaga->vg_ed_uf = $uorg['uf']; // colcar um case nulo entao df.
			$vaga->vg_vl_vagas_preestabelecidas = $faker->randomDigit; //0; // colocar random
			$vaga->vg_vl_vagas_ganhas = 1 ;
			$vaga->vg_vl_vagas_perdidas  = 1;
            $vaga->pai = $uorg['pai'];
            $vaga->id_pai = $uorg['id_pai'];

            echo 'Inserindo UORG , rec #'.$contador."#\r";
            $vaga->save();

		}
        echo "\r\n";

	}

	private function getUorgs()
	{
		$this->command->info('Pegando as  UORGs do RH');
//		$todosRH = $this->rh->getByMatricula('2195337');
//		dd($todosRH);
		$todosRH = $this->rh->getTodosUsuarios();
//		dd($todosRH);
        $this->command->info('passou..');

        // retirando matricula em branco
        $todosRH  = $this->funcao->removeElementWithValue($todosRH, "matricula", "");
        $todosRH  = $this->funcao->removeElementWithValue($todosRH, "matricula", "0000000");




		// loop pega todos os $todoRH[n]['unidadeOrganizacional']'[codigoSiape'] e $todoRH[n]['unidadeOrganizacional']['nome']
		$uorgs = [];
        $preuorgs = $this->funcao->unique_multidim_array($todosRH,'idUnidadeOrganizacional');
		$contador = 0;
		foreach ($preuorgs as  $preuorg)
		{

            echo 'Buscando por matricula #'.$preuorg['matricula']."#\r";
            $servidor = $this->rh->getByMatricula($preuorg['matricula']);  // getByNome($preuorg['nomeCompleto']);
            $uorgs[$contador]['id'] = $servidor[0]['unidadeOrganizacional']['id']; //$servidor[0]['unidadeOrganizacional']['unidadePai']['codigoSiape'];
			$uorgs[$contador]['nome'] = $servidor[0]['unidadeOrganizacional']['nome']; //$servidor[0]['unidadeOrganizacional']['unidadePai']['nome'];
			$uorgs[$contador]['uf'] = $servidor[0]['unidadeOrganizacional']['cidade']['uf']['sigla']; //$servidor[0]['unidadeOrganizacional']['unidadePai']['cidade']['uf']['sigla'];
			$uorgs[$contador]['pai'] = $servidor[0]['unidadeOrganizacional']['unidadePai']['nome'];
			$uorgs[$contador]['id_pai'] = $servidor[0]['unidadeOrganizacional']['unidadePai']['codigoSiape'];

			echo 'Recebendo dados de UORG , rec #'.$contador."\r";
			$contador++;
		}
        $uorgs  = $this->funcao->removeElementWithValue($uorgs, "uf", "");
        return array_unique($uorgs, SORT_REGULAR);

	}

}
