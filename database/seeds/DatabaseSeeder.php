<?php

use Illuminate\Database\Seeder;
//use Illuminate\Database\Eloquent\Model;
class DatabaseSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run() 
	{
        // rodando apenas dados reais
        Eloquent::unguard();
	 //   $this->call('TbSpSiapeTableSeeder');
            $this->call('TbSdServidorTableSeeder');
	 //   $this->call('TbVgVagaTableSeeder');
	  //  $this->call('TbCoConcursoTableSeeder');
            $this->call('TbIcInscricaoTableSeeder');
            $this->call('TbIoInscricaoOpcaoTableSeeder');




        $this->command->info('');

    }

}
