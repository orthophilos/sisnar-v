<?php

use Illuminate\Database\Seeder;

class TbSpSiapeTableSeeder extends Seeder {

	/**
	 * Auto generated seed file
	 *
	 * @return void
	 */
	public function run()
	{
		\DB::table('tb_sp_siape')->delete();
        
		\DB::table('tb_sp_siape')->insert(array (
			0 => 
			array (
				'sp_id_siape' => '1',
				'sp_cd_id_webservice' => '325',
				'sp_no_nome' => '4ª SRPRF/MG - DEL. 17 - NÚCLEO DE POLICIAMENTO E FISCALIZAÇÃO',
				'sp_fk_vg_vaga' => '301',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			1 => 
			array (
				'sp_id_siape' => '2',
				'sp_cd_id_webservice' => '237',
				'sp_no_nome' => '4ª SUPERINTENDÊNCIA DE POLÍCIA RODOVIÁRIA FEDERAL/MG',
				'sp_fk_vg_vaga' => '250',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			2 => 
			array (
				'sp_id_siape' => '3',
				'sp_cd_id_webservice' => '473',
				'sp_no_nome' => '7ª SRPRF/PR - DEL. 01 - DELEGACIA METROPOLITANA - COLOMBO',
				'sp_fk_vg_vaga' => '473',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			3 => 
			array (
				'sp_id_siape' => '4',
				'sp_cd_id_webservice' => '547',
				'sp_no_nome' => '8ª SRPRF/SC - DEL. 01 - DELEGACIA METROPOLITANA DE SÃO JOSÉ',
				'sp_fk_vg_vaga' => '2547',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			4 => 
			array (
				'sp_id_siape' => '5',
				'sp_cd_id_webservice' => '358',
				'sp_no_nome' => '5ª SRPRF/RJ - DEL. 02 - NÚCLEO DE POLICIAMENTO E FISCALIZAÇÃO',
				'sp_fk_vg_vaga' => '341',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			5 => 
			array (
				'sp_id_siape' => '6',
				'sp_cd_id_webservice' => '497',
				'sp_no_nome' => '7ª SRPRF/PR - DEL. 04 - DELEGACIA DE CASCAVEL',
				'sp_fk_vg_vaga' => '445',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			6 => 
			array (
				'sp_id_siape' => '7',
				'sp_cd_id_webservice' => '975',
				'sp_no_nome' => '17ª SRPRF/PI - SEÇÃO DE POLICIAMENTO E FISCALIZAÇÃO',
				'sp_fk_vg_vaga' => '880',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			7 => 
			array (
				'sp_id_siape' => '8',
				'sp_cd_id_webservice' => '1043',
				'sp_no_nome' => '18ª SRPRF/MA - DEL. 04 - DELEGACIA DE IMPERATRIZ',
				'sp_fk_vg_vaga' => '1043',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			8 => 
			array (
				'sp_id_siape' => '9',
				'sp_cd_id_webservice' => '688',
				'sp_no_nome' => '10ª SRPRF/BA - NÚCLEO DE PATRIMÔNIO E MATERIAL',
				'sp_fk_vg_vaga' => '580',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			9 => 
			array (
				'sp_id_siape' => '10',
				'sp_cd_id_webservice' => '650',
				'sp_no_nome' => '9ª SRPRF/RS - DEL. 09 - DELEGACIA DE SANTA MARIA',
				'sp_fk_vg_vaga' => '555',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			10 => 
			array (
				'sp_id_siape' => '11',
				'sp_cd_id_webservice' => '337',
				'sp_no_nome' => '5ª SRPRF/RJ - SEÇÃO DE POLICIAMENTO E FISCALIZAÇÃO',
				'sp_fk_vg_vaga' => '320',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			11 => 
			array (
				'sp_id_siape' => '12',
				'sp_cd_id_webservice' => '242',
				'sp_no_nome' => '4ª SRPRF/MG - NÚCLEO DE OPERAÇÕES ESPECIAIS',
				'sp_fk_vg_vaga' => '250',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			12 => 
			array (
				'sp_id_siape' => '13',
				'sp_cd_id_webservice' => '639',
				'sp_no_nome' => '9ª SRPRF/RS - DEL. 07 - DELEGACIA DE PELOTAS',
				'sp_fk_vg_vaga' => '551',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			13 => 
			array (
				'sp_id_siape' => '14',
				'sp_cd_id_webservice' => '24',
				'sp_no_nome' => 'DIVISÃO DE PROJETOS DE INFRA-ESTRUTURA',
				'sp_fk_vg_vaga' => '2',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			14 => 
			array (
				'sp_id_siape' => '15',
				'sp_cd_id_webservice' => '10',
				'sp_no_nome' => 'COORDENAÇÃO DE APOIO ADMINISTRATIVO',
				'sp_fk_vg_vaga' => '2',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			15 => 
			array (
				'sp_id_siape' => '16',
				'sp_cd_id_webservice' => '241',
				'sp_no_nome' => '4ª SRPRF/MG - SEÇÃO DE POLICIAMENTO E FISCALIZAÇÃO',
				'sp_fk_vg_vaga' => '250',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			16 => 
			array (
				'sp_id_siape' => '17',
				'sp_cd_id_webservice' => '297',
				'sp_no_nome' => '4ª SRPRF/MG - DEL. 10 - DELEGACIA DE POUSO ALEGRE',
				'sp_fk_vg_vaga' => '287',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			17 => 
			array (
				'sp_id_siape' => '18',
				'sp_cd_id_webservice' => '875',
				'sp_no_nome' => '14ª SRPRF/PB - DEL. 01 - DELEGACIA DE METROPOLITANA',
				'sp_fk_vg_vaga' => '876',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			18 => 
			array (
				'sp_id_siape' => '19',
				'sp_cd_id_webservice' => '161',
				'sp_no_nome' => '2ª SRPRF/MT - DEL. 05 - DELEGACIA DE PRIMAVERA DO LESTE',
				'sp_fk_vg_vaga' => '161',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			19 => 
			array (
				'sp_id_siape' => '20',
				'sp_cd_id_webservice' => '669',
				'sp_no_nome' => '9ª SRPRF/RS - DEL. 13 - DELEGACIA DE URUGUAIANA',
				'sp_fk_vg_vaga' => '563',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			20 => 
			array (
				'sp_id_siape' => '21',
				'sp_cd_id_webservice' => '816',
				'sp_no_nome' => '12ª SRPRF/ES - DEL. 03 - DELEGACIA DE GUARAPARI',
				'sp_fk_vg_vaga' => '816',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			21 => 
			array (
				'sp_id_siape' => '22',
				'sp_cd_id_webservice' => '278',
				'sp_no_nome' => '4ª SRPRF/MG - DEL. 06 - NÚCLEO DE POLICIAMENTO E FISCALIZAÇÃO',
				'sp_fk_vg_vaga' => '277',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			22 => 
			array (
				'sp_id_siape' => '23',
				'sp_cd_id_webservice' => '962',
				'sp_no_nome' => '16ª SRPRF/CE - DEL. 04 - DELEGACIA DE SOBRAL',
				'sp_fk_vg_vaga' => '961',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			23 => 
			array (
				'sp_id_siape' => '24',
				'sp_cd_id_webservice' => '446',
				'sp_no_nome' => '6ª SRPRF/SP - DEL. 09 - DELEGACIA DE SÃO  JOSÉ DO RIO PRETO',
				'sp_fk_vg_vaga' => '446',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			24 => 
			array (
				'sp_id_siape' => '25',
				'sp_cd_id_webservice' => '410',
				'sp_no_nome' => '6ª SRPRF/SP - NÚCLEO DE ASSUNTOS INTERNOS',
				'sp_fk_vg_vaga' => '370',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			25 => 
			array (
				'sp_id_siape' => '26',
				'sp_cd_id_webservice' => '64',
				'sp_no_nome' => 'DIVISÃO DE PATRIMÔNIO',
				'sp_fk_vg_vaga' => '2',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			26 => 
			array (
				'sp_id_siape' => '27',
				'sp_cd_id_webservice' => '418',
				'sp_no_nome' => '6ª SRPRF/SP - DEL. 03 - DELEGACIA DE ATIBAIA',
				'sp_fk_vg_vaga' => '393',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			27 => 
			array (
				'sp_id_siape' => '28',
				'sp_cd_id_webservice' => '96',
				'sp_no_nome' => '1ª SRPRF/GO - DEL. 01 - NÚCLEO DE POLICIAMENTO E FISCALIZAÇÃO',
				'sp_fk_vg_vaga' => '95',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			28 => 
			array (
				'sp_id_siape' => '29',
				'sp_cd_id_webservice' => '350',
				'sp_no_nome' => '5ª SRPRF/RJ - CORREGEDORIA REGIONAL',
				'sp_fk_vg_vaga' => '320',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			29 => 
			array (
				'sp_id_siape' => '30',
				'sp_cd_id_webservice' => '454',
				'sp_no_nome' => '7ª SUPERINTENDÊNCIA REGIONAL DE POLÍCIA RODOVIÁRIA FEDERAL/PR',
				'sp_fk_vg_vaga' => '420',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			30 => 
			array (
				'sp_id_siape' => '31',
				'sp_cd_id_webservice' => '892',
				'sp_no_nome' => '15ª SUPERINTENDÊNCIA REGIONAL DE POLÍCIA RODOVIÁRIA FEDERAL/RN',
				'sp_fk_vg_vaga' => '800',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			31 => 
			array (
				'sp_id_siape' => '32',
				'sp_cd_id_webservice' => '619',
				'sp_no_nome' => '9ª SRPRF/RS - DEL. 03 - DELEGACIA DE OSÓRIO',
				'sp_fk_vg_vaga' => '543',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			32 => 
			array (
				'sp_id_siape' => '33',
				'sp_cd_id_webservice' => '433',
				'sp_no_nome' => '6ª SRPRF/SP - DEL. 06 - DELEGACIA DE TAUBATÉ',
				'sp_fk_vg_vaga' => '433',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			33 => 
			array (
				'sp_id_siape' => '34',
				'sp_cd_id_webservice' => '1149',
				'sp_no_nome' => '21ª SRPRF/RO - DEL. 02 - DELEGACIA DE JI-PARANÁ',
				'sp_fk_vg_vaga' => '1149',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			34 => 
			array (
				'sp_id_siape' => '35',
				'sp_cd_id_webservice' => '711',
				'sp_no_nome' => '10ª SRPRF/BA - DEL. 04 - DELEGACIA DE SENHOR DO BONFIM',
				'sp_fk_vg_vaga' => '605',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			35 => 
			array (
				'sp_id_siape' => '36',
				'sp_cd_id_webservice' => '1002',
				'sp_no_nome' => '17ª SRPRF/PI - DEL. 04 - DELEGACIA DE PICOS',
				'sp_fk_vg_vaga' => '905',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			36 => 
			array (
				'sp_id_siape' => '37',
				'sp_cd_id_webservice' => '339',
				'sp_no_nome' => '5ª SRPRF/RJ - NÚCLEO DE MULTAS E PENALIDADES',
				'sp_fk_vg_vaga' => '320',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			37 => 
			array (
				'sp_id_siape' => '38',
				'sp_cd_id_webservice' => '333',
				'sp_no_nome' => '5ª SUPERINTENDÊNCIA DE POLÍCIA RODOVIÁRIA FEDERAL/RJ',
				'sp_fk_vg_vaga' => '320',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			38 => 
			array (
				'sp_id_siape' => '39',
				'sp_cd_id_webservice' => '907',
				'sp_no_nome' => '15ª SRPRF/RN - NÚCLEO DE LEGISLAÇÃO E CAPACITAÇÃO DE PESSOAL',
				'sp_fk_vg_vaga' => '800',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			39 => 
			array (
				'sp_id_siape' => '40',
				'sp_cd_id_webservice' => '265',
				'sp_no_nome' => '4ª SRPRF/MG - DEL. 03 - NÚCLEO DE POLICIAMENTO E FISCALIZAÇÃO',
				'sp_fk_vg_vaga' => '264',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			40 => 
			array (
				'sp_id_siape' => '41',
				'sp_cd_id_webservice' => '147',
				'sp_no_nome' => '2ª SRPRF/MT - DEL. 02 - DELEGACIA DE RONDONÓPOLIS',
				'sp_fk_vg_vaga' => '147',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			41 => 
			array (
				'sp_id_siape' => '42',
				'sp_cd_id_webservice' => '735',
				'sp_no_nome' => '10ª SRPRF/BA - DEL. 09 - NÚCLEO DE POLICIAMENTO E FISCALIZAÇÃO',
				'sp_fk_vg_vaga' => '615',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			42 => 
			array (
				'sp_id_siape' => '43',
				'sp_cd_id_webservice' => '364',
				'sp_no_nome' => '5ª SRPRF/RJ - DEL. 03 - NÚCLEO DE POLICIAMENTO E FISCALIZAÇÃO',
				'sp_fk_vg_vaga' => '343',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			43 => 
			array (
				'sp_id_siape' => '44',
				'sp_cd_id_webservice' => '345',
				'sp_no_nome' => '5ª SRPRF/RJ - NÚCLEO DE SERVIÇOS GERAIS',
				'sp_fk_vg_vaga' => '320',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			44 => 
			array (
				'sp_id_siape' => '45',
				'sp_cd_id_webservice' => '490',
				'sp_no_nome' => '7ª SRPRF/PR - DEL. 03 - DELEGACIA DE PONTA GROSSA',
				'sp_fk_vg_vaga' => '443',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			45 => 
			array (
				'sp_id_siape' => '46',
				'sp_cd_id_webservice' => '49',
				'sp_no_nome' => 'DIVISÃO DE RECURSOS HUMANOS',
				'sp_fk_vg_vaga' => '2',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			46 => 
			array (
				'sp_id_siape' => '47',
				'sp_cd_id_webservice' => '388',
				'sp_no_nome' => '5ª SRPRF/RJ - DEL. 08 - NÚCLEO DE POLICIAMENTO E FISCALIZAÇÃO',
				'sp_fk_vg_vaga' => '357',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			47 => 
			array (
				'sp_id_siape' => '48',
				'sp_cd_id_webservice' => '721',
				'sp_no_nome' => '10ª SRPRF/BA - DEL. 06 - NÚCLEO DE POLICIAMENTO E FISCALIZAÇÃO',
				'sp_fk_vg_vaga' => '720',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			48 => 
			array (
				'sp_id_siape' => '49',
				'sp_cd_id_webservice' => '357',
				'sp_no_nome' => '5ª SRPRF/RJ - DEL. 02 - DELEGACIA DE NITERÓI',
				'sp_fk_vg_vaga' => '341',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			49 => 
			array (
				'sp_id_siape' => '50',
				'sp_cd_id_webservice' => '256',
				'sp_no_nome' => '4ª SRPRF/MG - DEL. 01 - DELEGACIA DE METROPOLITANA',
				'sp_fk_vg_vaga' => '256',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			50 => 
			array (
				'sp_id_siape' => '51',
				'sp_cd_id_webservice' => '1203',
				'sp_no_nome' => '5º DRPRF/RR - NÚCLEO DE POLICIAMENTO E FISCALIZAÇÃO',
				'sp_fk_vg_vaga' => '1120',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			51 => 
			array (
				'sp_id_siape' => '52',
				'sp_cd_id_webservice' => '1129',
				'sp_no_nome' => '21ª SRPRF/RO - NÚCLEO DE INTELIGÊNCIA',
				'sp_fk_vg_vaga' => '1040',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			52 => 
			array (
				'sp_id_siape' => '53',
				'sp_cd_id_webservice' => '16',
				'sp_no_nome' => 'DIVISÃO DE GESTÃO CORREICIONAL',
				'sp_fk_vg_vaga' => '2',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			53 => 
			array (
				'sp_id_siape' => '54',
				'sp_cd_id_webservice' => '7',
				'sp_no_nome' => 'COORDENAÇÃO DE INTELIGÊNCIA',
				'sp_fk_vg_vaga' => '2',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			54 => 
			array (
				'sp_id_siape' => '55',
				'sp_cd_id_webservice' => '1139',
				'sp_no_nome' => '21ª SRPRF/RO - NÚCLEO DE INFORMÁTICA E TELECOMUNICAÇÕES',
				'sp_fk_vg_vaga' => '1040',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			55 => 
			array (
				'sp_id_siape' => '56',
				'sp_cd_id_webservice' => '192',
				'sp_no_nome' => '3ª SRPRF/MS - NÚCLEO DE ADMINISTRAÇÃO DE PESSOAL',
				'sp_fk_vg_vaga' => '200',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			56 => 
			array (
				'sp_id_siape' => '57',
				'sp_cd_id_webservice' => '647',
				'sp_no_nome' => '9ª SRPRF/RS - DEL. 08 - DELEGACIA DE PASSO FUNDO',
				'sp_fk_vg_vaga' => '646',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			57 => 
			array (
				'sp_id_siape' => '58',
				'sp_cd_id_webservice' => '289',
				'sp_no_nome' => '4ª SRPRF/MG - DEL. 08 - NÚCLEO DE POLICIAMENTO E FISCALIZAÇÃO',
				'sp_fk_vg_vaga' => '283',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			58 => 
			array (
				'sp_id_siape' => '59',
				'sp_cd_id_webservice' => '593',
				'sp_no_nome' => '9ª SRPRF/RS - SEÇÃO DE POLICIAMENTO E FISCALIZAÇÃO',
				'sp_fk_vg_vaga' => '520',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			59 => 
			array (
				'sp_id_siape' => '60',
				'sp_cd_id_webservice' => '353',
				'sp_no_nome' => '5ª SRPRF/RJ - DEL. 01 - NÚCLEO DE POLICIAMENTO E FISCALIZAÇÃO',
				'sp_fk_vg_vaga' => '339',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			60 => 
			array (
				'sp_id_siape' => '61',
				'sp_cd_id_webservice' => '1102',
				'sp_no_nome' => '20ª SRPRF/SE - SEÇÃO DE POLICIAMENTO E FISCALIZAÇÃO',
				'sp_fk_vg_vaga' => '1000',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			61 => 
			array (
				'sp_id_siape' => '62',
				'sp_cd_id_webservice' => '1180',
				'sp_no_nome' => '2º DRPRF/TO - NÚCLEO DE POLICIAMENTO E FISCALIZAÇÃO',
				'sp_fk_vg_vaga' => '1174',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			62 => 
			array (
				'sp_id_siape' => '63',
				'sp_cd_id_webservice' => '383',
				'sp_no_nome' => '5ª SRPRF/RJ - DEL. 07 - NÚCLEO DE POLICIAMENTO E FISCALIZAÇÃO',
				'sp_fk_vg_vaga' => '355',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			63 => 
			array (
				'sp_id_siape' => '64',
				'sp_cd_id_webservice' => '687',
				'sp_no_nome' => '10ª SRPRF/BA - NÚCLEO DE ORÇAMENTO E FINANÇAS',
				'sp_fk_vg_vaga' => '580',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			64 => 
			array (
				'sp_id_siape' => '65',
				'sp_cd_id_webservice' => '1051',
				'sp_no_nome' => '19ª SUPERINTENDÊNCIA REGIONAL DE POLÍCIA RODOVIÁRIA FEDERAL/PA',
				'sp_fk_vg_vaga' => '960',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			65 => 
			array (
				'sp_id_siape' => '66',
				'sp_cd_id_webservice' => '1000',
				'sp_no_nome' => '17ª SRPRF/PI - DEL. 03 - NÚCLEO DE POLICIAMENTO E FISCALIZAÇÃO',
				'sp_fk_vg_vaga' => '903',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			66 => 
			array (
				'sp_id_siape' => '67',
				'sp_cd_id_webservice' => '557',
				'sp_no_nome' => '8ª SRPRF/SC - DEL. 03 - DELEGACIA METROPOLITANA DE JOINVILLE',
				'sp_fk_vg_vaga' => '493',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			67 => 
			array (
				'sp_id_siape' => '68',
				'sp_cd_id_webservice' => '1121',
				'sp_no_nome' => '20ª SRPRF/SE - DEL. 02 - DELEGACIA DE SÃO CRISTOVÃO',
				'sp_fk_vg_vaga' => '1021',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			68 => 
			array (
				'sp_id_siape' => '69',
				'sp_cd_id_webservice' => '597',
				'sp_no_nome' => '9ª SRPRF/RS - SEÇÃO ADMINISTRATIVA E FINANCEIRA',
				'sp_fk_vg_vaga' => '520',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			69 => 
			array (
				'sp_id_siape' => '70',
				'sp_cd_id_webservice' => '1170',
				'sp_no_nome' => '1º DRPRF/DF - NÚCLEO DE POLICIAMENTO E FISCALIZAÇÃO',
				'sp_fk_vg_vaga' => '1080',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			70 => 
			array (
				'sp_id_siape' => '71',
				'sp_cd_id_webservice' => '860',
				'sp_no_nome' => '14ª SRPRF/PB - SEÇÃO DE POLICIAMENTO E FISCALIZAÇÃO',
				'sp_fk_vg_vaga' => '760',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			71 => 
			array (
				'sp_id_siape' => '72',
				'sp_cd_id_webservice' => '624',
				'sp_no_nome' => '9ª SRPRF/RS - DEL. 04 - DELEGACIA DE LAJEADO',
				'sp_fk_vg_vaga' => '624',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			72 => 
			array (
				'sp_id_siape' => '73',
				'sp_cd_id_webservice' => '1126',
				'sp_no_nome' => '21ª SUPERINTENDÊNCIA REGIONAL DE POLÍCIA RODOVIÁRIA FEDERAL/RO',
				'sp_fk_vg_vaga' => '1040',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			73 => 
			array (
				'sp_id_siape' => '74',
				'sp_cd_id_webservice' => '343',
				'sp_no_nome' => '5ª SRPRF/RJ - NÚCLEO DE PATRIMÔNIO E MATERIAL',
				'sp_fk_vg_vaga' => '320',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			74 => 
			array (
				'sp_id_siape' => '75',
				'sp_cd_id_webservice' => '310',
				'sp_no_nome' => '4ª SRPRF/MG - DEL. 13 - NÚCLEO DE POLICIAMENTO E FISCALIZAÇÃO',
				'sp_fk_vg_vaga' => '293',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			75 => 
			array (
				'sp_id_siape' => '76',
				'sp_cd_id_webservice' => '76',
				'sp_no_nome' => '1ª SUPERINTENDÊNCIA DE POLÍCIA RODOVIÁRIA FEDERAL/GO',
				'sp_fk_vg_vaga' => '100',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			76 => 
			array (
				'sp_id_siape' => '77',
				'sp_cd_id_webservice' => '27',
				'sp_no_nome' => 'COORDENAÇÃO DE CONTROLE OPERACIONAL',
				'sp_fk_vg_vaga' => '2',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			77 => 
			array (
				'sp_id_siape' => '78',
				'sp_cd_id_webservice' => '172',
				'sp_no_nome' => '2ª SRPRF/MT - DEL. 08 - DELEGACIA DE BARRA DO GARÇAS',
				'sp_fk_vg_vaga' => '173',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			78 => 
			array (
				'sp_id_siape' => '79',
				'sp_cd_id_webservice' => '262',
				'sp_no_nome' => '4ª SRPRF/MG - DEL. 02 - NÚCLEO DE POLICIAMENTO E FISCALIZAÇÃO',
				'sp_fk_vg_vaga' => '261',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			79 => 
			array (
				'sp_id_siape' => '80',
				'sp_cd_id_webservice' => '1153',
				'sp_no_nome' => '21ª SRPRF/RO - DEL. 03 - DELEGACIA DE ARIQUEMES',
				'sp_fk_vg_vaga' => '1063',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			80 => 
			array (
				'sp_id_siape' => '81',
				'sp_cd_id_webservice' => '30',
				'sp_no_nome' => 'NÚCLEO DE ESTATÍSTICA',
				'sp_fk_vg_vaga' => '2',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			81 => 
			array (
				'sp_id_siape' => '82',
				'sp_cd_id_webservice' => '703',
				'sp_no_nome' => '10ª SRPRF/BA - DEL. 02 - DELEGACIA DE FEIRA DE SANTANA',
				'sp_fk_vg_vaga' => '601',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			82 => 
			array (
				'sp_id_siape' => '83',
				'sp_cd_id_webservice' => '1145',
				'sp_no_nome' => '21ª SRPRF/RO - DEL. 01 - DELEGACIA DE PORTO VELHO',
				'sp_fk_vg_vaga' => '1045',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			83 => 
			array (
				'sp_id_siape' => '84',
				'sp_cd_id_webservice' => '971',
				'sp_no_nome' => '17ª SUPERINTENDÊNCIA REGIONAL DE POLÍCIA RODOVIÁRIA FEDERAL/PI',
				'sp_fk_vg_vaga' => '880',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			84 => 
			array (
				'sp_id_siape' => '85',
				'sp_cd_id_webservice' => '1161',
				'sp_no_nome' => '21ª SRPRF/RO - DEL. 05 - DELEGACIA DE RIO BRANCO',
				'sp_fk_vg_vaga' => '1067',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			85 => 
			array (
				'sp_id_siape' => '86',
				'sp_cd_id_webservice' => '1034',
				'sp_no_nome' => '18ª SRPRF/MA - DEL. 02 - DELEGACIA DE SANTA INÊS',
				'sp_fk_vg_vaga' => '1034',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			86 => 
			array (
				'sp_id_siape' => '87',
				'sp_cd_id_webservice' => '927',
				'sp_no_nome' => '16ª SUPERINTENDÊNCIA REGIONAL DE POLÍCIA RODOVIÁRIA FEDERAL/CE',
				'sp_fk_vg_vaga' => '840',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			87 => 
			array (
				'sp_id_siape' => '88',
				'sp_cd_id_webservice' => '42',
				'sp_no_nome' => 'COORDENAÇÃO GERAL DE RECURSOS HUMANOS',
				'sp_fk_vg_vaga' => '2',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			88 => 
			array (
				'sp_id_siape' => '89',
				'sp_cd_id_webservice' => '1084',
				'sp_no_nome' => '19ª SRPRF/PA - DEL. 03 - DELEGACIA DE MARABÁ',
				'sp_fk_vg_vaga' => '983',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			89 => 
			array (
				'sp_id_siape' => '90',
				'sp_cd_id_webservice' => '730',
				'sp_no_nome' => '10ª SRPRF/BA - DEL. 08 - NÚCLEO DE POLICIAMENTO E FISCALIZAÇÃO',
				'sp_fk_vg_vaga' => '613',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			90 => 
			array (
				'sp_id_siape' => '91',
				'sp_cd_id_webservice' => '243',
				'sp_no_nome' => '4ª SRPRF/MG - NÚCLEO DE MULTAS E PENALIDADES',
				'sp_fk_vg_vaga' => '250',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			91 => 
			array (
				'sp_id_siape' => '92',
				'sp_cd_id_webservice' => '865',
				'sp_no_nome' => '14ª SRPRF/PB - NÚCLEO DE ORÇAMENTO E FINANÇAS',
				'sp_fk_vg_vaga' => '760',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			92 => 
			array (
				'sp_id_siape' => '93',
				'sp_cd_id_webservice' => '726',
				'sp_no_nome' => '10ª SRPRF/BA - DEL. 07 - NÚCLEO DE POLICIAMENTO E FISCALIZAÇÃO',
				'sp_fk_vg_vaga' => '725',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			93 => 
			array (
				'sp_id_siape' => '94',
				'sp_cd_id_webservice' => '678',
				'sp_no_nome' => '10ª SUPERINTENDÊNCIA REGIONAL DE POLÍCIA RODOVIÁRIA FEDERAL/BA',
				'sp_fk_vg_vaga' => '580',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			94 => 
			array (
				'sp_id_siape' => '95',
				'sp_cd_id_webservice' => '686',
				'sp_no_nome' => '10ª SRPRF/BA - SEÇÃO ADMINISTRATIVA E FINANCEIRA',
				'sp_fk_vg_vaga' => '580',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			95 => 
			array (
				'sp_id_siape' => '96',
				'sp_cd_id_webservice' => '505',
				'sp_no_nome' => '7ª SRPRF/PR - DEL. 05 - DELEGACIA DE FOZ DO IGUAÇU',
				'sp_fk_vg_vaga' => '447',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			96 => 
			array (
				'sp_id_siape' => '97',
				'sp_cd_id_webservice' => '393',
				'sp_no_nome' => '6ª SRPRF/SP - NÚCLEO DE APOIO TÉCNICO',
				'sp_fk_vg_vaga' => '370',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			97 => 
			array (
				'sp_id_siape' => '98',
				'sp_cd_id_webservice' => '1197',
				'sp_no_nome' => '4º DRPRF/AP - NÚCLEO DE POLICIAMENTO E FISCALIZAÇÃO',
				'sp_fk_vg_vaga' => '1110',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			98 => 
			array (
				'sp_id_siape' => '99',
				'sp_cd_id_webservice' => '951',
				'sp_no_nome' => '16ª SRPRF/CE - DEL. 02 - DELEGACIA DE CANINDÉ',
				'sp_fk_vg_vaga' => '951',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			99 => 
			array (
				'sp_id_siape' => '100',
				'sp_cd_id_webservice' => '795',
				'sp_no_nome' => '12ª SRPRF/ES - NÚCLEO DE OPERAÇÕES ESPECIAIS',
				'sp_fk_vg_vaga' => '689',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			100 => 
			array (
				'sp_id_siape' => '101',
				'sp_cd_id_webservice' => '313',
				'sp_no_nome' => '4ª SRPRF/MG - DEL. 14 - NÚCLEO DE POLICIAMENTO E FISCALIZAÇÃO',
				'sp_fk_vg_vaga' => '295',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			101 => 
			array (
				'sp_id_siape' => '102',
				'sp_cd_id_webservice' => '946',
				'sp_no_nome' => '16ª SRPRF/CE - DEL. 01 - DELEGACIA DE CAUCAIA',
				'sp_fk_vg_vaga' => '946',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			102 => 
			array (
				'sp_id_siape' => '103',
				'sp_cd_id_webservice' => '931',
				'sp_no_nome' => '16ª SRPRF/CE - SEÇÃO DE POLICIAMENTO E FISCALIZAÇÃO',
				'sp_fk_vg_vaga' => '840',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			103 => 
			array (
				'sp_id_siape' => '104',
				'sp_cd_id_webservice' => '1201',
				'sp_no_nome' => '5º DISTRITO REGIONAL DE POLÍCIA RODOVIÁRIA FEDERAL/RR',
				'sp_fk_vg_vaga' => '1120',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			104 => 
			array (
				'sp_id_siape' => '105',
				'sp_cd_id_webservice' => '715',
				'sp_no_nome' => '10ª SRPRF/BA - DEL. 05 - DELEGACIA DE ITABUNA',
				'sp_fk_vg_vaga' => '715',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			105 => 
			array (
				'sp_id_siape' => '106',
				'sp_cd_id_webservice' => '729',
				'sp_no_nome' => '10ª SRPRF/BA - DEL. 08 - DELEGACIA DE VITÓRIA DA CONQUISTA',
				'sp_fk_vg_vaga' => '613',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			106 => 
			array (
				'sp_id_siape' => '107',
				'sp_cd_id_webservice' => '319',
				'sp_no_nome' => '4ª SRPRF/MG - DEL. 16 - DELEGACIA DE PARACATU',
				'sp_fk_vg_vaga' => '299',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			107 => 
			array (
				'sp_id_siape' => '108',
				'sp_cd_id_webservice' => '93',
				'sp_no_nome' => '1ª SRPRF/GO - SEÇÃO DE CORREGEDORIA REGIONAL',
				'sp_fk_vg_vaga' => '100',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			108 => 
			array (
				'sp_id_siape' => '109',
				'sp_cd_id_webservice' => '1010',
				'sp_no_nome' => '18ª SUPERINTENDÊNCIA REGIONAL DE POLÍCIA RODOVIÁRIA FEDERAL/MA',
				'sp_fk_vg_vaga' => '920',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			109 => 
			array (
				'sp_id_siape' => '110',
				'sp_cd_id_webservice' => '108',
				'sp_no_nome' => '1ª SRPRF/GO - DEL. 04 - DELEGACIA DE RIO VERDE',
				'sp_fk_vg_vaga' => '125',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			110 => 
			array (
				'sp_id_siape' => '111',
				'sp_cd_id_webservice' => '682',
				'sp_no_nome' => '10ª SRPRF/BA - SEÇÃO DE POLICIAMENTO E FISCALIZAÇÃO',
				'sp_fk_vg_vaga' => '580',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			111 => 
			array (
				'sp_id_siape' => '112',
				'sp_cd_id_webservice' => '635',
				'sp_no_nome' => '9ª SRPRF/RS - DEL. 06 - DELEGACIA DE VACARIA',
				'sp_fk_vg_vaga' => '549',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			112 => 
			array (
				'sp_id_siape' => '113',
				'sp_cd_id_webservice' => '741',
				'sp_no_nome' => '10ª SRPRF/BA - DEL. 10 - NÚCLEO DE POLICIAMENTO E FISCALIZAÇÃO',
				'sp_fk_vg_vaga' => '617',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			113 => 
			array (
				'sp_id_siape' => '114',
				'sp_cd_id_webservice' => '613',
				'sp_no_nome' => '9ª SRPRF/RS - DEL. 02 - DELEGACIA DE ELDORADO DO SUL',
				'sp_fk_vg_vaga' => '541',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			114 => 
			array (
				'sp_id_siape' => '115',
				'sp_cd_id_webservice' => '284',
				'sp_no_nome' => '4ª SRPRF/MG - DEL. 07 - NÚCLEO DE POLICIAMENTO E FISCALIZAÇÃO',
				'sp_fk_vg_vaga' => '281',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			115 => 
			array (
				'sp_id_siape' => '116',
				'sp_cd_id_webservice' => '743',
				'sp_no_nome' => '11ª SUPERINTENDÊNCIA REGIONAL DE POLÍCIA RODOVIÁRIA FEDERAL/PE',
				'sp_fk_vg_vaga' => '630',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			116 => 
			array (
				'sp_id_siape' => '117',
				'sp_cd_id_webservice' => '320',
				'sp_no_nome' => '4ª SRPRF/MG - DEL. 16 - NÚCLEO DE POLICIAMENTO E FISCALIZAÇÃO',
				'sp_fk_vg_vaga' => '299',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			117 => 
			array (
				'sp_id_siape' => '118',
				'sp_cd_id_webservice' => '1098',
				'sp_no_nome' => '20ª SUPERINTENDÊNCIA REGIONAL DE POLÍCIA RODOVIÁRIA FEDERAL/SE',
				'sp_fk_vg_vaga' => '1000',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			118 => 
			array (
				'sp_id_siape' => '119',
				'sp_cd_id_webservice' => '695',
				'sp_no_nome' => '10ª SRPRF/BA - CORREGEDORIA REGIONAL',
				'sp_fk_vg_vaga' => '580',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			119 => 
			array (
				'sp_id_siape' => '120',
				'sp_cd_id_webservice' => '80',
				'sp_no_nome' => '1ª SRPRF/GO - SEÇÃO DE POLICIAMENTO E FISCALIZAÇÃO',
				'sp_fk_vg_vaga' => '100',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			120 => 
			array (
				'sp_id_siape' => '121',
				'sp_cd_id_webservice' => '655',
				'sp_no_nome' => '9ª SRPRF/RS - DEL. 10 - DELEGACIA DE IJUÍ',
				'sp_fk_vg_vaga' => '557',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			121 => 
			array (
				'sp_id_siape' => '122',
				'sp_cd_id_webservice' => '1117',
				'sp_no_nome' => '20ª SRPRF/SE - DEL. 01 - DELEGACIA DE NOSSA SENHORA DO SOCORRO',
				'sp_fk_vg_vaga' => '1019',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			122 => 
			array (
				'sp_id_siape' => '123',
				'sp_cd_id_webservice' => '58',
				'sp_no_nome' => 'DIVISÃO DE GESTÃO DOCUMENTAL E NORMATIZAÇÃO',
				'sp_fk_vg_vaga' => '2',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			123 => 
			array (
				'sp_id_siape' => '124',
				'sp_cd_id_webservice' => '120',
				'sp_no_nome' => '1ª SRPRF/GO - DEL. 07 - DELEGACIA DE PORANGATU',
				'sp_fk_vg_vaga' => '120',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			124 => 
			array (
				'sp_id_siape' => '125',
				'sp_cd_id_webservice' => '176',
				'sp_no_nome' => '3ª SUPERINTENDÊNCIA DE POLÍCIA RODOVIÁRIA FEDERAL/MS',
				'sp_fk_vg_vaga' => '200',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			125 => 
			array (
				'sp_id_siape' => '126',
				'sp_cd_id_webservice' => '222',
				'sp_no_nome' => '3ª SRPRF/MS - DEL. 07 - DELEGACIA DE BATAGUASSU',
				'sp_fk_vg_vaga' => '222',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			126 => 
			array (
				'sp_id_siape' => '127',
				'sp_cd_id_webservice' => '704',
				'sp_no_nome' => '10ª SRPRF/BA - DEL. 02 - NÚCLEO DE POLICIAMENTO E FISCALIZAÇÃO',
				'sp_fk_vg_vaga' => '601',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			127 => 
			array (
				'sp_id_siape' => '128',
				'sp_cd_id_webservice' => '517',
				'sp_no_nome' => '7ª SRPRF/PR - DEL. 07 - DELEGACIA DE LONDRINA',
				'sp_fk_vg_vaga' => '451',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			128 => 
			array (
				'sp_id_siape' => '129',
				'sp_cd_id_webservice' => '101',
				'sp_no_nome' => '1ª SRPRF/GO - DEL. 02 - NÚCLEO DE POLICIAMENTO E FISCALIZAÇÃO',
				'sp_fk_vg_vaga' => '121',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			129 => 
			array (
				'sp_id_siape' => '130',
				'sp_cd_id_webservice' => '423',
				'sp_no_nome' => '6ª SRPRF/SP - DEL. 04 - DELEGACIA DE ITAPECERICA DA SERRA',
				'sp_fk_vg_vaga' => '423',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			130 => 
			array (
				'sp_id_siape' => '131',
				'sp_cd_id_webservice' => '14',
				'sp_no_nome' => 'DIVISÃO DE FISCALIZAÇÃO E ASSUNTOS INTERNOS',
				'sp_fk_vg_vaga' => '2',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			131 => 
			array (
				'sp_id_siape' => '132',
				'sp_cd_id_webservice' => '546',
				'sp_no_nome' => '8ª SRPRF/SC - NÚCLEO DE ASSUNTOS INTERNOS',
				'sp_fk_vg_vaga' => '470',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			132 => 
			array (
				'sp_id_siape' => '133',
				'sp_cd_id_webservice' => '552',
				'sp_no_nome' => '8ª SRPRF/SC - DEL. 02 - DELEGACIA METROPOLITANA DE TUBARÃO',
				'sp_fk_vg_vaga' => '491',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			133 => 
			array (
				'sp_id_siape' => '134',
				'sp_cd_id_webservice' => '112',
				'sp_no_nome' => '1ª SRPRF/GO - DEL. 05 - DELEGACIA DE JATAí',
				'sp_fk_vg_vaga' => '127',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			134 => 
			array (
				'sp_id_siape' => '135',
				'sp_cd_id_webservice' => '377',
				'sp_no_nome' => '5ª SRPRF/RJ - DEL. 06 - DELEGACIA DE PETRÓPOLIS',
				'sp_fk_vg_vaga' => '353',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			135 => 
			array (
				'sp_id_siape' => '136',
				'sp_cd_id_webservice' => '11',
				'sp_no_nome' => 'CORREGEDORIA GERAL',
				'sp_fk_vg_vaga' => '2',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			136 => 
			array (
				'sp_id_siape' => '137',
				'sp_cd_id_webservice' => '134',
				'sp_no_nome' => '2ª SRPRF/MT - NÚCLEO DE PATRIMÔNIO E MATERIAL',
				'sp_fk_vg_vaga' => '150',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			137 => 
			array (
				'sp_id_siape' => '138',
				'sp_cd_id_webservice' => '415',
				'sp_no_nome' => '6ª SRPRF/SP - DEL. 02 - DELEGACIA DE SÃO JOSÉ DOS CAMPOS',
				'sp_fk_vg_vaga' => '391',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			138 => 
			array (
				'sp_id_siape' => '139',
				'sp_cd_id_webservice' => '806',
				'sp_no_nome' => '12ª SRPRF/ES - NÚCLEO DE ADMINISTRAÇÃO DE PESSOAL',
				'sp_fk_vg_vaga' => '689',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			139 => 
			array (
				'sp_id_siape' => '140',
				'sp_cd_id_webservice' => '767',
				'sp_no_nome' => 'DEL.01/UOP.04 - Unidade Operacional de Gravatá',
				'sp_fk_vg_vaga' => '767',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			140 => 
			array (
				'sp_id_siape' => '141',
				'sp_cd_id_webservice' => '317',
				'sp_no_nome' => '4ª SRPRF/MG - DEL. 15 - NÚCLEO DE POLICIAMENTO E FISCALIZAÇÃO',
				'sp_fk_vg_vaga' => '297',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			141 => 
			array (
				'sp_id_siape' => '142',
				'sp_cd_id_webservice' => '288',
				'sp_no_nome' => '4ª SRPRF/MG - DEL. 08 - DELEGACIA DE ARAXÁ',
				'sp_fk_vg_vaga' => '283',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			142 => 
			array (
				'sp_id_siape' => '143',
				'sp_cd_id_webservice' => '605',
				'sp_no_nome' => '9ª SRPRF/RS - NÚCLEO DE ADMINISTRAÇÃO DE PESSOAL',
				'sp_fk_vg_vaga' => '520',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			143 => 
			array (
				'sp_id_siape' => '144',
				'sp_cd_id_webservice' => '1014',
				'sp_no_nome' => '18ª SRPRF/MA - SEÇÃO DE POLICIAMENTO E FISCALIZAÇÃO',
				'sp_fk_vg_vaga' => '920',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			144 => 
			array (
				'sp_id_siape' => '145',
				'sp_cd_id_webservice' => '218',
				'sp_no_nome' => '3ª SRPRF/MS - DEL. 06 - DELEGACIA DE COXIM',
				'sp_fk_vg_vaga' => '218',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			145 => 
			array (
				'sp_id_siape' => '146',
				'sp_cd_id_webservice' => '153',
				'sp_no_nome' => '2ª SRPRF/MT - DEL. 03 - DELEGACIA DE CÁCERES',
				'sp_fk_vg_vaga' => '153',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			146 => 
			array (
				'sp_id_siape' => '147',
				'sp_cd_id_webservice' => '810',
				'sp_no_nome' => '12ª SRPRF/ES - DEL. 01 - NÚCLEO DE POLICIAMENTO E FISCALIZAÇÃO',
				'sp_fk_vg_vaga' => '810',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			147 => 
			array (
				'sp_id_siape' => '148',
				'sp_cd_id_webservice' => '813',
				'sp_no_nome' => '12ª SRPRF/ES - DEL. 02 - DELEGACIA DE SERRA',
				'sp_fk_vg_vaga' => '813',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			148 => 
			array (
				'sp_id_siape' => '149',
				'sp_cd_id_webservice' => '19',
				'sp_no_nome' => 'DIVISÃO DE MODERNIZAÇÃO E TECNOLOGIAS',
				'sp_fk_vg_vaga' => '2',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			149 => 
			array (
				'sp_id_siape' => '150',
				'sp_cd_id_webservice' => '1182',
				'sp_no_nome' => '2º DRPRF/TO - NÚCLEO DE RECURSOS HUMANOS',
				'sp_fk_vg_vaga' => '1174',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			150 => 
			array (
				'sp_id_siape' => '151',
				'sp_cd_id_webservice' => '128',
				'sp_no_nome' => '2ª SRPRF/MT - SEÇÃO DE POLICIAMENTO E FISCALIZAÇÃO',
				'sp_fk_vg_vaga' => '150',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			151 => 
			array (
				'sp_id_siape' => '152',
				'sp_cd_id_webservice' => '775',
				'sp_no_nome' => 'DEL.03/UOP.02 - Unidade Operacional de Cruzeiro do Nordeste',
				'sp_fk_vg_vaga' => '775',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			152 => 
			array (
				'sp_id_siape' => '153',
				'sp_cd_id_webservice' => '824',
				'sp_no_nome' => '13ª SUPERINTENDÊNCIA REGIONAL DE POLÍCIA RODOVIÁRIA FEDERAL/AL',
				'sp_fk_vg_vaga' => '824',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			153 => 
			array (
				'sp_id_siape' => '154',
				'sp_cd_id_webservice' => '70',
				'sp_no_nome' => 'NÚCLEO DE ADMINISTRAÇÃO PREDIAL',
				'sp_fk_vg_vaga' => '2',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			154 => 
			array (
				'sp_id_siape' => '155',
				'sp_cd_id_webservice' => '584',
				'sp_no_nome' => '8ª SRPRF/SC - DEL. 08 - DELEGACIA DE FRONTEIRA DE CHAPECÓ',
				'sp_fk_vg_vaga' => '503',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			155 => 
			array (
				'sp_id_siape' => '156',
				'sp_cd_id_webservice' => '1164',
				'sp_no_nome' => '1º DISTRITO REGIONAL DE POLÍCIA RODOVIÁRIA FEDERAL/DF',
				'sp_fk_vg_vaga' => '1080',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			156 => 
			array (
				'sp_id_siape' => '157',
				'sp_cd_id_webservice' => '1070',
				'sp_no_nome' => '19ª SRPRF/PA - DEL. 01 - DELEGACIA DE BENEVIDES',
				'sp_fk_vg_vaga' => '979',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			157 => 
			array (
				'sp_id_siape' => '158',
				'sp_cd_id_webservice' => '182',
				'sp_no_nome' => '3ª SRPRF/MS - NÚCLEO DE MULTAS E PENALIDADES',
				'sp_fk_vg_vaga' => '200',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			158 => 
			array (
				'sp_id_siape' => '159',
				'sp_cd_id_webservice' => '51',
				'sp_no_nome' => 'DIVISÃO DE CADASTRO',
				'sp_fk_vg_vaga' => '2',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			159 => 
			array (
				'sp_id_siape' => '160',
				'sp_cd_id_webservice' => '177',
				'sp_no_nome' => '3ª SRPRF/MS - NÚCLEO DE APOIO TÉCNICO',
				'sp_fk_vg_vaga' => '200',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			160 => 
			array (
				'sp_id_siape' => '161',
				'sp_cd_id_webservice' => '562',
				'sp_no_nome' => '8ª SRPRF/SC - DEL. 04 - DELEGACIA METROPOLITANA DE ITAJAÍ',
				'sp_fk_vg_vaga' => '495',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			161 => 
			array (
				'sp_id_siape' => '162',
				'sp_cd_id_webservice' => '665',
				'sp_no_nome' => '9ª SRPRF/RS - DEL. 12 - DELEGACIA DE SÃO BORJA',
				'sp_fk_vg_vaga' => '561',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			162 => 
			array (
				'sp_id_siape' => '163',
				'sp_cd_id_webservice' => '1039',
				'sp_no_nome' => '18ª SRPRF/MA - DEL. 03 - DELEGACIA DE CAXIAS',
				'sp_fk_vg_vaga' => '943',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			163 => 
			array (
				'sp_id_siape' => '164',
				'sp_cd_id_webservice' => '809',
				'sp_no_nome' => '12ª SRPRF/ES - DEL. 01 - DELEGACIA DE VIANA',
				'sp_fk_vg_vaga' => '810',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			164 => 
			array (
				'sp_id_siape' => '165',
				'sp_cd_id_webservice' => '978',
				'sp_no_nome' => '17ª SRPRF/PI - NÚCLEO DE REGISTRO E MEDICINA RODOVIÁRIA',
				'sp_fk_vg_vaga' => '880',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			165 => 
			array (
				'sp_id_siape' => '166',
				'sp_cd_id_webservice' => '608',
				'sp_no_nome' => '9ª SRPRF/RS - DEL. 01 - DELEGACIA DE PORTO ALEGRE',
				'sp_fk_vg_vaga' => '539',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			166 => 
			array (
				'sp_id_siape' => '167',
				'sp_cd_id_webservice' => '201',
				'sp_no_nome' => '3ª SRPRF/MS - DEL. 02 - DELEGACIA DE NOVA ALVORADA DO SUL',
				'sp_fk_vg_vaga' => '201',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			167 => 
			array (
				'sp_id_siape' => '168',
				'sp_cd_id_webservice' => '990',
				'sp_no_nome' => '17ª SRPRF/PI - DEL. 01 - DELEGACIA DE TERESINA',
				'sp_fk_vg_vaga' => '899',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			168 => 
			array (
				'sp_id_siape' => '169',
				'sp_cd_id_webservice' => '1171',
				'sp_no_nome' => '1º DRPRF/DF - NÚCLEO ADMINISTRATIVO E FINANCEIRO',
				'sp_fk_vg_vaga' => '1080',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			169 => 
			array (
				'sp_id_siape' => '170',
				'sp_cd_id_webservice' => '85',
				'sp_no_nome' => '1ª SRPRF/GO - NÚCLEO DE ORÇAMENTO E FINANÇAS',
				'sp_fk_vg_vaga' => '100',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			170 => 
			array (
				'sp_id_siape' => '171',
				'sp_cd_id_webservice' => '1079',
				'sp_no_nome' => '19ª SRPRF/PA - DEL. 02 - DELEGACIA DE IPIXUNA',
				'sp_fk_vg_vaga' => '981',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			171 => 
			array (
				'sp_id_siape' => '172',
				'sp_cd_id_webservice' => '794',
				'sp_no_nome' => '12ª SRPRF/ES - SEÇÃO DE POLICIAMENTO E FISCALIZAÇÃO',
				'sp_fk_vg_vaga' => '689',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			172 => 
			array (
				'sp_id_siape' => '173',
				'sp_cd_id_webservice' => '630',
				'sp_no_nome' => '9ª SRPRF/RS - DEL. 05 - DELEGACIA DE CAXIAS DO SUL',
				'sp_fk_vg_vaga' => '1547',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			173 => 
			array (
				'sp_id_siape' => '174',
				'sp_cd_id_webservice' => '1026',
				'sp_no_nome' => '18ª SRPRF/MA - NÚCLEO DE ADMINISTRAÇÃO DE PESSOAL',
				'sp_fk_vg_vaga' => '920',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			174 => 
			array (
				'sp_id_siape' => '175',
				'sp_cd_id_webservice' => '589',
				'sp_no_nome' => '9ª SUPERINTENDÊNCIA REGIONAL DE POLÍCIA RODOVIÁRIA FEDERAL/RS',
				'sp_fk_vg_vaga' => '520',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			175 => 
			array (
				'sp_id_siape' => '176',
				'sp_cd_id_webservice' => '370',
				'sp_no_nome' => '5ª SRPRF/RJ - DEL. 04 - NÚCLEO DE POLICIAMENTO E FISCALIZAÇÃO',
				'sp_fk_vg_vaga' => '345',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			176 => 
			array (
				'sp_id_siape' => '177',
				'sp_cd_id_webservice' => '54',
				'sp_no_nome' => 'DIVISÃO DE ACOMPANHAMENTO DE DECISÕES JUDICIAIS',
				'sp_fk_vg_vaga' => '2',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			177 => 
			array (
				'sp_id_siape' => '178',
				'sp_cd_id_webservice' => '856',
				'sp_no_nome' => '14ª SUPERINTENDÊNCIA REGIONAL DE POLÍCIA RODOVIÁRIA FEDERAL/PB',
				'sp_fk_vg_vaga' => '760',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			178 => 
			array (
				'sp_id_siape' => '179',
				'sp_cd_id_webservice' => '748',
				'sp_no_nome' => '11ª SRPRF/PE - NÚCLEO DE OPERAÇÕES ESPECIAIS',
				'sp_fk_vg_vaga' => '630',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			179 => 
			array (
				'sp_id_siape' => '180',
				'sp_cd_id_webservice' => '1205',
				'sp_no_nome' => '5º DRPRF/RR - NÚCLEO DE RECURSOS HUMANOS',
				'sp_fk_vg_vaga' => '1120',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			180 => 
			array (
				'sp_id_siape' => '181',
				'sp_cd_id_webservice' => '707',
				'sp_no_nome' => '10ª SRPRF/BA - DEL. 03 - DELEGACIA DE JEQUIÉ',
				'sp_fk_vg_vaga' => '603',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			181 => 
			array (
				'sp_id_siape' => '182',
				'sp_cd_id_webservice' => '336',
				'sp_no_nome' => '5ª SRPRF/RJ - NÚCLEO DE INTELIGÊNCIA',
				'sp_fk_vg_vaga' => '320',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			182 => 
			array (
				'sp_id_siape' => '183',
				'sp_cd_id_webservice' => '829',
				'sp_no_nome' => '13ª SRPRF/AL - NÚCLEO DE OPERAÇÕES ESPECIAIS',
				'sp_fk_vg_vaga' => '824',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			183 => 
			array (
				'sp_id_siape' => '184',
				'sp_cd_id_webservice' => '518',
				'sp_no_nome' => '7ª SRPRF/PR - DEL. 07 - NÚCLEO DE POLICIAMENTO E FISCALIZAÇÃO',
				'sp_fk_vg_vaga' => '451',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			184 => 
			array (
				'sp_id_siape' => '185',
				'sp_cd_id_webservice' => '67',
				'sp_no_nome' => 'DIVISÃO DE LICITAÇÃO, CONTRATAÇÃO E CONVÊNIOS',
				'sp_fk_vg_vaga' => '2',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			185 => 
			array (
				'sp_id_siape' => '186',
				'sp_cd_id_webservice' => '143',
				'sp_no_nome' => '2ª SRPRF/MT - DEL. 01 - DELEGACIA DE CUIABÁ',
				'sp_fk_vg_vaga' => '143',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			186 => 
			array (
				'sp_id_siape' => '187',
				'sp_cd_id_webservice' => '157',
				'sp_no_nome' => '2ª SRPRF/MT - DEL. 04 - DELEGACIA DE DIAMANTINO',
				'sp_fk_vg_vaga' => '157',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			187 => 
			array (
				'sp_id_siape' => '188',
				'sp_cd_id_webservice' => '387',
				'sp_no_nome' => '5ª SRPRF/RJ - DEL. 08 - DELEGACIA DE CAMPOS DOS GOYTACAZES',
				'sp_fk_vg_vaga' => '357',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			188 => 
			array (
				'sp_id_siape' => '189',
				'sp_cd_id_webservice' => '230',
				'sp_no_nome' => '3ª SRPRF/MS - DEL. 09 - DELEGACIA DE PARANAÍBA',
				'sp_fk_vg_vaga' => '230',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			189 => 
			array (
				'sp_id_siape' => '190',
				'sp_cd_id_webservice' => '1016',
				'sp_no_nome' => '18ª SRPRF/MA - NÚCLEO DE MULTAS E PENALIDADES',
				'sp_fk_vg_vaga' => '920',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			190 => 
			array (
				'sp_id_siape' => '191',
				'sp_cd_id_webservice' => '459',
				'sp_no_nome' => '7ª SRPRF/PR - NÚCLEO DE OPERAÇÕES ESPECIAIS',
				'sp_fk_vg_vaga' => '420',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			191 => 
			array (
				'sp_id_siape' => '192',
				'sp_cd_id_webservice' => '578',
				'sp_no_nome' => '8ª SRPRF/SC - DEL. 07 - DELEGACIA DE FRONTEIRA DE JOAÇABA',
				'sp_fk_vg_vaga' => '501',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			192 => 
			array (
				'sp_id_siape' => '193',
				'sp_cd_id_webservice' => '779',
				'sp_no_nome' => 'DEL.04/UOP.02 - Unidade Operacional de Floresta',
				'sp_fk_vg_vaga' => '779',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			193 => 
			array (
				'sp_id_siape' => '194',
				'sp_cd_id_webservice' => '1158',
				'sp_no_nome' => '21ª SRPRF/RO - DEL. 04 - DELEGACIA DE VILHENA',
				'sp_fk_vg_vaga' => '1158',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			194 => 
			array (
				'sp_id_siape' => '195',
				'sp_cd_id_webservice' => '378',
				'sp_no_nome' => '5ª SRPRF/RJ - DEL. 06 - NÚCLEO DE POLICIAMENTO E FISCALIZAÇÃO',
				'sp_fk_vg_vaga' => '353',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			195 => 
			array (
				'sp_id_siape' => '196',
				'sp_cd_id_webservice' => '293',
				'sp_no_nome' => '4ª SRPRF/MG - DEL. 09 - DELEGACIA DE CAXAMBÚ',
				'sp_fk_vg_vaga' => '285',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			196 => 
			array (
				'sp_id_siape' => '197',
				'sp_cd_id_webservice' => '349',
				'sp_no_nome' => '5ª SRPRF/RJ - NÚCLEO DE ADMINISTRAÇÃO DE PESSOAL',
				'sp_fk_vg_vaga' => '320',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			197 => 
			array (
				'sp_id_siape' => '198',
				'sp_cd_id_webservice' => '195',
				'sp_no_nome' => '3ª SRPRF/MS - DEL. 01 - DELEGACIA DE CAMPO GRANDE',
				'sp_fk_vg_vaga' => '195',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			198 => 
			array (
				'sp_id_siape' => '199',
				'sp_cd_id_webservice' => '37',
				'sp_no_nome' => 'NÚCLEO DE POLICIAMENTO ESPECIALIZADO',
				'sp_fk_vg_vaga' => '2',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			199 => 
			array (
				'sp_id_siape' => '200',
				'sp_cd_id_webservice' => '254',
				'sp_no_nome' => '4ª SRPRF/MG - CORREGEDORIA REGIONAL',
				'sp_fk_vg_vaga' => '250',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			200 => 
			array (
				'sp_id_siape' => '201',
				'sp_cd_id_webservice' => '790',
				'sp_no_nome' => '12ª SUPERINTENDÊNCIA REGIONAL DE POLÍCIA RODOVIÁRIA FEDERAL/ES',
				'sp_fk_vg_vaga' => '689',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			201 => 
			array (
				'sp_id_siape' => '202',
				'sp_cd_id_webservice' => '344',
				'sp_no_nome' => '5ª SRPRF/RJ - NÚCLEO DE DOCUMENTAÇÃO',
				'sp_fk_vg_vaga' => '320',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			202 => 
			array (
				'sp_id_siape' => '203',
				'sp_cd_id_webservice' => '283',
				'sp_no_nome' => '4ª SRPRF/MG - DEL. 07 - DELEGACIA DE LEOPOLDINA',
				'sp_fk_vg_vaga' => '281',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			203 => 
			array (
				'sp_id_siape' => '204',
				'sp_cd_id_webservice' => '539',
				'sp_no_nome' => '8ª SRPRF/SC - NÚCLEO DE DOCUMENTAÇÃO',
				'sp_fk_vg_vaga' => '470',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			204 => 
			array (
				'sp_id_siape' => '205',
				'sp_cd_id_webservice' => '81',
				'sp_no_nome' => '1ª SRPRF/GO - NÚCLEO DE OPERAÇÕES ESPECIAIS',
				'sp_fk_vg_vaga' => '100',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			205 => 
			array (
				'sp_id_siape' => '206',
				'sp_cd_id_webservice' => '210',
				'sp_no_nome' => '3ª SRPRF/MS - DEL. 04 - DELEGACIA DE DOURADOS',
				'sp_fk_vg_vaga' => '210',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			206 => 
			array (
				'sp_id_siape' => '207',
				'sp_cd_id_webservice' => '438',
				'sp_no_nome' => '6ª SRPRF/SP - DEL. 07 - DELEGACIA DE UBATUBA',
				'sp_fk_vg_vaga' => '438',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			207 => 
			array (
				'sp_id_siape' => '208',
				'sp_cd_id_webservice' => '458',
				'sp_no_nome' => '7ª SRPRF/PR - SEÇÃO DE POLICIAMENTO E FISCALIZAÇÃO',
				'sp_fk_vg_vaga' => '420',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			208 => 
			array (
				'sp_id_siape' => '209',
				'sp_cd_id_webservice' => '511',
				'sp_no_nome' => '7ª SRPRF/PR - DEL. 06 - DELEGACIA DE GUAÍRA',
				'sp_fk_vg_vaga' => '511',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			209 => 
			array (
				'sp_id_siape' => '210',
				'sp_cd_id_webservice' => '659',
				'sp_no_nome' => '9ª SRPRF/RS - DEL. 11 - DELEGACIA DE SANTANA DO LIVRAMENTO',
				'sp_fk_vg_vaga' => '559',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			210 => 
			array (
				'sp_id_siape' => '211',
				'sp_cd_id_webservice' => '720',
				'sp_no_nome' => '10ª SRPRF/BA - DEL. 06 - DELEGACIA DE SEABRA',
				'sp_fk_vg_vaga' => '720',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			211 => 
			array (
				'sp_id_siape' => '212',
				'sp_cd_id_webservice' => '257',
				'sp_no_nome' => '4ª SRPRF/MG - DEL. 01 - NÚCLEO DE POLICIAMENTO E FISCALIZAÇÃO',
				'sp_fk_vg_vaga' => '256',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			212 => 
			array (
				'sp_id_siape' => '213',
				'sp_cd_id_webservice' => '826',
				'sp_no_nome' => '13ª SRPRF/AL - NÚCLEO DE COMUNICAÇÃO SOCIAL',
				'sp_fk_vg_vaga' => '824',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			213 => 
			array (
				'sp_id_siape' => '214',
				'sp_cd_id_webservice' => '100',
				'sp_no_nome' => '1ª SRPRF/GO - DEL. 02 - DELEGACIA DE ANÁPOLIS',
				'sp_fk_vg_vaga' => '121',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			214 => 
			array (
				'sp_id_siape' => '215',
				'sp_cd_id_webservice' => '441',
				'sp_no_nome' => '6ª SRPRF/SP - DEL. 08 - DELEGACIA DE CACHOEIRA PAULISTA',
				'sp_fk_vg_vaga' => '403',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			215 => 
			array (
				'sp_id_siape' => '216',
				'sp_cd_id_webservice' => '187',
				'sp_no_nome' => '3ª SRPRF/MS - NÚCLEO DE DOCUMENTAÇÃO',
				'sp_fk_vg_vaga' => '200',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			216 => 
			array (
				'sp_id_siape' => '217',
				'sp_cd_id_webservice' => '793',
				'sp_no_nome' => '12ª SRPRF/ES - NÚCLEO DE INTELIGÊNCIA',
				'sp_fk_vg_vaga' => '689',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			217 => 
			array (
				'sp_id_siape' => '218',
				'sp_cd_id_webservice' => '603',
				'sp_no_nome' => '9ª SRPRF/RS - SEÇÃO DE RECURSOS HUMANOS',
				'sp_fk_vg_vaga' => '520',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			218 => 
			array (
				'sp_id_siape' => '219',
				'sp_cd_id_webservice' => '1048',
				'sp_no_nome' => '18ª SRPRF/MA - DEL. 05 - DELEGACIA DE BALSAS',
				'sp_fk_vg_vaga' => '1048',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			219 => 
			array (
				'sp_id_siape' => '220',
				'sp_cd_id_webservice' => '352',
				'sp_no_nome' => '5ª SRPRF/RJ - DEL. 01 - DELEGACIA DE DUQUE DE CAXIAS',
				'sp_fk_vg_vaga' => '339',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			220 => 
			array (
				'sp_id_siape' => '221',
				'sp_cd_id_webservice' => '1104',
				'sp_no_nome' => '20ª SRPRF/SE - NÚCLEO DE MULTAS E PENALIDADES',
				'sp_fk_vg_vaga' => '1000',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			221 => 
			array (
				'sp_id_siape' => '222',
				'sp_cd_id_webservice' => '590',
				'sp_no_nome' => '9ª SRPRF/RS - NÚCLEO DE APOIO TÉCNICO',
				'sp_fk_vg_vaga' => '520',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			222 => 
			array (
				'sp_id_siape' => '223',
				'sp_cd_id_webservice' => '68',
				'sp_no_nome' => 'NÚCLEO DE COMPRAS',
				'sp_fk_vg_vaga' => '2',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			223 => 
			array (
				'sp_id_siape' => '224',
				'sp_cd_id_webservice' => '77',
				'sp_no_nome' => '1ª SRPRF/GO - NÚCLEO DE APOIO TÉCNICO',
				'sp_fk_vg_vaga' => '100',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			224 => 
			array (
				'sp_id_siape' => '225',
				'sp_cd_id_webservice' => '966',
				'sp_no_nome' => '16ª SRPRF/CE - DEL. 05 - DELEGACIA DE ICÓ',
				'sp_fk_vg_vaga' => '966',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			225 => 
			array (
				'sp_id_siape' => '226',
				'sp_cd_id_webservice' => '179',
				'sp_no_nome' => '3ª SRPRF/MS - NÚCLEO DE INTELIGÊNCIA',
				'sp_fk_vg_vaga' => '200',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			226 => 
			array (
				'sp_id_siape' => '227',
				'sp_cd_id_webservice' => '50',
				'sp_no_nome' => 'SEÇÃO DE APOSENTADORIAS E PENSÕES',
				'sp_fk_vg_vaga' => '2',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			227 => 
			array (
				'sp_id_siape' => '228',
				'sp_cd_id_webservice' => '532',
				'sp_no_nome' => '8ª SRPRF/SC - SEÇÃO DE POLICIAMENTO E FISCALIZAÇÃO',
				'sp_fk_vg_vaga' => '470',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			228 => 
			array (
				'sp_id_siape' => '229',
				'sp_cd_id_webservice' => '298',
				'sp_no_nome' => '4ª SRPRF/MG - DEL. 10 - NÚCLEO DE POLICIAMENTO E FISCALIZAÇÃO',
				'sp_fk_vg_vaga' => '287',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			229 => 
			array (
				'sp_id_siape' => '230',
				'sp_cd_id_webservice' => '673',
				'sp_no_nome' => '9ª SRPRF/RS - DEL. 14 - DELEGACIA DE SARANDI',
				'sp_fk_vg_vaga' => '565',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			230 => 
			array (
				'sp_id_siape' => '231',
				'sp_cd_id_webservice' => '1022',
				'sp_no_nome' => '18ª SRPRF/MA - NÚCLEO DE SERVIÇOS GERAIS',
				'sp_fk_vg_vaga' => '920',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			231 => 
			array (
				'sp_id_siape' => '232',
				'sp_cd_id_webservice' => '771',
				'sp_no_nome' => 'DEL.02/UOP.02 - Unidade Operacional de São Caetano',
				'sp_fk_vg_vaga' => '771',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			232 => 
			array (
				'sp_id_siape' => '233',
				'sp_cd_id_webservice' => '169',
				'sp_no_nome' => '2ª SRPRF/MT - DEL. 07 - DELEGACIA DE PONTES E LACERDA',
				'sp_fk_vg_vaga' => '169',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			233 => 
			array (
				'sp_id_siape' => '234',
				'sp_cd_id_webservice' => '1189',
				'sp_no_nome' => '3º DRPRF/AM - NÚCLEO DE POLICIAMENTO E FISCALIZAÇÃO',
				'sp_fk_vg_vaga' => '1100',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			234 => 
			array (
				'sp_id_siape' => '235',
				'sp_cd_id_webservice' => '165',
				'sp_no_nome' => '2ª SRPRF/MT - DEL. 06 - DELEGACIA DE SORRISO',
				'sp_fk_vg_vaga' => '165',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			235 => 
			array (
				'sp_id_siape' => '236',
				'sp_cd_id_webservice' => '398',
				'sp_no_nome' => '6ª SRPRF/SP - NÚCLEO DE MULTAS E PENALIDADES',
				'sp_fk_vg_vaga' => '370',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			236 => 
			array (
				'sp_id_siape' => '237',
				'sp_cd_id_webservice' => '999',
				'sp_no_nome' => '17ª SRPRF/PI - DEL. 03 - DELEGACIA DE FLORIANO',
				'sp_fk_vg_vaga' => '903',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			237 => 
			array (
				'sp_id_siape' => '238',
				'sp_cd_id_webservice' => '979',
				'sp_no_nome' => '17ª SRPRF/PI - SEÇÃO ADMINISTRATIVA E FINANCEIRA',
				'sp_fk_vg_vaga' => '880',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			238 => 
			array (
				'sp_id_siape' => '239',
				'sp_cd_id_webservice' => '1058',
				'sp_no_nome' => '19ª SRPRF/PA - NÚCLEO DE REGISTRO E MEDICINA RODOVIÁRIA',
				'sp_fk_vg_vaga' => '960',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			239 => 
			array (
				'sp_id_siape' => '240',
				'sp_cd_id_webservice' => '411',
				'sp_no_nome' => '6ª SRPRF/SP - DEL. 01 - DELEGACIA DE GUARULHOS',
				'sp_fk_vg_vaga' => '411',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			240 => 
			array (
				'sp_id_siape' => '241',
				'sp_cd_id_webservice' => '113',
				'sp_no_nome' => '1ª SRPRF/GO - DEL. 05 - NÚCLEO DE POLICIAMENTO E FISCALIZAÇÃO',
				'sp_fk_vg_vaga' => '127',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			241 => 
			array (
				'sp_id_siape' => '242',
				'sp_cd_id_webservice' => '1173',
				'sp_no_nome' => '1º DRPRF/DF - NÚCLEO DE CORREGEDORIA E ASSUNTOS INTERNOS',
				'sp_fk_vg_vaga' => '1080',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			242 => 
			array (
				'sp_id_siape' => '243',
				'sp_cd_id_webservice' => '392',
				'sp_no_nome' => '6ª SUPERINTENDÊNCIA REGIONAL DE POLÍCIA RODOVIÁRIA FEDERAL/SP',
				'sp_fk_vg_vaga' => '370',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			243 => 
			array (
				'sp_id_siape' => '244',
				'sp_cd_id_webservice' => '1198',
				'sp_no_nome' => '4º DRPRF/AP - NÚCLEO ADMINISTRATIVO E FINANCEIRO',
				'sp_fk_vg_vaga' => '1110',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			244 => 
			array (
				'sp_id_siape' => '245',
				'sp_cd_id_webservice' => '544',
				'sp_no_nome' => '8ª SRPRF/SC - NÚCLEO DE ADMINISTRAÇÃO DE PESSOAL',
				'sp_fk_vg_vaga' => '470',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			245 => 
			array (
				'sp_id_siape' => '246',
				'sp_cd_id_webservice' => '1128',
				'sp_no_nome' => '21ª SRPRF/RO - NÚCLEO DE COMUNICAÇÃO SOCIAL',
				'sp_fk_vg_vaga' => '1040',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			246 => 
			array (
				'sp_id_siape' => '247',
				'sp_cd_id_webservice' => '694',
				'sp_no_nome' => '10ª SRPRF/BA - NÚCLEO DE ADMINISTRAÇÃO DE PESSOAL',
				'sp_fk_vg_vaga' => '580',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			247 => 
			array (
				'sp_id_siape' => '248',
				'sp_cd_id_webservice' => '449',
				'sp_no_nome' => '6ª SRPRF/SP - DEL. 10 - DELEGACIA DE MARÍLIA',
				'sp_fk_vg_vaga' => '449',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			248 => 
			array (
				'sp_id_siape' => '249',
				'sp_cd_id_webservice' => '708',
				'sp_no_nome' => '10ª SRPRF/BA - DEL. 03 - NÚCLEO DE POLICIAMENTO E FISCALIZAÇÃO',
				'sp_fk_vg_vaga' => '603',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			249 => 
			array (
				'sp_id_siape' => '250',
				'sp_cd_id_webservice' => '455',
				'sp_no_nome' => '7ª SRPRF/PR - NÚCLEO DE APOIO TÉCNICO',
				'sp_fk_vg_vaga' => '420',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			250 => 
			array (
				'sp_id_siape' => '251',
				'sp_cd_id_webservice' => '253',
				'sp_no_nome' => '4ª SRPRF/MG - NÚCLEO DE ADMINISTRAÇÃO DE PESSOAL',
				'sp_fk_vg_vaga' => '250',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			251 => 
			array (
				'sp_id_siape' => '252',
				'sp_cd_id_webservice' => '1118',
				'sp_no_nome' => '20ª SRPRF/SE - DEL. 01 - NÚCLEO DE POLICIAMENTO E FISCALIZAÇÃO',
				'sp_fk_vg_vaga' => '1019',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			252 => 
			array (
				'sp_id_siape' => '253',
				'sp_cd_id_webservice' => '150',
				'sp_no_nome' => '2ª SUPERINTENDÊNCIA DE POLÍCIA RODOVIÁRIA FEDERAL/MT',
				'sp_fk_vg_vaga' => '150',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			253 => 
			array (
				'sp_id_siape' => '254',
				'sp_cd_id_webservice' => '69',
				'sp_no_nome' => 'DIVISÃO DE ADMINISTRAÇÃO E SERVIÇOS GERAIS',
				'sp_fk_vg_vaga' => '2',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			254 => 
			array (
				'sp_id_siape' => '255',
				'sp_cd_id_webservice' => '17',
				'sp_no_nome' => 'NÚCLEO CARTORIAL',
				'sp_fk_vg_vaga' => '2',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			255 => 
			array (
				'sp_id_siape' => '256',
				'sp_cd_id_webservice' => '911',
				'sp_no_nome' => '15ª SRPRF/RN - DEL. 01 - DELEGACIA DE MACAÍBA',
				'sp_fk_vg_vaga' => '819',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			256 => 
			array (
				'sp_id_siape' => '257',
				'sp_cd_id_webservice' => '843',
				'sp_no_nome' => '13ª SRPRF/AL - DEL. 01 - DELEGACIA DE MACEIÓ',
				'sp_fk_vg_vaga' => '843',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			257 => 
			array (
				'sp_id_siape' => '258',
				'sp_cd_id_webservice' => '828',
				'sp_no_nome' => '13ª SRPRF/AL - SEÇÃO DE POLICIAMENTO E FISCALIZAÇÃO',
				'sp_fk_vg_vaga' => '824',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			258 => 
			array (
				'sp_id_siape' => '259',
				'sp_cd_id_webservice' => '698',
				'sp_no_nome' => '10ª SRPRF/BA - DEL. 01 - NÚCLEO DE POLICIAMENTO E FISCALIZAÇÃO',
				'sp_fk_vg_vaga' => '599',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			259 => 
			array (
				'sp_id_siape' => '260',
				'sp_cd_id_webservice' => '129',
				'sp_no_nome' => '2ª SRPRF/MT - NÚCLEO DE OPERAÇÕES ESPECIAIS',
				'sp_fk_vg_vaga' => '150',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			260 => 
			array (
				'sp_id_siape' => '261',
				'sp_cd_id_webservice' => '789',
				'sp_no_nome' => 'DEL.06/UOP.03 - Unidade Operacional de Santa Maria da Boa Vista',
				'sp_fk_vg_vaga' => '789',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			261 => 
			array (
				'sp_id_siape' => '262',
				'sp_cd_id_webservice' => '233',
				'sp_no_nome' => '3ª SRPRF/MS - DEL. 10 - DELEGACIA DE NAVIRAÍ',
				'sp_fk_vg_vaga' => '233',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			262 => 
			array (
				'sp_id_siape' => '263',
				'sp_cd_id_webservice' => '52',
				'sp_no_nome' => 'NÚCLEO DE CADASTRO E LOTAÇÃO',
				'sp_fk_vg_vaga' => '2',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			263 => 
			array (
				'sp_id_siape' => '264',
				'sp_cd_id_webservice' => '374',
				'sp_no_nome' => '5ª SRPRF/RJ - DEL. 05 - NÚCLEO DE POLICIAMENTO E FISCALIZAÇÃO',
				'sp_fk_vg_vaga' => '351',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			264 => 
			array (
				'sp_id_siape' => '265',
				'sp_cd_id_webservice' => '573',
				'sp_no_nome' => '8ª SRPRF/SC - DEL. 06 - DELEGACIA DE MAFRA',
				'sp_fk_vg_vaga' => '499',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			265 => 
			array (
				'sp_id_siape' => '266',
				'sp_cd_id_webservice' => '1055',
				'sp_no_nome' => '19ª SRPRF/PA - SEÇÃO DE POLICIAMENTO E FISCALIZAÇÃO',
				'sp_fk_vg_vaga' => '960',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			266 => 
			array (
				'sp_id_siape' => '267',
				'sp_cd_id_webservice' => '841',
				'sp_no_nome' => '13ª SRPRF/AL - CORREGEDORIA REGIONAL',
				'sp_fk_vg_vaga' => '824',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			267 => 
			array (
				'sp_id_siape' => '268',
				'sp_cd_id_webservice' => '887',
				'sp_no_nome' => '14ª SRPRF/PB - DEL. 03 - DELEGACIA DE PATOS',
				'sp_fk_vg_vaga' => '783',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			268 => 
			array (
				'sp_id_siape' => '269',
				'sp_cd_id_webservice' => '215',
				'sp_no_nome' => '3ª SRPRF/MS - DEL. 05 - DELEGACIA DE GUIA LOPES DA LAGUNA',
				'sp_fk_vg_vaga' => '215',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			269 => 
			array (
				'sp_id_siape' => '270',
				'sp_cd_id_webservice' => '681',
				'sp_no_nome' => '10ª SRPRF/BA - NÚCLEO DE INTELIGÊNCIA',
				'sp_fk_vg_vaga' => '580',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			270 => 
			array (
				'sp_id_siape' => '271',
				'sp_cd_id_webservice' => '861',
				'sp_no_nome' => '14ª SRPRF/PB - NÚCLEO DE OPERAÇÕES ESPECIAIS',
				'sp_fk_vg_vaga' => '760',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			271 => 
			array (
				'sp_id_siape' => '272',
				'sp_cd_id_webservice' => '277',
				'sp_no_nome' => '4ª SRPRF/MG - DEL. 06 - DELEGACIA DE GOVERNADOR VALADARES',
				'sp_fk_vg_vaga' => '277',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			272 => 
			array (
				'sp_id_siape' => '273',
				'sp_cd_id_webservice' => '679',
				'sp_no_nome' => '10ª SRPRF/BA - NÚCLEO DE APOIO TÉCNICO',
				'sp_fk_vg_vaga' => '580',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			273 => 
			array (
				'sp_id_siape' => '274',
				'sp_cd_id_webservice' => '542',
				'sp_no_nome' => '8ª SRPRF/SC - SEÇÃO DE RECURSOS HUMANOS',
				'sp_fk_vg_vaga' => '470',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			274 => 
			array (
				'sp_id_siape' => '275',
				'sp_cd_id_webservice' => '820',
				'sp_no_nome' => '12ª SRPRF/ES - DEL. 04 - DELEGACIA DE LINHARES',
				'sp_fk_vg_vaga' => '705',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			275 => 
			array (
				'sp_id_siape' => '276',
				'sp_cd_id_webservice' => '1094',
				'sp_no_nome' => '19ª SRPRF/PA - DEL. 05 - DELEGACIA DE SANTARÉM',
				'sp_fk_vg_vaga' => '987',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			276 => 
			array (
				'sp_id_siape' => '277',
				'sp_cd_id_webservice' => '484',
				'sp_no_nome' => '7ª SRPRF/PR - DEL. 02 - DELEGACIA DE PATO BRANCO',
				'sp_fk_vg_vaga' => '441',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			277 => 
			array (
				'sp_id_siape' => '278',
				'sp_cd_id_webservice' => '825',
				'sp_no_nome' => '13ª SRPRF/AL - NÚCLEO DE APOIO TÉCNICO',
				'sp_fk_vg_vaga' => '824',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			278 => 
			array (
				'sp_id_siape' => '279',
				'sp_cd_id_webservice' => '923',
				'sp_no_nome' => '15ª SRPRF/RN - DEL. 04 - DELEGACIA DE MOSSORÓ',
				'sp_fk_vg_vaga' => '825',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			279 => 
			array (
				'sp_id_siape' => '280',
				'sp_cd_id_webservice' => '528',
				'sp_no_nome' => '8ª SUPERINTENDÊNCIA REGIONAL DE POLÍCIA RODOVIÁRIA FEDERAL/SC',
				'sp_fk_vg_vaga' => '470',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			280 => 
			array (
				'sp_id_siape' => '281',
				'sp_cd_id_webservice' => '848',
				'sp_no_nome' => '13ª SRPRF/AL - DEL. 02 - DELEGACIA DE SÃO MIGUEL DOS CAMPOS',
				'sp_fk_vg_vaga' => '849',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			281 => 
			array (
				'sp_id_siape' => '282',
				'sp_cd_id_webservice' => '406',
				'sp_no_nome' => '6ª SRPRF/SP - SEÇÃO DE RECURSOS HUMANOS',
				'sp_fk_vg_vaga' => '370',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			282 => 
			array (
				'sp_id_siape' => '283',
				'sp_cd_id_webservice' => '919',
				'sp_no_nome' => '15ª SRPRF/RN - DEL. 03 - DELEGACIA DE CURRAIS NOVOS',
				'sp_fk_vg_vaga' => '919',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			283 => 
			array (
				'sp_id_siape' => '284',
				'sp_cd_id_webservice' => '803',
				'sp_no_nome' => '12ª SRPRF/ES - NÚCLEO DE INFORMÁTICA E TELECOMUNICAÇÕES',
				'sp_fk_vg_vaga' => '689',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			284 => 
			array (
				'sp_id_siape' => '285',
				'sp_cd_id_webservice' => '1007',
				'sp_no_nome' => '17ª SRPRF/PI - DEL. 05 - DELEGACIA DE PARNAIBA',
				'sp_fk_vg_vaga' => '1007',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			285 => 
			array (
				'sp_id_siape' => '286',
				'sp_cd_id_webservice' => '988',
				'sp_no_nome' => '17ª SRPRF/PI - CORREGEDORIA REGIONAL',
				'sp_fk_vg_vaga' => '880',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			286 => 
			array (
				'sp_id_siape' => '287',
				'sp_cd_id_webservice' => '397',
				'sp_no_nome' => '6ª SRPRF/SP - NÚCLEO DE OPERAÇÕES ESPECIAIS',
				'sp_fk_vg_vaga' => '370',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			287 => 
			array (
				'sp_id_siape' => '288',
				'sp_cd_id_webservice' => '852',
				'sp_no_nome' => '13ª SRPRF/AL - DEL. 03 - DELEGACIA DE PALMEIRAS DOS ÍNDIOS',
				'sp_fk_vg_vaga' => '852',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			288 => 
			array (
				'sp_id_siape' => '289',
				'sp_cd_id_webservice' => '692',
				'sp_no_nome' => '10ª SRPRF/BA - SEÇÃO DE RECURSOS HUMANOS',
				'sp_fk_vg_vaga' => '580',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			289 => 
			array (
				'sp_id_siape' => '290',
				'sp_cd_id_webservice' => '1204',
				'sp_no_nome' => '5º DRPRF/RR - NÚCLEO ADMINISTRATIVO E FINANCEIRO',
				'sp_fk_vg_vaga' => '1120',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			290 => 
			array (
				'sp_id_siape' => '291',
				'sp_cd_id_webservice' => '893',
				'sp_no_nome' => '15ª SRPRF/RN - NÚCLEO DE APOIO TÉCNICO',
				'sp_fk_vg_vaga' => '800',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			291 => 
			array (
				'sp_id_siape' => '292',
				'sp_cd_id_webservice' => '897',
				'sp_no_nome' => '15ª SRPRF/RN - NÚCLEO DE OPERAÇÕES ESPECIAIS',
				'sp_fk_vg_vaga' => '800',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			292 => 
			array (
				'sp_id_siape' => '293',
				'sp_cd_id_webservice' => '977',
				'sp_no_nome' => '17ª SRPRF/PI - NÚCLEO DE MULTAS E PENALIDADES',
				'sp_fk_vg_vaga' => '880',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			293 => 
			array (
				'sp_id_siape' => '294',
				'sp_cd_id_webservice' => '274',
				'sp_no_nome' => '4ª SRPRF/MG - DEL. 05 - NÚCLEO DE POLICIAMENTO E FISCALIZAÇÃO',
				'sp_fk_vg_vaga' => '273',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			294 => 
			array (
				'sp_id_siape' => '295',
				'sp_cd_id_webservice' => '932',
				'sp_no_nome' => '16ª SRPRF/CE - NÚCLEO DE OPERAÇÕES ESPECIAIS',
				'sp_fk_vg_vaga' => '840',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			295 => 
			array (
				'sp_id_siape' => '296',
				'sp_cd_id_webservice' => '873',
				'sp_no_nome' => '14ª SRPRF/PB - CORREGEDORIA REGIONAL',
				'sp_fk_vg_vaga' => '760',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			296 => 
			array (
				'sp_id_siape' => '297',
				'sp_cd_id_webservice' => '895',
				'sp_no_nome' => '15ª SRPRF/RN - NÚCLEO DE INTELIGÊNCIA',
				'sp_fk_vg_vaga' => '800',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			297 => 
			array (
				'sp_id_siape' => '298',
				'sp_cd_id_webservice' => '36',
				'sp_no_nome' => 'DIVISÃO DE COMBATE AO CRIME',
				'sp_fk_vg_vaga' => '2',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			298 => 
			array (
				'sp_id_siape' => '299',
				'sp_cd_id_webservice' => '881',
				'sp_no_nome' => '14ª SRPRF/PB - DEL. 02 - DELEGACIA DE CAMPINA GRANDE',
				'sp_fk_vg_vaga' => '781',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			299 => 
			array (
				'sp_id_siape' => '300',
				'sp_cd_id_webservice' => '697',
				'sp_no_nome' => '10ª SRPRF/BA - DEL. 01 - DELEGACIA DE SIMÕES FILHO',
				'sp_fk_vg_vaga' => '599',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			300 => 
			array (
				'sp_id_siape' => '301',
				'sp_cd_id_webservice' => '746',
				'sp_no_nome' => '11ª SRPRF/PE - NÚCLEO DE INTELIGÊNCIA',
				'sp_fk_vg_vaga' => '630',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			301 => 
			array (
				'sp_id_siape' => '302',
				'sp_cd_id_webservice' => '712',
				'sp_no_nome' => '10ª SRPRF/BA - DEL. 04 - NÚCLEO DE POLICIAMENTO E FISCALIZAÇÃO',
				'sp_fk_vg_vaga' => '605',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			302 => 
			array (
				'sp_id_siape' => '303',
				'sp_cd_id_webservice' => '745',
				'sp_no_nome' => '11ª SRPRF/PE - NÚCLEO DE COMUNICAÇÃO SOCIAL',
				'sp_fk_vg_vaga' => '630',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			303 => 
			array (
				'sp_id_siape' => '304',
				'sp_cd_id_webservice' => '1184',
				'sp_no_nome' => '3º DISTRITO REGIONAL DE POLÍCIA RODOVIÁRIA FEDERAL/AM',
				'sp_fk_vg_vaga' => '1100',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			304 => 
			array (
				'sp_id_siape' => '305',
				'sp_cd_id_webservice' => '808',
				'sp_no_nome' => '12ª SRPRF/ES - NÚCLEO DE ASSUNTOS INTERNOS',
				'sp_fk_vg_vaga' => '689',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			305 => 
			array (
				'sp_id_siape' => '306',
				'sp_cd_id_webservice' => '684',
				'sp_no_nome' => '10ª SRPRF/BA - NÚCLEO DE MULTAS E PENALIDADES',
				'sp_fk_vg_vaga' => '580',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			306 => 
			array (
				'sp_id_siape' => '307',
				'sp_cd_id_webservice' => '691',
				'sp_no_nome' => '10ª SRPRF/BA - NÚCLEO DE INFORMÁTICA E TELECOMUNICAÇÕES',
				'sp_fk_vg_vaga' => '580',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			307 => 
			array (
				'sp_id_siape' => '308',
				'sp_cd_id_webservice' => '1144',
				'sp_no_nome' => '21ª SRPRF/RO - NÚCLEO DE ASSUNTOS INTERNOS',
				'sp_fk_vg_vaga' => '1040',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			308 => 
			array (
				'sp_id_siape' => '309',
				'sp_cd_id_webservice' => '205',
				'sp_no_nome' => '3ª SRPRF/MS - DEL. 03 - DELEGACIA DE CORUMBÁ',
				'sp_fk_vg_vaga' => '205',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			309 => 
			array (
				'sp_id_siape' => '310',
				'sp_cd_id_webservice' => '896',
				'sp_no_nome' => '15ª SRPRF/RN - SEÇÃO DE POLICIAMENTO E FISCALIZAÇÃO',
				'sp_fk_vg_vaga' => '800',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			310 => 
			array (
				'sp_id_siape' => '311',
				'sp_cd_id_webservice' => '761',
				'sp_no_nome' => '11ª SRPRF/PE - NÚCLEO DE ASSUNTOS INTERNOS',
				'sp_fk_vg_vaga' => '630',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			311 => 
			array (
				'sp_id_siape' => '312',
				'sp_cd_id_webservice' => '78',
				'sp_no_nome' => '1ª SRPRF/GO - NÚCLEO DE COMUNICAÇÃO SOCIAL',
				'sp_fk_vg_vaga' => '100',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			312 => 
			array (
				'sp_id_siape' => '313',
				'sp_cd_id_webservice' => '324',
				'sp_no_nome' => '4ª SRPRF/MG - DEL. 17 - DELEGACIA DE UBERLÂNDIA',
				'sp_fk_vg_vaga' => '301',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			313 => 
			array (
				'sp_id_siape' => '314',
				'sp_cd_id_webservice' => '302',
				'sp_no_nome' => '4ª SRPRF/MG - DEL. 11 - DELEGACIA DE PATOS DE MINAS',
				'sp_fk_vg_vaga' => '302',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			314 => 
			array (
				'sp_id_siape' => '315',
				'sp_cd_id_webservice' => '751',
				'sp_no_nome' => '11ª SRPRF/PE - SEÇÃO ADMINISTRATIVA E FINANCEIRA',
				'sp_fk_vg_vaga' => '630',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			315 => 
			array (
				'sp_id_siape' => '316',
				'sp_cd_id_webservice' => '900',
				'sp_no_nome' => '15ª SRPRF/RN - SEÇÃO ADMINISTRATIVA E FINANCEIRA',
				'sp_fk_vg_vaga' => '800',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			316 => 
			array (
				'sp_id_siape' => '317',
				'sp_cd_id_webservice' => '34',
				'sp_no_nome' => 'DIVISÃO DE OPERAÇÕES AÉREAS',
				'sp_fk_vg_vaga' => '2',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			317 => 
			array (
				'sp_id_siape' => '318',
				'sp_cd_id_webservice' => '1141',
				'sp_no_nome' => '21ª SRPRF/RO - NÚCLEO DE LEGISLAÇÃO E CAPACITAÇÃO DE PESSOAL',
				'sp_fk_vg_vaga' => '1040',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			318 => 
			array (
				'sp_id_siape' => '319',
				'sp_cd_id_webservice' => '22',
				'sp_no_nome' => 'DIVISÃO DE TELEMÁTICA',
				'sp_fk_vg_vaga' => '2',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			319 => 
			array (
				'sp_id_siape' => '320',
				'sp_cd_id_webservice' => '1091',
				'sp_no_nome' => '19ª SRPRF/PA - DEL. 04 - DELEGACIA DE ALTAMIRA',
				'sp_fk_vg_vaga' => '1091',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			320 => 
			array (
				'sp_id_siape' => '321',
				'sp_cd_id_webservice' => '468',
				'sp_no_nome' => '7ª SRPRF/PR - SEÇÃO DE RECURSOS HUMANOS',
				'sp_fk_vg_vaga' => '420',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			321 => 
			array (
				'sp_id_siape' => '322',
				'sp_cd_id_webservice' => '760',
				'sp_no_nome' => '11ª SRPRF/PE - CORREGEDORIA REGIONAL',
				'sp_fk_vg_vaga' => '630',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			322 => 
			array (
				'sp_id_siape' => '323',
				'sp_cd_id_webservice' => '1103',
				'sp_no_nome' => '20ª SRPRF/SE - NÚCLEO DE OPERAÇÕES ESPECIAIS',
				'sp_fk_vg_vaga' => '1000',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			323 => 
			array (
				'sp_id_siape' => '324',
				'sp_cd_id_webservice' => '690',
				'sp_no_nome' => '10ª SRPRF/BA - NÚCLEO DE SERVIÇOS GERAIS',
				'sp_fk_vg_vaga' => '580',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			324 => 
			array (
				'sp_id_siape' => '325',
				'sp_cd_id_webservice' => '935',
				'sp_no_nome' => '16ª SRPRF/CE - SEÇÃO ADMINISTRATIVA E FINANCEIRA',
				'sp_fk_vg_vaga' => '840',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			325 => 
			array (
				'sp_id_siape' => '326',
				'sp_cd_id_webservice' => '273',
				'sp_no_nome' => '4ª SRPRF/MG - DEL. 05 - DELEGACIA DE JUIZ DE FORA',
				'sp_fk_vg_vaga' => '273',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			326 => 
			array (
				'sp_id_siape' => '327',
				'sp_cd_id_webservice' => '261',
				'sp_no_nome' => '4ª SRPRF/MG - DEL. 02 - DELEGACIA DE SETE LAGOAS',
				'sp_fk_vg_vaga' => '261',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			327 => 
			array (
				'sp_id_siape' => '328',
				'sp_cd_id_webservice' => '467',
				'sp_no_nome' => '7ª SRPRF/PR -NÚCLEO DE INFORMÁTICA E TELECOMUNICAÇÕES',
				'sp_fk_vg_vaga' => '420',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			328 => 
			array (
				'sp_id_siape' => '329',
				'sp_cd_id_webservice' => '784',
				'sp_no_nome' => 'DEL.05/UOP.03 - Unidade Operacional de Trevo do Ibó',
				'sp_fk_vg_vaga' => '784',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			329 => 
			array (
				'sp_id_siape' => '330',
				'sp_cd_id_webservice' => '541',
				'sp_no_nome' => '8ª SRPRF/SC - NÚCLEO DE INFORMÁTICA E TELECOMUNICAÇÕES',
				'sp_fk_vg_vaga' => '470',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			330 => 
			array (
				'sp_id_siape' => '331',
				'sp_cd_id_webservice' => '916',
				'sp_no_nome' => '15ª SRPRF/RN - DEL. 02 - DELEGACIA DE CEARÁ-MIRIM',
				'sp_fk_vg_vaga' => '916',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			331 => 
			array (
				'sp_id_siape' => '332',
				'sp_cd_id_webservice' => '956',
				'sp_no_nome' => '16ª SRPRF/CE - DEL. 03 - DELEGACIA DE RUSSAS',
				'sp_fk_vg_vaga' => '863',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			332 => 
			array (
				'sp_id_siape' => '333',
				'sp_cd_id_webservice' => '1020',
				'sp_no_nome' => '18ª SRPRF/MA - NÚCLEO DE PATRIMÔNIO E MATERIAL',
				'sp_fk_vg_vaga' => '920',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			333 => 
			array (
				'sp_id_siape' => '334',
				'sp_cd_id_webservice' => '373',
				'sp_no_nome' => '5ª SRPRF/RJ - DEL. 05 - DELEGACIA DE BARRA DO PIRAÍ',
				'sp_fk_vg_vaga' => '351',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			334 => 
			array (
				'sp_id_siape' => '335',
				'sp_cd_id_webservice' => '268',
				'sp_no_nome' => '4ª SRPRF/MG - DEL. 04 - DELEGACIA DE OLIVEIRA',
				'sp_fk_vg_vaga' => '268',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			335 => 
			array (
				'sp_id_siape' => '336',
				'sp_cd_id_webservice' => '26',
				'sp_no_nome' => 'COORDENAÇÃO GERAL DE OPERAÇÕES',
				'sp_fk_vg_vaga' => '2',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			336 => 
			array (
				'sp_id_siape' => '337',
				'sp_cd_id_webservice' => '340',
				'sp_no_nome' => '5ª SRPRF/RJ - NÚCLEO DE REGISTRO E MEDICINA RODOVIÁRIA',
				'sp_fk_vg_vaga' => '320',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			337 => 
			array (
				'sp_id_siape' => '338',
				'sp_cd_id_webservice' => '912',
				'sp_no_nome' => '15ª SRPRF/RN - DEL. 01 - NÚCLEO DE POLICIAMENTO E FISCALIZAÇÃO',
				'sp_fk_vg_vaga' => '819',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			338 => 
			array (
				'sp_id_siape' => '339',
				'sp_cd_id_webservice' => '211',
				'sp_no_nome' => '3ª SRPRF/MS - DEL. 04 - NÚCLEO DE POLICIAMENTO E FISCALIZAÇÃO',
				'sp_fk_vg_vaga' => '210',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			339 => 
			array (
				'sp_id_siape' => '340',
				'sp_cd_id_webservice' => '908',
				'sp_no_nome' => '15ª SRPRF/RN - NÚCLEO DE ADMINISTRAÇÃO DE PESSOAL',
				'sp_fk_vg_vaga' => '800',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			340 => 
			array (
				'sp_id_siape' => '341',
				'sp_cd_id_webservice' => '606',
				'sp_no_nome' => '9ª SRPRF/RS - CORREGEDORIA REGIONAL',
				'sp_fk_vg_vaga' => '520',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			341 => 
			array (
				'sp_id_siape' => '342',
				'sp_cd_id_webservice' => '904',
				'sp_no_nome' => '15ª SRPRF/RN - NÚCLEO DE SERVIÇOS GERAIS',
				'sp_fk_vg_vaga' => '800',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			342 => 
			array (
				'sp_id_siape' => '343',
				'sp_cd_id_webservice' => '834',
				'sp_no_nome' => '13ª SRPRF/AL - NÚCLEO DE PATRIMÔNIO E MATERIAL',
				'sp_fk_vg_vaga' => '824',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			343 => 
			array (
				'sp_id_siape' => '344',
				'sp_cd_id_webservice' => '181',
				'sp_no_nome' => '3ª SRPRF/MS - NÚCLEO DE OPERAÇÕES ESPECIAIS',
				'sp_fk_vg_vaga' => '200',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			344 => 
			array (
				'sp_id_siape' => '345',
				'sp_cd_id_webservice' => '1029',
				'sp_no_nome' => '18ª SRPRF/MA - DEL. 01 - DELEGACIA DE PEDRINHAS',
				'sp_fk_vg_vaga' => '1029',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			345 => 
			array (
				'sp_id_siape' => '346',
				'sp_cd_id_webservice' => '4',
				'sp_no_nome' => 'ASSESSORIA TÉCNICA DE GABINETE',
				'sp_fk_vg_vaga' => '2',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			346 => 
			array (
				'sp_id_siape' => '347',
				'sp_cd_id_webservice' => '835',
				'sp_no_nome' => '13ª SRPRF/AL - NÚCLEO DE DOCUMENTAÇÃO',
				'sp_fk_vg_vaga' => '824',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			347 => 
			array (
				'sp_id_siape' => '348',
				'sp_cd_id_webservice' => '898',
				'sp_no_nome' => '15ª SRPRF/RN - NÚCLEO DE MULTAS E PENALIDADES',
				'sp_fk_vg_vaga' => '800',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			348 => 
			array (
				'sp_id_siape' => '349',
				'sp_cd_id_webservice' => '600',
				'sp_no_nome' => '9ª SRPRF/RS - NÚCLEO DE DOCUMENTAÇÃO',
				'sp_fk_vg_vaga' => '520',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			349 => 
			array (
				'sp_id_siape' => '350',
				'sp_cd_id_webservice' => '563',
				'sp_no_nome' => '8ª SRPRF/SC - DEL. 04 - NÚCLEO DE POLICIAMENTO E FISCALIZAÇÃO',
				'sp_fk_vg_vaga' => '495',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			350 => 
			array (
				'sp_id_siape' => '351',
				'sp_cd_id_webservice' => '255',
				'sp_no_nome' => '4ª SRPRF/MG - NÚCLEO DE ASSUNTOS INTERNOS',
				'sp_fk_vg_vaga' => '250',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			351 => 
			array (
				'sp_id_siape' => '352',
				'sp_cd_id_webservice' => '240',
				'sp_no_nome' => '4ª SRPRF/MG - NÚCLEO DE INTELIGÊNCIA',
				'sp_fk_vg_vaga' => '250',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			352 => 
			array (
				'sp_id_siape' => '353',
				'sp_cd_id_webservice' => '45',
				'sp_no_nome' => 'DIVISÃO DE CONCURSO, SELEÇÃO E PROCESSOS',
				'sp_fk_vg_vaga' => '2',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			353 => 
			array (
				'sp_id_siape' => '354',
				'sp_cd_id_webservice' => '264',
				'sp_no_nome' => '4ª SRPRF/MG - DEL. 03 - DELEGACIA DE JOÃO MONLEVADE',
				'sp_fk_vg_vaga' => '264',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			354 => 
			array (
				'sp_id_siape' => '355',
				'sp_cd_id_webservice' => '29',
				'sp_no_nome' => 'DIVISÃO DE PLANEJAMENTO OPERACIONAL',
				'sp_fk_vg_vaga' => '2',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			355 => 
			array (
				'sp_id_siape' => '356',
				'sp_cd_id_webservice' => '55',
				'sp_no_nome' => 'DIVISÃO DE SAÚDE E ASSISTÊNCIA SOCIAL',
				'sp_fk_vg_vaga' => '2',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			356 => 
			array (
				'sp_id_siape' => '357',
				'sp_cd_id_webservice' => '329',
				'sp_no_nome' => '4ª SRPRF/MG - DEL. 18 - NÚCLEO DE POLICIAMENTO E FISCALIZAÇÃO',
				'sp_fk_vg_vaga' => '303',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			357 => 
			array (
				'sp_id_siape' => '358',
				'sp_cd_id_webservice' => '338',
				'sp_no_nome' => '5ª SRPRF/RJ - NÚCLEO DE OPERAÇÕES ESPECIAIS',
				'sp_fk_vg_vaga' => '320',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			358 => 
			array (
				'sp_id_siape' => '359',
				'sp_cd_id_webservice' => '905',
				'sp_no_nome' => '15ª SRPRF/RN - NÚCLEO DE INFORMÁTICA E TELECOMUNICAÇÕES',
				'sp_fk_vg_vaga' => '800',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			359 => 
			array (
				'sp_id_siape' => '360',
				'sp_cd_id_webservice' => '466',
				'sp_no_nome' => '7ª SRPRF/PR - NÚCLEO DE SERVIÇOS GERAIS',
				'sp_fk_vg_vaga' => '420',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			360 => 
			array (
				'sp_id_siape' => '361',
				'sp_cd_id_webservice' => '1',
				'sp_no_nome' => 'DEPARTAMENTO DE POLÍCIA RODOVIÁRIA FEDERAL',
				'sp_fk_vg_vaga' => '2',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			361 => 
			array (
				'sp_id_siape' => '362',
				'sp_cd_id_webservice' => '269',
				'sp_no_nome' => '4ª SRPRF/MG - DEL. 04 - NÚCLEO DE POLICIAMENTO E FISCALIZAÇÃO',
				'sp_fk_vg_vaga' => '268',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			362 => 
			array (
				'sp_id_siape' => '363',
				'sp_cd_id_webservice' => '73',
				'sp_no_nome' => 'DIVISÃO DE DOCUMENTAÇÃO',
				'sp_fk_vg_vaga' => '2',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			363 => 
			array (
				'sp_id_siape' => '364',
				'sp_cd_id_webservice' => '90',
				'sp_no_nome' => '1ª SRPRF/GO - SEÇÃO DE RECURSOS HUMANOS',
				'sp_fk_vg_vaga' => '100',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			364 => 
			array (
				'sp_id_siape' => '365',
				'sp_cd_id_webservice' => '595',
				'sp_no_nome' => '9ª SRPRF/RS - NÚCLEO DE MULTAS E PENALIDADES',
				'sp_fk_vg_vaga' => '520',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			365 => 
			array (
				'sp_id_siape' => '366',
				'sp_cd_id_webservice' => '312',
				'sp_no_nome' => '4ª SRPRF/MG - DEL. 14 - DELEGACIA DE MONTES CLAROS',
				'sp_fk_vg_vaga' => '295',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			366 => 
			array (
				'sp_id_siape' => '367',
				'sp_cd_id_webservice' => '226',
				'sp_no_nome' => '3ª SRPRF/MS - DEL. 08 - DELEGACIA DE TRÊS LAGOAS',
				'sp_fk_vg_vaga' => '226',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			367 => 
			array (
				'sp_id_siape' => '368',
				'sp_cd_id_webservice' => '1193',
				'sp_no_nome' => '4º DISTRITO REGIONAL DE POLÍCIA RODOVIÁRIA FEDERAL/AP',
				'sp_fk_vg_vaga' => '1110',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			368 => 
			array (
				'sp_id_siape' => '369',
				'sp_cd_id_webservice' => '348',
				'sp_no_nome' => '5ª SRPRF/RJ - NÚCLEO DE LEGISLAÇÃO E CAPACITAÇÃO DE PESSOAL',
				'sp_fk_vg_vaga' => '320',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			369 => 
			array (
				'sp_id_siape' => '370',
				'sp_cd_id_webservice' => '683',
				'sp_no_nome' => '10ª SRPRF/BA - NÚCLEO DE OPERAÇÕES ESPECIAIS',
				'sp_fk_vg_vaga' => '580',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			370 => 
			array (
				'sp_id_siape' => '371',
				'sp_cd_id_webservice' => '185',
				'sp_no_nome' => '3ª SRPRF/MS - NÚCLEO DE ORÇAMENTO E FINANÇAS',
				'sp_fk_vg_vaga' => '200',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			371 => 
			array (
				'sp_id_siape' => '372',
				'sp_cd_id_webservice' => '1027',
				'sp_no_nome' => '18ª SRPRF/MA - CORREGEDORIA REGIONAL',
				'sp_fk_vg_vaga' => '920',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			372 => 
			array (
				'sp_id_siape' => '373',
				'sp_cd_id_webservice' => '538',
				'sp_no_nome' => '8ª SRPRF/SC - NÚCLEO DE PATRIMONIO E MATERIAL',
				'sp_fk_vg_vaga' => '470',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			373 => 
			array (
				'sp_id_siape' => '374',
				'sp_cd_id_webservice' => '693',
				'sp_no_nome' => '10ª SRPRF/BA - NÚCLEO DE LEGISLAÇÃO E CAPACITAÇÃO DE PESSOAL',
				'sp_fk_vg_vaga' => '580',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			374 => 
			array (
				'sp_id_siape' => '375',
				'sp_cd_id_webservice' => '983',
				'sp_no_nome' => '17ª SRPRF/PI - NÚCLEO DE SERVIÇOS GERAIS',
				'sp_fk_vg_vaga' => '880',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			375 => 
			array (
				'sp_id_siape' => '376',
				'sp_cd_id_webservice' => '46',
				'sp_no_nome' => 'DIVISÃO DE PLANEJAMENTO, ENSINO A DISTÂNCIA E EDUCAÇÃO DE TRÂNSITO',
				'sp_fk_vg_vaga' => '43',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			376 => 
			array (
				'sp_id_siape' => '377',
				'sp_cd_id_webservice' => '18',
				'sp_no_nome' => 'COORDENAÇÃO-GERAL DE PLANEJAMENTO E MODERNIZAÇÃO RODOVIÁRIA',
				'sp_fk_vg_vaga' => '2',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			377 => 
			array (
				'sp_id_siape' => '378',
				'sp_cd_id_webservice' => '428',
				'sp_no_nome' => '6ª SRPRF/SP - DEL. 05 - DELEGACIA DE REGISTRO',
				'sp_fk_vg_vaga' => '397',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			378 => 
			array (
				'sp_id_siape' => '379',
				'sp_cd_id_webservice' => '396',
				'sp_no_nome' => '6ª SRPRF/SP - SEÇÃO DE POLICIAMENTO E FISCALIZAÇÃO',
				'sp_fk_vg_vaga' => '370',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			379 => 
			array (
				'sp_id_siape' => '380',
				'sp_cd_id_webservice' => '3',
				'sp_no_nome' => 'GABINETE',
				'sp_fk_vg_vaga' => '2',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			380 => 
			array (
				'sp_id_siape' => '381',
				'sp_cd_id_webservice' => '245',
				'sp_no_nome' => '4ª SRPRF/MG - SEÇÃO ADMINISTRATIVA E FINANCEIRA',
				'sp_fk_vg_vaga' => '250',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			381 => 
			array (
				'sp_id_siape' => '382',
				'sp_cd_id_webservice' => '248',
				'sp_no_nome' => '4ª SRPRF/MG - NÚCLEO DE DOCUMENTAÇÃO',
				'sp_fk_vg_vaga' => '250',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			382 => 
			array (
				'sp_id_siape' => '383',
				'sp_cd_id_webservice' => '244',
				'sp_no_nome' => '4ª SRPRF/MG - NÚCLEO DE REGISTRO E MEDICINA RODOVIÁRIA',
				'sp_fk_vg_vaga' => '250',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			383 => 
			array (
				'sp_id_siape' => '384',
				'sp_cd_id_webservice' => '457',
				'sp_no_nome' => '7ª SRPRF/PR - NÚCLEO DE INTELIGÊNCIA',
				'sp_fk_vg_vaga' => '420',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			384 => 
			array (
				'sp_id_siape' => '385',
				'sp_cd_id_webservice' => '531',
				'sp_no_nome' => '8ª SRPRF/SC - NÚCLEO DE INTELIGÊNCIA',
				'sp_fk_vg_vaga' => '470',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			385 => 
			array (
				'sp_id_siape' => '386',
				'sp_cd_id_webservice' => '136',
				'sp_no_nome' => '2ª SRPRF/MT - NÚCLEO DE SERVIÇOS GERAIS',
				'sp_fk_vg_vaga' => '150',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			386 => 
			array (
				'sp_id_siape' => '387',
				'sp_cd_id_webservice' => '363',
				'sp_no_nome' => '5ª SRPRF/RJ - DEL. 03 - DELEGACIA DE ITAGUAÍ',
				'sp_fk_vg_vaga' => '343',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			387 => 
			array (
				'sp_id_siape' => '388',
				'sp_cd_id_webservice' => '12',
				'sp_no_nome' => 'DIVISÃO DE CORREGEDORIA',
				'sp_fk_vg_vaga' => '2',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			388 => 
			array (
				'sp_id_siape' => '389',
				'sp_cd_id_webservice' => '1183',
				'sp_no_nome' => '2º DRPRF/TO - NÚCLEO DE CORREGEDORIA E ASSUNTOS INTERNOS',
				'sp_fk_vg_vaga' => '1174',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			389 => 
			array (
				'sp_id_siape' => '390',
				'sp_cd_id_webservice' => '1061',
				'sp_no_nome' => '19ª SRPRF/PA - NÚCLEO DE PATRIMÔNIO E MATERIAL',
				'sp_fk_vg_vaga' => '960',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			390 => 
			array (
				'sp_id_siape' => '391',
				'sp_cd_id_webservice' => '807',
				'sp_no_nome' => '12ª SRPRF/ES - CORREGEDORIA REGIONAL',
				'sp_fk_vg_vaga' => '689',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			391 => 
			array (
				'sp_id_siape' => '392',
				'sp_cd_id_webservice' => '1122',
				'sp_no_nome' => '20ª SRPRF/SE - DEL. 02 NÚCLEO DE POLICIAMENTO E FISCALIZAÇÃO',
				'sp_fk_vg_vaga' => '1021',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			392 => 
			array (
				'sp_id_siape' => '393',
				'sp_cd_id_webservice' => '798',
				'sp_no_nome' => '12ª SRPRF/ES - SEÇÃO ADMINISTRATIVA E FINANCEIRA',
				'sp_fk_vg_vaga' => '689',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			393 => 
			array (
				'sp_id_siape' => '394',
				'sp_cd_id_webservice' => '57',
				'sp_no_nome' => 'COORDENAÇÃO GERAL DE ADMINISTRAÇÃO',
				'sp_fk_vg_vaga' => '2',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			394 => 
			array (
				'sp_id_siape' => '395',
				'sp_cd_id_webservice' => '472',
				'sp_no_nome' => '7ª SRPRF/PR - NÚCLEO DE ASSUNTOS INTERNOS',
				'sp_fk_vg_vaga' => '420',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			395 => 
			array (
				'sp_id_siape' => '396',
				'sp_cd_id_webservice' => '1199',
				'sp_no_nome' => '4º DRPRF/AP - NÚCLEO DE RECURSOS HUMANOS',
				'sp_fk_vg_vaga' => '1110',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			396 => 
			array (
				'sp_id_siape' => '397',
				'sp_cd_id_webservice' => '47',
				'sp_no_nome' => 'DIVISÃO DE FORMAÇÃO, DESENVOLVIMENTO E CULTURA',
				'sp_fk_vg_vaga' => '43',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			397 => 
			array (
				'sp_id_siape' => '398',
				'sp_cd_id_webservice' => '995',
				'sp_no_nome' => '17ª SRPRF/PI - DEL. 02 - DELEGACIA DE PIRIPIRI',
				'sp_fk_vg_vaga' => '995',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			398 => 
			array (
				'sp_id_siape' => '399',
				'sp_cd_id_webservice' => '82',
				'sp_no_nome' => '1ª SRPRF/GO - NÚCLEO DE MULTAS E PENALIDADES',
				'sp_fk_vg_vaga' => '100',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			399 => 
			array (
				'sp_id_siape' => '400',
				'sp_cd_id_webservice' => '1130',
				'sp_no_nome' => '21ª SRPRF/RO - SEÇÃO DE POLICIAMENTO E FISCALIZAÇÃO',
				'sp_fk_vg_vaga' => '1040',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			400 => 
			array (
				'sp_id_siape' => '401',
				'sp_cd_id_webservice' => '1140',
				'sp_no_nome' => '21ª SRPRF/RO - SEÇÃO DE RECURSOS HUMANOS',
				'sp_fk_vg_vaga' => '1040',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			401 => 
			array (
				'sp_id_siape' => '402',
				'sp_cd_id_webservice' => '65',
				'sp_no_nome' => 'NÚCLEO DE PATRIMÔNIO',
				'sp_fk_vg_vaga' => '2',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			402 => 
			array (
				'sp_id_siape' => '403',
				'sp_cd_id_webservice' => '178',
				'sp_no_nome' => '3ª SRPRF/MS - NÚCLEO DE COMUNICAÇÃO SOCIAL',
				'sp_fk_vg_vaga' => '200',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			403 => 
			array (
				'sp_id_siape' => '404',
				'sp_cd_id_webservice' => '20',
				'sp_no_nome' => 'DIVISÃO DE ADMINISTRAÇÃO DE SISTEMAS',
				'sp_fk_vg_vaga' => '2',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			404 => 
			array (
				'sp_id_siape' => '405',
				'sp_cd_id_webservice' => '39',
				'sp_no_nome' => 'NÚCLEO DE FISCALIZAÇÃO DE TRANSPORTE DE PASSAGEIROS E CARGAS',
				'sp_fk_vg_vaga' => '2',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			405 => 
			array (
				'sp_id_siape' => '406',
				'sp_cd_id_webservice' => '1172',
				'sp_no_nome' => '1º DRPRF/DF - NÚCLEO DE RECURSOS HUMANOS',
				'sp_fk_vg_vaga' => '1080',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			406 => 
			array (
				'sp_id_siape' => '407',
				'sp_cd_id_webservice' => '870',
				'sp_no_nome' => '14ª SRPRF/PB - SEÇÃO DE RECURSOS HUMANOS',
				'sp_fk_vg_vaga' => '760',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			407 => 
			array (
				'sp_id_siape' => '408',
				'sp_cd_id_webservice' => '471',
				'sp_no_nome' => '7ª SRPRF/PR - CORREGEDORIA REGIONAL',
				'sp_fk_vg_vaga' => '420',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			408 => 
			array (
				'sp_id_siape' => '409',
				'sp_cd_id_webservice' => '696',
				'sp_no_nome' => '10ª SRPRF/BA - NÚCLEO DE ASSUNTOS INTERNOS',
				'sp_fk_vg_vaga' => '580',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			409 => 
			array (
				'sp_id_siape' => '410',
				'sp_cd_id_webservice' => '568',
				'sp_no_nome' => '8ª SRPRF/SC - DEL. 05 - DELEGACIA DE LAGES',
				'sp_fk_vg_vaga' => '497',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			410 => 
			array (
				'sp_id_siape' => '411',
				'sp_cd_id_webservice' => '1062',
				'sp_no_nome' => '19ª SRPRF/PA - NÚCLEO DE DOCUMENTAÇÃO',
				'sp_fk_vg_vaga' => '960',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			411 => 
			array (
				'sp_id_siape' => '412',
				'sp_cd_id_webservice' => '41',
				'sp_no_nome' => 'NÚCLEO DE APOIO ADMINISTRATIVO',
				'sp_fk_vg_vaga' => '2',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			412 => 
			array (
				'sp_id_siape' => '413',
				'sp_cd_id_webservice' => '369',
				'sp_no_nome' => '5ª SRPRF/RJ - DEL. 04 - DELEGACIA DE MAGÉ',
				'sp_fk_vg_vaga' => '345',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			413 => 
			array (
				'sp_id_siape' => '414',
				'sp_cd_id_webservice' => '1131',
				'sp_no_nome' => '21ª SRPRF/RO - NÚCLEO DE OPERAÇÕES ESPECIAIS',
				'sp_fk_vg_vaga' => '1040',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			414 => 
			array (
				'sp_id_siape' => '415',
				'sp_cd_id_webservice' => '836',
				'sp_no_nome' => '13ª SRPRF/AL - NÚCLEO DE SERVIÇOS GERAIS',
				'sp_fk_vg_vaga' => '824',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			415 => 
			array (
				'sp_id_siape' => '416',
				'sp_cd_id_webservice' => '28',
				'sp_no_nome' => 'NÚCLEO DE INFORMAÇÕES OPERACIONAIS',
				'sp_fk_vg_vaga' => '2',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			416 => 
			array (
				'sp_id_siape' => '417',
				'sp_cd_id_webservice' => '853',
				'sp_no_nome' => '13ª SRPRF/AL - DEL. 03 - NÚCLEO DE POLICIAMENTO E FISCALIZAÇÃO',
				'sp_fk_vg_vaga' => '852',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			417 => 
			array (
				'sp_id_siape' => '418',
				'sp_cd_id_webservice' => '1138',
				'sp_no_nome' => '21ª SRPRF/RO - NÚCLEO DE SERVIÇOS GERAIS',
				'sp_fk_vg_vaga' => '1040',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			418 => 
			array (
				'sp_id_siape' => '419',
				'sp_cd_id_webservice' => '461',
				'sp_no_nome' => '7ª SRPRF/PR - NÚCLEO DE REGISTRO E MEDICINA RODOVIÁRIA',
				'sp_fk_vg_vaga' => '420',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			419 => 
			array (
				'sp_id_siape' => '420',
				'sp_cd_id_webservice' => '1135',
				'sp_no_nome' => '21ª SRPRF/RO - NÚCLEO DE ORÇAMENTO E FINANÇAS',
				'sp_fk_vg_vaga' => '1040',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			420 => 
			array (
				'sp_id_siape' => '421',
				'sp_cd_id_webservice' => '347',
				'sp_no_nome' => '5ª SRPRF/RJ - SEÇÃO DE RECURSOS HUMANOS',
				'sp_fk_vg_vaga' => '320',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			421 => 
			array (
				'sp_id_siape' => '422',
				'sp_cd_id_webservice' => '796',
				'sp_no_nome' => '12ª SRPRF/ES - NÚCLEO DE MULTAS E PENALIDADES',
				'sp_fk_vg_vaga' => '689',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			422 => 
			array (
				'sp_id_siape' => '423',
				'sp_cd_id_webservice' => '92',
				'sp_no_nome' => '1ª SRPRF/GO - NÚCLEO DE ADMINISTRAÇÃO DE PESSOAL',
				'sp_fk_vg_vaga' => '100',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			423 => 
			array (
				'sp_id_siape' => '424',
				'sp_cd_id_webservice' => '536',
				'sp_no_nome' => '8ª SRPRF/SC - SEÇÃO ADMINISTRATIVA E FINANCEIRA',
				'sp_fk_vg_vaga' => '470',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			424 => 
			array (
				'sp_id_siape' => '425',
				'sp_cd_id_webservice' => '44',
				'sp_no_nome' => 'NÚCLEO DE EXECUÇÃO ADMINISTRATIVA',
				'sp_fk_vg_vaga' => '2',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			425 => 
			array (
				'sp_id_siape' => '426',
				'sp_cd_id_webservice' => '403',
				'sp_no_nome' => '6ª SRPRF/SP - NÚCLEO DE DOCUMENTAÇÃO',
				'sp_fk_vg_vaga' => '370',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			426 => 
			array (
				'sp_id_siape' => '427',
				'sp_cd_id_webservice' => '839',
				'sp_no_nome' => '13ª SRPRF/AL - NÚCLEO DE LEGISLAÇÃO E CAPACITAÇÃO DE PESSOAL',
				'sp_fk_vg_vaga' => '824',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			427 => 
			array (
				'sp_id_siape' => '428',
				'sp_cd_id_webservice' => '506',
				'sp_no_nome' => '7ª SRPRF/PR - DEL. 05 - NÚCLEO DE POLICIAMENTO E FISCALIZAÇÃO',
				'sp_fk_vg_vaga' => '447',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			428 => 
			array (
				'sp_id_siape' => '429',
				'sp_cd_id_webservice' => '756',
				'sp_no_nome' => '11ª SRPRF/PE - NÚCLEO DE INFORMÁTICA E TELECOMUNICAÇÕES',
				'sp_fk_vg_vaga' => '630',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			429 => 
			array (
				'sp_id_siape' => '430',
				'sp_cd_id_webservice' => '469',
				'sp_no_nome' => '7ª SRPRF/PR - NÚCLEO DE LEGISLAÇÃO E CAPACITAÇÃO DE PESSOAL',
				'sp_fk_vg_vaga' => '420',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			430 => 
			array (
				'sp_id_siape' => '431',
				'sp_cd_id_webservice' => '346',
				'sp_no_nome' => '5ª SRPRF/RJ - NÚCLEO DE INFORMÁTICA E TELECOMUNICAÇÕES',
				'sp_fk_vg_vaga' => '320',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			431 => 
			array (
				'sp_id_siape' => '432',
				'sp_cd_id_webservice' => '750',
				'sp_no_nome' => '11ª SRPRF/PE - NÚCLEO DE REGISTRO E MEDICINA RODOVIÁRIA',
				'sp_fk_vg_vaga' => '630',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			432 => 
			array (
				'sp_id_siape' => '433',
				'sp_cd_id_webservice' => '827',
				'sp_no_nome' => '13ª SRPRF/AL - NÚCLEO DE INTELIGÊNCIA',
				'sp_fk_vg_vaga' => '824',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			433 => 
			array (
				'sp_id_siape' => '434',
				'sp_cd_id_webservice' => '871',
				'sp_no_nome' => '14ª SRPRF/PB - NÚCLEO DE LEGISLAÇÃO E CAPACITAÇÃO DE PESSOAL',
				'sp_fk_vg_vaga' => '760',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			434 => 
			array (
				'sp_id_siape' => '435',
				'sp_cd_id_webservice' => '533',
				'sp_no_nome' => '8ª SRPRF/SC - NÚCLEO DE OPERAÇÕES ESPECIAIS',
				'sp_fk_vg_vaga' => '470',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			435 => 
			array (
				'sp_id_siape' => '436',
				'sp_cd_id_webservice' => '249',
				'sp_no_nome' => '4ª SRPRF/MG - NÚCLEO DE SERVIÇOS GERAIS',
				'sp_fk_vg_vaga' => '250',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			436 => 
			array (
				'sp_id_siape' => '437',
				'sp_cd_id_webservice' => '1040',
				'sp_no_nome' => '18ª SRPRF/MA - DEL. 03 - NÚCLEO DE POLICIAMENTO E FISCALIZAÇÃO',
				'sp_fk_vg_vaga' => '943',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			437 => 
			array (
				'sp_id_siape' => '438',
				'sp_cd_id_webservice' => '902',
				'sp_no_nome' => '15ª SRPRF/RN - NÚCLEO DE PATRIMÔNIO E MATERIAL',
				'sp_fk_vg_vaga' => '800',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			438 => 
			array (
				'sp_id_siape' => '439',
				'sp_cd_id_webservice' => '328',
				'sp_no_nome' => '4ª SRPRF/MG - DEL. 18 - DELEGACIA DE FRUTAL',
				'sp_fk_vg_vaga' => '303',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			439 => 
			array (
				'sp_id_siape' => '440',
				'sp_cd_id_webservice' => '83',
				'sp_no_nome' => '1ª SRPRF/GO - NÚCLEO DE REGISTRO E MEDICINA RODOVIÁRIA',
				'sp_fk_vg_vaga' => '100',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			440 => 
			array (
				'sp_id_siape' => '441',
				'sp_cd_id_webservice' => '545',
				'sp_no_nome' => '8ª SRPRF/SC - CORREGEDORIA REGIONAL',
				'sp_fk_vg_vaga' => '470',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			441 => 
			array (
				'sp_id_siape' => '442',
				'sp_cd_id_webservice' => '1134',
				'sp_no_nome' => '21ª SRPRF/RO - SEÇÃO ADMINISTRATIVA E FINANCEIRA',
				'sp_fk_vg_vaga' => '1040',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			442 => 
			array (
				'sp_id_siape' => '443',
				'sp_cd_id_webservice' => '804',
				'sp_no_nome' => '12ª SRPRF/ES - SEÇÃO DE RECURSOS HUMANOS',
				'sp_fk_vg_vaga' => '689',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			443 => 
			array (
				'sp_id_siape' => '444',
				'sp_cd_id_webservice' => '464',
				'sp_no_nome' => '7ª SRPRF/PR - NÚCLEO DE PATRIMÔNIO E MATERIAL',
				'sp_fk_vg_vaga' => '420',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			444 => 
			array (
				'sp_id_siape' => '445',
				'sp_cd_id_webservice' => '460',
				'sp_no_nome' => '7ª SRPRF/PR - NÚCLEO DE MULTAS E PENALIDADES',
				'sp_fk_vg_vaga' => '420',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			445 => 
			array (
				'sp_id_siape' => '446',
				'sp_cd_id_webservice' => '1142',
				'sp_no_nome' => '21ª SRPRF/RO - NÚCLEO DE ADMINISTRAÇÃO DE PESSOAL',
				'sp_fk_vg_vaga' => '1040',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			446 => 
			array (
				'sp_id_siape' => '447',
				'sp_cd_id_webservice' => '753',
				'sp_no_nome' => '11ª SRPRF/PE - NÚCLEO DE PATRIMÔNIO E MATERIAL',
				'sp_fk_vg_vaga' => '630',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			447 => 
			array (
				'sp_id_siape' => '448',
				'sp_cd_id_webservice' => '1235',
				'sp_no_nome' => '11ª SRPRF/PE - CENTRAL DE INFORMAÇÕES OPERACIONAIS',
				'sp_fk_vg_vaga' => '630',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			448 => 
			array (
				'sp_id_siape' => '449',
				'sp_cd_id_webservice' => '246',
				'sp_no_nome' => '4ª SRPRF/MG - NÚCLEO DE ORÇAMENTO E FINANÇAS',
				'sp_fk_vg_vaga' => '250',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			449 => 
			array (
				'sp_id_siape' => '450',
				'sp_cd_id_webservice' => '1063',
				'sp_no_nome' => '19ª SRPRF/PA - NÚCLEO DE SERVIÇOS GERAIS',
				'sp_fk_vg_vaga' => '960',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			450 => 
			array (
				'sp_id_siape' => '451',
				'sp_cd_id_webservice' => '104',
				'sp_no_nome' => '1ª SRPRF/GO - DEL. 03 - DELEGACIA DE MORRINHOS',
				'sp_fk_vg_vaga' => '123',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			451 => 
			array (
				'sp_id_siape' => '452',
				'sp_cd_id_webservice' => '755',
				'sp_no_nome' => '11ª SRPRF/PE - NÚCLEO DE SERVIÇOS GERAIS',
				'sp_fk_vg_vaga' => '630',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			452 => 
			array (
				'sp_id_siape' => '453',
				'sp_cd_id_webservice' => '456',
				'sp_no_nome' => '7ª SRPRF/PR - NÚCLEO DE COMUNICAÇÃO SOCIAL',
				'sp_fk_vg_vaga' => '420',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			453 => 
			array (
				'sp_id_siape' => '454',
				'sp_cd_id_webservice' => '1127',
				'sp_no_nome' => '21ª SRPRF/RO - NÚCLEO DE APOIO TÉCNICO',
				'sp_fk_vg_vaga' => '1040',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			454 => 
			array (
				'sp_id_siape' => '455',
				'sp_cd_id_webservice' => '833',
				'sp_no_nome' => '13ª SRPRF/AL - NÚCLEO DE ORÇAMENTO E FINANÇAS',
				'sp_fk_vg_vaga' => '824',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			455 => 
			array (
				'sp_id_siape' => '456',
				'sp_cd_id_webservice' => '1057',
				'sp_no_nome' => '19ª SRPRF/PA - NÚCLEO DE MULTAS E PENALIDADES',
				'sp_fk_vg_vaga' => '960',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			456 => 
			array (
				'sp_id_siape' => '457',
				'sp_cd_id_webservice' => '910',
				'sp_no_nome' => '15ª SRPRF/RN - NÚCLEO DE ASSUNTOS INTERNOS',
				'sp_fk_vg_vaga' => '800',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			457 => 
			array (
				'sp_id_siape' => '458',
				'sp_cd_id_webservice' => '252',
				'sp_no_nome' => '4ª SRPRF/MG - NÚCLEO DE LEGISLAÇÃO E CAPACITAÇÃO DE PESSOAL',
				'sp_fk_vg_vaga' => '250',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			458 => 
			array (
				'sp_id_siape' => '459',
				'sp_cd_id_webservice' => '404',
				'sp_no_nome' => '6ª SRPRF/SP - NÚCLEO DE SERVIÇOS GERAIS',
				'sp_fk_vg_vaga' => '370',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			459 => 
			array (
				'sp_id_siape' => '460',
				'sp_cd_id_webservice' => '188',
				'sp_no_nome' => '3ª SRPRF/MS - NÚCLEO DE SERVIÇOS GERAIS',
				'sp_fk_vg_vaga' => '200',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			460 => 
			array (
				'sp_id_siape' => '461',
				'sp_cd_id_webservice' => '791',
				'sp_no_nome' => '12ª SRPRF/ES - NÚCLEO DE APOIO TÉCNICO',
				'sp_fk_vg_vaga' => '689',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			461 => 
			array (
				'sp_id_siape' => '462',
				'sp_cd_id_webservice' => '869',
				'sp_no_nome' => '14ª SRPRF/PB - NÚCLEO DE INFORMÁTICA E TELECOMUNICAÇÕES',
				'sp_fk_vg_vaga' => '760',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			462 => 
			array (
				'sp_id_siape' => '463',
				'sp_cd_id_webservice' => '529',
				'sp_no_nome' => '8ª SRPRF/SC - NÚCLEO DE APOIO TÉCNICO',
				'sp_fk_vg_vaga' => '470',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			463 => 
			array (
				'sp_id_siape' => '464',
				'sp_cd_id_webservice' => '382',
				'sp_no_nome' => '5ª SRPRF/RJ - DEL. 07 - DELEGACIA DE RESENDE',
				'sp_fk_vg_vaga' => '355',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			464 => 
			array (
				'sp_id_siape' => '465',
				'sp_cd_id_webservice' => '838',
				'sp_no_nome' => '13ª SRPRF/AL - SEÇÃO DE RECURSOS HUMANOS',
				'sp_fk_vg_vaga' => '824',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			465 => 
			array (
				'sp_id_siape' => '466',
				'sp_cd_id_webservice' => '1106',
				'sp_no_nome' => '20ª SRPRF/SE - SEÇÃO ADMINISTRATIVA E FINANCEIRA',
				'sp_fk_vg_vaga' => '1000',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			466 => 
			array (
				'sp_id_siape' => '467',
				'sp_cd_id_webservice' => '888',
				'sp_no_nome' => '14ª SRPRF/PB - DEL. 03 - NÚCLEO DE POLICIAMENTO E FISCALIZAÇÃO',
				'sp_fk_vg_vaga' => '783',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			467 => 
			array (
				'sp_id_siape' => '468',
				'sp_cd_id_webservice' => '251',
				'sp_no_nome' => '4ª SRPRF/MG - SEÇÃO DE RECURSOS HUMANOS',
				'sp_fk_vg_vaga' => '250',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			468 => 
			array (
				'sp_id_siape' => '469',
				'sp_cd_id_webservice' => '534',
				'sp_no_nome' => '8ª SRPRF/SC - NÚCLEO DE MULTAS E PENALIDADES',
				'sp_fk_vg_vaga' => '470',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			469 => 
			array (
				'sp_id_siape' => '470',
				'sp_cd_id_webservice' => '13',
				'sp_no_nome' => 'NÚCLEO DE ANÁLISE PROCESSUAL',
				'sp_fk_vg_vaga' => '2',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			470 => 
			array (
				'sp_id_siape' => '471',
				'sp_cd_id_webservice' => '569',
				'sp_no_nome' => '8ª SRPRF/SC - DEL. 05 - NÚCLEO DE POLICIAMENTO E FISCALIZAÇÃO',
				'sp_fk_vg_vaga' => '497',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			471 => 
			array (
				'sp_id_siape' => '472',
				'sp_cd_id_webservice' => '924',
				'sp_no_nome' => '15ª SRPRF/RN - DEL. 04 - NÚCLEO DE POLICIAMENTO E FISCALIZAÇÃO',
				'sp_fk_vg_vaga' => '825',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			472 => 
			array (
				'sp_id_siape' => '473',
				'sp_cd_id_webservice' => '306',
				'sp_no_nome' => '4ª SRPRF/MG - DEL. 12 - NÚCLEO DE POLICIAMENTO E FISCALIZAÇÃO',
				'sp_fk_vg_vaga' => '291',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			473 => 
			array (
				'sp_id_siape' => '474',
				'sp_cd_id_webservice' => '250',
				'sp_no_nome' => '4ª SRPRF/MG - NÚCLEO DE INFORMÁTICA E TELECOMUNICAÇÕES',
				'sp_fk_vg_vaga' => '250',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			474 => 
			array (
				'sp_id_siape' => '475',
				'sp_cd_id_webservice' => '309',
				'sp_no_nome' => '4ª SRPRF/MG - DEL. 13 - DELEGACIA DE POÇOS DE CALDAS',
				'sp_fk_vg_vaga' => '293',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			475 => 
			array (
				'sp_id_siape' => '476',
				'sp_cd_id_webservice' => '63',
				'sp_no_nome' => 'SEÇÃO DE EXECUÇÃO FINANCEIRA',
				'sp_fk_vg_vaga' => '2',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			476 => 
			array (
				'sp_id_siape' => '477',
				'sp_cd_id_webservice' => '305',
				'sp_no_nome' => '4ª SRPRF/MG - DEL. 12 - DELEGACIA DE TEÓFILO OTONI',
				'sp_fk_vg_vaga' => '291',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			477 => 
			array (
				'sp_id_siape' => '478',
				'sp_cd_id_webservice' => '540',
				'sp_no_nome' => '8ª SRPRF/SC - NÚCLEO DE SERVIÇOS GERAIS',
				'sp_fk_vg_vaga' => '470',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			478 => 
			array (
				'sp_id_siape' => '479',
				'sp_cd_id_webservice' => '773',
				'sp_no_nome' => '11ª SRPRF/PE - DEL. 03 - NÚCLEO DE POLICIAMENTO E FISCALIZAÇÃO',
				'sp_fk_vg_vaga' => '653',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			479 => 
			array (
				'sp_id_siape' => '480',
				'sp_cd_id_webservice' => '31',
				'sp_no_nome' => 'DIVISÃO DE MULTAS E PENALIDADES',
				'sp_fk_vg_vaga' => '2',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			480 => 
			array (
				'sp_id_siape' => '481',
				'sp_cd_id_webservice' => '25',
				'sp_no_nome' => 'NÚCLEO DE PLANEJAMENTO INSTITUCIONAL',
				'sp_fk_vg_vaga' => '2',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			481 => 
			array (
				'sp_id_siape' => '482',
				'sp_cd_id_webservice' => '689',
				'sp_no_nome' => '10ª SRPRF/BA - NÚCLEO DE DOCUMENTAÇÃO',
				'sp_fk_vg_vaga' => '580',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			482 => 
			array (
				'sp_id_siape' => '483',
				'sp_cd_id_webservice' => '821',
				'sp_no_nome' => '12ª SRPRF/ES - DEL. 04 - NÚCLEO DE POLICIAMENTO E FISCALIZAÇÃO',
				'sp_fk_vg_vaga' => '705',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			483 => 
			array (
				'sp_id_siape' => '484',
				'sp_cd_id_webservice' => '759',
				'sp_no_nome' => '11ª SRPRF/PE - NÚCLEO DE ADMINISTRAÇÃO DE PESSOAL',
				'sp_fk_vg_vaga' => '630',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			484 => 
			array (
				'sp_id_siape' => '485',
				'sp_cd_id_webservice' => '1018',
				'sp_no_nome' => '18ª SRPRF/MA - SEÇÃO ADMINISTRATIVA E FINANCEIRA',
				'sp_fk_vg_vaga' => '920',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			485 => 
			array (
				'sp_id_siape' => '486',
				'sp_cd_id_webservice' => '543',
				'sp_no_nome' => '8ª SRPRF/SC - NÚCLEO DE LEGISLAÇÃO E CAPACITAÇÃO DE PESSOAL',
				'sp_fk_vg_vaga' => '470',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			486 => 
			array (
				'sp_id_siape' => '487',
				'sp_cd_id_webservice' => '1021',
				'sp_no_nome' => '18ª SRPRF/MA - NÚCLEO DE DOCUMENTAÇÃO',
				'sp_fk_vg_vaga' => '920',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			487 => 
			array (
				'sp_id_siape' => '488',
				'sp_cd_id_webservice' => '974',
				'sp_no_nome' => '17ª SRPRF/PI - NÚCLEO DE INTELIGÊNCIA',
				'sp_fk_vg_vaga' => '880',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			488 => 
			array (
				'sp_id_siape' => '489',
				'sp_cd_id_webservice' => '874',
				'sp_no_nome' => '14ª SRPRF/PB - NÚCLEO DE ASSUNTOS INTERNOS',
				'sp_fk_vg_vaga' => '760',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			489 => 
			array (
				'sp_id_siape' => '490',
				'sp_cd_id_webservice' => '842',
				'sp_no_nome' => '13ª SRPRF/AL - NÚCLEO DE ASSUNTOS INTERNOS',
				'sp_fk_vg_vaga' => '824',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			490 => 
			array (
				'sp_id_siape' => '491',
				'sp_cd_id_webservice' => '734',
				'sp_no_nome' => '10ª SRPRF/BA - DEL. 09 - DELEGACIA DE EUNÁPOLIS',
				'sp_fk_vg_vaga' => '615',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			491 => 
			array (
				'sp_id_siape' => '492',
				'sp_cd_id_webservice' => '976',
				'sp_no_nome' => '17ª SRPRF/PI - NÚCLEO DE OPERAÇÕES ESPECIAIS',
				'sp_fk_vg_vaga' => '880',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			492 => 
			array (
				'sp_id_siape' => '493',
				'sp_cd_id_webservice' => '470',
				'sp_no_nome' => '7ª SRPRF/PR - NÚCLEO DE ADMINISTRAÇÃO DE PESSOAL',
				'sp_fk_vg_vaga' => '420',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			493 => 
			array (
				'sp_id_siape' => '494',
				'sp_cd_id_webservice' => '53',
				'sp_no_nome' => 'DIVISÃO DE PAGAMENTO',
				'sp_fk_vg_vaga' => '2',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			494 => 
			array (
				'sp_id_siape' => '495',
				'sp_cd_id_webservice' => '247',
				'sp_no_nome' => '4ª SRPRF/MG - NÚCLEO DE PATRIMÔNIO E MATERIAL',
				'sp_fk_vg_vaga' => '250',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			495 => 
			array (
				'sp_id_siape' => '496',
				'sp_cd_id_webservice' => '981',
				'sp_no_nome' => '17ª SRPRF/PI - NÚCLEO DE PATRIMÔNIO E MATERIAL',
				'sp_fk_vg_vaga' => '880',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			496 => 
			array (
				'sp_id_siape' => '497',
				'sp_cd_id_webservice' => '416',
				'sp_no_nome' => '6ª SRPRF/SP - DEL. 02 - NÚCLEO DE POLICIAMENTO E FISCALIZAÇÃO',
				'sp_fk_vg_vaga' => '391',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			497 => 
			array (
				'sp_id_siape' => '498',
				'sp_cd_id_webservice' => '238',
				'sp_no_nome' => '4ª SRPRF/MG - NÚCLEO DE APOIO TÉCNICO',
				'sp_fk_vg_vaga' => '250',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			498 => 
			array (
				'sp_id_siape' => '499',
				'sp_cd_id_webservice' => '407',
				'sp_no_nome' => '6ª SRPRF/SP - NÚCLEO DE LEGISLAÇÃO E CAPACITAÇÃO DE PESSOAL',
				'sp_fk_vg_vaga' => '370',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			499 => 
			array (
				'sp_id_siape' => '500',
				'sp_cd_id_webservice' => '180',
				'sp_no_nome' => '3ª SRPRF/MS - SEÇÃO DE POLICIAMENTO E FISCALIZAÇÃO',
				'sp_fk_vg_vaga' => '200',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
		));
		\DB::table('tb_sp_siape')->insert(array (
			0 => 
			array (
				'sp_id_siape' => '501',
				'sp_cd_id_webservice' => '1015',
				'sp_no_nome' => '18ª SRPRF/MA - NÚCLEO DE OPERAÇÕES ESPECIAIS',
				'sp_fk_vg_vaga' => '920',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			1 => 
			array (
				'sp_id_siape' => '502',
				'sp_cd_id_webservice' => '140',
				'sp_no_nome' => '2ª SRPRF/MT - NÚCLEO DE ADMINISTRAÇÃO DE PESSOAL',
				'sp_fk_vg_vaga' => '150',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			2 => 
			array (
				'sp_id_siape' => '503',
				'sp_cd_id_webservice' => '316',
				'sp_no_nome' => '4ª SRPRF/MG - DEL. 15 - DELEGACIA DE UBERABA',
				'sp_fk_vg_vaga' => '297',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			3 => 
			array (
				'sp_id_siape' => '504',
				'sp_cd_id_webservice' => '1181',
				'sp_no_nome' => '2º DRPRF/TO - NÚCLEO ADMINISTRATIVO E FINANCEIRO',
				'sp_fk_vg_vaga' => '1174',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			4 => 
			array (
				'sp_id_siape' => '505',
				'sp_cd_id_webservice' => '725',
				'sp_no_nome' => '10ª SRPRF/BA - DEL. 07 - DELEGACIA DE PAULO AFONSO',
				'sp_fk_vg_vaga' => '725',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			5 => 
			array (
				'sp_id_siape' => '506',
				'sp_cd_id_webservice' => '409',
				'sp_no_nome' => '6ª SRPRF/SP - CORREGEDORIA REGIONAL',
				'sp_fk_vg_vaga' => '370',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			6 => 
			array (
				'sp_id_siape' => '507',
				'sp_cd_id_webservice' => '239',
				'sp_no_nome' => '4ª SRPRF/MG - NÚCLEO DE COMUNICAÇÃO SOCIAL',
				'sp_fk_vg_vaga' => '250',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			7 => 
			array (
				'sp_id_siape' => '508',
				'sp_cd_id_webservice' => '405',
				'sp_no_nome' => '6ª SRPRF/SP - NÚCLEO DE INFORMÁTICA E TELECOMUNICAÇÕES',
				'sp_fk_vg_vaga' => '370',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			8 => 
			array (
				'sp_id_siape' => '509',
				'sp_cd_id_webservice' => '1200',
				'sp_no_nome' => '4º DRPRF/AP - NÚCLEO DE ASSUNTOS INTERNOS',
				'sp_fk_vg_vaga' => '1110',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			9 => 
			array (
				'sp_id_siape' => '510',
				'sp_cd_id_webservice' => '48',
				'sp_no_nome' => 'NÚCLEO DE FORMAÇÃO, QUALIFICAÇÃO E REQUALIFICAÇÃO PROFISSIONAL',
				'sp_fk_vg_vaga' => '43',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			10 => 
			array (
				'sp_id_siape' => '511',
				'sp_cd_id_webservice' => '184',
				'sp_no_nome' => '3ª SRPRF/MS - SEÇÃO ADMINISTRATIVA E FINANCEIRA',
				'sp_fk_vg_vaga' => '200',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			11 => 
			array (
				'sp_id_siape' => '512',
				'sp_cd_id_webservice' => '133',
				'sp_no_nome' => '2ª SRPRF/MT - NÚCLEO DE ORÇAMENTO E FINANÇAS',
				'sp_fk_vg_vaga' => '150',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			12 => 
			array (
				'sp_id_siape' => '513',
				'sp_cd_id_webservice' => '943',
				'sp_no_nome' => '16ª SRPRF/CE - NÚCLEO DE ADMINISTRAÇÃO DE PESSOAL',
				'sp_fk_vg_vaga' => '840',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			13 => 
			array (
				'sp_id_siape' => '514',
				'sp_cd_id_webservice' => '303',
				'sp_no_nome' => '4ª SRPRF/MG - DEL. 11 - NÚCLEO DE POLICIAMENTO E FISCALIZAÇÃO',
				'sp_fk_vg_vaga' => '302',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			14 => 
			array (
				'sp_id_siape' => '515',
				'sp_cd_id_webservice' => '685',
				'sp_no_nome' => '10ª SRPRF/BA - NÚCLEO DE REGISTRO E MED. RODOVIARIA',
				'sp_fk_vg_vaga' => '580',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			15 => 
			array (
				'sp_id_siape' => '516',
				'sp_cd_id_webservice' => '9',
				'sp_no_nome' => 'SEÇÃO DE CONTRA-INTELIGÊNCIA',
				'sp_fk_vg_vaga' => '2',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			16 => 
			array (
				'sp_id_siape' => '517',
				'sp_cd_id_webservice' => '40',
				'sp_no_nome' => 'NÚCLEO DE MOTOCICLISMO',
				'sp_fk_vg_vaga' => '2',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			17 => 
			array (
				'sp_id_siape' => '518',
				'sp_cd_id_webservice' => '752',
				'sp_no_nome' => '11ª SRPRF/PE - NÚCLEO DE ORÇAMENTO E FINANÇAS',
				'sp_fk_vg_vaga' => '630',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			18 => 
			array (
				'sp_id_siape' => '519',
				'sp_cd_id_webservice' => '1019',
				'sp_no_nome' => '18ª SRPRF/MA - NÚCLEO DE ORÇAMENTO E FINANÇAS',
				'sp_fk_vg_vaga' => '920',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			19 => 
			array (
				'sp_id_siape' => '520',
				'sp_cd_id_webservice' => '757',
				'sp_no_nome' => '11ª SRPRF/PE - SEÇÃO DE RECURSOS HUMANOS',
				'sp_fk_vg_vaga' => '630',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			20 => 
			array (
				'sp_id_siape' => '521',
				'sp_cd_id_webservice' => '173',
				'sp_no_nome' => '2ª SRPRF/MT - DEL. 08 - NÚCLEO DE POLICIAMENTO E FISCALIZAÇÃO',
				'sp_fk_vg_vaga' => '173',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			21 => 
			array (
				'sp_id_siape' => '522',
				'sp_cd_id_webservice' => '465',
				'sp_no_nome' => '7ª SRPRF/PR - NÚCLEO DE DOCUMENTAÇÃO',
				'sp_fk_vg_vaga' => '420',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			22 => 
			array (
				'sp_id_siape' => '523',
				'sp_cd_id_webservice' => '749',
				'sp_no_nome' => '11ª SRPRF/PE - NÚCLEO DE MULTAS E PENALIDADES',
				'sp_fk_vg_vaga' => '630',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			23 => 
			array (
				'sp_id_siape' => '524',
				'sp_cd_id_webservice' => '680',
				'sp_no_nome' => '10ª SRPRF/BA - NÚCLEO DE COMUNICAÇÃO SOCIAL',
				'sp_fk_vg_vaga' => '580',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			24 => 
			array (
				'sp_id_siape' => '525',
				'sp_cd_id_webservice' => '43',
				'sp_no_nome' => 'COORDENAÇÃO DE ENSINO',
				'sp_fk_vg_vaga' => '43',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			25 => 
			array (
				'sp_id_siape' => '526',
				'sp_cd_id_webservice' => '60',
				'sp_no_nome' => 'SEÇÃO DE EXECUÇÃO ORÇAMENTÁRIA',
				'sp_fk_vg_vaga' => '2',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			26 => 
			array (
				'sp_id_siape' => '527',
				'sp_cd_id_webservice' => '91',
				'sp_no_nome' => '1ª SRPRF/GO - NÚCLEO DE LEGISLAÇÃO E CAPACITAÇÃO DE PESSOAL',
				'sp_fk_vg_vaga' => '100',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			27 => 
			array (
				'sp_id_siape' => '528',
				'sp_cd_id_webservice' => '61',
				'sp_no_nome' => 'NÚCLEO DE ARRECADAÇÃO',
				'sp_fk_vg_vaga' => '2',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			28 => 
			array (
				'sp_id_siape' => '529',
				'sp_cd_id_webservice' => '901',
				'sp_no_nome' => '15ª SRPRF/RN - NÚCLEO DE ORÇAMENTO E FINANÇAS',
				'sp_fk_vg_vaga' => '800',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			29 => 
			array (
				'sp_id_siape' => '530',
				'sp_cd_id_webservice' => '1060',
				'sp_no_nome' => '19ª SRPRF/PA - NÚCLEO DE ORÇAMENTO E FINANÇAS',
				'sp_fk_vg_vaga' => '960',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			30 => 
			array (
				'sp_id_siape' => '531',
				'sp_cd_id_webservice' => '498',
				'sp_no_nome' => '7ª SRPRF/PR - DEL. 04 - NÚCLEO DE POLICIAMENTO E FISCALIZAÇÃO',
				'sp_fk_vg_vaga' => '445',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			31 => 
			array (
				'sp_id_siape' => '532',
				'sp_cd_id_webservice' => '351',
				'sp_no_nome' => '5ª SRPRF/RJ - NÚCLEO DE ASSUNTOS INTERNOS',
				'sp_fk_vg_vaga' => '320',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			32 => 
			array (
				'sp_id_siape' => '533',
				'sp_cd_id_webservice' => '342',
				'sp_no_nome' => '5ª SRPRF/RJ - NÚCLEO DE ORÇAMENTO E FINANÇAS',
				'sp_fk_vg_vaga' => '320',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			33 => 
			array (
				'sp_id_siape' => '534',
				'sp_cd_id_webservice' => '8',
				'sp_no_nome' => 'DIVISÃO DE OPERAÇÕES DE INTELIGÊNCIA',
				'sp_fk_vg_vaga' => '2',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			34 => 
			array (
				'sp_id_siape' => '535',
				'sp_cd_id_webservice' => '194',
				'sp_no_nome' => '3ª SRPRF/MS - NÚCLEO DE ASSUNTOS INTERNOS',
				'sp_fk_vg_vaga' => '200',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			35 => 
			array (
				'sp_id_siape' => '536',
				'sp_cd_id_webservice' => '408',
				'sp_no_nome' => '6ª SRPRF/SP - NÚCLEO DE ADMINISTRAÇÃO DE PESSOAL',
				'sp_fk_vg_vaga' => '370',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			36 => 
			array (
				'sp_id_siape' => '537',
				'sp_cd_id_webservice' => '862',
				'sp_no_nome' => '14ª SRPRF/PB - NÚCLEO DE MULTAS E PENALIDADES',
				'sp_fk_vg_vaga' => '760',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			37 => 
			array (
				'sp_id_siape' => '538',
				'sp_cd_id_webservice' => '335',
				'sp_no_nome' => '5ª SRPRF/RJ - NÚCLEO DE COMUNICAÇÃO SOCIAL',
				'sp_fk_vg_vaga' => '320',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			38 => 
			array (
				'sp_id_siape' => '539',
				'sp_cd_id_webservice' => '1054',
				'sp_no_nome' => '19ª SRPRF/PA - NÚCLEO DE INTELIGÊNCIA',
				'sp_fk_vg_vaga' => '960',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			39 => 
			array (
				'sp_id_siape' => '540',
				'sp_cd_id_webservice' => '162',
				'sp_no_nome' => '2ª SRPRF/MT - DEL. 05 - NÚCLEO DE POLICIAMENTO E FISCALIZAÇÃO',
				'sp_fk_vg_vaga' => '161',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			40 => 
			array (
				'sp_id_siape' => '541',
				'sp_cd_id_webservice' => '462',
				'sp_no_nome' => '7ª SRPRF/PR - SEÇÃO ADMINISTRATIVA E FINANCEIRA',
				'sp_fk_vg_vaga' => '420',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			41 => 
			array (
				'sp_id_siape' => '542',
				'sp_cd_id_webservice' => '805',
				'sp_no_nome' => '12ª SRPRF/ES - NÚCLEO DE LEGISLAÇÃO E CAPACITAÇÃO DE PESSOAL',
				'sp_fk_vg_vaga' => '689',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			42 => 
			array (
				'sp_id_siape' => '543',
				'sp_cd_id_webservice' => '5',
				'sp_no_nome' => 'ASSESSORIA TÉCNICA DE COMUNICAÇÃO SOCIAL',
				'sp_fk_vg_vaga' => '2',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			43 => 
			array (
				'sp_id_siape' => '544',
				'sp_cd_id_webservice' => '985',
				'sp_no_nome' => '17ª SRPRF/PI - SEÇÃO DE RECURSOS HUMANOS',
				'sp_fk_vg_vaga' => '880',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			44 => 
			array (
				'sp_id_siape' => '545',
				'sp_cd_id_webservice' => '604',
				'sp_no_nome' => '9ª SRPRF/RS - NÚCLEO DE LEGISLAÇÃO E CAPACITAÇÃO DE PESSOAL',
				'sp_fk_vg_vaga' => '520',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			45 => 
			array (
				'sp_id_siape' => '546',
				'sp_cd_id_webservice' => '189',
				'sp_no_nome' => '3ª SRPRF/MS - NÚCLEO DE INFORMÁTICA E TELECOMUNICAÇÕES',
				'sp_fk_vg_vaga' => '200',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			46 => 
			array (
				'sp_id_siape' => '547',
				'sp_cd_id_webservice' => '868',
				'sp_no_nome' => '14ª SRPRF/PB - NÚCLEO DE SERVIÇOS GERAIS',
				'sp_fk_vg_vaga' => '760',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			47 => 
			array (
				'sp_id_siape' => '548',
				'sp_cd_id_webservice' => '939',
				'sp_no_nome' => '16ª SRPRF/CE - NÚCLEO DE SERVIÇOS GERAIS',
				'sp_fk_vg_vaga' => '840',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			48 => 
			array (
				'sp_id_siape' => '549',
				'sp_cd_id_webservice' => '938',
				'sp_no_nome' => '16ª SRPRF/CE - NÚCLEO DE DOCUMENTAÇÃO',
				'sp_fk_vg_vaga' => '840',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			49 => 
			array (
				'sp_id_siape' => '550',
				'sp_cd_id_webservice' => '940',
				'sp_no_nome' => '16ª SRPRF/CE - NÚCLEO DE INFORMÁTICA E TELECOMUNICAÇÕES',
				'sp_fk_vg_vaga' => '840',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			50 => 
			array (
				'sp_id_siape' => '551',
				'sp_cd_id_webservice' => '872',
				'sp_no_nome' => '14ª SRPRF/PB - NÚCLEO DE ADMINISTRAÇÃO DE PESSOAL',
				'sp_fk_vg_vaga' => '760',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			51 => 
			array (
				'sp_id_siape' => '552',
				'sp_cd_id_webservice' => '126',
				'sp_no_nome' => '2ª SRPRF/MT - NÚCLEO DE COMUNICAÇÃO SOCIAL',
				'sp_fk_vg_vaga' => '150',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			52 => 
			array (
				'sp_id_siape' => '553',
				'sp_cd_id_webservice' => '1137',
				'sp_no_nome' => '21ª SRPRF/RO - NÚCLEO DE DOCUMENTAÇÃO',
				'sp_fk_vg_vaga' => '1040',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			53 => 
			array (
				'sp_id_siape' => '554',
				'sp_cd_id_webservice' => '740',
				'sp_no_nome' => '10ª SRPRF/BA - DEL. 10 - DELEGACIA DE BARREIRAS',
				'sp_fk_vg_vaga' => '617',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			54 => 
			array (
				'sp_id_siape' => '555',
				'sp_cd_id_webservice' => '66',
				'sp_no_nome' => 'NÚCLEO DE ALMOXARIFADO',
				'sp_fk_vg_vaga' => '1080',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			55 => 
			array (
				'sp_id_siape' => '556',
				'sp_cd_id_webservice' => '341',
				'sp_no_nome' => '5ª SRPRF/RJ - SEÇÃO ADMINISTRATIVA E FINANCEIRA',
				'sp_fk_vg_vaga' => '320',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			56 => 
			array (
				'sp_id_siape' => '557',
				'sp_cd_id_webservice' => '1174',
				'sp_no_nome' => '2º DISTRITO REGIONAL DE POLÍCIA RODOVIÁRIA FEDERAL/TO',
				'sp_fk_vg_vaga' => '1174',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			57 => 
			array (
				'sp_id_siape' => '558',
				'sp_cd_id_webservice' => '800',
				'sp_no_nome' => '12ª SRPRF/ES - NÚCLEO DE PATRIM E MATERIAL',
				'sp_fk_vg_vaga' => '689',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			58 => 
			array (
				'sp_id_siape' => '559',
				'sp_cd_id_webservice' => '395',
				'sp_no_nome' => '6ª SRPRF/SP - NÚCLEO DE INTELIGÊNCIA',
				'sp_fk_vg_vaga' => '370',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			59 => 
			array (
				'sp_id_siape' => '560',
				'sp_cd_id_webservice' => '909',
				'sp_no_nome' => '15ª SRPRF/RN - CORREGEDORIA REGIONAL',
				'sp_fk_vg_vaga' => '800',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			60 => 
			array (
				'sp_id_siape' => '561',
				'sp_cd_id_webservice' => '640',
				'sp_no_nome' => '9ª SRPRF/RS - DEL. 07 - NÚCLEO DE POLICIAMENTO E FISCALIZAÇÃO',
				'sp_fk_vg_vaga' => '551',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			61 => 
			array (
				'sp_id_siape' => '562',
				'sp_cd_id_webservice' => '876',
				'sp_no_nome' => '14ª SRPRF/PB - DEL. 01 - NÚCLEO DE POLICIAMENTO E FISCALIZAÇÃO',
				'sp_fk_vg_vaga' => '876',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			62 => 
			array (
				'sp_id_siape' => '563',
				'sp_cd_id_webservice' => '2',
				'sp_no_nome' => 'DIREÇÃO GERAL',
				'sp_fk_vg_vaga' => '2',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			63 => 
			array (
				'sp_id_siape' => '564',
				'sp_cd_id_webservice' => '537',
				'sp_no_nome' => '8ª SRPRF/SC - NÚCLEO DE ORÇAMENTO E FINANÇAS',
				'sp_fk_vg_vaga' => '470',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			64 => 
			array (
				'sp_id_siape' => '565',
				'sp_cd_id_webservice' => '334',
				'sp_no_nome' => '5ª SRPRF/RJ - NÚCLEO DE APOIO TÉCNICO',
				'sp_fk_vg_vaga' => '320',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			65 => 
			array (
				'sp_id_siape' => '566',
				'sp_cd_id_webservice' => '933',
				'sp_no_nome' => '16ª SRPRF/CE - NÚCLEO DE MULTAS E PENALIDADES',
				'sp_fk_vg_vaga' => '840',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			66 => 
			array (
				'sp_id_siape' => '567',
				'sp_cd_id_webservice' => '894',
				'sp_no_nome' => '15ª SRPRF/RN - NÚCLEO DE COMUNICAÇÃO SOCIAL',
				'sp_fk_vg_vaga' => '800',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			67 => 
			array (
				'sp_id_siape' => '568',
				'sp_cd_id_webservice' => '402',
				'sp_no_nome' => '6ª SRPRF/SP - NÚCLEO DE PATRIMÔNIO E MATERIAL',
				'sp_fk_vg_vaga' => '370',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			68 => 
			array (
				'sp_id_siape' => '569',
				'sp_cd_id_webservice' => '399',
				'sp_no_nome' => '6ª SRPRF/SP - NÚCLEO DE REGISTRO E MEDICINA RODOVIÁRIA',
				'sp_fk_vg_vaga' => '370',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			69 => 
			array (
				'sp_id_siape' => '570',
				'sp_cd_id_webservice' => '535',
				'sp_no_nome' => '8ª SRPRF/SC - NÚCLEO DE REGISTRO E MEDICINA RODOVIÁRIA',
				'sp_fk_vg_vaga' => '470',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			70 => 
			array (
				'sp_id_siape' => '571',
				'sp_cd_id_webservice' => '1101',
				'sp_no_nome' => '20ª SRPRF/SE - NÚCLEO DE INTELIGÊNCIA',
				'sp_fk_vg_vaga' => '1000',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			71 => 
			array (
				'sp_id_siape' => '572',
				'sp_cd_id_webservice' => '15',
				'sp_no_nome' => 'NÚCLEO DE OPERAÇÕES CORREICIONAIS',
				'sp_fk_vg_vaga' => '1080',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			72 => 
			array (
				'sp_id_siape' => '573',
				'sp_cd_id_webservice' => '1133',
				'sp_no_nome' => '21ª SRPRF/RO - NÚCLEO DE REGISTRO E MEDICINA RODOVIÁRIA',
				'sp_fk_vg_vaga' => '1040',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			73 => 
			array (
				'sp_id_siape' => '574',
				'sp_cd_id_webservice' => '945',
				'sp_no_nome' => '16ª SRPRF/CE - NÚCLEO DE ASSUNTOS INTERNOS',
				'sp_fk_vg_vaga' => '840',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			74 => 
			array (
				'sp_id_siape' => '575',
				'sp_cd_id_webservice' => '1132',
				'sp_no_nome' => '21ª SRPRF/RO - NÚCLEO DE MULTAS E PENALIDADES',
				'sp_fk_vg_vaga' => '1040',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			75 => 
			array (
				'sp_id_siape' => '576',
				'sp_cd_id_webservice' => '831',
				'sp_no_nome' => '13ª SRPRF/AL - NÚCLEO DE REGISTRO E MEDICINA RODOVIÁRIA',
				'sp_fk_vg_vaga' => '824',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			76 => 
			array (
				'sp_id_siape' => '577',
				'sp_cd_id_webservice' => '837',
				'sp_no_nome' => '13ª SRPRF/AL - NÚCLEO DE INFORMÁTICA E TELECOMUNICAÇÕES',
				'sp_fk_vg_vaga' => '824',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			77 => 
			array (
				'sp_id_siape' => '578',
				'sp_cd_id_webservice' => '832',
				'sp_no_nome' => '13ª SRPRF/AL - SEÇÃO ADMINISTRATIVA E FINANCEIRA',
				'sp_fk_vg_vaga' => '824',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			78 => 
			array (
				'sp_id_siape' => '579',
				'sp_cd_id_webservice' => '1190',
				'sp_no_nome' => '3º DRPRF/AM - NÚCLEO ADMINISTRATIVO E FINANCEIRO',
				'sp_fk_vg_vaga' => '1100',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			79 => 
			array (
				'sp_id_siape' => '580',
				'sp_cd_id_webservice' => '1191',
				'sp_no_nome' => '3º DRPRF/AM - NÚCLEO DE RECURSOS HUMANOS',
				'sp_fk_vg_vaga' => '1100',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			80 => 
			array (
				'sp_id_siape' => '581',
				'sp_cd_id_webservice' => '1136',
				'sp_no_nome' => '21ª SRPRF/RO - NÚCLEO DE PATRIMÔNIO E MATERIAL',
				'sp_fk_vg_vaga' => '1040',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			81 => 
			array (
				'sp_id_siape' => '582',
				'sp_cd_id_webservice' => '401',
				'sp_no_nome' => '6ª SRPRF/SP - NÚCLEO DE ORÇAMENTO E FINANÇAS',
				'sp_fk_vg_vaga' => '370',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			82 => 
			array (
				'sp_id_siape' => '583',
				'sp_cd_id_webservice' => '1065',
				'sp_no_nome' => '19ª SRPRF/PA - SEÇÃO DE RECURSOS HUMANOS',
				'sp_fk_vg_vaga' => '960',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			83 => 
			array (
				'sp_id_siape' => '584',
				'sp_cd_id_webservice' => '792',
				'sp_no_nome' => '12ª SRPRF/ES - NÚCLEO DE COMUNICAÇÃO SOCIAL',
				'sp_fk_vg_vaga' => '689',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			84 => 
			array (
				'sp_id_siape' => '585',
				'sp_cd_id_webservice' => '74',
				'sp_no_nome' => 'NÚCLEO DE PROTOCOLO E ARQUIVO',
				'sp_fk_vg_vaga' => '2',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			85 => 
			array (
				'sp_id_siape' => '586',
				'sp_cd_id_webservice' => '1066',
				'sp_no_nome' => '19ª SRPRF/PA - NÚCLEO DE LEGISLAÇÃO E CAPACITAÇÃO DE PESSOAL',
				'sp_fk_vg_vaga' => '960',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			86 => 
			array (
				'sp_id_siape' => '587',
				'sp_cd_id_webservice' => '21',
				'sp_no_nome' => 'SEÇÃO DE DESENVOLVIMENTO DE SISTEMAS',
				'sp_fk_vg_vaga' => '2',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			87 => 
			array (
				'sp_id_siape' => '588',
				'sp_cd_id_webservice' => '802',
				'sp_no_nome' => '12ª SRPRF/ES - NÚCLEO DE SERVIÇOS GERAIS',
				'sp_fk_vg_vaga' => '689',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			88 => 
			array (
				'sp_id_siape' => '589',
				'sp_cd_id_webservice' => '592',
				'sp_no_nome' => '9ª SRPRF/RS - NÚCLEO DE INTELIGÊNCIA',
				'sp_fk_vg_vaga' => '520',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			89 => 
			array (
				'sp_id_siape' => '590',
				'sp_cd_id_webservice' => '1080',
				'sp_no_nome' => '19ª SRPRF/PA - DEL. 02 - NÚCLEO DE POLICIAMENTO E FISCALIZAÇÃO',
				'sp_fk_vg_vaga' => '981',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			90 => 
			array (
				'sp_id_siape' => '591',
				'sp_cd_id_webservice' => '866',
				'sp_no_nome' => '14ª SRPRF/PB - NÚCLEO DE PATRIMÔNIO E MATERIAL',
				'sp_fk_vg_vaga' => '760',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			91 => 
			array (
				'sp_id_siape' => '592',
				'sp_cd_id_webservice' => '941',
				'sp_no_nome' => '16ª SRPRF/CE - SEÇÃO DE RECURSOS HUMANOS',
				'sp_fk_vg_vaga' => '840',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			92 => 
			array (
				'sp_id_siape' => '593',
				'sp_cd_id_webservice' => '400',
				'sp_no_nome' => '6ª SRPRF/SP - SEÇÃO ADMINISTRATIVA E FINANCEIRA',
				'sp_fk_vg_vaga' => '370',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			93 => 
			array (
				'sp_id_siape' => '594',
				'sp_cd_id_webservice' => '1028',
				'sp_no_nome' => '18ª SRPRF/MA - NÚCLEO DE ASSUNTOS INTERNOS',
				'sp_fk_vg_vaga' => '920',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			94 => 
			array (
				'sp_id_siape' => '595',
				'sp_cd_id_webservice' => '601',
				'sp_no_nome' => '9ª SRPRF/RS - NÚCLEO DE SERVIÇOS GERAIS',
				'sp_fk_vg_vaga' => '520',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			95 => 
			array (
				'sp_id_siape' => '596',
				'sp_cd_id_webservice' => '1110',
				'sp_no_nome' => '20ª SRPRF/SE - NÚCLEO DE SERVIÇOS GERAIS',
				'sp_fk_vg_vaga' => '1000',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			96 => 
			array (
				'sp_id_siape' => '597',
				'sp_cd_id_webservice' => '117',
				'sp_no_nome' => '1ª SRPRF/GO - DEL. 06 - DELEGACIA DE CATALÃO',
				'sp_fk_vg_vaga' => '117',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			97 => 
			array (
				'sp_id_siape' => '598',
				'sp_cd_id_webservice' => '89',
				'sp_no_nome' => '1ª SRPRF/GO - NÚCLEO DE INFORMÁTICA E TELECOMUNICAÇÕES',
				'sp_fk_vg_vaga' => '100',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			98 => 
			array (
				'sp_id_siape' => '599',
				'sp_cd_id_webservice' => '1105',
				'sp_no_nome' => '20ª SRPRF/SE - NÚCLEO DE REGISTRO E MEDICINA RODOVIÁRIA',
				'sp_fk_vg_vaga' => '1000',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			99 => 
			array (
				'sp_id_siape' => '600',
				'sp_cd_id_webservice' => '1024',
				'sp_no_nome' => '18ª SRPRF/MA - SEÇÃO DE RECURSOS HUMANOS',
				'sp_fk_vg_vaga' => '920',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			100 => 
			array (
				'sp_id_siape' => '601',
				'sp_cd_id_webservice' => '801',
				'sp_no_nome' => '12ª SRPRF/ES - NÚCLEO DE DOCUMENTAÇÃO',
				'sp_fk_vg_vaga' => '689',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			101 => 
			array (
				'sp_id_siape' => '602',
				'sp_cd_id_webservice' => '906',
				'sp_no_nome' => '15ª SRPRF/RN - SEÇÃO DE RECURSOS HUMANOS',
				'sp_fk_vg_vaga' => '800',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			102 => 
			array (
				'sp_id_siape' => '603',
				'sp_cd_id_webservice' => '594',
				'sp_no_nome' => '9ª SRPRF/RS - NÚCLEO DE OPERAÇÕES ESPECIAIS',
				'sp_fk_vg_vaga' => '520',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			103 => 
			array (
				'sp_id_siape' => '604',
				'sp_cd_id_webservice' => '88',
				'sp_no_nome' => '1ª SRPRF/GO - NÚCLEO DE SERVIÇOS GERAIS',
				'sp_fk_vg_vaga' => '100',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			104 => 
			array (
				'sp_id_siape' => '605',
				'sp_cd_id_webservice' => '840',
				'sp_no_nome' => '13ª SRPRF/AL - NÚCLEO DE ADMINISTRAÇÃO DE PESSOAL',
				'sp_fk_vg_vaga' => '824',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			105 => 
			array (
				'sp_id_siape' => '606',
				'sp_cd_id_webservice' => '1068',
				'sp_no_nome' => '19ª SRPRF/PA - CORREGEDORIA REGIONAL',
				'sp_fk_vg_vaga' => '960',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			106 => 
			array (
				'sp_id_siape' => '607',
				'sp_cd_id_webservice' => '882',
				'sp_no_nome' => '14ª SRPRF/PB - DEL. 02 - NÚCLEO DE POLICIAMENTO E FISCALIZAÇÃO',
				'sp_fk_vg_vaga' => '781',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			107 => 
			array (
				'sp_id_siape' => '608',
				'sp_cd_id_webservice' => '984',
				'sp_no_nome' => '17ª SRPRF/PI - NÚCLEO DE INFORMÁTICA E TELECOMUNICAÇÕES',
				'sp_fk_vg_vaga' => '880',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			108 => 
			array (
				'sp_id_siape' => '609',
				'sp_cd_id_webservice' => '142',
				'sp_no_nome' => '2ª SRPRF/MT - NÚCLEO DE ASSUNTOS INTERNOS',
				'sp_fk_vg_vaga' => '150',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			109 => 
			array (
				'sp_id_siape' => '610',
				'sp_cd_id_webservice' => '930',
				'sp_no_nome' => '16ª SRPRF/CE - NÚCLEO DE INTELIGÊNCIA',
				'sp_fk_vg_vaga' => '840',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			110 => 
			array (
				'sp_id_siape' => '611',
				'sp_cd_id_webservice' => '858',
				'sp_no_nome' => '14ª SRPRF/PB - NÚCLEO DE COMUNICAÇÃO SOCIAL',
				'sp_fk_vg_vaga' => '760',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			111 => 
			array (
				'sp_id_siape' => '612',
				'sp_cd_id_webservice' => '830',
				'sp_no_nome' => '13ª SRPRF/AL - NÚCLEO DE MULTAS E PENALIDADES',
				'sp_fk_vg_vaga' => '824',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			112 => 
			array (
				'sp_id_siape' => '613',
				'sp_cd_id_webservice' => '87',
				'sp_no_nome' => '1ª SRPRF/GO - NÚCLEO DE DOCUMENTAÇÃO',
				'sp_fk_vg_vaga' => '100',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			113 => 
			array (
				'sp_id_siape' => '614',
				'sp_cd_id_webservice' => '130',
				'sp_no_nome' => '2ª SRPRF/MT - NÚCLEO DE MULTAS E PENALIDADES',
				'sp_fk_vg_vaga' => '150',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			114 => 
			array (
				'sp_id_siape' => '615',
				'sp_cd_id_webservice' => '864',
				'sp_no_nome' => '14ª SRPRF/PB - SEÇÃO ADMINISTRATIVA E FINANCEIRA',
				'sp_fk_vg_vaga' => '760',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			115 => 
			array (
				'sp_id_siape' => '616',
				'sp_cd_id_webservice' => '744',
				'sp_no_nome' => '11ª SRPRF/PE - NÚCLEO DE APOIO TÉCNICO',
				'sp_fk_vg_vaga' => '630',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			116 => 
			array (
				'sp_id_siape' => '617',
				'sp_cd_id_webservice' => '598',
				'sp_no_nome' => '9ª SRPRF/RS - NÚCLEO DE ORÇAMENTO E FINANÇAS',
				'sp_fk_vg_vaga' => '520',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			117 => 
			array (
				'sp_id_siape' => '618',
				'sp_cd_id_webservice' => '294',
				'sp_no_nome' => '4ª SRPRF/MG - DEL. 09 - NÚCLEO DE POLICIAMENTO E FISCALIZAÇÃO',
				'sp_fk_vg_vaga' => '285',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			118 => 
			array (
				'sp_id_siape' => '619',
				'sp_cd_id_webservice' => '1013',
				'sp_no_nome' => '18ª SRPRF/MA - NÚCLEO DE INTELIGÊNCIA',
				'sp_fk_vg_vaga' => '920',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			119 => 
			array (
				'sp_id_siape' => '620',
				'sp_cd_id_webservice' => '903',
				'sp_no_nome' => '15ª SRPRF/RN - NÚCLEO DE DOCUMENTAÇÃO',
				'sp_fk_vg_vaga' => '800',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			120 => 
			array (
				'sp_id_siape' => '621',
				'sp_cd_id_webservice' => '202',
				'sp_no_nome' => '3ª SRPRF/MS - DEL. 02 - NÚCLEO DE POLICIAMENTO E FISCALIZAÇÃO',
				'sp_fk_vg_vaga' => '201',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			121 => 
			array (
				'sp_id_siape' => '622',
				'sp_cd_id_webservice' => '127',
				'sp_no_nome' => '2ª SRPRF/MT - NÚCLEO DE INTELIGÊNCIA',
				'sp_fk_vg_vaga' => '150',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			122 => 
			array (
				'sp_id_siape' => '623',
				'sp_cd_id_webservice' => '928',
				'sp_no_nome' => '16ª SRPRF/CE - NÚCLEO DE APOIO TÉCNICO',
				'sp_fk_vg_vaga' => '840',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			123 => 
			array (
				'sp_id_siape' => '624',
				'sp_cd_id_webservice' => '777',
				'sp_no_nome' => '11ª SRPRF/PE - DEL. 04 - NÚCLEO DE POLICIAMENTO E FISCALIZAÇÃO',
				'sp_fk_vg_vaga' => '655',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			124 => 
			array (
				'sp_id_siape' => '625',
				'sp_cd_id_webservice' => '1162',
				'sp_no_nome' => '21ª SRPRF/RO - DEL. 05 - NÚCLEO DE POLICIAMENTO E FISCALIZAÇÃO',
				'sp_fk_vg_vaga' => '1067',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			125 => 
			array (
				'sp_id_siape' => '626',
				'sp_cd_id_webservice' => '986',
				'sp_no_nome' => '17ª SRPRF/PI - NÚCLEO DE LEGISLAÇÃO E CAPACITAÇÃO DE PESSOAL',
				'sp_fk_vg_vaga' => '880',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			126 => 
			array (
				'sp_id_siape' => '627',
				'sp_cd_id_webservice' => '987',
				'sp_no_nome' => '17ª SRPRF/PI - NÚCLEO DE ADMINISTRAÇÃO DE PESSOAL',
				'sp_fk_vg_vaga' => '880',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			127 => 
			array (
				'sp_id_siape' => '628',
				'sp_cd_id_webservice' => '797',
				'sp_no_nome' => '12ª SRPRF/ES - NÚCLEO DE REGISTRO E MEDICINA RODOVIÁRIA',
				'sp_fk_vg_vaga' => '689',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			128 => 
			array (
				'sp_id_siape' => '629',
				'sp_cd_id_webservice' => '485',
				'sp_no_nome' => '7ª SRPRF/PR - DEL. 02 - NÚCLEO DE POLICIAMENTO E FISCALIZAÇÃO',
				'sp_fk_vg_vaga' => '441',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			129 => 
			array (
				'sp_id_siape' => '630',
				'sp_cd_id_webservice' => '193',
				'sp_no_nome' => '3ª SRPRF/MS - CORREGEDORIA REGIONAL',
				'sp_fk_vg_vaga' => '200',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			130 => 
			array (
				'sp_id_siape' => '631',
				'sp_cd_id_webservice' => '56',
				'sp_no_nome' => 'NÚCLEO DE ASSISTÊNCIA SOCIAL',
				'sp_fk_vg_vaga' => '2',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			131 => 
			array (
				'sp_id_siape' => '632',
				'sp_cd_id_webservice' => '1069',
				'sp_no_nome' => '19ª SRPRF/PA - NÚCLEO DE ASSUNTOS INTERNOS',
				'sp_fk_vg_vaga' => '960',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			132 => 
			array (
				'sp_id_siape' => '633',
				'sp_cd_id_webservice' => '1053',
				'sp_no_nome' => '19ª SRPRF/PA - NÚCLEO DE COMUNICAÇÃO SOCIAL',
				'sp_fk_vg_vaga' => '960',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			133 => 
			array (
				'sp_id_siape' => '634',
				'sp_cd_id_webservice' => '6',
				'sp_no_nome' => 'ASSISTENCIA DA DIRETORIA',
				'sp_fk_vg_vaga' => '2',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			134 => 
			array (
				'sp_id_siape' => '635',
				'sp_cd_id_webservice' => '1064',
				'sp_no_nome' => '19ª SRPRF/PA - NÚCLEO DE INFORMÁTICA E TELECOMUNICAÇÕES',
				'sp_fk_vg_vaga' => '960',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			135 => 
			array (
				'sp_id_siape' => '636',
				'sp_cd_id_webservice' => '1067',
				'sp_no_nome' => '19ª SRPRF/PA - NÚCLEO DE ADMINISTRAÇÃO DE PESSOAL',
				'sp_fk_vg_vaga' => '960',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			136 => 
			array (
				'sp_id_siape' => '637',
				'sp_cd_id_webservice' => '1071',
				'sp_no_nome' => '19ª SRPRF/PA - DEL. 01 - NÚCLEO DE POLICIAMENTO E FISCALIZAÇÃO',
				'sp_fk_vg_vaga' => '979',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			137 => 
			array (
				'sp_id_siape' => '638',
				'sp_cd_id_webservice' => '86',
				'sp_no_nome' => '1ª SRPRF/GO - NÚCLEO DE PATRIMÔNIO E MATERIAL',
				'sp_fk_vg_vaga' => '100',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			138 => 
			array (
				'sp_id_siape' => '639',
				'sp_cd_id_webservice' => '62',
				'sp_no_nome' => 'DIVISÃO DE GERENCIAMENTO E EXECUÇÃO FINANCEIRA',
				'sp_fk_vg_vaga' => '2',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			139 => 
			array (
				'sp_id_siape' => '640',
				'sp_cd_id_webservice' => '84',
				'sp_no_nome' => '1ª SRPRF/GO - SEÇÃO ADMINISTRATIVA E FINANCEIRA',
				'sp_fk_vg_vaga' => '100',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			140 => 
			array (
				'sp_id_siape' => '641',
				'sp_cd_id_webservice' => '1056',
				'sp_no_nome' => '19ª SRPRF/PA - NÚCLEO DE OPERAÇÕES ESPECIAIS',
				'sp_fk_vg_vaga' => '960',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			141 => 
			array (
				'sp_id_siape' => '642',
				'sp_cd_id_webservice' => '1085',
				'sp_no_nome' => '19ª SRPRF/PA - DEL. 03 - NÚCLEO DE POLICIAMENTO E FISCALIZAÇÃO',
				'sp_fk_vg_vaga' => '983',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			142 => 
			array (
				'sp_id_siape' => '643',
				'sp_cd_id_webservice' => '867',
				'sp_no_nome' => '14ª SRPRF/PB - NÚCLEO DE DOCUMENTAÇÃO',
				'sp_fk_vg_vaga' => '760',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			143 => 
			array (
				'sp_id_siape' => '644',
				'sp_cd_id_webservice' => '394',
				'sp_no_nome' => '6ª SRPRF/SP - NÚCLEO DE COMUNICAÇÃO SOCIAL',
				'sp_fk_vg_vaga' => '370',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			144 => 
			array (
				'sp_id_siape' => '645',
				'sp_cd_id_webservice' => '1052',
				'sp_no_nome' => '19ª SRPRF/PA - NÚCLEO DE APOIO TÉCNICO',
				'sp_fk_vg_vaga' => '960',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			145 => 
			array (
				'sp_id_siape' => '646',
				'sp_cd_id_webservice' => '71',
				'sp_no_nome' => 'NÚCLEO DE TRANSPORTE E MANUTENÇÃO DA FROTA',
				'sp_fk_vg_vaga' => '2',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			146 => 
			array (
				'sp_id_siape' => '647',
				'sp_cd_id_webservice' => '899',
				'sp_no_nome' => '15ª SRPRF/RN - NÚCLEO DE REGISTRO E MEDICINA RODOVIÁRIA',
				'sp_fk_vg_vaga' => '800',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			147 => 
			array (
				'sp_id_siape' => '648',
				'sp_cd_id_webservice' => '937',
				'sp_no_nome' => '16ª SRPRF/CE - NÚCLEO DE PATRIMÔNIO E MATERIAL',
				'sp_fk_vg_vaga' => '840',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			148 => 
			array (
				'sp_id_siape' => '649',
				'sp_cd_id_webservice' => '1059',
				'sp_no_nome' => '19ª SRPRF/PA - SEÇÃO ADMINISTRATIVA E FINANCEIRA',
				'sp_fk_vg_vaga' => '960',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			149 => 
			array (
				'sp_id_siape' => '650',
				'sp_cd_id_webservice' => '131',
				'sp_no_nome' => '2ª SRPRF/MT - NÚCLEO DE REGISTRO E MEDICINA RODOVIÁRIA',
				'sp_fk_vg_vaga' => '150',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			150 => 
			array (
				'sp_id_siape' => '651',
				'sp_cd_id_webservice' => '982',
				'sp_no_nome' => '17ª SRPRF/PI - NÚCLEO DE DOCUMENTAÇÃO',
				'sp_fk_vg_vaga' => '880',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			151 => 
			array (
				'sp_id_siape' => '652',
				'sp_cd_id_webservice' => '934',
				'sp_no_nome' => '16ª SRPRF/CE - NÚCLEO DE REGISTRO E MEDICINA RODOVIÁRIA',
				'sp_fk_vg_vaga' => '840',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			152 => 
			array (
				'sp_id_siape' => '653',
				'sp_cd_id_webservice' => '991',
				'sp_no_nome' => '17ª SRPRF/PI - DEL. 01 - NÚCLEO DE POLICIAMENTO E FISCALIZAÇÃO',
				'sp_fk_vg_vaga' => '899',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			153 => 
			array (
				'sp_id_siape' => '654',
				'sp_cd_id_webservice' => '989',
				'sp_no_nome' => '17ª SRPRF/PI - NÚCLEO DE ASSUNTOS INTERNOS',
				'sp_fk_vg_vaga' => '880',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			154 => 
			array (
				'sp_id_siape' => '655',
				'sp_cd_id_webservice' => '849',
				'sp_no_nome' => '13ª SRPRF/AL - DEL. 02 - NÚCLEO DE POLICIAMENTO E FISCALIZAÇÃO',
				'sp_fk_vg_vaga' => '849',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			155 => 
			array (
				'sp_id_siape' => '656',
				'sp_cd_id_webservice' => '186',
				'sp_no_nome' => '3ª SRPRF/MS - NÚCLEO DE PATRIMÔNIO E MATERIAL',
				'sp_fk_vg_vaga' => '200',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			156 => 
			array (
				'sp_id_siape' => '657',
				'sp_cd_id_webservice' => '1095',
				'sp_no_nome' => '19ª SRPRF/PA - DEL. 05 - NÚCLEO DE POLICIAMENTO E FISCALIZAÇÃO',
				'sp_fk_vg_vaga' => '987',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			157 => 
			array (
				'sp_id_siape' => '658',
				'sp_cd_id_webservice' => '135',
				'sp_no_nome' => '2ª SRPRF/MT - NÚCLEO DE DOCUMENTAÇÃO',
				'sp_fk_vg_vaga' => '150',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			158 => 
			array (
				'sp_id_siape' => '659',
				'sp_cd_id_webservice' => '942',
				'sp_no_nome' => '16ª SRPRF/CE - NÚCLEO DE LEGISLAÇÃO E CAPACITAÇÃO DE PESSOAL',
				'sp_fk_vg_vaga' => '840',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			159 => 
			array (
				'sp_id_siape' => '660',
				'sp_cd_id_webservice' => '512',
				'sp_no_nome' => '7ª SRPRF/PR - DEL. 06 - NÚCLEO DE POLICIAMENTO E FISCALIZAÇÃO',
				'sp_fk_vg_vaga' => '511',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			160 => 
			array (
				'sp_id_siape' => '661',
				'sp_cd_id_webservice' => '602',
				'sp_no_nome' => '9ª SRPRF/RS - NÚCLEO DE INFORMÁTICA E TELECOMUNICAÇÕES',
				'sp_fk_vg_vaga' => '520',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			161 => 
			array (
				'sp_id_siape' => '662',
				'sp_cd_id_webservice' => '1115',
				'sp_no_nome' => '20ª SRPRF/SE - CORREGEDORIA REGIONAL',
				'sp_fk_vg_vaga' => '1000',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			162 => 
			array (
				'sp_id_siape' => '663',
				'sp_cd_id_webservice' => '558',
				'sp_no_nome' => '8ª SRPRF/SC - DEL. 03 - NÚCLEO DE POLICIAMENTO E FISCALIZAÇÃO',
				'sp_fk_vg_vaga' => '493',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			163 => 
			array (
				'sp_id_siape' => '664',
				'sp_cd_id_webservice' => '138',
				'sp_no_nome' => '2ª SRPRF/MT - SEÇÃO DE RECURSOS HUMANOS',
				'sp_fk_vg_vaga' => '150',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			164 => 
			array (
				'sp_id_siape' => '665',
				'sp_cd_id_webservice' => '1011',
				'sp_no_nome' => '18ª SRPRF/MA - NÚCLEO DE APOIO TÉCNICO',
				'sp_fk_vg_vaga' => '920',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			165 => 
			array (
				'sp_id_siape' => '666',
				'sp_cd_id_webservice' => '141',
				'sp_no_nome' => '2ª SRPRF/MT - CORREGEDORIA REGIONAL',
				'sp_fk_vg_vaga' => '150',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			166 => 
			array (
				'sp_id_siape' => '667',
				'sp_cd_id_webservice' => '859',
				'sp_no_nome' => '14ª SRPRF/PB - NÚCLEO DE INTELIGÊNCIA',
				'sp_fk_vg_vaga' => '760',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			167 => 
			array (
				'sp_id_siape' => '668',
				'sp_cd_id_webservice' => '607',
				'sp_no_nome' => '9ª SRPRF/RS - NÚCLEO DE ASSUNTOS INTERNOS',
				'sp_fk_vg_vaga' => '520',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			168 => 
			array (
				'sp_id_siape' => '669',
				'sp_cd_id_webservice' => '980',
				'sp_no_nome' => '17ª SRPRF/PI - NÚCLEO DE ORÇAMENTO E FINANÇAS',
				'sp_fk_vg_vaga' => '880',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			169 => 
			array (
				'sp_id_siape' => '670',
				'sp_cd_id_webservice' => '1025',
				'sp_no_nome' => '18ª SRPRF/MA - NÚCLEO DE LEGISLAÇÃO E CAPACITAÇÃO DE PESSOAL',
				'sp_fk_vg_vaga' => '920',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			170 => 
			array (
				'sp_id_siape' => '671',
				'sp_cd_id_webservice' => '591',
				'sp_no_nome' => '9ª SRPRF/RS - NÚCLEO DE COMUNICAÇÃO SOCIAL',
				'sp_fk_vg_vaga' => '520',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			171 => 
			array (
				'sp_id_siape' => '672',
				'sp_cd_id_webservice' => '1012',
				'sp_no_nome' => '18ª SRPRF/MA - NÚCLEO DE COMUNICAÇÃO SOCIAL',
				'sp_fk_vg_vaga' => '920',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			172 => 
			array (
				'sp_id_siape' => '673',
				'sp_cd_id_webservice' => '599',
				'sp_no_nome' => '9ª SRPRF/RS - NÚCLEO DE PATRIMÔNIO E MATERIAL',
				'sp_fk_vg_vaga' => '520',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			173 => 
			array (
				'sp_id_siape' => '674',
				'sp_cd_id_webservice' => '799',
				'sp_no_nome' => '12ª SRPRF/ES - NÚCLEO DE ORÇAMENTO E FINANÇAS',
				'sp_fk_vg_vaga' => '689',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			174 => 
			array (
				'sp_id_siape' => '675',
				'sp_cd_id_webservice' => '132',
				'sp_no_nome' => '2ª SRPRF/MT - SEÇÃO ADMINISTRATIVA E FINANCEIRA',
				'sp_fk_vg_vaga' => '150',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			175 => 
			array (
				'sp_id_siape' => '676',
				'sp_cd_id_webservice' => '944',
				'sp_no_nome' => '16ª SRPRF/CE - CORREGEDORIA REGIONAL',
				'sp_fk_vg_vaga' => '840',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			176 => 
			array (
				'sp_id_siape' => '677',
				'sp_cd_id_webservice' => '442',
				'sp_no_nome' => '6ª SRPRF/SP - DEL. 08 - NÚCLEO DE POLICIAMENTO E FISCALIZAÇÃO',
				'sp_fk_vg_vaga' => '403',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			177 => 
			array (
				'sp_id_siape' => '678',
				'sp_cd_id_webservice' => '23',
				'sp_no_nome' => 'NÚCLEO DE TELECOMUNICAÇÕES',
				'sp_fk_vg_vaga' => '2',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			178 => 
			array (
				'sp_id_siape' => '679',
				'sp_cd_id_webservice' => '936',
				'sp_no_nome' => '16ª SRPRF/CE - NÚCLEO DE ORÇAMENTO E FINANÇAS',
				'sp_fk_vg_vaga' => '840',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			179 => 
			array (
				'sp_id_siape' => '680',
				'sp_cd_id_webservice' => '1017',
				'sp_no_nome' => '18ª SRPRF/MA - NÚCLEO DE REGISTRO E MEDICINA RODOVIÁRIA',
				'sp_fk_vg_vaga' => '920',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			180 => 
			array (
				'sp_id_siape' => '681',
				'sp_cd_id_webservice' => '139',
				'sp_no_nome' => '2ª SRPRF/MT - NÚCLEO DE LEGISLAÇÃO E CAPACITAÇÃO DE PESSOAL',
				'sp_fk_vg_vaga' => '150',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			181 => 
			array (
				'sp_id_siape' => '682',
				'sp_cd_id_webservice' => '1114',
				'sp_no_nome' => '20ª SRPRF/SE - NÚCLEO DE ADMINISTRAÇÃO DE PESSOAL',
				'sp_fk_vg_vaga' => '1000',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			182 => 
			array (
				'sp_id_siape' => '683',
				'sp_cd_id_webservice' => '38',
				'sp_no_nome' => 'DIVISÃO DE FISCALIZAÇÃO DE TRÂNSITO',
				'sp_fk_vg_vaga' => '2',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			183 => 
			array (
				'sp_id_siape' => '684',
				'sp_cd_id_webservice' => '973',
				'sp_no_nome' => '17ª SRPRF/PI - NÚCLEO DE COMUNICAÇÃO SOCIAL',
				'sp_fk_vg_vaga' => '880',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			184 => 
			array (
				'sp_id_siape' => '685',
				'sp_cd_id_webservice' => '183',
				'sp_no_nome' => '3ª SRPRF/MS - NÚCLEO DE REGISTRO E MEDICINA RODOVIÁRIA',
				'sp_fk_vg_vaga' => '200',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			185 => 
			array (
				'sp_id_siape' => '686',
				'sp_cd_id_webservice' => '530',
				'sp_no_nome' => '8ª SRPRF/SC - NÚCLEO DE COMUNICAÇÃO SOCIAL',
				'sp_fk_vg_vaga' => '470',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			186 => 
			array (
				'sp_id_siape' => '687',
				'sp_cd_id_webservice' => '929',
				'sp_no_nome' => '16ª SRPRF/CE - NÚCLEO DE COMUNICAÇÃO SOCIAL',
				'sp_fk_vg_vaga' => '840',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			187 => 
			array (
				'sp_id_siape' => '688',
				'sp_cd_id_webservice' => '614',
				'sp_no_nome' => '9ª SRPRF/RS - DEL. 02 - NÚCLEO DE POLICIAMENTO E FISCALIZAÇÃO',
				'sp_fk_vg_vaga' => '541',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			188 => 
			array (
				'sp_id_siape' => '689',
				'sp_cd_id_webservice' => '1023',
				'sp_no_nome' => '18ª SRPRF/MA - NÚCLEO DE INFORMÁTICA E TELECOMUNICAÇÕES',
				'sp_fk_vg_vaga' => '920',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			189 => 
			array (
				'sp_id_siape' => '690',
				'sp_cd_id_webservice' => '94',
				'sp_no_nome' => '1ª SRPRF/GO - NÚCLEO DE ASSUNTOS INTERNOS',
				'sp_fk_vg_vaga' => '100',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			190 => 
			array (
				'sp_id_siape' => '691',
				'sp_cd_id_webservice' => '1107',
				'sp_no_nome' => '20ª SRPRF/SE - NÚCLEO DE ORÇAMENTO E FINANÇAS',
				'sp_fk_vg_vaga' => '1000',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			191 => 
			array (
				'sp_id_siape' => '692',
				'sp_cd_id_webservice' => '1206',
				'sp_no_nome' => '5º DRPRF/RR - NÚCLEO DE CORREGEDORIA E ASSUNTOS INTERNOS',
				'sp_fk_vg_vaga' => '1120',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			192 => 
			array (
				'sp_id_siape' => '693',
				'sp_cd_id_webservice' => '137',
				'sp_no_nome' => '2ª SRPRF/MT - NÚCLEO DE INFORMÁTICA E TELECOMUNICAÇÕES',
				'sp_fk_vg_vaga' => '150',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			193 => 
			array (
				'sp_id_siape' => '694',
				'sp_cd_id_webservice' => '79',
				'sp_no_nome' => '1ª SRPRF/GO - NÚCLEO DE INTELIGÊNCIA',
				'sp_fk_vg_vaga' => '100',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			194 => 
			array (
				'sp_id_siape' => '695',
				'sp_cd_id_webservice' => '190',
				'sp_no_nome' => '3ª SRPRF/MS - SEÇÃO DE RECURSOS HUMANOS',
				'sp_fk_vg_vaga' => '200',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			195 => 
			array (
				'sp_id_siape' => '696',
				'sp_cd_id_webservice' => '191',
				'sp_no_nome' => '3ª SRPRF/MS - NÚCLEO DE LEGISLAÇÃO E CAPACITAÇÃO DE PESSOAL',
				'sp_fk_vg_vaga' => '200',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			196 => 
			array (
				'sp_id_siape' => '697',
				'sp_cd_id_webservice' => '574',
				'sp_no_nome' => '8ª SRPRF/SC - DEL. 06 - NÚCLEO DE POLICIAMENTO E FISCALIZAÇÃO',
				'sp_fk_vg_vaga' => '499',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			197 => 
			array (
				'sp_id_siape' => '698',
				'sp_cd_id_webservice' => '579',
				'sp_no_nome' => '8ª SRPRF/SC - DEL. 07 - NÚCLEO DE POLICIAMENTO E FISCALIZAÇÃO',
				'sp_fk_vg_vaga' => '501',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			198 => 
			array (
				'sp_id_siape' => '699',
				'sp_cd_id_webservice' => '105',
				'sp_no_nome' => '1ª SRPRF/GO - DEL. 03 - NÚCLEO DE POLICIAMENTO E FISCALIZAÇÃO',
				'sp_fk_vg_vaga' => '123',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			199 => 
			array (
				'sp_id_siape' => '700',
				'sp_cd_id_webservice' => '109',
				'sp_no_nome' => '1ª SRPRF/GO - DEL. 04 - NÚCLEO DE POLICIAMENTO E FISCALIZAÇÃO',
				'sp_fk_vg_vaga' => '125',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			200 => 
			array (
				'sp_id_siape' => '701',
				'sp_cd_id_webservice' => '1154',
				'sp_no_nome' => '21ª SRPRF/RO - DEL. 03 - NÚCLEO DE POLICIAMENTO E FISCALIZAÇÃO',
				'sp_fk_vg_vaga' => '1063',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			201 => 
			array (
				'sp_id_siape' => '702',
				'sp_cd_id_webservice' => '121',
				'sp_no_nome' => '1ª SRPRF/GO - DEL. 07 - NÚCLEO DE POLICIAMENTO E FISCALIZAÇÃO',
				'sp_fk_vg_vaga' => '120',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			202 => 
			array (
				'sp_id_siape' => '703',
				'sp_cd_id_webservice' => '651',
				'sp_no_nome' => '9ª SRPRF/RS - DEL. 09 - NÚCLEO DE POLICIAMENTO E FISCALIZAÇÃO',
				'sp_fk_vg_vaga' => '555',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			203 => 
			array (
				'sp_id_siape' => '704',
				'sp_cd_id_webservice' => '656',
				'sp_no_nome' => '9ª SRPRF/RS - DEL. 10 - NÚCLEO DE POLICIAMENTO E FISCALIZAÇÃO',
				'sp_fk_vg_vaga' => '557',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			204 => 
			array (
				'sp_id_siape' => '705',
				'sp_cd_id_webservice' => '1112',
				'sp_no_nome' => '20ª SRPRF/SE - SEÇÃO DE RECURSOS HUMANOS',
				'sp_fk_vg_vaga' => '1000',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			205 => 
			array (
				'sp_id_siape' => '706',
				'sp_cd_id_webservice' => '1192',
				'sp_no_nome' => '3º DRPRF/AM - NÚCLEO DE CORREGEDORIA E ASSUNTOS INTERNOS',
				'sp_fk_vg_vaga' => '1100',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			206 => 
			array (
				'sp_id_siape' => '707',
				'sp_cd_id_webservice' => '666',
				'sp_no_nome' => '9ª SRPRF/RS - DEL. 12 - NÚCLEO DE POLICIAMENTO E FISCALIZAÇÃO',
				'sp_fk_vg_vaga' => '561',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			207 => 
			array (
				'sp_id_siape' => '708',
				'sp_cd_id_webservice' => '1113',
				'sp_no_nome' => '20ª SRPRF/SE - NÚCLEO DE LEGISLAÇÃO E CAPACITAÇÃO DE PESSOAL',
				'sp_fk_vg_vaga' => '1000',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			208 => 
			array (
				'sp_id_siape' => '709',
				'sp_cd_id_webservice' => '33',
				'sp_no_nome' => 'NÚCLEO DE NORMAS E PROCEDIMENTOS DE TRÂNSITO',
				'sp_fk_vg_vaga' => '1080',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			209 => 
			array (
				'sp_id_siape' => '710',
				'sp_cd_id_webservice' => '59',
				'sp_no_nome' => 'DIVISÃO DE PLANEJAMENTO E CONTROLE ORÇAMENTÁRIO',
				'sp_fk_vg_vaga' => '2',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			210 => 
			array (
				'sp_id_siape' => '711',
				'sp_cd_id_webservice' => '1099',
				'sp_no_nome' => '20ª SRPRF/SE - NÚCLEO DE APOIO TÉCNICO',
				'sp_fk_vg_vaga' => '1000',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			211 => 
			array (
				'sp_id_siape' => '712',
				'sp_cd_id_webservice' => '636',
				'sp_no_nome' => '9ª SRPRF/RS - DEL. 06 - NÚCLEO DE POLICIAMENTO E FISCALIZAÇÃO',
				'sp_fk_vg_vaga' => '549',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			212 => 
			array (
				'sp_id_siape' => '713',
				'sp_cd_id_webservice' => '758',
				'sp_no_nome' => '11ª SRPRF/PE - NÚCLEO DE LEGISLAÇÃO E CAPACITAÇÃO DE PESSOAL',
				'sp_fk_vg_vaga' => '630',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			213 => 
			array (
				'sp_id_siape' => '714',
				'sp_cd_id_webservice' => '35',
				'sp_no_nome' => 'NÚCLEO DE SEGURANÇA DE VÔO E PROCEDIMENTOS ADMINISTRATIVOS',
				'sp_fk_vg_vaga' => '2',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			214 => 
			array (
				'sp_id_siape' => '715',
				'sp_cd_id_webservice' => '972',
				'sp_no_nome' => '17ª SRPRF/PI - NÚCLEO DE APOIO TÉCNICO',
				'sp_fk_vg_vaga' => '880',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			215 => 
			array (
				'sp_id_siape' => '716',
				'sp_cd_id_webservice' => '609',
				'sp_no_nome' => '9ª SRPRF/RS - DEL. 01 - NÚCLEO DE POLICIAMENTO E FISCALIZAÇÃO',
				'sp_fk_vg_vaga' => '539',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			216 => 
			array (
				'sp_id_siape' => '717',
				'sp_cd_id_webservice' => '32',
				'sp_no_nome' => 'NÚCLEO DE CONTROLE PROCESSUAL',
				'sp_fk_vg_vaga' => '1080',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			217 => 
			array (
				'sp_id_siape' => '718',
				'sp_cd_id_webservice' => '957',
				'sp_no_nome' => '16ª SRPRF/CE - DEL. 03 - NÚCLEO DE POLICIAMENTO E FISCALIZAÇÃO',
				'sp_fk_vg_vaga' => '863',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			218 => 
			array (
				'sp_id_siape' => '719',
				'sp_cd_id_webservice' => '596',
				'sp_no_nome' => '9ª SRPRF/RS - NÚCLEO DE REGISTRO E MEDICINA RODOVIÁRIA',
				'sp_fk_vg_vaga' => '520',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			219 => 
			array (
				'sp_id_siape' => '720',
				'sp_cd_id_webservice' => '1100',
				'sp_no_nome' => '20ª SRPRF/SE - NÚCLEO DE COMUNICAÇÃO SOCIAL',
				'sp_fk_vg_vaga' => '1000',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			220 => 
			array (
				'sp_id_siape' => '721',
				'sp_cd_id_webservice' => '754',
				'sp_no_nome' => '11ª SRPRF/PE - NÚCLEO DE DOCUMENTAÇÃO',
				'sp_fk_vg_vaga' => '630',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			221 => 
			array (
				'sp_id_siape' => '722',
				'sp_cd_id_webservice' => '1143',
				'sp_no_nome' => '21ª SRPRF/RO - CORREGEDORIA REGIONAL',
				'sp_fk_vg_vaga' => '1040',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			222 => 
			array (
				'sp_id_siape' => '723',
				'sp_cd_id_webservice' => '148',
				'sp_no_nome' => '2ª SRPRF/MT - DEL. 02 - NÚCLEO DE POLICIAMENTO E FISCALIZAÇÃO',
				'sp_fk_vg_vaga' => '147',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			223 => 
			array (
				'sp_id_siape' => '724',
				'sp_cd_id_webservice' => '585',
				'sp_no_nome' => '8ª SRPRF/SC - DEL. 08 - NÚCLEO DE POLICIAMENTO E FISCALIZAÇÃO',
				'sp_fk_vg_vaga' => '503',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			224 => 
			array (
				'sp_id_siape' => '725',
				'sp_cd_id_webservice' => '125',
				'sp_no_nome' => '2ª SRPRF/MT - NÚCLEO DE APOIO TÉCNICO',
				'sp_fk_vg_vaga' => '150',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			225 => 
			array (
				'sp_id_siape' => '726',
				'sp_cd_id_webservice' => '1108',
				'sp_no_nome' => '20ª SRPRF/SE - NÚCLEO DE PATRIMÔNIO E MATERIAL',
				'sp_fk_vg_vaga' => '1000',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			226 => 
			array (
				'sp_id_siape' => '727',
				'sp_cd_id_webservice' => '95',
				'sp_no_nome' => '1ª SRPRF/GO - DEL. 01 - DELEGACIA DE GOIÂNIA',
				'sp_fk_vg_vaga' => '95',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			227 => 
			array (
				'sp_id_siape' => '728',
				'sp_cd_id_webservice' => '491',
				'sp_no_nome' => '7ª SRPRF/PR - DEL. 03 - NÚCLEO DE POLICIAMENTO E FISCALIZAÇÃO',
				'sp_fk_vg_vaga' => '443',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			228 => 
			array (
				'sp_id_siape' => '729',
				'sp_cd_id_webservice' => '674',
				'sp_no_nome' => '9ª SRPRF/RS - DEL. 14 - NÚCLEO DE POLICIAMENTO E FISCALIZAÇÃO',
				'sp_fk_vg_vaga' => '565',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			229 => 
			array (
				'sp_id_siape' => '730',
				'sp_cd_id_webservice' => '429',
				'sp_no_nome' => '6ª SRPRF/SP - DEL. 05 - NÚCLEO DE POLICIAMENTO E FISCALIZAÇÃO',
				'sp_fk_vg_vaga' => '397',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			230 => 
			array (
				'sp_id_siape' => '731',
				'sp_cd_id_webservice' => '474',
				'sp_no_nome' => '7ª SRPRF/PR - DEL. 01 - NÚCLEO DE POLICIAMENTO E FISCALIZAÇÃO',
				'sp_fk_vg_vaga' => '473',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			231 => 
			array (
				'sp_id_siape' => '732',
				'sp_cd_id_webservice' => '75',
				'sp_no_nome' => 'SEÇÃO ORÇAMENTÁRIA E FINANCEIRA DE PESSOAL',
				'sp_fk_vg_vaga' => '2',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			232 => 
			array (
				'sp_id_siape' => '733',
				'sp_cd_id_webservice' => '857',
				'sp_no_nome' => '14ª SRPRF/PB - NÚCLEO DE APOIO TÉCNICO',
				'sp_fk_vg_vaga' => '760',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			233 => 
			array (
				'sp_id_siape' => '734',
				'sp_cd_id_webservice' => '631',
				'sp_no_nome' => '9ª SRPRF/RS - DEL. 05 - NÚCLEO DE POLICIAMENTO E FISCALIZAÇÃO',
				'sp_fk_vg_vaga' => '1547',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			234 => 
			array (
				'sp_id_siape' => '735',
				'sp_cd_id_webservice' => '781',
				'sp_no_nome' => '11ª SRPRF/PE - DEL. 05 - NÚCLEO DE POLICIAMENTO E FISCALIZAÇÃO',
				'sp_fk_vg_vaga' => '657',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			235 => 
			array (
				'sp_id_siape' => '736',
				'sp_cd_id_webservice' => '419',
				'sp_no_nome' => '6ª SRPRF/SP - DEL. 03 - NÚCLEO DE POLICIAMENTO E FISCALIZAÇÃO',
				'sp_fk_vg_vaga' => '393',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			236 => 
			array (
				'sp_id_siape' => '737',
				'sp_cd_id_webservice' => '625',
				'sp_no_nome' => '9ª SRPRF/RS - DEL. 04 - NÚCLEO DE POLICIAMENTO E FISCALIZAÇÃO',
				'sp_fk_vg_vaga' => '624',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			237 => 
			array (
				'sp_id_siape' => '738',
				'sp_cd_id_webservice' => '463',
				'sp_no_nome' => '7ª SRPRF/PR - NÚCLEO DE ORÇAMENTO E FINANÇAS',
				'sp_fk_vg_vaga' => '420',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			238 => 
			array (
				'sp_id_siape' => '739',
				'sp_cd_id_webservice' => '1003',
				'sp_no_nome' => '17ª SRPRF/PI - DEL. 04 - NÚCLEO DE POLICIAMENTO E FISCALIZAÇÃO',
				'sp_fk_vg_vaga' => '905',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			239 => 
			array (
				'sp_id_siape' => '740',
				'sp_cd_id_webservice' => '660',
				'sp_no_nome' => '9ª SRPRF/RS - DEL. 11 - NÚCLEO DE POLICIAMENTO E FISCALIZAÇÃO',
				'sp_fk_vg_vaga' => '559',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			240 => 
			array (
				'sp_id_siape' => '741',
				'sp_cd_id_webservice' => '1111',
				'sp_no_nome' => '20ª SRPRF/SE - NÚCLEO DE INFORMÁTICA E TELECOMUNICAÇÕES',
				'sp_fk_vg_vaga' => '1000',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			241 => 
			array (
				'sp_id_siape' => '742',
				'sp_cd_id_webservice' => '620',
				'sp_no_nome' => '9ª SRPRF/RS - DEL. 03 - NÚCLEO DE POLICIAMENTO E FISCALIZAÇÃO',
				'sp_fk_vg_vaga' => '543',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			242 => 
			array (
				'sp_id_siape' => '743',
				'sp_cd_id_webservice' => '670',
				'sp_no_nome' => '9ª SRPRF/RS - DEL. 13 - NÚCLEO DE POLICIAMENTO E FISCALIZAÇÃO',
				'sp_fk_vg_vaga' => '563',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			243 => 
			array (
				'sp_id_siape' => '744',
				'sp_cd_id_webservice' => '72',
				'sp_no_nome' => 'NÚCLEO DE GESTÃO E ABASTECIMENTO DA FROTA NACIONAL',
				'sp_fk_vg_vaga' => '2',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			244 => 
			array (
				'sp_id_siape' => '745',
				'sp_cd_id_webservice' => '553',
				'sp_no_nome' => '8ª SRPRF/SC - DEL. 02 - NÚCLEO DE POLICIAMENTO E FISCALIZAÇÃO',
				'sp_fk_vg_vaga' => '491',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			245 => 
			array (
				'sp_id_siape' => '746',
				'sp_cd_id_webservice' => '762',
				'sp_no_nome' => '11ª SRPRF/PE - DEL. 01 - DELEGACIA METROPOLITANA',
				'sp_fk_vg_vaga' => '649',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			246 => 
			array (
				'sp_id_siape' => '749',
				'sp_cd_id_webservice' => '961',
				'sp_no_nome' => '16ª SRPRF/CE - DEL. 04 - DEL. DE SOBRAL',
				'sp_fk_vg_vaga' => '961',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			247 => 
			array (
				'sp_id_siape' => '750',
				'sp_cd_id_webservice' => '158',
				'sp_no_nome' => '2ª SRPRF/MT - DEL. 04 - NÚCLEO DE POLICIAMENTO E FISCALIZAÇÃO',
				'sp_fk_vg_vaga' => '157',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			248 => 
			array (
				'sp_id_siape' => '751',
				'sp_cd_id_webservice' => '548',
				'sp_no_nome' => '8ª SRPRF/SC - DEL. 01 - NÚCLEO DE POLICIAMENTO E FISCALIZAÇÃO',
				'sp_fk_vg_vaga' => '2547',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			249 => 
			array (
				'sp_id_siape' => '752',
				'sp_cd_id_webservice' => '785',
				'sp_no_nome' => '11ª SRPRF/PE - DEL. 06 - DELEGACIA DE PETROLINA',
				'sp_fk_vg_vaga' => '659',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			250 => 
			array (
				'sp_id_siape' => '753',
				'sp_cd_id_webservice' => '880',
				'sp_no_nome' => 'DEL.01/UOP.04 - Unidade Operacional de Mamanguape',
				'sp_fk_vg_vaga' => '876',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			251 => 
			array (
				'sp_id_siape' => '754',
				'sp_cd_id_webservice' => '1238',
				'sp_no_nome' => '14ª SRPRF/PB - CENTRAL DE INFORMAÇÕES OPERACIONAIS',
				'sp_fk_vg_vaga' => '760',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			252 => 
			array (
				'sp_id_siape' => '755',
				'sp_cd_id_webservice' => '891',
				'sp_no_nome' => 'DEL.03/UOP.03 - Unidade Operacional de Cajazeiras',
				'sp_fk_vg_vaga' => '783',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			253 => 
			array (
				'sp_id_siape' => '756',
				'sp_cd_id_webservice' => '772',
				'sp_no_nome' => '11ª SRPRF/PE - DEL. 03 - DELEGACIA DE GARANHUNS',
				'sp_fk_vg_vaga' => '653',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			254 => 
			array (
				'sp_id_siape' => '757',
				'sp_cd_id_webservice' => '776',
				'sp_no_nome' => '11ª SRPRF/PE - DEL. 04 - DELEGACIA DE SERRA TALHADA',
				'sp_fk_vg_vaga' => '655',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			255 => 
			array (
				'sp_id_siape' => '759',
				'sp_cd_id_webservice' => '886',
				'sp_no_nome' => 'DEL.02/UOP.04 - Unidade Operacional de São Miguel',
				'sp_fk_vg_vaga' => '781',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			256 => 
			array (
				'sp_id_siape' => '761',
				'sp_cd_id_webservice' => '124',
				'sp_no_nome' => '2ª SUPERINTENDÊNCIA DE POLÍCIA RODOVIÁRIA FEDERAL/ MT',
				'sp_fk_vg_vaga' => '150',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			257 => 
			array (
				'sp_id_siape' => '762',
				'sp_cd_id_webservice' => '206',
				'sp_no_nome' => '3ª SRPRF/MS - DEL. 03 - NÚCLEO DE POLICIAMENTO E FISCALIZAÇÃO',
				'sp_fk_vg_vaga' => '205',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			258 => 
			array (
				'sp_id_siape' => '763',
				'sp_cd_id_webservice' => '1035',
				'sp_no_nome' => '18ª SRPRF/MA - DEL. 02 - NÚCLEO DE POLICIAMENTO E FISCALIZAÇÃO',
				'sp_fk_vg_vaga' => '1034',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			259 => 
			array (
				'sp_id_siape' => '764',
				'sp_cd_id_webservice' => '1241',
				'sp_no_nome' => '17ª SRPRF/PI - CENTRAL DE INFORMAÇÕES OPERACIONAIS',
				'sp_fk_vg_vaga' => '880',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			260 => 
			array (
				'sp_id_siape' => '765',
				'sp_cd_id_webservice' => '780',
				'sp_no_nome' => '11ª SRPRF/PE - DEL. 05 - DELEGACIA DE SALGUEIRO',
				'sp_fk_vg_vaga' => '657',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			261 => 
			array (
				'sp_id_siape' => '766',
				'sp_cd_id_webservice' => '917',
				'sp_no_nome' => '15ª SRPRF/RN - DEL. 02 - NÚCLEO DE POLICIAMENTO E FISCALIZAÇÃO',
				'sp_fk_vg_vaga' => '916',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			262 => 
			array (
				'sp_id_siape' => '767',
				'sp_cd_id_webservice' => '768',
				'sp_no_nome' => '11ª SRPRF/PE - DEL. 02 - DELEGACIA DE CARUARU',
				'sp_fk_vg_vaga' => '651',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			263 => 
			array (
				'sp_id_siape' => '769',
				'sp_cd_id_webservice' => '747',
				'sp_no_nome' => '11ª SRPRF/PE - SEÇÃO DE POLICIAMENTO E FISCALIZAÇÃO',
				'sp_fk_vg_vaga' => '630',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			264 => 
			array (
				'sp_id_siape' => '774',
				'sp_cd_id_webservice' => '763',
				'sp_no_nome' => '11ª SRPRF/PE - DEL. 01 - NÚCLEO DE POLICIAMENTO E FISCALIZAÇÃO',
				'sp_fk_vg_vaga' => '649',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			265 => 
			array (
				'sp_id_siape' => '775',
				'sp_cd_id_webservice' => '1050',
				'sp_no_nome' => 'DEL.05/UOP.01 -Balsas',
				'sp_fk_vg_vaga' => '1048',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			266 => 
			array (
				'sp_id_siape' => '776',
				'sp_cd_id_webservice' => '1047',
				'sp_no_nome' => 'DEL.04/UOP.03 - Açailândia',
				'sp_fk_vg_vaga' => '1043',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			267 => 
			array (
				'sp_id_siape' => '777',
				'sp_cd_id_webservice' => '1242',
				'sp_no_nome' => '18ª SRPRF/MA - CENTRAL DE INFORMAÇÕES OPERACIONAIS',
				'sp_fk_vg_vaga' => '920',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			268 => 
			array (
				'sp_id_siape' => '778',
				'sp_cd_id_webservice' => '1090',
				'sp_no_nome' => 'DEL.03/UOP.05 - Unidade Operacional de Novo Repartimento',
				'sp_fk_vg_vaga' => '983',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			269 => 
			array (
				'sp_id_siape' => '779',
				'sp_cd_id_webservice' => '1042',
				'sp_no_nome' => 'DEL.03/UOP.02 - Caxias',
				'sp_fk_vg_vaga' => '943',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			270 => 
			array (
				'sp_id_siape' => '780',
				'sp_cd_id_webservice' => '1083',
				'sp_no_nome' => 'DEL.02/UOP.03 - Unidade Operacional de Dom Eliseu',
				'sp_fk_vg_vaga' => '981',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			271 => 
			array (
				'sp_id_siape' => '781',
				'sp_cd_id_webservice' => '1078',
				'sp_no_nome' => 'DEL.01/UOP.07 - Unidade Operacional de Bragança',
				'sp_fk_vg_vaga' => '979',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			272 => 
			array (
				'sp_id_siape' => '782',
				'sp_cd_id_webservice' => '1097',
				'sp_no_nome' => 'DEL.05/UOP.02 - Unidade Operacional de Itaituba',
				'sp_fk_vg_vaga' => '987',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			273 => 
			array (
				'sp_id_siape' => '783',
				'sp_cd_id_webservice' => '814',
				'sp_no_nome' => '12ª SRPRF/ES - DEL. 02 - NÚCLEO DE POLICIAMENTO E FISCALIZAÇÃO',
				'sp_fk_vg_vaga' => '813',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			274 => 
			array (
				'sp_id_siape' => '784',
				'sp_cd_id_webservice' => '166',
				'sp_no_nome' => '2ª SRPRF/MT - DEL. 06 - NÚCLEO DE POLICIAMENTO E FISCALIZAÇÃO',
				'sp_fk_vg_vaga' => '165',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			275 => 
			array (
				'sp_id_siape' => '785',
				'sp_cd_id_webservice' => '1146',
				'sp_no_nome' => '21ª SRPRF/RO - DEL. 01 - NÚCLEO DE POLICIAMENTO E FISCALIZAÇÃO',
				'sp_fk_vg_vaga' => '1045',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			276 => 
			array (
				'sp_id_siape' => '786',
				'sp_cd_id_webservice' => '170',
				'sp_no_nome' => '2ª SRPRF/MT - DEL. 07 - NÚCLEO DE POLICIAMENTO E FISCALIZAÇÃO',
				'sp_fk_vg_vaga' => '169',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			277 => 
			array (
				'sp_id_siape' => '787',
				'sp_cd_id_webservice' => '967',
				'sp_no_nome' => '16ª SRPRF/CE - DEL. 05 - NÚCLEO DE POLICIAMENTO E FISCALIZAÇÃO',
				'sp_fk_vg_vaga' => '966',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			278 => 
			array (
				'sp_id_siape' => '788',
				'sp_cd_id_webservice' => '817',
				'sp_no_nome' => '12ª SRPRF/ES - DEL. 03 - NÚCLEO DE POLICIAMENTO E FISCALIZAÇÃO',
				'sp_fk_vg_vaga' => '816',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			279 => 
			array (
				'sp_id_siape' => '789',
				'sp_cd_id_webservice' => '227',
				'sp_no_nome' => '3ª SRPRF/MS - DEL. 08 - NÚCLEO DE POLICIAMENTO E FISCALIZAÇÃO',
				'sp_fk_vg_vaga' => '226',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			280 => 
			array (
				'sp_id_siape' => '790',
				'sp_cd_id_webservice' => '434',
				'sp_no_nome' => '6ª SRPRF/SP - DEL. 06 - NÚCLEO DE POLICIAMENTO E FISCALIZAÇÃO',
				'sp_fk_vg_vaga' => '433',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			281 => 
			array (
				'sp_id_siape' => '791',
				'sp_cd_id_webservice' => '450',
				'sp_no_nome' => '6ª SRPRF/SP - DEL. 10 - NÚCLEO DE POLICIAMENTO E FISCALIZAÇÃO',
				'sp_fk_vg_vaga' => '433',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			282 => 
			array (
				'sp_id_siape' => '792',
				'sp_cd_id_webservice' => '952',
				'sp_no_nome' => '16ª SRPRF/CE - DEL. 02 - NÚCLEO DE POLICIAMENTO E FISCALIZAÇÃO',
				'sp_fk_vg_vaga' => '951',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			283 => 
			array (
				'sp_id_siape' => '793',
				'sp_cd_id_webservice' => '447',
				'sp_no_nome' => '6ª SRPRF/SP - DEL. 09 - NÚCLEO DE POLICIAMENTO E FISCALIZAÇÃO',
				'sp_fk_vg_vaga' => '446',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			284 => 
			array (
				'sp_id_siape' => '794',
				'sp_cd_id_webservice' => '223',
				'sp_no_nome' => '3ª SRPRF/MS - DEL. 07 - NÚCLEO DE POLICIAMENTO E FISCALIZAÇÃO',
				'sp_fk_vg_vaga' => '222',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			285 => 
			array (
				'sp_id_siape' => '795',
				'sp_cd_id_webservice' => '1044',
				'sp_no_nome' => '18ª SRPRF/MA - DEL .04 - NÚCLEO DE POLICIAMENTO E FISCALIZAÇÃO',
				'sp_fk_vg_vaga' => '1043',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			286 => 
			array (
				'sp_id_siape' => '796',
				'sp_cd_id_webservice' => '154',
				'sp_no_nome' => '2ª SRPRF/MT - DEL. 03 - NÚCLEO DE POLICIAMENTO E FISCALIZAÇÃO',
				'sp_fk_vg_vaga' => '153',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			287 => 
			array (
				'sp_id_siape' => '797',
				'sp_cd_id_webservice' => '1159',
				'sp_no_nome' => '21ª SRPRF/RO - DEL. 04 - NÚCLEO DE POLICIAMENTO E FISCALIZAÇÃO',
				'sp_fk_vg_vaga' => '1158',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			288 => 
			array (
				'sp_id_siape' => '798',
				'sp_cd_id_webservice' => '1030',
				'sp_no_nome' => '18ª SRPRF/MA - DEL. 01 - NÚCLEO DE POLICIAMENTO E FISCALIZAÇÃO',
				'sp_fk_vg_vaga' => '1029',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			289 => 
			array (
				'sp_id_siape' => '799',
				'sp_cd_id_webservice' => '1008',
				'sp_no_nome' => '17ª SRPRF/PI - DEL. 05 - NÚCLEO DE POLICIAMENTO E FISCALIZAÇÃO',
				'sp_fk_vg_vaga' => '1007',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			290 => 
			array (
				'sp_id_siape' => '800',
				'sp_cd_id_webservice' => '1049',
				'sp_no_nome' => '18ª SRPRF/MA - DEL. 05 - NÚCLEO DE POLICIAMENTO E FISCALIZAÇÃO',
				'sp_fk_vg_vaga' => '1048',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			291 => 
			array (
				'sp_id_siape' => '801',
				'sp_cd_id_webservice' => '216',
				'sp_no_nome' => '3ª SRPRF/MS - DEL. 05 - NÚCLEO DE POLICIAMENTO E FISCALIZAÇÃO',
				'sp_fk_vg_vaga' => '215',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			292 => 
			array (
				'sp_id_siape' => '802',
				'sp_cd_id_webservice' => '219',
				'sp_no_nome' => '3ª SRPRF/MS - DEL. 06 - NÚCLEO DE POLICIAMENTO E FISCALIZAÇÃO',
				'sp_fk_vg_vaga' => '218',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			293 => 
			array (
				'sp_id_siape' => '803',
				'sp_cd_id_webservice' => '144',
				'sp_no_nome' => ' 2ª SRPRF/MT - DEL. 01 - NÚCLEO DE POLICIAMENTO E FISCALIZAÇÃO',
				'sp_fk_vg_vaga' => '143',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			294 => 
			array (
				'sp_id_siape' => '804',
				'sp_cd_id_webservice' => '1116',
				'sp_no_nome' => '20ª SRPRF/SE - NÚCLEO DE ASSUNTOS INTERNOS',
				'sp_fk_vg_vaga' => '1000',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			295 => 
			array (
				'sp_id_siape' => '805',
				'sp_cd_id_webservice' => '786',
				'sp_no_nome' => '11ª SRPRF/PE - DEL. 06 - NÚCLEO DE POLICIAMENTO E FISCALIZAÇÃO',
				'sp_fk_vg_vaga' => '659',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			296 => 
			array (
				'sp_id_siape' => '806',
				'sp_cd_id_webservice' => '1167',
				'sp_no_nome' => ' UOP.03 - Unidade Operacional de Zarzur Pacheco',
				'sp_fk_vg_vaga' => '1080',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			297 => 
			array (
				'sp_id_siape' => '808',
				'sp_cd_id_webservice' => '646',
				'sp_no_nome' => '9ª SRPRF/RS- DEL. 08 - DELEGACIA DE PASSO FUNDO',
				'sp_fk_vg_vaga' => '646',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			298 => 
			array (
				'sp_id_siape' => '809',
				'sp_cd_id_webservice' => '947',
				'sp_no_nome' => '16ª SRPRF/CE - DEL. 01 - NÚCLEO DE POLICIAMENTO E FISCALIZAÇÃO',
				'sp_fk_vg_vaga' => '946',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
			299 => 
			array (
				'sp_id_siape' => '810',
				'sp_cd_id_webservice' => '996',
				'sp_no_nome' => '17ª SRPRF/PI - DEL. 02 - NÚCLEO DE  pOLICIAMENTO E FISCALIZAÇÃO',
				'sp_fk_vg_vaga' => '995',
				'sp_dt_created_at' => '0000-00-00 00:00:00',
				'sp_dt_updated_at' => NULL,
				'sp_dt_deleted_at' => NULL,
			),
		));
	}

}
