@extends('templates.layout')

@section('scripts')
	<script type="text/javascript">
		$(document).ready(function () {
			$("#menuInscricao").addClass("ativo");
		});
	</script>
@stop

@section('conteudo')
	<div class="container">
		<div class="span12">
			<div class="text-center"><h4><strong> NO MOMENTO NÃO E POSSÍVEL FAZER SUA INSCRICAO</strong></h4></div>
		</div>
		<div class="span12">
			<div class="text-justify">
				<strong>CASO VERIFIQUE QUE HAJA ALGUMA IMPROPRIEDADE RECOMENDAMOS QUE IMPRIMA ESSA PÁGINA E A ENVIE PARA A CGRH PARA AS DEVIDAS CORREÇÕES</strong>
				<br>
				Serão analisados os dados e sua justiifcativa e caso pleito seja considerado procedente tua solicitação será provida.<br>
				<br><h2>Obedeça as regras editalícias.</h2><br>

				<BR>
				<h5>DADOS DA INCONSISTÊNCIA:</h5>
				{{--{{$errorMessage}}--}}
				{{--{!! $e->getMessage() !!}}}--}}
				@if (isset($errorMessage))
					<strong>
					{{$errorMessage}}
					</strong>
				@endif
			</div>
		</div>
	</div>
@stop

