<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="utf-8">
    <title>{{config("PRF.nomeSistema")}}</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- Le styles -->

    {{--<link href="https://www.prf.gov.br/design/assets/css/pig.css" rel="stylesheet">--}}
    <!--<link href="https://www.prf.gov.br/design/assets/css/docs.css" rel="stylesheet">-->
    <link rel="stylesheet" type="text/css" href="https://www.prf.gov.br/design/assets/css/prf-sistemas-internos.css" media="all" >
    <script src="https://www.prf.gov.br/design/assets/js/jquery.js"></script>
    <script src="https://www.prf.gov.br/design/assets/js/bootstrap.js"></script>
    {{--<script src="https://www.prf.gov.br/design/assets/js/prf.js"></script>--}}
    {{--<script src="https://www.prf.gov.br/design/assets/js/bootstrap-inputmask.min.js"></script>--}}
    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
    <!--<script src="https://www.prf.gov.br/design/assets/js/html5shiv.js"></script>-->
    <![endif]-->

    @section("scripts")
    @show


</head>

<body data-spy="scroll">
@section('cabecalho')
<div class="row-fluid">
	<div class="span12" style="text-align: center;">
        {!! HTML::image('img/armasbra.gif') !!}
	</div>
</div>
<div class="row-fluid">
     <div class="span12" style="text-align: center;">
         <strong>MINISTÉRIO DA JUSTIÇA</strong>
     </div>
 </div>
<div class="row-fluid">
     <div class="span12" style="text-align: center;">
         <strong>DEPARTAMENTO DE POLÍCIA RODOVIÁRIA FEDERAL</strong>
     </div>
 </div>
<div class="row-fluid">
     <div class="span12" style="text-align: center;">
         <strong>COMPROVANTE DE INSCRIÇÃO - SISNAR</strong>
     </div>
 </div>

    {{--cabeçalho colocar o brasão da república como se fosse um documento --}}
@show


@section('conteudo')
    ﻿
    {{-- aqui irá ser incluido o  comprovante de inscricao.--}}
@show

@section('rodape')
    <br>
    <hr>
	<center>
		<small>
			 SISNAR - Sistema Nacional de Remoção e Permuta de Servidores Policiais da PRF  - Versão 5.0<br>
			 Data/Hora do documento : {!! date('d-m-Y h:i:s') !!}
		 </small>
	 </center>



    {{--assinatura do sistema --}}

@show
</body>
</html>