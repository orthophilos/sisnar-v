<div id="sidebar" class="fixed">
    <ul class="nav nav-list nav-open">
        <li><a href="#" class="fechar-sidebar"><i class="icon-reorder"></i> Fechar menu </a></li>

        {{--Início dos ítens do menu.--}}

        <li id="menuInicio"><a href="{{URL::to('/')}}"><i class="icon-home"></i> Início</a></li>
        @if (\App\Funcoes\Funcoes::temConcurso())
        <!-- subMenu Minha Inscricao -->
        <li id="menuInscricao">

            <a href="#" class="dropdown-toggle"><i class="icon-edit-sign"></i>
                Minha Inscricao <b class="arrow icon-angle-down"></b>
            </a>
            <ul class="submenu" >
                <li >
                    {{--<a href="{{URL::route('montaTeladeCadastroMedicao')}}">--}}
                    <a href="{{URL::to('/')}}">
                        <i class="icon-resize-horizontal"></i> Inscrever-me
                    </a>
                </li>
                <li>
                    <a href="{{URL::route('comprovanteInscricao')}}">
                        <i class="icon-resize-horizontal"></i> Visualizar Inscrição
                    </a>
                </li>

            </ul>
        </li>
<!-- ./subMenu Minhas Inscricao -->
<!-- subMenu Estatisticas-->
		<li id="Estatisticas" class="open">
            <a href="#" class="dropdown-toggle"><i class="icon-bar-chart"></i>
                <small>Estatísticas </small><b class="arrow icon-angle-down"></b></a>

            <ul class="submenu" style="display: block;">
                <li id="submenuEstatisticaResumo">
					<a href="{{URL::route('cenarioGeral')}}"><i class="icon-male"></i>Quadro Resumo</a>
			</li>
            </ul>


        </li>
<!-- ./subMenu Estatisticas-->
        @endif
<!-- SubMenu Recursos -->
        @if (\App\Funcoes\Funcoes::temPermissao('gerenciar'))

        <li id="Recursos">
            <a href="#" class="dropdown-toggle"><i class="icon-book"></i>
                <small>Recursos </small><b class="arrow icon-angle-down"></b></a>

            <ul class="submenu" >
                <li id="submenuAlteraDadosManualment">
                    <a href="{{URL::route('indexRecurso')}}"><i class="icon-male"></i>Lista de Inscrições</a>
                </li>
                @if (\App\Funcoes\Funcoes::temPermissao('root'))
                    <li id="submenuResultadoUltiomConcurso">
                        <a href="{{URL::route('painelDeComandos')}}"><i class="icon-male"></i>Painel de Comandos</a>
                    </li>
                @endif
                <li id="submenuConcursosAruivados">
                    <a href="{{URL::route('resultadoFinalConcurso')}}"><i class="icon-male"></i>Resultado Final</a>
                </li>
            </ul>

        </li>
        @endif
<!-- ./subMenu Recursos -->
<!-- subMenu Admin -->
        @if (\App\Funcoes\Funcoes::temPermissao('root'))
    {{--@if(session('status') == 'gerenciar')--}}
        <li id="menuGerencia">
            <a href="#" class="dropdown-toggle"><i class="icon-tasks"></i>
                Gerencia <b class="arrow icon-angle-down"></b></a>
            <ul class="submenu" >
                @if (\App\Funcoes\Funcoes::temConcurso())

                    <li>
                        <a href="{{URL::route('montaTelaConcursoAtual')}}">
                            <i class="icon-double-angle-right"></i> <small>Concurso Atual</small>
                        </a>
                    </li>
                @endif
                <li>
                    <a href="{{URL::route('montaTelaNovoConcurso')}}">
                        <i class="icon-double-angle-right"></i> <small>Novo Concurso</small>
                    </a>
                </li>

            </ul>
        </li>

    @endif

        <li id="menuInstrucoesDeUso"><a href="{{URL::route('instrucoesDeUso')}}"><i class="icon-info-sign"></i> Instruções de Uso</a></li>
    </ul>

    {{--@endif--}}
    <ul class="nav nav-list nav-close" style="display:none">
        <li><a href="#" class="fechar-sidebar"><i class="icon-reorder"></i> Abrir menu</a></li>
    </ul>
</div>

<!-- subMenu Admin -->



