@extends('templates.layout')

@section('scripts')
	<link type="text/css" href="{{url('/css/infographic.css')}}" rel="stylesheet">
	<script src="{{url('/angular/angular.min.js')}}"></script>

    <script type="text/javascript">
        $(document).ready(function () {
            $("#menuInicio").addClass("ativo");
        });
    </script>
	 <script>
        var app = angular.module('sisnarApp', []);
        app.config(function ($interpolateProvider) {
            $interpolateProvider.startSymbol('//');
            $interpolateProvider.endSymbol('//');
        });
        app.controller('MainController', function ($http, $window) {
            var self = this;
			self.isFazendoAlgo = false;
			self.resultado = '';

			self.revisaNotas = function(){
				var url = "{{URL::route('revisaNotas')}}";
				self.isFazendoAlgo = true;
				$window.location.href = url;
				/*$http.get(url).then(
				 function(response){
				 self.isFazendoAlgo = false;
				 self.resultado = JSON.stringify(response)
				 //self.resultado = response.data;
				 console.debug(response);
				 },
				 function(response){
				 self.isFazendoAlgo = false;
				 self.resultado = "Servidor retornou erro "+response.status+"\n\n"+response.data;
				 console.debug(response);
				 }
				 );*/
			};

			self.executarConcurso = function(){
				var url = "{{URL::route('executarConcurso')}}";
				self.isFazendoAlgo = true;
				$window.location.href = url;
				/*$http.get(url).then(
					function(response){
						self.isFazendoAlgo = false;
						self.resultado = JSON.stringify(response)
						//self.resultado = response.data;
						console.debug(response);
					},
					function(response){
						self.isFazendoAlgo = false;
						self.resultado = "Servidor retornou erro "+response.status+"\n\n"+response.data;
						console.debug(response);
					}
				);*/
			};
			
			self.rebootExecutarConcurso = function(){
				var url = "{{URL::route('rebootExecutarConcurso')}}";
				self.isFazendoAlgo = true;
				$window.location.href = url;
				/*$http.get(url).then(
					function(response){
						self.isFazendoAlgo = false;
						self.resultado = response.data;
						console.debug(response);
					},
					function(response){
						self.isFazendoAlgo = false;
						self.resultado = "Servidor retornou erro "+response.status+"\n\n"+response.data;
						console.debug(response);
					}
				);*/
			};
            
			self.arquivarConcurso = function(){
				var url = "{{URL::route('arquivarConcurso')}}";
				self.isFazendoAlgo = true;
				$window.location.href = url;
//				$http.get(url).then(
//					function(response){
//						self.isFazendoAlgo = false;
//						self.resultado = response.data;
//						console.debug(response);
//					},
//					function(response){
//						self.isFazendoAlgo = false;
//						self.resultado = "Servidor retornou erro "+response.status+"\n\n"+response.data;
//						console.debug(response);
//					}
//				);
			};
        });
    </script>
@stop

@section('conteudo')
	<style>

	</style>
    
	{{--﻿@if ( count($errors) > 0)--}}
		{{--@foreach ($errors->all() as $erro)--}}
			{{--<div class="alert alert-error">--}}
				{{--<button type="button" class="close" data-dismiss="alert">&times;</button>--}}
				{{--<h4>Erro!</h4>--}}
				{{--{{ $erro }}--}}
			{{--</div>--}}
		{{--@endforeach--}}
	{{--@endif--}}
	<div  ng-app="sisnarApp" ng-controller="MainController as controller" >
		<div class="box">
			<div class="box-header">
			<span class="title" style="font-size: 18px;">
				<div class="text-center">
					<strong>
						<span class="">
							<i class="icon-cogs"></i>
						</span>
							<span class="">
							Painel de Comandos
						</span>
					</strong>
				</div>
			</span>
			</div>
			<div class="box-content padded ">
				<div class="row-fluid">
					<div class="span3">
					<div class="alert-info infographic-box " ng-click="controller.revisaNotas()">
							<i class="icon-check"></i>
							<span class="headline">Revisar Notas</span>
							<br/>
								<span class="value">
									Executar o algoritmo que revisa todas as notas dos candidatos.
									Caso haja alteração no histórico de remocoes ou na base de dados RH ao executar
									esta rotina, as notas dos candidatos também serão modificadas.
								</span>
						</div>

					</div>
					
					<div class="span3">
						
							<div class="alert-success infographic-box " ng-click="controller.executarConcurso()">
								<i class="icon-thumbs-up"></i>
								<span class="headline">Classificar Candidatos</span>
								<br/>
								<span class="value">
									Executar o algoritmo que classifica os candidatos conforme a regra do concurso.
									Este comando irá em sequência: realizar o reboot e classificar o concurso.
									Caso o concurso já tenha sido executado essa ação será desconsiderada.
								</span>
							</div>
						
					</div>
					
					<div class="span3">
						
						<div class="alert-danger infographic-box " ng-click="controller.rebootExecutarConcurso()">
							<i class="icon-refresh"></i>
							<span class="headline">Reboot Concurso</span>
							<br/>
							<span class="value">
								Trazer as inscrições a estado anterior das classificações. Essa opção deve ser usada quando,
								ao se detectar algum erro de dados na inscrição posteriormente à classificação do concurso, o  gestor ter a possibilidade de , primeiro
								dar reboot no concurso, alterar os dados e poder executar o concurso novamente.
							</span>
						</div>
						
					</div>
					
					<div class="span3">
						{{--<a href="{{URL::route('arquivarConcurso')}}">--}}
							<div class="alert-danger infographic-box" ng-click="controller.arquivarConcurso()">
								<i class="icon-archive"></i>
								<span class="headline">Arquivar Concurso</span>
								<br/>
								<span class="value">
									 Salva os dados de Inscricao, concurso e Opcoes, e zera o banco
									de dados .Após essa opção não será possível reexecutar o concurso e muitos dados serão perdidos. Mas
									possibilitará a configuração e a preparação para novo concurso de SISNAR ou remanejamento.
								</span>
							</div>
						{{--</a>--}}
					</div>


						{{--</a>--}}

				</div>
				<pre>//controller.resultado//</pre>

				<br>
			   
			</div>
		</div>
		
		<div class="modalBackground" ng-show="controller.isFazendoAlgo">
			<div class="modal fade in" id="pleaseWaitDialog" data-backdrop="static" data-keyboard="false">
				<div class="modal-header">
					<h1>Ação iniciada</h1>
				</div>
				<div class="modal-body">
					Aguarde até 15 minutos para que a ação seja executada.
				</div>
			</div>
		</div>
		
	</div>
@stop