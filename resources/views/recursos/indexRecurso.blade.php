@extends('templates.layout')

@section('scripts')
	<script src="{{url('/angular/angular.min.js')}}"></script>
	<script src="{{url('/moment/moment.min.js')}}"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $("#Recursos").addClass("ativo");
        });
		function confirmaClick(){
			var ok = confirm('Deseja excluir DEFINITIVAMENTE essa inscrição?');
			return ok;
		}
    </script>
	<script type="text/javascript">
		var app = angular.module('sisnarApp', []);
        app.config(function ($interpolateProvider) {
            $interpolateProvider.startSymbol('//');
            $interpolateProvider.endSymbol('//');
        });
		
		app.controller('mainController', function ($http) {
			var self = this;
			self.modalVisible = false;
			self.idDelete;
			self.nomeDelete;
			self.justificativa = "";
			
			self.showModal = function(){
				self.modalVisible  = true;
			}
			self.closeModal = function(){
				self.modalVisible  = false;
			}
			self.confirmarDelete = function(id, nome){
				self.justificativa = "";
				self.idDelete = id;
				self.nomeDelete = nome;
				self.showModal();
			}
			
		});
	</script>
	<script src="{{url('/angular/diretrizes.js')}}"></script>
@stop

@section('conteudo')
<style>
	.colunaAcoes{
		white-space: nowrap;
		
	}
	.colunaAcoes a:hover{
		text-decoration: none;
	}
</style>
	{{--@if($errors->any())--}}
    {{--<div class="box">--}}
        {{----}}
            {{--<ul class="alert alert-danger">--}}
                {{--@foreach($errors->all() as $erro)--}}
                    {{--<li>{{ $erro }}</li>--}}
                {{--@endforeach--}}
            {{--</ul>--}}
        {{----}}
    {{--</div>--}}
	{{--@endif--}}
	<div  ng-app="sisnarApp" ng-controller="mainController as mController" >
		<div class="alert-info">
			<h4>Planilha Todas as Inscricoes : </h4><br>
			- <a href="{{URL::route('listaInscricoes_xls')}}">Clique aqui para download de planilha com lista das inscrições.</a>
		</div>
	<br>
		<div class="box" >
			<div class="box-header">
				<span class="title" style="font-size: 18px;">
					<div class="text-center">
						<strong>
							<span class="text-error">
								<i class="ui-icon-document"></i>
							</span>
							<span class="text-success">
								Inscrições no SISNAR
							</span>
						</strong>
					</div>
				</span>
			</div>
			<div class="box-content padded">
			
				<div class="row-fluid form-inline">
					<div class="span8">
						{{--form de pesquisa--}}
						{!! Form::open(['url' => action('RecursoController@search',[],true), 'method' => 'get', 'class' => 'form-inline']) !!}
							<select id="fieldname" name="fieldname">
								<option {{$selected}} value="sd_no_nome">Nome Servidor</option>
								<option {{$selected}} value="sd_cd_matricula_servidor">Matrícula Servidor</option>
								<option {{$selected}} value="vg_no_nome">Regional de Origem</option>
							</select>
							<input id="value" name="value" type="text" placeholder="Pesquise por" value={{$value}}>

							&nbsp;&nbsp;
							<button id="botaoPesquisar" type="submit" class="btn btn-primary"><i class="icon-search"></i> Pesquisar</button>

						{!! Form::close() !!}
					</div>
					<div class="span4">
						<a href="{{URL::route('montaTelaAddInscricao')}}">
							<button type="button" class="btn btn-default pull-right"><i class="icon-plus"></i> Cadastrar inscrição</button>
						</a>
					</div>
				</div>
				<div class="form-inline" id="painelPesquisa">
					
				</div>

				
				<table class="table">
					<tr>	<th>Alterado</th>
							<th>
								CPF do Último Gestor<br>
								que Alterou
							</th>
							<th>ID</th>
							<th>Nome</th>
							<th>Matrícula</th>
							<th>UORG</th>
							<th>Pontuação</th>
							<th>Ações</th>
					</tr>
					@foreach($inscricoes as $inscricao)
						
						<tr>
							<td class="center">
							@if ($inscricao->alteradoManualmente == 1)
									<i class="icon-pencil icon-2x"></i>
							@endif
							</td>
							<td> {{ $inscricao->ic_cd_cpf_recurso}} </td>
							<td> {{ $inscricao->ic_id_inscricao}} </td>
							<td> {{ $inscricao->sd_no_nome }} </td>
							<td> {{ $inscricao->sd_cd_matricula_servidor }} </td>
							<td> {{ $inscricao->vg_no_nome}} </td> <!-- origem UORG -->
							<td> {!!floor($inscricao->ic_vl_pontuacao)!!} </td>
							<td class="colunaAcoes">
								<a href="{{URL::route('mostraInscricao',[$inscricao->ic_id_inscricao])}}">
									<button type="button" class="btn btn-default"><i class="icon-eye-open"></i></button>
								</a>
								<button type="button" class="btn btn-danger" ng-click="mController.confirmarDelete({{ $inscricao->ic_id_inscricao }}, '{{ $inscricao->sd_no_nome }}')"><i class="icon-trash"></i></button>
								<!--a href="{{URL::route('apagaInscricao',[$inscricao->ic_id_inscricao])}}" onclick="return confirmaClick();">
									
								</a!-->
							</td>
						</tr>
					@endforeach
				</table>
				<div class="pagination"> {!! str_replace('/?', '?', $inscricoes->render()) !!}</div>

			</div>

		</div>
		
		<!--pre>//mController|json//</pre-->
		
		<button type="button" class="close" ng-click="mController.showModal()">×</button>
		<div id="myModal" class="modal" ng-class="{'hide': !mController.modalVisible}" tabindex="-1" role="dialog" aria-labelledby="modal" aria-hidden="true">
		  <div class="modal-header">
			<button type="button" class="close"  ng-click="mController.closeModal()">×</button>
			<h3 id="myModalLabel">Confirmar Exclusão</h3>
		  </div>
		  {!! Form::open(['url' => URL::route('apagaInscricao'), 'id'=>'form' , 'name'=>'form' , 'method' => 'post','class'=>'form-horizontal ']) !!}
			<input type="hidden" name="ic_id_inscricao" ng-value="mController.idDelete">
			  <div class="modal-body">
				<p>Tem certeza que deseja excluir DEFINITIVAMENTE a inscrição #//mController.idDelete// de //mController.nomeDelete//. Não será impossível reincluir nova inscrição para este servidor?</p>
				<div class="control-group">
					<label class="control-label" for="ic_st_justificativa_alteracao">Justificativa da exclusão</label>
					<div class="controls">
						<textarea class="input-xlarge" rows="3" id="ic_st_justificativa_alteracao" name="ic_st_justificativa_alteracao"  ng-model="mController.justificativa" validation="required maiorQue20"></textarea>
					</div>
				</div>
				<div ng-show="form.ic_st_justificativa_alteracao.$touched" >
					<div class="alert alert-error" ng-show="form.ic_st_justificativa_alteracao.$error.required">Campo de preenchimento obrigatório!</div> 
					<div class="alert alert-error" ng-show="form.ic_st_justificativa_alteracao.$error.maiorQue20">A justificativa deve ter ao menos 20 caracteres!</div>
				</div>
			  </div>
			  <div class="modal-footer">
				<button type="button" class="btn" ng-click="mController.closeModal()">Fechar</button>
				<button type="submit" ng-disabled="form.$invalid" class="btn btn-danger">Excluir</button>
			  </div>
		  {!! Form::close() !!}
		</div>
	</div>

@stop

