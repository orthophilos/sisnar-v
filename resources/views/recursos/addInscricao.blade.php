@extends('templates.layout')

@section('scripts')
	<script src="{{url('/angular/angular.min.js')}}"></script>
	<script src="{{url('/moment/moment.min.js')}}"></script>
	
	
    <script type="text/javascript">
        $(document).ready(function () {
            $("#Recursos").addClass("ativo");
        });
    </script>
	<script type="text/javascript">
		var app = angular.module('sisnarApp', []);
        app.config(function ($interpolateProvider) {
            $interpolateProvider.startSymbol('//');
            $interpolateProvider.endSymbol('//');
        });
		
		
		
		
		app.controller('MainController', function ($http, $scope) {
			var self = this;
			self.locais = <?php echo(($uorgs))?>;
			
			
			self.sd_cd_matricula_servidor = "";
			self.justificativa = "";
			self.sd_cd_cpf_servidor = "";
			self.locais = <?php echo(($uorgs))?>;
			self.idUorgOrigem = undefined;
			
			
			self.getDadosWebService = function(){
				var cpf = self.sd_cd_cpf_servidor.replace(/\D/g, "");
				var url = '{{URL::route('index')}}/recursos/getDadosWebService/' + cpf;
				$http.get(url).then(
					function sucess(response){
						console.debug(response);
						var data = response.data[0];
						
						self.sd_no_nome = data.nomeCompleto;
						self.dataNasc =  moment(data.dataNascimento).format("DD/MM/YYYY"); 
						self.sd_cd_matricula_servidor = data.matricula;
						self.dataExercicio = moment(data.dataExercicio).format("DD/MM/YYYY");
						self.dataLotacao = moment(data.dataLotacao).format("DD/MM/YYYY");
						self.idUorgOrigem = {"vg_id_uorg": parseInt( data.unidadeOrganizacional.codigoSiape )};
					},
					function error(response){
						console.debug(response);
					}
				);
				
				
				
			};
			
			
			
			//data exercicio
			self.co_dt_publicacao_edital = moment("<?php echo(($proximoConcurso->co_dt_publicacao_edital))?>");
			self.diasExercicio = undefined; 
			self.dataExercicio =  "";
			 
			$scope.$watch(
				function watch( scope ) {
					return( self.dataExercicio );
				},
				function(newValue, oldValue) {
					var momentDate = moment(newValue, "DD/MM/YYYY");
					self.diasExercicio = self.co_dt_publicacao_edital.diff(momentDate, 'days');
				}
			);
			
			
			//data lotação
			self.diasLotacao = undefined; 
			self.dataLotacao =  "";
			 
			$scope.$watch(
				function watch( scope ) {
					return( self.dataLotacao );
				},
				function(newValue, oldValue) {
					var momentDate = moment(newValue, "DD/MM/YYYY");
					self.diasLotacao = self.co_dt_publicacao_edital.clone().diff(momentDate, 'days');
				}
			);
			
			
			//data nascimento
			self.idade = undefined;
			self.dataNasc =  "";
			$scope.$watch(
				function watch( scope ) {
					return( self.dataNasc );
				},
				
				function(newValue, oldValue) {
					var momentDate = moment(newValue, "DD/MM/YYYY");
					
					var anos = Math.abs(momentDate.diff(self.co_dt_publicacao_edital, 'years'));
					console.debug('anos:'+anos);
					
					var dias = Math.abs( momentDate.clone().add(anos, 'years').diff(self.co_dt_publicacao_edital,'days') );
					
					dias =  ((dias/365) % 1).toFixed(3);
					
					console.debug('dias:'+dias);
					console.debug(momentDate);
					console.debug(self.co_dt_publicacao_edital);
					self.idade = parseInt(anos) + parseFloat(dias);
				}
			);
			
		});
		 
		 
		 
		
		 
		 
		 
		
        app.controller('opcoesController', function ($http) {
            var self = this;
			
            self.locais = <?php echo(($uorgs))?>;
            self.opcoes = [];
		
			self.maximoOpcoes = {{$quantidadeMaxOpcoes}};

            self.podeAddOpcao = function () {
				for(var i=0;i<self.opcoes.length;i++){
					if(self.opcoes[i].vg_id_uorg === undefined)
						return false;
				}
					
                return (self.opcoes.length < self.maximoOpcoes);
            }
            self.addOpcao = function () {
                if (self.podeAddOpcao()) {
                    self.opcoes.push({});
                }
            }
			self.removeOpcao = function(idx){
				self.opcoes.splice(idx,1);
			}
			self.subir = function(idx){
				if(idx==0)
					return;
				var opcao = self.opcoes.splice(idx,1)[0];
				self.opcoes.splice(idx-1,0,opcao);;
			}
			self.descer = function(idx){
				if(idx==self.opcoes.length-1)
					return;
				var opcao = self.opcoes.splice(idx,1)[0];
				self.opcoes.splice(idx+1,0,opcao);     
			}
			
			//retorna true se o local já foi selecionado por outra opção
			self.isSelecionada = function(local, idx){
				//console.debug(local);
				//console.debug(idx);
				//console.debug('self.opcoes.indexOf(local): '+self.opcoes.indexOf(local))
				for(var i=0;i<self.opcoes.length;i++){
					if(local.vg_id_uorg == self.opcoes[i].vg_id_uorg 
						&& idx != i){
						return true;
					}
				}
				return false;
			} 
			
			self.mostrarProsseguir = function(){
				return (self.opcoes.length>0) && (self.opcoes[0].vg_id_uorg);
			}
			
        });
    </script>
	<script src="{{url('/angular/diretrizes.js')}}"></script>
@stop

@section('conteudo')
	<style>
	input.ng-invalid.ng-touched, textarea.ng-invalid.ng-touched{
		background-color: #fccfcf !important;
		border-color: #a94442 !important;
	}
	</style>
	
    
	{{--@if($errors->any())--}}
		{{--<div class="box">--}}
            {{--<ul class="alert alert-danger">--}}
                {{--@foreach($errors->all() as $erro)--}}
                    {{--<li>{{ $erro }}</li>--}}
                {{--@endforeach--}}
            {{--</ul>--}}
			{{--</div>--}}
	{{--@endif--}}
	<div  ng-app="sisnarApp" ng-controller="MainController as mController" >
		{!! Form::open(['url' => URL::route('gravaInscricao'), 'id'=>'form' , 'name'=>'form' , 'method' => 'post','class'=>'form-horizontal ']) !!}
		<div  class="box">
			<div class="box-header">
				<span class="title" style="font-size: 18px;">
					<div class="text-center">
						<strong>
							<span class="text-error">
								<i class="ui-icon-document"></i>
							</span>
							<span class="text-success">
								Nova Inscrição 
							</span>
						</strong>
					</div>
				</span>
			</div>
			<div class="box-content padded">
		
				<div class="control-group">
					<label class="control-label" for="sd_no_nome">CPF Servidor</label>
					<div class="controls">
						<input class="input-medium" type="text" id="sd_cd_cpf_servidor" name="sd_cd_cpf_servidor" ng-model="mController.sd_cd_cpf_servidor" formatter="cpf">
						<button type="button" class="btn btn-default" label="Buscar" aria-label="Buscar" ng-click="mController.getDadosWebService()">
							<i class="icon-search"></i>
						</button>
					</div>
				</div>
				
				
				<div class="control-group">
					<label class="control-label" for="sd_no_nome">Matricula/Servidor</label>
					<div class="controls">
						<input class="input-small" type="text" id="sd_cd_matricula_servidor" name="sd_cd_matricula_servidor" ng-model="mController.sd_cd_matricula_servidor" formatter="numberOnly">
						<input class="input-xxlarge" type="text" id="sd_no_nome" name="sd_no_nome" ng-model="mController.sd_no_nome">
					</div>
				</div>
				
				<div class="control-group " >
					<label class="control-label" for="ic_vl_data_nasc">Data nascimento</label>
					<div class="controls">
						<input class="input-small"  type="text" id="ic_vl_data_nasc" name="ic_vl_data_nasc" ng-model="mController.dataNasc" formatter="date" validation="required date"/> 
						<span ng-show="form.ic_vl_data_nasc.$valid">(//mController.idade// anos da publicação do edital)</span>
						<input class="" type="hidden" id="ic_vl_idade" name="ic_vl_idade" ng-value="mController.idade">
					</div>
				</div>
				<div ng-show="form.submitted || !form.ic_vl_data_nasc.$pristine || form.ic_vl_data_nasc.$touched">
					<div class="alert alert-error" ng-show="form.ic_vl_data_nasc.$error.required">Campo de preenchimento obrigatório!</div> 
					<div class="alert alert-error" ng-show="form.ic_vl_data_nasc.$error.date">Data inválida!</div>
				</div>
				
				<div class="control-group">
					<label class="control-label" for="vg_id_uorg">UORG origem</label>
					<div class="controls">
						<select name="vg_id_uorg" 
								id="vg_id_uorg"
								class="input-xxlarge"
								ng-options="local.vg_no_nome group by local.vg_ed_uf  for local in mController.locais track by local.vg_id_uorg" 
								ng-model="mController.idUorgOrigem"
								 >
						</select>
					</div>
				</div>
				<div class="control-group">
					<label class="control-label" for="ic_vl_data_ini_serv">Data da entrada em exercício </label>
					<div class="controls">
						<input class="input-small" type="text" id="ic_vl_data_ini_serv" name="ic_vl_data_ini_serv" ng-model="mController.dataExercicio" formatter="date" validation="required date"/> 
						<span ng-show="form.ic_vl_data_ini_serv.$valid">(//mController.diasExercicio// dias da publicação do edital)</span>
						<input class="" type="hidden" id="ic_vl_dias_desd_ini_serv" name="ic_vl_dias_desd_ini_serv" ng-value="mController.diasExercicio">
					</div>
				</div>
				<div ng-show="form.submitted || !form.ic_vl_data_ini_serv.$pristine || form.ic_vl_data_ini_serv.$touched">
					<div class="alert alert-error" ng-show="form.ic_vl_data_ini_serv.$error.required">Campo de preenchimento obrigatório!</div> 
					<div class="alert alert-error" ng-show="form.ic_vl_data_ini_serv.$error.date">Data inválida!</div>
				</div>
				
				
				<div class="control-group">
					<label class="control-label" for="ic_vl_dias_desd_ini_serv">Data da última lotação</label>
					<div class="controls">
						
						<input class="input-small" type="text" id="ic_vl_data_ult_lotacao" name="ic_vl_data_ult_lotacao" ng-model="mController.dataLotacao" formatter="date" validation="required date"/>
						<span ng-show="form.ic_vl_data_ult_lotacao.$valid">(//mController.diasLotacao// dias da publicação do edital)</span>
						<input class="" type="hidden" id="ic_vl_dias_desd_ult_lotacao" name="ic_vl_dias_desd_ult_lotacao" ng-value="mController.diasLotacao">
					</div>
				</div>
				<div ng-show="form.submitted || !form.ic_vl_data_ult_lotacao.$pristine || form.ic_vl_data_ult_lotacao.$touched">
					<div class="alert alert-error" ng-show="form.ic_vl_data_ult_lotacao.$error.required">Campo de preenchimento obrigatório!</div> 
					<div class="alert alert-error" ng-show="form.ic_vl_data_ult_lotacao.$error.date">Data inválida!</div>
				</div>
				
				<div ng-controller="opcoesController as controller">
					<div class="box-content">
						<div >
							<table class="table table-striped">
								<thead>
									<tr>
										<th colspan="3">Opções</th>
									</tr>
									<tr>
										<td>Prioridade</td>
										<td>UORG</td>
										<td>Ações</td>
									</tr>
								</thead>
								<tbody>
									<tr ng-repeat="opcao in controller.opcoes">
										<td class="text-center"> 
											//$index+1//
										</td>
										<td  class="text-center">
											<select name="io_cd_uorg_destino_//$index//" id="io_cd_uorg_destino//$index//" ng-options="local.vg_no_nome group by local.vg_ed_uf disable when controller.isSelecionada(local,$index)  for local in controller.locais track by local.vg_id_uorg" ng-model="controller.opcoes[$index]" style="width: 100%"></select>
										</td>
										<td class="text-center">
											<button type="button" class="btn btn-default" label="Descer" aria-label="Descer"  ng-disabled="($index == controller.opcoes.length-1)" ng-click="controller.descer($index)">
												<i class="icon-arrow-down"></i>
											</button>
											<button type="button" class="btn btn-default" label="Subir" aria-label="Subir"  ng-disabled="($index == 0)" ng-click="controller.subir($index)">
												<i class="icon-arrow-up"></i>
											</button>
											<button type="button" class="btn btn-danger" label="Remover" aria-label="Remover" ng-disabled="(controller.opcoes.length == 1)" ng-click="controller.removeOpcao($index)">
												<i class="icon-remove"></i>
											</button>
										</td>
									</tr>
							</table>
							<br/>
							<div class="row-fluid">
								<div class="span10"></div>
								<div class="span2">
									<button id="botaoAddOpcao"
											type="button"
											class="btn btn-success"
											ng-click="controller.addOpcao()"
											ng-show="controller.podeAddOpcao()"> Adicionar Opção
									</button>
								</div>
							</div>
						</div>
					</div>
				</div>
				
				<div class="control-group">
					<label class="control-label" for="ic_st_justificativa_alteracao">Justificativa da inserção</label>
					<div class="controls">
						<div class="row-fluid">
							<textarea class="input-xxlarge" rows="3" id="ic_st_justificativa_alteracao" name="ic_st_justificativa_alteracao" ng-model="mController.justificativa" validation="required maiorQue20"></textarea>
						</div>
						<br>
						<div class="row-fluid">
							<div class="checkbox">
								<label>{!!  Form::checkbox('forcaServidor', 1, true)   !!} Não forçar sobreposição de cadastro de servidor</label>
							</div>


						</div>
					</div>
				</div>
				<div ng-show="form.submitted || !form.ic_st_justificativa_alteracao.$pristine || form.ic_st_justificativa_alteracao.$touched">
					<div class="alert alert-error" ng-show="form.ic_st_justificativa_alteracao.$error.required">Campo de preenchimento obrigatório!</div> 
					<div class="alert alert-error" ng-show="form.ic_st_justificativa_alteracao.$error.maiorQue20">A justificativa deve ter ao menos 20 caracteres!</div>
				</div>
				
				<br/>
				
				
			</div>
		</div>
	
		<div class="row-fluid">
			<div class="span2">
				<button id="botaoVoltar" type="button" class="btn btn-primary" onclick="window.history.back();"><i class="icon-arrow-left"></i> Voltar</button>
			</div>
			<div class="span7"></div>
			<div class="span3">
				
					
					<button id="botaoSubmit" 
							type="submit"
							class="btn btn-primary pull-right"
							data-toggle="tooltip"
							title="Salvar"
							ng-disabled="form.$invalid"
							>
								<i class="icon-save"></i> Salvar 
					</button>
				 
			</div>
		</div>
		{!! Form::close() !!}

	
	</div>
		
	{{--<pre>	--}}
    {{--{{$proximoConcurso}}--}}
    {{----}}
	{{--{{$quantidadeMaxOpcoes}}--}}
    {{----}}
	{{--{{$uorgs}}--}}
{{----}}
    {{--rota para pegar os dados webservice === > /recursos/getDadosWebService/{cpf}--}}
{{----}}
            {{--ADICIONA NOVA INSCRICAO--}}
			{{----}}
	{{--</pre>--}}
@stop
