@extends('templates.layout')

@section('scripts')
	<script src="{{url('/angular/angular.min.js')}}"></script>
	<link rel="stylesheet" href="{{url('/ngtable/dist/ng-table.min.css')}}">
	<script src="{{url('/ngtable/dist/ng-table.js')}}"></script>
	<script type="text/javascript">
        $(document).ready(function () {
            $("#Recursos").addClass("ativo");
        });
    </script>
	<script>
		var app = angular.module('sisnarApp', ["ngTable"]);
		app.config(function ($interpolateProvider) {
			$interpolateProvider.startSymbol('//');
			$interpolateProvider.endSymbol('//');
		});
		
		
		app.controller('resultadoFinalController', function (NgTableParams) {
			var self = this;
			self.data = <?php echo(( $aprovadosConcurso )) ?>; 
			self.tableParams = new NgTableParams({}, { dataset: self.data});
		});
    </script>
@stop 

@section('conteudo')

    <div class="page-header">
		<h1> Resultado Final do Concurso</h1>
        {{--<h1> {{ config("PRF.nomeCurto") }}--}}
            {{--<small><i class="icon-double-angle-right"></i> {{ config("PRF.nomeSistema") }}</small>--}}
		{{--<small><i class="icon-double-angle-right"></i> Lista com os contemplados</small>--}}
        {{--</h1>--}}
    </div>
	{{--﻿@if ( count($errors) > 0)--}}
		{{--@foreach ($errors->all() as $erro)--}}
			{{--<div class="alert alert-error">--}}
				{{--<button type="button" class="close" data-dismiss="alert">&times;</button>--}}
				{{--<h4>Erro!</h4>--}}
				{{--{{ $erro }}--}}
			{{--</div>--}}
		{{--@endforeach--}}
	{{--@endif--}}
	
    <div class="alert-info">
        <h4>Planilha Resultado Classificação : </h4>
       - <a href="{{URL::route('resultadoFinalConcurso_xls')}}">Clique aqui para download de planilha resultado classificação;</a><br>
	   - <a href="{{URL::route('listaDeficit_xls')}}">Clique aqui para download de planilha deficit gerado nas UORGs devido a SISNAR;</a>
    </div>
	<br>
	<div class="box" ng-app="sisnarApp" ng-controller="resultadoFinalController as controller">
		<div class="box-header">
			<span class="title" style="font-size: 18px;">
				<div class="text-center">
					<strong>
						<span class="text-error">
							<i class="ui-icon-document"></i>
						</span>
						<span class="text-success">
							Resultado Final
						</span>
					</strong>
				</div>
			</span>
        </div>
		<div class="box-content padded">
			<div class="box-content">
				<div class="pull-right">Total: //controller.data.length//</div><br>
				<table ng-table="controller.tableParams" class="table" show-filter="true">
					<tr ng-repeat="row in $data">
						<td title="'Nome'" filter="{ sd_no_nome: 'text'}" sortable="'sd_no_nome'">
							//row.sd_no_nome//</td>
						<td title="'Matrícula'" filter="{ sd_cd_matricula_servidor: 'text'}" sortable="'sd_cd_matricula_servidor'">
							//row.sd_cd_matricula_servidor//</td>
						<td title="'CPF'" filter="{ sd_cd_cpf: 'text'}" sortable="'sd_cd_cpf'">
							//row.sd_cd_cpf//</td>
					
						<td title="'ID'" filter="{ io_id_inscricao_opcao: 'number'}" sortable="'io_id_inscricao_opcao'">
							//row.io_id_inscricao_opcao//</td>
						<td title="'ID UORG Origem'" filter="{ io_cd_vg_uorg_origem: 'number'}">
							//row.io_cd_vg_uorg_origem//</td>
						<td title="'ID UORG Destino'" filter="{ io_cd_uorg_destino: 'number'}">
							//row.io_cd_uorg_destino//</td>	
						<td title="'UORG Destino'" filter="{ vg_no_nome: 'text'}">
							//row.vg_no_nome//</td>	
						
							
						
						<td title="'Pontuação'" filter="{ ic_vl_pontuacao: 'number'}" sortable="'ic_vl_pontuacao'">
							//row.ic_vl_pontuacao//</td>
						
						<td title="'Contemplado'" filter="{ io_st_contemplado: 'number'}" sortable="'io_st_contemplado'">
							//row.io_st_contemplado//</td>
					</tr>
				</table>
			</div>
		</div>
     
    </div>
	
	<pre>{{-- var_dump($aprovadosConcurso) --}}</pre>
@stop