@extends('templates.layout')

@section('scripts')
	<script src="{{url('/angular/angular.min.js')}}"></script>
	<link rel="stylesheet" href="{{url('/ngtable/dist/ng-table.min.css')}}">
	<script src="{{url('/ngtable/dist/ng-table.js')}}"></script>
	<script type="text/javascript">
        $(document).ready(function () {
            $("#submenuEstatisticaResumo").addClass("ativo");
        });
    </script>
	<script>
		var app = angular.module('sisnarApp', ["ngTable"]);
		app.config(function ($interpolateProvider) {
			$interpolateProvider.startSymbol('//');
			$interpolateProvider.endSymbol('//');
		});
		app.controller('cenarioGeralController', function (NgTableParams) {
			var self = this;
			self.data = <?php echo(json_encode( $cenarioGeral )) ?>;
			console.debug(self.data);
			self.tableParams = new NgTableParams({}, { dataset: self.data});
		});
    </script>

@stop

@section('conteudo')
	
	<!--BOOTSTRAP FIX-->
	<style>
		.text-center {
			text-align: center !important;
		}
	</style>
	@if($errors->any())
		<ul class="alert alert-danger">
			@foreach($errors->all() as $erro)
				<li>{{ $erro }}</li>
			@endforeach
		</ul>
	@endif
	
	
	<div class="box" ng-app="sisnarApp" ng-controller="cenarioGeralController as controller">
		<div class="box-header">
			<span class="title" style="font-size: 18px;">
				<div class="text-center">
					<strong>
						<span class="text-error">
							<i class="ui-icon-document"></i>
						</span>
						<span class="text-success">
							Cenário Geral
						</span>
					</strong>
				</div>
			</span>
        </div>
		
		<div class="box-content padded">
			<div class="box-content">
				<div class="pull-right">Total de Inscrições : {{$sumarioInscricoes}}</div><br>
				<table ng-table="controller.tableParams" class="table" show-filter="true">
					<tr ng-repeat="row in $data">
						<td title="'UORG'" filter="{ nomeUorg: 'text'}" sortable="'nomeUorg'">
							//row.nomeUorg//</td>
						<td title="'UF'" filter="{ UF: 'text'}" sortable="'UF'">
							//row.UF//</td>
						<td title="'Vagas'" filter="{ vagasPreEstabelecidas: 'number'}" sortable="'vagasPreEstabelecidas'">
							//row.vagasPreEstabelecidas//</td>
						<td title="'Candidatos'" filter="{ totalCandidatos: 'number'}" sortable="'totalCandidatos'">
							//row.totalCandidatos//</td>
						<td title="'Pontuação último colocado'" filter="{ pontuacaoUltimoColocado: 'number'}" sortable="'pontuacaoUltimoColocado'">
							//row.pontuacaoUltimoColocado//</td>
					</tr>
				</table>
			</div>
		</div>
		<small>* pontuação considerando apenas as primeiras opções, os valores e probabilidades podem se alterar ao longo do concurso</small>
	</div>
@stop