 @extends('templates.layout')

@section('scripts')

    <script src="{{url('/angular/angular.min.js')}}"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $("#menuInscricao").addClass("ativo");
        });
    </script>
    {{--{!! dd($listaUorgs,$dadosOpcoesCadastradas, $checkVisualizaEstatisticaHoje ) !!}--}}

    <script>
        var app = angular.module('sisnarApp', []);
        app.config(function ($interpolateProvider) {
            $interpolateProvider.startSymbol('//');
            $interpolateProvider.endSymbol('//');
        });
        app.controller('inscricaoPasso2Controller', function ($http) {
            var self = this;
			
            self.locais = <?php echo($listaUorgs)?>;
            self.opcoes = <?php echo($dadosOpcoesCadastradas)?>;
			
			self.checkVisualizaEstatisticaHoje = <?php echo($checkVisualizaEstatisticaHoje)?>;
			
			if(self.checkVisualizaEstatisticaHoje){
				self.textBotaoProsseguir = "Visualizar cenário";
			}else{
				self.textBotaoProsseguir = "Confirmar inscrição definitivamente";
			}
			
            self.maximoOpcoes = {{$quantidadeMaxOpcoes}};

            self.podeAddOpcao = function () {
				for(var i=0;i<self.opcoes.length;i++){
					if(self.opcoes[i].vg_id_uorg === undefined)
						return false;
				}
					
                return (self.opcoes.length < self.maximoOpcoes);
            }
            self.addOpcao = function () {
                if (self.podeAddOpcao()) {
                    self.opcoes.push({});
                }
            }
			self.removeOpcao = function(idx){
				self.opcoes.splice(idx,1);
			}
			self.subir = function(idx){
				if(idx==0)
					return;
				var opcao = self.opcoes.splice(idx,1)[0];
				self.opcoes.splice(idx-1,0,opcao);;
			}
			self.descer = function(idx){
				if(idx==self.opcoes.length-1)
					return;
				var opcao = self.opcoes.splice(idx,1)[0];
				self.opcoes.splice(idx+1,0,opcao);
			}
			
			//retorna true se o local já foi selecionado por outra opção
			self.isSelecionada = function(local, idx){
				//console.debug(local);
				//console.debug(idx);
				//console.debug('self.opcoes.indexOf(local): '+self.opcoes.indexOf(local))
				for(var i=0;i<self.opcoes.length;i++){
					if(local.vg_id_uorg == self.opcoes[i].vg_id_uorg 
						&& idx != i){
						return true;
					}
				}
				return false;
			} 
			
			self.mostrarProsseguir = function(){
				return (self.opcoes.length>0) && (self.opcoes[0].vg_id_uorg);
			}
			
        });
    </script>

@stop

@section('conteudo')
	<!--BOOTSTRAP FIX-->
	<style>
		.text-center {
			text-align: center !important;
		}
	</style>
    {{--@if($errors->any())--}}
        {{--<ul class="alert alert-danger">--}}
            {{--@foreach($errors->all() as $erro)--}}
                {{--<li>{{ $erro }}</li>--}}
            {{--@endforeach--}}
        {{--</ul>--}}
    {{--@endif--}}
	<div ng-app="sisnarApp" ng-controller="inscricaoPasso2Controller as controller">
		
		{!! Form::open(['url' => action('MinhaInscricaoController@syncMinhasOpcoes',[],true), 'method' => 'post', 'class' => 'form-horizontal']) !!}
		<div  class="box">
			<div class="box-header">
				<span class="title" style="font-size: 18px;">
					<div class="text-center">
						<strong>
							<span class="text-error">
								<i class="ui-icon-document"></i>
							</span>
							<span class="text-success">
								Opções
							</span>
						</strong>
					</div>
				</span>
			</div>
			<div class="box-content padded">
				<div class="box-header">
					<span class="title">
						<h5>Onde gostaria de trabalhar ?</h5>
						<div class="text-center">
							<strong>
								Faça suas escolhas em ordem de prioridade até o máximo de {{$quantidadeMaxOpcoes}} opções.
								Para esclarecimento, pode ser escolhido apenas um ou duas opções, conforme desejo do servidor.
							</strong>
						</div>
					</span>
				</div>
				<div class="box-content">
			
					<div >
						
						<table class="table table-striped">
							<thead>
								<tr>
									<td>Prioridade</td>
									<td>UORG</td>
									<td>Ações</td>
								</tr>
							</thead>
							<tbody>
								<tr ng-repeat="opcao in controller.opcoes">
									<td  class="text-center"> 
										//$index+1//
									</td>
									<td  class="text-center">
										<select name="vg_id_uorg_//$index//" ng-options="local.vg_no_nome group by local.vg_ed_uf disable when controller.isSelecionada(local,$index)  for local in controller.locais  track by local.vg_id_uorg" ng-model="controller.opcoes[$index]" style="width: 100%"></select>
									</td>
									<td  class="text-center">
										<button type="button" class="btn btn-default" label="Descer" aria-label="Descer"  ng-disabled="($index == controller.opcoes.length-1)" ng-click="controller.descer($index)">
											<i class="icon-arrow-down"></i>
										</button>
										<button type="button" class="btn btn-default" label="Subir" aria-label="Subir"  ng-disabled="($index == 0)" ng-click="controller.subir($index)">
											<i class="icon-arrow-up"></i>
										</button>
										<button type="button" class="btn btn-danger" label="Remover" aria-label="Remover" ng-disabled="(controller.opcoes.length == 1)" ng-click="controller.removeOpcao($index)">
											<i class="icon-remove"></i>
										</button>
									</td>
								</tr>
						</table>
						<br/>
						<div class="row-fluid">
							<div class="span10"></div>
							<div class="span2">
								<button id="botaoAddOpcao"
										type="button"
										class="btn btn-success"
										ng-click="controller.addOpcao()"
										ng-show="controller.podeAddOpcao()"> Adicionar Opção
								</button>
							</div>
						</div>
					</div>
				</div>

			</div>
		</div>


		<div class="row-fluid">
			<div class="span2">
				<a href="{{URL::route('inscricaoPasso1')}}">
					<button id="botaoVoltar" type="button" class="btn btn-primary"><i class="icon-arrow-left"></i> Voltar</button>
				</a>
			</div>
			<div class="span7"></div>
			<div class="span3">
				
					<button id="botaoProsseguir" 
							type="submit"
							class="btn btn-primary pull-right"
							data-toggle="tooltip"
							title="A visualização de suas probabilidades estará disponível ate dia {{$dataMaxVisualizarEstatistica}}"
							ng-show="controller.mostrarProsseguir()"
							>
						//controller.textBotaoProsseguir// <i class="icon-arrow-right"></i>
					</button>
				
			</div>
			
		</div>
		{!! Form::close() !!}
	</div>
	{{--
	{{$dadosOpcoesCadastradas}} ,$quantidadeMaxOpcoes,$mostraEstatisticaCandidato,$listaUorgs)!!}
	--}}
@stop


