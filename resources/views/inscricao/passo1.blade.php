@extends('templates.layout')

@section('scripts')
    <script type="text/javascript">
        $(document).ready(function () {
            $("#menuInscricao").addClass("ativo");
        });
    </script>
@stop

@section('conteudo')
    <div class="box">
        {{--@if($errors->any())--}}
            {{--<ul class="alert alert-danger">--}}
                {{--@foreach($errors->all() as $erro)--}}
                    {{--<li>{{ $erro }}</li>--}}
                {{--@endforeach--}}
            {{--</ul>--}}
        {{--@endif--}}

        <div class="box-header">
        <span class="title" style="font-size: 18px;">
            <div class="text-center">
                <strong>
                    <span class="text-error">
                        <i class="ui-icon-document"></i>
                    </span>
                    <span class="text-success">
                        Dados do servidor
                    </span>
                </strong>
            </div>
        </span>
        </div>
        <div class="box-content padded">
            <div class="text"></div>
            <p>
                Confira seus dados, realize sua inscrição e depois confirme mesmo que haja alguma inconsistência nas
                informações apresentadas, a exemplo de erros quanto à lotação ou data de entrada em exercício.
                No período destinado aos recursos, faça referência aos erros encontrados.

                {{--Confira seus dados, caso encontre algum erro, faça a inscrição , a confirme e no período referente a--}}
                {{--recursos, notifique a administração:--}}
            </p>
            </div>
            <div class="row-fluid">
                <div class="span3">Nome:</div>
                <div class="span9">{{$dadosUsuario['sd_no_nome']}}</div>
            </div>
            <div class="row-fluid">
                <div class="span3">Matricula:</div>
                <div class="span9">{{$dadosUsuario['sd_cd_matricula_servidor']}}</div>
            </div>
            <div class="row-fluid">
                <div class="span3">Data Entrada em Exercício:</div>
                <div class="span9">
                    {!! \App\Funcoes\Funcoes::MyUnixTimeToDate($dadosUsuario['dataExercicio'])!!}
                </div>
            </div>
            <div class="row-fluid">
                <div class="span3">Lotação Atual:</div>
                <div class="span9">{{$dadosInscricao->vg_no_nome}}</div>
            </div>

            <div class="row-fluid">
                <div class="span3">Data de Início da Atual Lotação:</div>
                <div class="span9">
                    {!! \App\Funcoes\Funcoes::MyUnixTimeToDate($dadosUsuario['dataLotacaodoWebService'])!!}
                </div>
            </div>
            {{--<!----}}
            {{--<div class="row-fluid">--}}
                {{--<div class="span3">Lotação Considerada No SISNAR:</div>--}}
                {{--<div class="span9">--}}
                    {{--{{$dadosUsuario['dataLotacaodoWebService']}}--}}
                {{--</div>--}}
            {{--</div>-->--}}

            <div class="row-fluid">
                <div class="span3">Data Da Última Lotação (Considerada):</div>
                <div class="span9">
                    {{--{{$dadosUsuario['dataLotacao']}}--}}
                    {!! \App\Funcoes\Funcoes::MyUnixTimeToDate($dadosUsuario['dataLotacao'])!!}
                </div>
            </div>
            <div class="row-fluid">
                <div class="span3">Data Nascimento:</div>
                <div class="span9">
                    {!! \App\Funcoes\Funcoes::MyUnixTimeToDate($dadosUsuario['dataNascimento'])!!}
                </div>
            </div>
            <div class="row-fluid">
                <div class="span3">Pontuação:</div>
                <div class="span9">{{intval($dadosInscricao->ic_vl_pontuacao)}}</div>
            </div>


        </div>
    </div>
   <div class="row-fluid">
		<div class="span2">
			<a href="{{URL::route('index')}}">
				<button id="botaoProsseguir" type="submit" class="btn btn-primary"><i class="icon-arrow-left"></i> Voltar</button>
			</a>
        </div>
        <div class="span8"></div>
        <div class="span2">

            {{--@if ($dadosInscricao->ic_st_inscricao_confirmada)--}}
                {{--<a href="{{URL::route('comprovanteInscricao')}}">--}}
            {{--@else--}}
                <a href="{{URL::route('inscricaoPasso2')}}">
            {{--@endif--}}
				<button id="botaoProsseguir" type="submit" class="btn btn-primary pull-right">Prosseguir <i class="icon-arrow-right"></i></button>
			</a>
        </div>
    </div>
	<br/>
    
@stop