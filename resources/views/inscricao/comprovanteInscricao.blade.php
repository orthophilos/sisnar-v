@extends('templates.layoutPDF')
@section('scripts')
<script>
	$( document ).ready(
		function(){
			window.print();
		}
	);
</script>
@stop

@section('conteudo')
<!--BOOTSTRAP FIX-->
	<style>
		.text-center {
			text-align: center !important;
		}
	</style>
<br/><br/>
{{--{!! dd($dadosInscricao, $dadosInscricaoOpcoes) !!}--}}
<div class="container">
	<div class="row">
		<div class="span3"><strong>Matrícula no SISNAR:</strong></div>
		<div class="span9">{{$dadosInscricao['ic_id_inscricao']}}</div>
	</div>
	<div class="row">
		<div class="span3"><strong>Nome:</strong></div>
		<div class="span9">{{$dadosInscricao['sd_no_nome']}}</div>
	</div>
	<div class="row">
		<div class="span3"><strong>Matricula SIAPE:</strong></div>
		<div class="span9">{{$dadosInscricao['sd_cd_matricula_servidor']}}</div>
	</div>
	<div class="row">
		<div class="span3"><strong>Uorg Atual:</strong></div>
		<div class="span9">{{$dadosInscricao['vg_no_nome']}}</div>
	</div>


	<div class="row">
		<div class="span3"><strong>Dias desde entrada em exercício:</strong></div>
		<div class="span9">{{$dadosInscricao['ic_vl_dias_desd_ini_serv']}}</div>
	</div>
	<div class="row">
		<div class="span3"><strong>Dias desde última lotação:</strong></div>
		<div class="span9">{{$dadosInscricao['ic_vl_dias_desd_ult_lotacao']}}</div>
	</div>
	<div class="row">
		<div class="span3"><strong>Pontuação:</strong></div>
		<div class="span9">{!!floor($dadosInscricao['ic_vl_pontuacao'])!!}</div>
	</div>
	{{--<div class="row">--}}
		{{--<div class="span3"><strong>Código de Verificação:</strong></div>--}}
		{{--<div class="span9">{{$myHash}}</div>--}}
	{{--</div>--}}
	<br/>

	<div class="table-responsive">
		<table class="table">
		<tr><th colspan="3" class="text-center">OPÇÕES DE DESTINO SELECIONADAS PELO CANDIDATO</th></tr>
			<tr>
				<th>Prioridade<br></th>
				<th>UF</th>
				<th>Unidade Organizacional<br></th>
			</tr>
	@foreach($dadosInscricaoOpcoes  as  $dadoInscricaoOpcao )
				<tr>
					<td>{{$dadoInscricaoOpcao['io_vl_ordem_preferencia']}}</td>
					<td>{{$dadoInscricaoOpcao['vg_ed_uf']}}</td>
					<td>{{$dadoInscricaoOpcao['vg_no_nome']}}</td>
				</tr>
	@endforeach
		</table>
	</div>
	<br>
	<div class="text-center">
	<strong>Código de Comprovação</strong>
	</div>
	<div class="span12 text-center">
		<qrcode>
		{!!(QrCode::format('svg')->size(300)->generate($myHash))!!}
		</qrcode>
	</div>

</div>
<a href="{{URL::route('index')}}">
	<small>(Clique aqui para retornar ao sistema)</small>
</a>



@stop
