@extends('templates.layout')

@section('scripts')

    <script src="{{url('/angular/angular.min.js')}}"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $("#menuInscricao").addClass("ativo");
        });
		function confirmaClick(){
			var ok = confirm('Deseja confirmar DEFINITIVAMENTE sua inscrição? Após essa ação não sera mais possível alterar os dados de sua inscrição.');
			return ok;
		}
    </script>


    <script>
        var app = angular.module('sisnarApp', []);
        app.config(function ($interpolateProvider) {
            $interpolateProvider.startSymbol('//');
            $interpolateProvider.endSymbol('//');
        });
        app.controller('inscricaoPasso3Controller', function () {
			var self = this;
			self.pontuacaoCandidato = 1000;
			
            self.opcoes = <?php echo(json_encode( $estatisticadaOpcao )) ?>;
			
			self.isDetalhesExpandidos = [];
			self.toggleDetalhesExpandidos = function(idx){
				if(self.isDetalhesExpandidos[idx]===undefined){
					self.isDetalhesExpandidos[idx] = true;
				}else{
					self.isDetalhesExpandidos[idx] = !self.isDetalhesExpandidos[idx];
				}
			}
        });
    </script>

@stop

@section('conteudo')
	<!--BOOTSTRAP FIX-->
	<style>
		.text-center {
			text-align: center !important;
		}
	</style>
	{{--@if($errors->any())--}}
		{{--<ul class="alert alert-danger">--}}
			{{--@foreach($errors->all() as $erro)--}}
				{{--<li>{{ $erro }}</li>--}}
			{{--@endforeach--}}
		{{--</ul>--}}
	{{--@endif--}}
	<div class="box" ng-app="sisnarApp" ng-controller="inscricaoPasso3Controller as controller">
		<!--pre>//controller.opcoes//</pre-->
		<div class="box-header">
			<span class="title" style="font-size: 18px;">
				<div class="text-center">
					<strong>
						<span class="text-error">
							<i class="ui-icon-document"></i>
						</span>
						<span class="text-success">
							Cenário
						</span>
					</strong>
				</div>
			</span>
        </div>
		<div class="box-content padded">
			<div class="box-header">
				<span class="title">
					<div class="text-center">
						<strong>
							Cenário das inscrições realizadas e das probabilidades das opções de escolha até a data atual.
							Nos dados abaixo foram consideradas, também, as inscrições ainda não confirmadas definitivamente.
							A visualização deste cenário estará disponível até a data de {{$dataMaxVisualizarEstatistica}}.
						</strong>
					</div>
				</span>
			</div>

			<div class="box-content">
				<div >
					<div ng-repeat="opcao in controller.opcoes">
						<table class="table">
							<thead>
							<tr>
								<th>//$index+1//</th>
								<th>//opcao.estatisticaConcorrencia.nomeUorg//</th>
								<th>Probabilidade: //opcao.probabilidadeSucesso//</th>
								<th>
									<button type="button" class="btn btn-info pull-right" label="Detalhes" aria-label="Detalhes" ng-click="controller.toggleDetalhesExpandidos($index)">
										<i ng-class="{'icon-plus':!controller.isDetalhesExpandidos[$index], 'icon-minus':controller.isDetalhesExpandidos[$index] }"></i>
									</button>
								</th>
							</tr>
							</thead>
							<tbody ng-show="controller.isDetalhesExpandidos[$index]">
							<tr>
								<td colspan="4">
									<table class="table borderless" style="width: 100%">
										<tr>
											<td>Candidatos inscritos até o momento, cuja primeira opção foi esta UORG:</td>
											<td>//opcao.estatisticaConcorrencia.totalCandidatos//</td>
										</tr>
										<tr>
											<td>Vagas:</td>
											<td>//opcao.estatisticaConcorrencia.totalVagas//</td>

										</tr>
										{{--<tr>--}}
										{{--<td>Candidatos por vaga até o momento:</td>--}}
										{{--<td>//opcao.estatisticaConcorrencia.totalCandidatos//</td>--}}
										{{--</tr>--}}
										<tr>
											<td>Pontuação provável do primeiro excedente ou, no caso de zero vagas, do mais bem colocaodo, considerando apenas as 1as opções:



											</td>
											<td>//opcao.estatisticaConcorrencia.pontacaoUltimoCandidato//</td>
										</tr>
										<tr>
											<td>Sua Pontuação:</td>
											<td>//opcao.estatisticaConcorrencia.minhaPontuacao//</td>
										</tr>

									</table>
								</td>
							</tr>
							</tbody>
						</table>
					</div>





					<br/>

				</div>
			</div>

		</div>
	</div>
    <div class="row-fluid">
		<div class="span3">
			<a href="{{URL::route('inscricaoPasso2')}}">
				<button id="botaoProsseguir" type="button" class="btn btn-primary"><i class="icon-arrow-left"></i> Voltar</button>
			</a>
        </div>
        <div class="span6"></div>
        <div class="span3">
			@if($inscricaoConfirmada == 0)
				<a href="{{URL::route('confirmaInscricao')}}" onclick="return confirmaClick();"> <!-- target="_blank"> -->

					<button id="botaoProsseguir" type="button" class="btn btn-primary pull-right">Confirmar definitivamente <i class="icon-arrow-right"></i></button>
				</a>
			@elseif($inscricaoConfirmada == 1)
					<a href="{{URL::route('comprovanteInscricao')}}" onclick="return confirmaClick();">
						<button id="botaoProsseguir" type="button" class="btn btn-primary pull-right">Visualizar comprovante<i class="icon-arrow-right"></i></button>
					</a>
			@endif

        </div>
    </div>

@stop