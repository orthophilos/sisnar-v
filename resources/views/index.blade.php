@extends('templates.layout')

@section('scripts')
    <script type="text/javascript">
        $(document).ready(function () {
            $("#menuInicio").addClass("ativo");
        });
    </script>
@stop

@section('conteudo')

    {{--<div class="page-header">--}}
        {{--<h1> {{ config("PRF.nomeCurto") }}--}}
            {{--<small><i class="icon-double-angle-right"></i> {{ config("PRF.nomeSistema") }}</small>--}}
        {{--</h1>--}}
    {{--</div>--}}
	{{--﻿@if ( count($errors) > 0)--}}
		{{--@foreach ($errors->all() as $erro)--}}
			{{--<div class="alert alert-error">--}}
				{{--<button type="button" class="close" data-dismiss="alert">&times;</button>--}}
				{{--<h4>Erro!</h4>--}}
				{{--{{ $erro }}--}}
			{{--</div>--}}
		{{--@endforeach--}}
	{{--@endif--}}
    <div class="box">
        <div class="box-header">
        <span class="title" style="font-size: 18px;">
            <div class="text-center">
                <strong>
                    {{--<span class="text-error">--}}
                        {{--<i class="icon-pushpin"></i>--}}
                    {{--</span>--}}
                    <span class="text-success">
                         <h4> {{ config("PRF.nomeCurto") }}
                             <small><i class="icon-double-angle-right"></i> {{ config("PRF.nomeSistema") }}</small>
                         </h4>
                    </span>
                </strong>
            </div>
        </span>
        </div>
        <div class="box-content padded">
           @if(!is_null($proximoConcurso))
                <p>
                    Após a leitura do edital deste processo seletivo, siga para a próxima tela e verifique as opções de
                    escolha das localidades disponíveis. Verifique as probabilidades em alcançar o sucesso nas suas
                    escolhas e confirme sua inscrição.
                    Avalie as circunstâncias das escolhas antes de confirmar sua inscrição, pois ela será definitiva.
                    Antes de prosseguir, recomendamos a leitura nas
                    <a href="{{URL::route('instrucoesDeUso')}}"><i class="icon-info-sign"></i> Instruções de Uso</a> .
               </p>
               <p>
                   <strong> Próximo Concurso : </strong>
                </p>
                <div class="row-fluid">
                    <div class="span3">Data da Publicação do Edital de Abertura:</div>
                    <div class="span9">{{ date(\App\Funcoes\Funcoes::$dateFormat, strtotime($proximoConcurso->co_dt_publicacao_edital)) }}</div>
                </div>
                <div class="row-fluid">
                    <div class="span3">Período de Inscrição:</div>
                    <div class="span9">{{ date(\App\Funcoes\Funcoes::$dateFormat, strtotime($proximoConcurso->co_dt_inicio_inscricoes))}}
                        à
                        {{ date(\App\Funcoes\Funcoes::$dateFormat, strtotime($proximoConcurso->co_dt_fim_inscricoes)) }}
                    </div>
                </div>
                <div class="row-fluid">
                    <div class="span3">Data do Resultado Definitivo:</div>
                    <div class="span9">{{ date(\App\Funcoes\Funcoes::$dateFormat, strtotime($proximoConcurso->co_dt_data_execucao)) }}</div>
                </div>
                <div class="row-fluid">
                    <div class="span3">Data Provável da Remoção:</div>
                    <div class="span9">{{ date(\App\Funcoes\Funcoes::$dateFormat, strtotime($proximoConcurso->co_dt_remocao)) }}</div>
                </div>
                <div class="form-inline">
                    <a href="{{URL::route('instrucoesDeUso')}}">
                    <p><strong> Modalidade do Processo Seletivo : </strong></p>
                    <form role="form">
                        <label class="radio-inline">
                            <input type="radio"
                                   @if ($tipoConcurso == 'SISNAR')
                                        checked
                                   @else
                                        disabled
                                   @endif
                                   name="optradio">Remanejamento
                        </label>
                        <label class="radio-inline">
                            <input type="radio"
                                   @if ($tipoConcurso == 'Remanejamento')
                                        checked
                                   @else
                                        disabled
                                   @endif

                                   name="optradio">Permuta
                        </label>

                    </form>
                    </a>
           @else
               <div class="jumbotron">
                    <h3> Não há concurso de SISNAR ou REMANEJAMENTO previsto no momento!</h3>
               </div>
           @endif

            </div>
        </div>
    </div>
    <div class="row-fluid">
		<div class="span2">
			
        </div>
        <div class="span8"></div>
        @if(!is_null($proximoConcurso))
            <div class="span2">
                @if ($incricaoConfirmada)
                    <a href="{{URL::route('comprovanteInscricao')}}" target="_blank">
                @else
                    <a href="{{URL::route('inscricaoPasso1')}}">
                @endif
                    <button id="botaoProsseguir" type="submit" class="btn btn-primary pull-right">Prosseguir <i class="icon-arrow-right"></i></button>

                    </a>
            </div>
        @endif

    </div>
	<br/>
<!--
EQUIPE DESENVOLVEDORES:

    Thyago Ribeiro - Arquetipo;
    Jeferson Tadeu - Banco, Algoritmo Conservativo** e Nao Conservativo, BackEnds ;
    Claudir Galesky - Front End ;
    Marcelo Fontes -  Auxílio em Bugs / Testes / Função de recuperar historico de lotacao;

** Algoritmo conservativo foi evolução do primeiro algoritmo desenvolvido por Abel Vanderlei.

DPRF * DIRETORA GERAL : Maria Alice do Nascimento

    CH . GAB: Eduardo Aggio

    CGRH : Antonio Paim

    GERENTE PROJETO SETORIAL SISNAR: Guilherme Andrade
    REVISOR DE CONTEUDO DE BANCO: Bruno e Hudson

    Homenagem a todos os policiais que passaram dificuldades distantes de seus familiares em especial a

    Andre Portela e esposa
    Iuri
    Elaine Menezes (in Memorian)
    -->

@stop