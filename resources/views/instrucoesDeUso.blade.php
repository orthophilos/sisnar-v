@extends('templates.layout')

@section('scripts')
    <script type="text/javascript">
        $(document).ready(function () {
            $("#menuInscricao").addClass("ativo");
        });
    </script>
@stop

@section('conteudo')
	<style>
		.instrucoesDeUso p, .instrucoesDeUso li{
			text-align: justify;
		}
		.instrucoesDeUso p{ margin-left: 2em; }
		.instrucoesDeUso h4, .instrucoesDeUso h3, .instrucoesDeUso h2, .instrucoesDeUso h1 {
			padding-top: 90px;
		}
		.semPadding{
			padding-top: 0px !important;
		}
		
		.dadosTecnicos, .dadosTecnicos li{
			list-style-type: none;
		}
		
		ul.titulo1>li{
			font-weight: bold;
			font-size: 1.1em;
			
		}
		ul.titulo2 li{
			font-weight: bold;
		}
		ul.titulo2>li li {
			font-weight: normal;
		}
	</style>

    <div class="box">
        {{--@if($errors->any())--}}
            {{--<ul class="alert alert-danger">--}}
                {{--@foreach($errors->all() as $erro)--}}
                    {{--<li>{{ $erro }}</li>--}}
                {{--@endforeach--}}
            {{--</ul>--}}
        {{--@endif--}}

        <div class="box-header">
        <span class="title" style="font-size: 18px;">
            <div class="text-center">
                <strong>
                    <span class="text-error">
                        <i class="ui-icon-document"></i>
                    </span>
                    <span class="text-success">
                        Instruções de Uso e Funcionalidades SISNAR v.5.0
                    </span>
                </strong>
            </div>
        </span>
        </div>
        <div class="box-content padded">
            <div class="instrucoesDeUso">
				<h2 class="semPadding"><strong>Sumário</strong></h2>
				
				<ul id="markdown-toc">
					<li><a href="#Sistema">Sistema Sisnar</a></li>
					<li><a href="#remanejamento">Remanejamento</a></li>
					
					<li><a href="#Permuta">Permuta</a></li>
					<li><a href="#ComoSistemaEscolheAlgoritmo">Como o sistema escolhe o algorítmo</a></li>
					<li><a href="#estatistico">Entendendo Cenários e Estatística</a>
						<ul>
							<li><a href="#CalculoProbabilidade">O Cálculo de Probabilidade</a></li>
							<li><a href="#CandidatosInscritosAteoMomento">Candidatos Inscritos até o momento</a></li>
							<li><a href="#PontuacaoProvavelPrimeiroExcedente">Pontuação provaǘel do primeiro excedente</a></li>
						</ul>
					</li>
					
					
					
					<li><a href="#VagasRemanescentes">Vagas</a></li>
					<li><a href="#ConfirmarInscricaoDefinitivamente">Confrimar Inscrição definitivamente</a></li>
					<li><a href="#Recursos">Recursos</a></li>
					<li><a href="#PainelDeComandos">Painel de Comandos</a></li>
					<li><a href="#ResultadoFinal">Resultado Final</a></li>
					
					<li><a href="#ConcursoAtual">Concurso Atual</a></li>
					<li><a href="#NovoConcurso">Novo Concurso</a></li>
					<li><a href="#PrincipaisFuncionalidades">Principais funcionalidades do sistema</a></li>
					<li><a href="#DadosTecnicos">Ficha Técnica</a></li>
					
				</ul>
				
				<h2 id="Sistema">Sistema Sisnar</h2>
				<p>
					Sisnar é o concurso nacional de remoção de servidores baseado em Instrução Normativa que prevê três
					formas de alteração de lotação: recrutamento, remanejamento ou permuta.
				</p>
				
				<h2 id="remanejamento">Remanejamento</h2>
				<p>
					A presente aplicação, versão 5, pretende abranger o remanejamento de servidores e a permuta.
					O remanejamento é a abertura de vagas, em Unidades Organizacionais (UORG), para servidores PRFs,
					dada a iminência de nomeação de novos policiais. Vale lembrar que legalmente não é possível lotar o
					servidor mais novo no local em que há interesse de um servidor mais antigo.
				</p>
				<p>
					No sistema de Remanejamento poderá haver UORGs em que, após a classificação dos candidatos, ocorra
					uma diferença entre PRFs que entraram e que saíram. Chamamos a isso de método não conservativo.
				</p>
				
				<h2 id="Permuta">Permuta</h2>
				<p>
					Permuta é o cruzamento de interesses de lotação entre servidores. Esse sistema viabiliza o concurso
					de permuta periódico (anualmente por exemplo), independentemente de lotação de novos policiais.
					O algoritmo cruza interesses de remoção entre os atuais agentes públicos, sendo que no final as
					UORGs não serão prejudicadas, pois a diferença entre entrada e saída de servidores em cada unidade
					será obrigatoriamente zero; a isso damos o nome de método conservativo.
				</p>
				<p>
					Os servidores não saberão com quem permutarão, visto que esta modalidade utiliza o mesmo sistema de
					pontuação que o concurso de remanejamento (SISNAR), permitindo a troca entre servidores, por exemplo
					com 20 servidores envolvidos.
				</p>
				<p>Perfis de usuários possíveis.</p>
				
				<h3 id="ComoSistemaEscolheAlgoritmo">
					Como o sistema sabe qual algorítmo executar - conservativo ou não
					conservativo? Como o sistema sabe que se trata de concurso de Remanejamento ou de Permuta?
				</h3>
				<p>
					De acordo com as vagas preestabelecidas, se o administrador ajustou todas as vagas de todas as UORGs
					igual a zero, o sistema entenderá que se trata de um concurso de Permuta. Caso contrário, acionará
					automaticamente o algoritmo não conservativo.
				</p>
				
				<h2 id="InstrucoesCandidatos"></a>Instruções aos Candidatos</h2>
				<p>
					Ao se acessar a página inicial do sistema, caso o Administrador do Sistema tenha programado algum
					concurso e estejamos dentro do concurso, serão informadas as datas e aparecerá o botão PROSSEGUIR.
				</p>
				<p>O sistema irá recuperar as informações do candidato constantes no sistema SIGRH. Aqui o pretendente à
					remoção deverá conferir seus dados e verificar se há algum erro. Caso encontre algum erro, no
					período de recursos, poderá solicitar a correção dos dados. Lembrando que estes dados serão usados
					para calcular a pontuação do servidor.
				</p>
				<p>
					Após clicar em PROSSEGUIR, o sistema gravará as informações, registrando parte da inscrição.
					Na tela seguinte, o servidor deverá escolher as opções de remoção. O número total de escolha será
					igual ao número definido no sistema como número máximo.
				</p>
				<p>
					Clicando em Visualizar Estatística será possível verificar dados referentes a probabilidade de
					sucesso das opções escolhidas.
				</p>
				
				<h3 id="estatistico">Entendendo Cenários e Estatística</h3>
				<p>
					A proposta é visualizar dados referentes à concorrência, para que o candidato faça a escolha mais
					consciente e clara nos concursos de remoção e permuta. Há duas telas onde é possível visualizar os
					dados de concorrência das UORGs: a tela de cenário, onde constam os dados específicos relativos à
					escolha de UORG destino e a tela QUADRO RESUMO onde constam os dados de concorrência de todas as
					UORGs.
				</p>
				<p>Vale lembrar que as notas e pontuações consideram os inscritos até o momento.</p>
				<h4 id="CalculoProbabilidade">O Cálculo de probabilidade</h4>
				<p>
					As probabilidades são divididas em Alta , Média e Baixa Probabilidade .
					São calculadas da seguinte forma:
				</p>
				<p>T = Candidatos que possuem nota no SISNAR superior &agrave; do Candidato E que também estão concorrendo para a mesma UORG</p>
				<p>Probabilidade = &sum;(1 / T.ordem_de_preferencia)&nbsp; /&nbsp; Total_de_vagas_remanescentes da UORG</p>
				<p>Se Probabilidade &lt; = 1&nbsp;&nbsp;&nbsp; -&gt; 'alta probabilidade';</p>
				<p>Se&nbsp; 1 &lt; =&nbsp; Probabilidade &lt; = 2 &nbsp;&nbsp; -&gt; 'media probabilidade';</p>
				<p>Se Probabilidade&nbsp; .2&nbsp; - &gt; 'baixa probabilidade'</p>
				<h4  id="CandidatosInscritosAteoMomento">Candidatos Inscritos até o momento</h4>
				<p>
					Consideram-se aqui apenas aqueles que colocaram a UORG como primeira opção.
					Vale lembrar que há outros candidatos que colocaram essa UORG em 2ª, 3ª ou 4ª opção e assim por
					diante.
				</p>
				<h4 id="PontuacaoProvavelPrimeiroExcedente">Pontuação Provável do Primeiro Excedente</h4>
				<p>
					Considera-se aqui a nota do primeiro candidato classificado fora das vagas remanescentes, se
					houvesse apenas as primeiras opções.
				</p>
				<h4 id="VagasRemanescentes">Vagas</h4>
				<p>
					As vagas consideradas para a construção da estatística/quadro resumo são as preestabelecidas pelo
					administrador. Não se contam as vagas remanescentes resultantes da saída de outro candidato de sua
					UORG.
				</p>
				
				<h3 id="ConfirmarInscricaoDefinitivamente">Confirmar inscrição definitivamente</h3>
				<p>
					Antes de confirmar a inscricao é possível voltar e alterar suas opções de UORG. Entretanto, após
					confirmar a inscrição definitivamente será impossível alterar suas opções.
				</p>
				<p>Após a confirmação definitiva, o sistema emitirá um comprovante de inscrição que poderá ser
					visualizado a qualquer momento até o período de execução do concurso.
				</p>
				<p>
					Recomendamos que o candidato imprima o comprovante de inscrição após a confirmação definitiva. Ele
					terá codificadores que confirmam sua autenticidade.
				</p>
				
				<h2 id="Recursos">Recursos</h2>
				<p> A funcionalidade de Recursos só poderá ser acessadas pelos servidores que possuem perfil de gerente.
					É possível visualizar todas as inscrições do concurso atual, apagar qualquer inscrição ou alterar
					seus dados manualmente, bem como incluir inscrições.
				</p>
				<p>
					É importante lembrar que somente será possível incluir inscrição de servidor que esteja previamente
					cadastrado no sistema SIGRH.
				</p>
				
				<h3 id="PainelDeComandos">Painel de Comandos</h3>
				<p>
					O painel de comandos é uma forma de trazer o controle de execuções para o administrador do sistema -
					gerentes não possuem perfil para executar o painel de comandos. Entretanto, esses comandos devem ser
					usados SOMENTE SE SE PRODUZIR RESULTADOS DE MANEIRA INTEMPESTIVA.
				</p>
				<p>
					Deve-se entender que o sistema executa a classificação automaticamente na data programada pelo
					administrador - data de execução. Quando se cria um novo concurso, o sistema também arquiva o
					concurso antigo automaticamente.
				</p>
				<p>
					As utilidades de forçar as execuções do concurso, restartar o processo de classificação e arquivar
					manualmente ocorrem em último caso, após a execução da classificação programada identificar a
					necessidade de se alterar manualmente algum dado de vagas ou de candidatos.
				</p>
				<p>
					O painel de Comandos só é liberado para execução após a data programada de execução do concurso.
				</p>
				<ol>
					<li>
						<strong>Classificar Candidatos -</strong> Realiza a classificação dos candidatos utilizando o
						algoritmo conservativo (para concursos de permuta) ou o não-conservativo (para concursos de
						remanejamento);
					</li>
					<li>
						<strong>Reboot Concurso</strong> - Realiza a classificação dos candidatos utilizando o algoritmo
						conservativo (para concursos de permuta) ou o não-conservativo (para concursos de remanejamento);
					</li>
					<li><strong>Arquivar Concurso -</strong> A opção de arquivar concurso salvará os dados de inscrição
						em uma tabela separada do sistema. A opção guardará as informações de vagas preestabelecidas
						surgidas e perdidas em local especifico e apagará, exceto deste lugar arquivado, as informações
						de vagas, inscrições, opções de UORGs, etc. O sistema estará pronto para que o gestor configure
						novo concurso.
					</li>
				</ol>
				<h3 id="ResultadoFinal">Resultado Final</h3>
				<p>
					Em Resultado Final, com nível de acesso apenas pelos gerentes e administrador, ficará disponível a
					listagem dos candidatos contemplados com suas UORGs de destino. Também é possível baixar esse
					resultado em planilha .xls.
				</p>
				
				<h2 id="ConcursoAtual">Concurso Atual</h2>
				<p>
					No concurso atual será possível alterar as datas de execução, início e final de inscrições e data de
					remoção, antes da data de execução do concurso.
				</p>
				<p>
					Lembrando que essas alterações de datas devem ser realizadas antes do período de início de inscrição,
					pois o cálculo da nota do candidato, que leva em consideração várias dessas datas, é feito no momento
					da inscrição.
				</p>
				<p>Poderão ser alteradas também a quantidade de vagas até o momento da execução do concurso.</p>
				
				<h2 id="NovoConcurso">Novo Concurso</h2>
				<p>Ao escolher essa opção, os dados do concurso em curso ou executado serão arquivados automaticamente.
					As vagas também serão zeradas,  as inscrições apagadas e o gestor poderá programar um novo concurso.
				</p>
				
				<h2 id="PrincipaisFuncionalidades">Principais Funcionalidades do sistema</h2>
				<ol>
					<li>Cadastramento das inscricoes dos servidores;</li>
					<li>Visualização de dados estatistico quanto &agrave; suas intenções de remoção;</li>
					<li>Emissão de comprovante de inscrição ;</li>
					<li>Gestor pode editar dados de inscricao dos candidatos mediante justificativa registrada em banco;</li>
					<li>Gestor pode executar algoritmo de classificação</li>
					<li>Sistema define qual dos dois algoritmos serão executados com base nas vagas previamente configuradas pelo gestor do sistema, caso zero vagas para todas as uorgs, roda algoritmo conservativo, caso tenha alguma uorg com vaga ajustada, roda algoritmo nao conservativo</li>
					<li>Planilha de relatorio de aprovados no concurso;</li>
					<li>Arquiva dados do concurso;</li>
					<li>Controle de acessos e prerequisitos para inscricao;</li>
					<li>Gestor pode ajustar o numero de opcoes de intencao de remocao entre 1 e 10;</li>
				</ol>
				
				<h2 id="DadosTecnicos">Dados Técnicos</h2>
				<ul class="dadosTecnicos titulo1">
					
					
					<li>Versão</li>
					<ul>
						<li>5.0 Beta</li>
					</ul>
					<li>Framework/Script</li>
					<ul>
						<li>Laravel 5.0 / PHP</li>
					</ul>
					
					<li >Período Desenvolvimento</li>
					<ul>
						<li>1 Trim 2016</li>
					</ul>
					
					<li >Banco de Dados</li>
					<ul>
						<li>Mysql</li>
					</ul>
					
					<li>Gestores</li>
					<ul class="titulo2">
						<li>Diretora Geral
							<ul>
								<li>Maria Alice do Nascimento</li>
							</ul>
						</li>
						
						<li>Ch. Gabinete
							<ul>
								<li>Eduardo Aggio de Sa</li>
							</ul>
						</li>
						<li>CGE
							<ul>
								<li>Henrique Fontenelle Galvao dos Passos</li>
							</ul>
						</li>

						<li>CGRH
							<ul>
								<li>Antonio Paim de Abreu Junior</li>
							</ul>
						</li>
						<li>CGPLAM
							<ul>
								<li>Eduardo Muniz de Souza</li>
							</ul>
						</li>
						<li>Projeto Estratégico SISNAR V - Portaria Nº 74, DE 26 DE fevereiro DE 2016
							<ul>
								<li>Gerente de Projeto: Jeferson Tadeu de Souza</li>
							</ul>
						</li>
						<li>Equipe CGRH
							<ul>
								<li>Hudson SérGio Araújo</li>
								<li>Guilherme de Albuquerque Andrade</li>
								<li>Bruno Nonato dos Santos Pereira</li>
							</ul>
						</li>
						
						<li>Equipe Desenvolvimento
							<ul>
								<li>Jeferson Tadeu de Souza</li>
								<li>Claudir Galesky Júnior</li>
								<li>Marcelo Fontes Santana</li>

							</ul>
						</li>
						<li>Diasi
							<ul>
								<li>Nelio Ferreira Santana </li>
								<li>Juvenildo Carvalho Vieira</li>
								<li>Julio Cesar de Freitas Taveira </li>

							</ul>
						</li>

						<li>Testes
							<ul>
								<li>Marcelo Lambé</li>

							</ul>
						</li>
						<li>Ditel
							<ul>
								<li>Reinaldo dos Reis Junior</li>
								<li>Marco Aurélio Ferreira Borges</li>
								<li>Thiago Ferreira Nunes</li>
							</ul>
						</li>
						<li>Arquétipo
							<ul>
								<li>Thyago Ribeiro Assunção</li>
							</ul>
						</li>
						<li>Revisão Textual - ASCOM
							<ul>
								<li>Rodrigo Abreu</li>
								<li>Fabiana Reis</li>
								<li>João Alberto Tavares</li>

							</ul>
						</li>
						<li>Auxílio Administrativo
							<ul>
								<li>Gustavo Henrique Alcoforado</li>
							</ul>
						</li>

					</ul>

				</ul>
				<p>** ** Algoritmo conservativo foi evolução do primeiro algoritmo desenvolvido por Abel Vanderlei Nunes Junior.&nbsp;</p>
				
				
			</div>
		</div>
    </div>
@stop