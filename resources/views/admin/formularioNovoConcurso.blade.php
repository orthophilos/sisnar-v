@extends('templates.layout')

@section('scripts')
	<script src="{{url('/angular/angular.min.js')}}"></script>
	<script src="{{url('/moment/moment.min.js')}}"></script>
	
	<script src="{{url('/ngtable/dist/ng-table.js')}}"></script>
	
    <script type="text/javascript">
        $(document).ready(function () {
            $("#menuInicio").addClass("ativo");
        });
    </script>
	<script type="text/javascript">
		var app = angular.module('sisnarApp', ["ngTable"]);
        app.config(function ($interpolateProvider) {
            $interpolateProvider.startSymbol('//');
            $interpolateProvider.endSymbol('//');
        });
		
		app.controller('MainController', function ($http, $scope, NgTableParams) {
			var self = this;
			
			//---------------------------------------------------------------------//
			
				self.co_dt_publicacao_edital = "";
			
			//---------------------------------------------------------------------//
			
				self.co_dt_inicio_inscricoes = "";
			
			//---------------------------------------------------------------------//
			
				self.co_dt_fim_inscricoes = "";
			
			//---------------------------------------------------------------------//
			
				self.co_dt_data_execucao = "";
			
			//---------------------------------------------------------------------//
			
				self.co_dt_remocao = "";
			
			//---------------------------------------------------------------------//
			
			self.data = <?php echo(( $vagas )) ?>; 
			for(var i=0;i<self.data.length;i++){
				self.data[i].vg_vl_vagas_preestabelecidas = 0;
			}
			self.tableParams = new NgTableParams({}, { dataset: self.data});
		
			
		});
		
	</script>
	<script src="{{url('/angular/diretrizes.js')}}"></script>
@stop


@section('conteudo')
	<link rel="stylesheet" href="{{url('/ngtable/dist/ng-table.min.css')}}">
	<style>
	input.ng-invalid.ng-touched, textarea.ng-invalid.ng-touched{
		background-color: #fccfcf !important;
		border-color: #a94442 !important;
	}
	</style>
	
    
	{{--@if($errors->any())--}}
		{{--<div class="box">--}}
			{{--<div class="box-content padded">--}}
				{{--<ul class="alert alert-danger">--}}
					{{--@foreach($errors->all() as $erro)--}}
						{{--<li>{{ $erro }}</li>--}}
					{{--@endforeach--}}
				{{--</ul>--}}
			{{--</div>--}}
		{{--</div>--}}
	{{--@endif--}}
	<div  ng-app="sisnarApp" ng-controller="MainController as mController" >
		{!! Form::open(['url' => URL::route('gravaNovoConcurso'), 'id'=>'form' , 'name'=>'form' , 'method' => 'post','class'=>'form-horizontal ']) !!}
		

		<div  class="box">
			<div class="box-header">
				<span class="title" style="font-size: 18px;">
					<div class="text-center">
						<strong>
							<span class="text-error">
								<i class="ui-icon-document"></i>
							</span>
							<span class="text-success">
								Novo Concurso
							</span>
						</strong>
					</div>
				</span>
			</div>
			<div class="box-content padded">
		
				
				
				<div class="control-group " >
					<label class="control-label" for="co_dt_publicacao_edital">Data publicação do edital</label>
					<div class="controls">
						<input class="input-small"  type="text" id="co_dt_publicacao_edital" name="co_dt_publicacao_edital" ng-model="mController.co_dt_publicacao_edital" formatter="date" validation="required date"/> 
					</div>
				</div>
				<div ng-show="form.submitted || !form.co_dt_publicacao_edital.$pristine || form.co_dt_publicacao_edital.$touched">
					<div class="alert alert-error" ng-show="form.co_dt_publicacao_edital.$error.required">Campo de preenchimento obrigatório!</div> 
					<div class="alert alert-error" ng-show="form.co_dt_publicacao_edital.$error.date">Data inválida!</div>
				</div>
				
				<div class="control-group " >
					<label class="control-label" for="co_dt_inicio_inscricoes">Inscrições de</label>
					<div class="controls">
						<input class="input-small"  type="text" id="co_dt_inicio_inscricoes" name="co_dt_inicio_inscricoes" ng-model="mController.co_dt_inicio_inscricoes" formatter="date" validation="required date"/> 
						até
						<input class="input-small"  type="text" id="co_dt_fim_inscricoes" name="co_dt_fim_inscricoes" ng-model="mController.co_dt_fim_inscricoes" formatter="date" validation="required date"/> 
					</div>
				</div>
				<div ng-show="form.submitted || !form.co_dt_inicio_inscricoes.$pristine || form.co_dt_inicio_inscricoes.$touched">
					<div class="alert alert-error" ng-show="form.co_dt_inicio_inscricoes.$error.required">Campo de preenchimento obrigatório!</div> 
					<div class="alert alert-error" ng-show="form.co_dt_inicio_inscricoes.$error.date">Data inválida!</div>
				</div>
				<div ng-show="form.submitted || !form.co_dt_fim_inscricoes.$pristine || form.co_dt_fim_inscricoes.$touched">
					<div class="alert alert-error" ng-show="form.co_dt_fim_inscricoes.$error.required">Campo de preenchimento obrigatório!</div> 
					<div class="alert alert-error" ng-show="form.co_dt_fim_inscricoes.$error.date">Data inválida!</div>
				</div>
						
				<div class="control-group " >
					<label class="control-label" for="co_dt_data_execucao">Data de execução</label>
					<div class="controls">
						<input class="input-small"  type="text" id="co_dt_data_execucao" name="co_dt_data_execucao" ng-model="mController.co_dt_data_execucao" formatter="date" validation="required date"/> 
					</div>
				</div>
				<div ng-show="form.submitted || !form.co_dt_data_execucao.$pristine || form.co_dt_data_execucao.$touched">
					<div class="alert alert-error" ng-show="form.co_dt_data_execucao.$error.required">Campo de preenchimento obrigatório!</div> 
					<div class="alert alert-error" ng-show="form.co_dt_data_execucao.$error.date">Data inválida!</div>
				</div>
				
				<div class="control-group " >
					<label class="control-label" for="co_dt_remocao">Data remoção</label>
					<div class="controls">
						<input class="input-small"  type="text" id="co_dt_remocao" name="co_dt_remocao" ng-model="mController.co_dt_remocao" formatter="date" validation="required date"/> 
					</div>
				</div>
				<div ng-show="form.submitted || !form.co_dt_remocao.$pristine || form.co_dt_remocao.$touched">
					<div class="alert alert-error" ng-show="form.co_dt_remocao.$error.required">Campo de preenchimento obrigatório!</div> 
					<div class="alert alert-error" ng-show="form.co_dt_remocao.$error.date">Data inválida!</div>
				</div>
				
				
				<hr/>
				<div class="pull-right">Total: //mController.data.length//</div>
				<table ng-table="mController.tableParams" class="table" show-filter="true">
					<tr ng-repeat="row in $data">
						
					
						<td title="'UF'" filter="{ vg_ed_uf: 'text'}"  sortable="'vg_ed_uf'">
							//row.vg_ed_uf//
						</td>	
						
						<td title="'UORG Destino'" filter="{ vg_no_nome: 'text'}">
							//row.vg_no_nome//
						</td>	
							
						<td title="'Vagas Preestabelecidas'" filter="{ vg_vl_vagas_preestabelecidas: 'number'}" sortable="'vg_vl_vagas_preestabelecidas'">
							<input type="text" ng-model="row.vg_vl_vagas_preestabelecidas"/>	
						</td>
						
					</tr>
				</table>
				
				<input type="hidden" name="vagas" ng-value="mController.data | json"/>
			</div>
		</div>
	
		<div class="row-fluid">
			<div class="span2">
				<button id="botaoVoltar" type="button" class="btn btn-primary" onclick="window.history.back();"><i class="icon-arrow-left"></i> Voltar</button>
			</div>
			<div class="span8"></div>
			<div class="span2">
				<button id="botaoSubmit" 
						type="submit"
						class="btn btn-primary pull-right"
						data-toggle="tooltip"
						title="Salvar"
						ng-disabled="form.$invalid"
						>
							<i class="icon-save"></i> Salvar 
				</button>
			</div>
		</div>
		

	{!! Form::close() !!}
	</div>
	
		
	{{--<pre>	

       
				   
					DADOS VAGAS
					{{$vagas}}
				
	</pre>--}}
@stop