@extends('templates.layout')

@section('scripts')
	<script src="{{url('/angular/angular.min.js')}}"></script>
	<script src="{{url('/moment/moment.min.js')}}"></script>
	
	<script src="{{url('/ngtable/dist/ng-table.js')}}"></script>
	
    <script type="text/javascript">
        $(document).ready(function () {
            $("#menuInicio").addClass("ativo");
        });
    </script>
	<script type="text/javascript">
		var app = angular.module('sisnarApp', ["ngTable"]);
        app.config(function ($interpolateProvider) {
            $interpolateProvider.startSymbol('//');
            $interpolateProvider.endSymbol('//');
        });

		app.controller('MainController', function ($http, $scope, NgTableParams) {
			var self = this;

			//---------------------------------------------------------------------//
			if(moment("<?php echo(($concursoAtual->co_dt_publicacao_edital))?>").isValid() ){
				self.co_dt_publicacao_edital = moment("<?php echo(($concursoAtual->co_dt_publicacao_edital))?>").format("DD/MM/YYYY");
			}else{
				self.co_dt_publicacao_edital = "";
			}
			//---------------------------------------------------------------------//
			if(moment("<?php echo(($concursoAtual->co_dt_inicio_inscricoes))?>").isValid() ){
				self.co_dt_inicio_inscricoes = moment("<?php echo(($concursoAtual->co_dt_inicio_inscricoes))?>").format("DD/MM/YYYY");
			}else{
				self.co_dt_inicio_inscricoes = "";
			}
			//---------------------------------------------------------------------//
			if(moment("<?php echo(($concursoAtual->co_dt_fim_inscricoes))?>").isValid() ){
				self.co_dt_fim_inscricoes = moment("<?php echo(($concursoAtual->co_dt_fim_inscricoes))?>").format("DD/MM/YYYY");
			}else{
				self.co_dt_fim_inscricoes = "";
			}
			//---------------------------------------------------------------------//
			if(moment("<?php echo(($concursoAtual->co_dt_data_execucao))?>").isValid() ){
				self.co_dt_data_execucao = moment("<?php echo(($concursoAtual->co_dt_data_execucao))?>").format("DD/MM/YYYY");
			}else{
				self.co_dt_data_execucao = "";
			}
			//---------------------------------------------------------------------//
			if(moment("<?php echo(($concursoAtual->co_dt_remocao))?>").isValid() ){
				self.co_dt_remocao = moment("<?php echo(($concursoAtual->co_dt_remocao))?>").format("DD/MM/YYYY");
			}else{
				self.co_dt_remocao = "";
			}
			//---------------------------------------------------------------------//

			self.data = <?php echo(( $vagas )) ?>;
			self.tableParams = new NgTableParams({}, { dataset: self.data});


			self.enviar = function(){ 
				//var url = "{{URL::route('updateConcursoAtual')}}";
				self.isFazendoAlgo = true;
				$("#enviarFormulario").click();
				
				/*var dados = {};
				dados.co_dt_publicacao_edital = self.co_dt_publicacao_edital;
				dados.co_dt_inicio_inscricoes = self.co_dt_inicio_inscricoes;
				dados.co_dt_fim_inscricoes = self.co_dt_fim_inscricoes;
				dados.co_dt_data_execucao = self.co_dt_data_execucao;
				dados.co_dt_remocao = self.co_dt_remocao;
				dados.vagas = self.data;


				$http.post(url,dados).then(
					function(response){
						self.isFazendoAlgo = false;
						self.resultado = response.data;
						alert(self.resultado);
						console.debug(response);
					},
					function(response){
						self.isFazendoAlgo = false;
						self.resultado = "Servidor retornou erro "+response.status+"\n\n"+response.data;
						alert(self.resultado);
						console.debug(response);
					}
				);*/
			};

		});

	</script>
	<script src="{{url('/angular/diretrizes.js')}}"></script>
@stop


@section('conteudo')
	<link rel="stylesheet" href="{{url('/ngtable/dist/ng-table.min.css')}}">
	<style>
	input.ng-invalid.ng-touched, textarea.ng-invalid.ng-touched{
		background-color: #fccfcf !important;
		border-color: #a94442 !important;
	}
	</style>
	
    
	{{--@if($errors->any())--}}
		{{--<div class="box">--}}
			{{--<div class="box-content padded">--}}
				{{--<ul class="alert alert-danger">--}}
					{{--@foreach($errors->all() as $erro)--}}
						{{--<li>{{ $erro }}</li>--}}
					{{--@endforeach--}}
				{{--</ul>--}}
			{{--</div>--}}
		{{--</div>--}}
	{{--@endif--}}
	<div  ng-app="sisnarApp" ng-controller="MainController as mController" >
		{!! Form::open(['url' => URL::route('updateConcursoAtual'), 'id'=>'form' , 'name'=>'form' , 'method' => 'post','class'=>'form-horizontal ']) !!}
		{!! Form::hidden('co_id_concurso',$concursoAtual->co_id_concurso) !!}

		<div  class="box">
			<div class="box-header">
				<span class="title" style="font-size: 18px;">
					<div class="text-center">
						<strong>
							<span class="text-error">
								<i class="ui-icon-document"></i>
							</span>
							<span class="text-success">
								Concurso Atual #{{$concursoAtual->co_id_concurso}}
							</span>
						</strong>
					</div>
				</span>
			</div>
			<div class="box-content padded">
		
				
				
				<div class="control-group " >
					<label class="control-label" for="co_dt_publicacao_edital">Data publicação do edital</label>
					<div class="controls">
						<input class="input-small"  type="text" id="co_dt_publicacao_edital" name="co_dt_publicacao_edital" ng-model="mController.co_dt_publicacao_edital" formatter="date" validation="required date"/> 
					</div>
				</div>
				<div ng-show="form.submitted || !form.co_dt_publicacao_edital.$pristine || form.co_dt_publicacao_edital.$touched">
					<div class="alert alert-error" ng-show="form.co_dt_publicacao_edital.$error.required">Campo de preenchimento obrigatório!</div> 
					<div class="alert alert-error" ng-show="form.co_dt_publicacao_edital.$error.date">Data inválida!</div>
				</div>
				
				<div class="control-group " >
					<label class="control-label" for="co_dt_inicio_inscricoes">Inscrições de</label>
					<div class="controls">
						<input class="input-small"  type="text" id="co_dt_inicio_inscricoes" name="co_dt_inicio_inscricoes" ng-model="mController.co_dt_inicio_inscricoes" formatter="date" validation="required date"/> 
						até
						<input class="input-small"  type="text" id="co_dt_fim_inscricoes" name="co_dt_fim_inscricoes" ng-model="mController.co_dt_fim_inscricoes" formatter="date" validation="required date"/> 
					</div>
				</div>
				<div ng-show="form.submitted || !form.co_dt_inicio_inscricoes.$pristine || form.co_dt_inicio_inscricoes.$touched">
					<div class="alert alert-error" ng-show="form.co_dt_inicio_inscricoes.$error.required">Campo de preenchimento obrigatório!</div> 
					<div class="alert alert-error" ng-show="form.co_dt_inicio_inscricoes.$error.date">Data inválida!</div>
				</div>
				<div ng-show="form.submitted || !form.co_dt_fim_inscricoes.$pristine || form.co_dt_fim_inscricoes.$touched">
					<div class="alert alert-error" ng-show="form.co_dt_fim_inscricoes.$error.required">Campo de preenchimento obrigatório!</div> 
					<div class="alert alert-error" ng-show="form.co_dt_fim_inscricoes.$error.date">Data inválida!</div>
				</div>
						
				<div class="control-group " >
					<label class="control-label" for="co_dt_data_execucao">Data de execução</label>
					<div class="controls">
						<input class="input-small"  type="text" id="co_dt_data_execucao" name="co_dt_data_execucao" ng-model="mController.co_dt_data_execucao" formatter="date" validation="required date"/> 
					</div>
				</div>
				<div ng-show="form.submitted || !form.co_dt_data_execucao.$pristine || form.co_dt_data_execucao.$touched">
					<div class="alert alert-error" ng-show="form.co_dt_data_execucao.$error.required">Campo de preenchimento obrigatório!</div> 
					<div class="alert alert-error" ng-show="form.co_dt_data_execucao.$error.date">Data inválida!</div>
				</div>
				
				<div class="control-group " >
					<label class="control-label" for="co_dt_remocao">Data remoção</label>
					<div class="controls">
						<input class="input-small"  type="text" id="co_dt_remocao" name="co_dt_remocao" ng-model="mController.co_dt_remocao" formatter="date" validation="required date"/> 
					</div>
				</div>
				<div ng-show="form.submitted || !form.co_dt_remocao.$pristine || form.co_dt_remocao.$touched">
					<div class="alert alert-error" ng-show="form.co_dt_remocao.$error.required">Campo de preenchimento obrigatório!</div> 
					<div class="alert alert-error" ng-show="form.co_dt_remocao.$error.date">Data inválida!</div>
				</div>
				
				
				<hr/>
				<div class="pull-right">Total: //mController.data.length//</div>
				<table ng-table="mController.tableParams" class="table" show-filter="true">
					<tr ng-repeat="row in $data">
						
					
						<td title="'UF'" filter="{ vg_ed_uf: 'text'}"  sortable="'vg_ed_uf'">
							//row.vg_ed_uf//
						</td>	
						
						<td title="'UORG Destino'" filter="{ vg_no_nome: 'text'}">
							//row.vg_no_nome//
						</td>	
							
						<td title="'Vagas Preestabelecidas'" filter="{ vg_vl_vagas_preestabelecidas: 'number'}" sortable="'vg_vl_vagas_preestabelecidas'">
							<input type="text" ng-model="row.vg_vl_vagas_preestabelecidas"/>	
						</td>
						
					</tr>
				</table>
				
				<input type="hidden" name="vagas" ng-value="mController.data | json"/>
			</div>
		</div>
	
		<div class="row-fluid">
			<div class="span2">
				<button id="botaoVoltar" type="button" class="btn btn-primary" onclick="window.history.back();"><i class="icon-arrow-left"></i> Voltar</button>
			</div>
			<div class="span8"></div>
			<div class="span2">
				<button type="submit" id="enviarFormulario" style="display: none"></button>
				<button id="botaoSubmit"
						type="button"
						class="btn btn-primary pull-right"
						data-toggle="tooltip"
						title="Salvar"
						ng-disabled="form.$invalid"
						ng-click="mController.enviar()"
						>
							<i class="icon-save"></i> Salvar
				</button>
			</div>
		</div>
		

		{!! Form::close() !!}
		<div class="modalBackground" ng-show="mController.isFazendoAlgo">
			<div class="modal fade in" id="pleaseWaitDialog" data-backdrop="static" data-keyboard="false">
				<div class="modal-header">
					<h1>Aguarde...</h1>
				</div>
				<div class="modal-body">
					<div class="progress progress-striped active">
						<div class="bar" style="width: 100%;"></div>
					</div>
				</div>
			</div>
		</div>
	</div>
	
		
	{{--<pre>	

     
				   DADOS CONCURSO ATUAL
					{{$concursoAtual}}
					
					DADOS VAGAS
					{{$vagas}}
				
	</pre>--}}	
@stop