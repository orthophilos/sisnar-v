<?php

/*
Para chamar qualquer um desses atributos em qualquer lugar da aplicação use: Config::get("PRF.nomeAtributo")
    Exemplo: Config::get("PRF.nomeCurto"); retornará o nome curto definido abaixo

return [
    "nomeSistema" => "SISNAR III - Sistema Nacional de Remoção de Servidores", // Nome que aparece em algumas partes do sistema
    "nomeCurto" => "SISNAR", // Nome que aparece no cabeçalho do sistema
    "siglaSistema" => "sisnar4", // Sigla do sistema cadastrado no DPRFSeguranca
    "versao" => "Alpha 1", // Versão do sistema
    "producao" => false, // Setar para false se o sistema estiver em modo homologação; true para produção
];

*/
return [
    "nomeSistema" => "Sisnar - Sistema Nacional de Remoção de Servidores", // Nome que aparece em algumas partes do sistema
    "nomeCurto" => "SISNAR", // Nome que aparece no cabeçalho do sistema
    "siglaSistema" => "sisnar", // Sigla do sistema cadastrado no DPRFSeguranca
    "versao" => "05.00.04", // Versão do sistema
    "producao" => true , // Setar para false se o sistema estiver em modo homologação; true para produção
];
